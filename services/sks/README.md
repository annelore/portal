Build

```bash
podman build -t sks .
```

Run with

```bash
podman run -p 11371:11371 -p 11370:11370 --rm sks
```

Send keys:

```bash
gpg [--homedir <tmp store>] --gen-key
gpg [--homedir <tmp store>] --keyserver hkp://localhost --send-keys <key-id>
```
