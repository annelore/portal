#!/usr/bin/env bash

volume="$1"
[[ -z "$volume" ]] && echo "No volume specified" >&2 && exit 1

exec docker run --rm -v "$volume":/volume:rw ubuntu bash -c 'cd /volume && tar xvf -'
