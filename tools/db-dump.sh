#!/usr/bin/env bash

set -o pipefail
container="${1:-portal_db_1}"

exec docker exec -i "$container" bash -c 'pg_dump -U $DB_USER -d $DB_NAME' | gzip
