#!/usr/bin/env bash

TIMESTAMP="$(date +'%Y_%m_%dT%H_%M_%S')"
out="/tmp/backup_${TIMESTAMP}_db.sql.gz"

exec ./db-dump.sh portal_db_1 > "$out"
