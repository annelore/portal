#!/usr/bin/env python3
"""Extracts literal env variable declarations from kustomization.yml files.
It will be used to make sure that the set of env variables from the k8s deployment and those
from the docker deployment dont diverge.

Example input `kustomization.yml`:

```yml
resources:
  - deployment.yml

configMapGenerator:
  - name: backend-env
    literals:
      - DEBUG=False
  - name: frontend-env
    literals:
      - NEXT_PUBLIC_CONTACT_EMAIL=<secret>
```

`extract_env_vars.py kustomization.yml backend-env` would return
```
DEBUG
```

`extract_env_vars.py <file> backend-env frontend-env` would return
```
DEBUG
NEXT_PUBLIC_CONTACT_EMAIL
```
"""

from __future__ import annotations
import sys
from collections.abc import Collection
import yaml


def extract_env_keys(manifest: dict, sections: Collection[str]) -> list[str]:
    """Core function, does the actual extraction"""
    generators = manifest.get("configMapGenerator", []) + manifest.get(
        "secretGenerator", []
    )
    entries: list[str] = sum(
        (m["literals"] for m in generators if m["name"] in sections), []
    )
    return [entry.split("=", 1)[0] for entry in entries]


if __name__ == "__main__":
    _path, *_sections = sys.argv[1:]
    with open(_path, encoding="utf-8") as _f:
        _manifest = yaml.safe_load(_f)
    _keys = extract_env_keys(_manifest, _sections)
    print("\n".join(_keys))
