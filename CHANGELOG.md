# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.12.0-dev.9](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.8...3.12.0-dev.9) (2021-10-28)


### Bug Fixes

* **frontend/UsersList:** ensure newly added users don't have any roles ([1fecf82](https://gitlab.com/biomedit/portal/commit/1fecf820c42d212ba157d8fa8949cf485bac6b13)), closes [#458](https://gitlab.com/biomedit/portal/issues/458)

## [3.12.0-dev.8](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.7...3.12.0-dev.8) (2021-10-28)

## [3.12.0-dev.7](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.6...3.12.0-dev.7) (2021-10-21)


### Features

* **models/PeriodicTaskRun:** implement `__str__` method ([f283cba](https://gitlab.com/biomedit/portal/commit/f283cba4ab287411636829f4f71c6c03e8d89a09))

## [3.12.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.5...3.12.0-dev.6) (2021-10-21)


### Bug Fixes

* **celery:** only write logs with at least INFO level ([18df68b](https://gitlab.com/biomedit/portal/commit/18df68b6f7cec91235f5362146dbf6b74d084ea8)), closes [#469](https://gitlab.com/biomedit/portal/issues/469)

## [3.12.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.4...3.12.0-dev.5) (2021-10-19)


### Features

* add service hostname to all logs ([b0bad35](https://gitlab.com/biomedit/portal/commit/b0bad357b541c61137c516372bb3e53242037c99)), closes [#461](https://gitlab.com/biomedit/portal/issues/461)

## [3.12.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.3...3.12.0-dev.4) (2021-10-18)


### Features

* **backend/node:** create data provider groups (technical admin, coordinator, manager, viewer) when creating a data provider ([f10cc7d](https://gitlab.com/biomedit/portal/commit/f10cc7ded9edd4d1e394e19517475068d9cc026c)), closes [#453](https://gitlab.com/biomedit/portal/issues/453)
* **backend/node:** create node admin, node viewer and node manager groups when creating a node ([892abd6](https://gitlab.com/biomedit/portal/commit/892abd63d1b591ef626cd6b084625d45c76d10e5)), closes [#453](https://gitlab.com/biomedit/portal/issues/453)


### Bug Fixes

* **frontend/GroupManage:** make new groups appear when changing to the group tab after creating a dp or node ([8fbffbe](https://gitlab.com/biomedit/portal/commit/8fbffbe48a0d14576cf62ad4a39ef86238e9f761)), closes [#453](https://gitlab.com/biomedit/portal/issues/453)

## [3.12.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.2...3.12.0-dev.3) (2021-10-13)


### Features

* **backend:** Use python:slim to reduce web container size ([f8cf139](https://gitlab.com/biomedit/portal/commit/f8cf139376524bdadb985bb08d8ceb2192ea6128))

## [3.12.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.1...3.12.0-dev.2) (2021-10-12)


### Features

* **backend:** Improve error message triggered by data_package.check_user_permissions ([4e40125](https://gitlab.com/biomedit/portal/commit/4e40125e4752565a7a3a2823928e8c9d702f191f))

## [3.12.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.0...3.12.0-dev.1) (2021-10-12)


### Bug Fixes

* **backend:** remove deprecated fields ([7e36cf4](https://gitlab.com/biomedit/portal/commit/7e36cf44255ecf2ca84e0321b96850de502cf8e3)), closes [#373](https://gitlab.com/biomedit/portal/issues/373)

## [3.12.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.11.1-dev.0...3.12.0-dev.0) (2021-10-12)


### Features

* **pl:** data managers AND project leaders should be able to list their data transfers ([46e4876](https://gitlab.com/biomedit/portal/commit/46e4876f955da5dbabb333193603d93f32bc1e02)), closes [#440](https://gitlab.com/biomedit/portal/issues/440)
* **pl:** enable access for project leaders to data transfers menu item ([046d72c](https://gitlab.com/biomedit/portal/commit/046d72c31d8b276d768e5077c7eaee2b873c2acb)), closes [#440](https://gitlab.com/biomedit/portal/issues/440)

### [3.11.1-dev.0](https://gitlab.com/biomedit/portal/compare/3.11.0...3.11.1-dev.0) (2021-10-06)

## [3.11.0](https://gitlab.com/biomedit/portal/compare/3.11.0-dev.6...3.11.0) (2021-10-05)

## [3.11.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.11.0-dev.5...3.11.0-dev.6) (2021-09-30)


### Features

* **frontend/Projects:** add tooltip and separator to action buttons in project list items ([e359175](https://gitlab.com/biomedit/portal/commit/e3591752013585e81e15b568632622110668dd25)), closes [#434](https://gitlab.com/biomedit/portal/issues/434)

## [3.11.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.11.0-dev.4...3.11.0-dev.5) (2021-09-30)

## [3.11.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.11.0-dev.3...3.11.0-dev.4) (2021-09-24)


### Bug Fixes

* **FeedList:** decrease font size of title and message feed items ([b550cfb](https://gitlab.com/biomedit/portal/commit/b550cfba0fd73f45f2bbdcdf21ab4289567b6110)), closes [#445](https://gitlab.com/biomedit/portal/issues/445)

## [3.11.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.11.0-dev.2...3.11.0-dev.3) (2021-09-20)


### Features

* k8s deployment ([8db0a7b](https://gitlab.com/biomedit/portal/commit/8db0a7bca931c9b7c2f63c99eb03a292bd6ba5d4))


### Bug Fixes

* remove pylint ignores for consider-using-f-string ([fe3ab83](https://gitlab.com/biomedit/portal/commit/fe3ab83ea8f82d2bde7ae14f53e6c8e22f3c7f0e))
* Remove urls from sample portal config ([c782e7a](https://gitlab.com/biomedit/portal/commit/c782e7af186287ce7554ae4636f5365a4963e96e))

## [3.11.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.11.0-dev.1...3.11.0-dev.2) (2021-09-17)

## [3.11.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.11.0-dev.0...3.11.0-dev.1) (2021-09-17)


### Bug Fixes

* **backend/user:** return all users when making a `GET` request to `/users` as node admin ([c84759c](https://gitlab.com/biomedit/portal/commit/c84759c0f22a93be1b5e8ef0cb2e5c50f355acf0)), closes [#454](https://gitlab.com/biomedit/portal/issues/454)

## [3.11.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.10.1-dev.0...3.11.0-dev.0) (2021-09-16)


### Features

* show data package sender PGP key info in the transfer log ([7198d66](https://gitlab.com/biomedit/portal/commit/7198d66931d8fa9071bd090dca58a97b4def6c21)), closes [#417](https://gitlab.com/biomedit/portal/issues/417)

### [3.10.1-dev.0](https://gitlab.com/biomedit/portal/compare/3.10.0...3.10.1-dev.0) (2021-09-16)

## [3.10.0](https://gitlab.com/biomedit/portal/compare/3.10.0-dev.0...3.10.0) (2021-09-15)

## [3.10.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.10.0-dev.0...3.10.0-dev.1) (2021-09-15)

## [3.10.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.9.1-dev.1...3.10.0-dev.0) (2021-09-15)


### Features

* **backend/data_transfer:** prevent creating or editing data transfers associated to archived projects ([87cae2e](https://gitlab.com/biomedit/portal/commit/87cae2ef6a83daf8edd4f8f35f8f8f9614ce7a5d)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **backend/project:** prevent editing of archived projects ([453beae](https://gitlab.com/biomedit/portal/commit/453beae8866517727182b9eabfc422125b0f9932)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **backend/project:** send notification to node and project users when a project is archived ([b2104dc](https://gitlab.com/biomedit/portal/commit/b2104dc7270fad84d8c80f8611337fd2588d0452)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **backend/project:** set all data transfers related to a project to status `EXPIRED` when the project is archived ([4376d85](https://gitlab.com/biomedit/portal/commit/4376d85e2739208760edbc6402c430b43ec09ea9)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/DataTransferOverviewTable:** prevent editing DTRs of archived projects ([d260ba4](https://gitlab.com/biomedit/portal/commit/d260ba417168eac61ca1c8dbf1b1825e948e2d16)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/ProjectList:** add filter for archived projects and hide archived projects from "All Projects" and "My Projects" ([3b65842](https://gitlab.com/biomedit/portal/commit/3b658429417ad92707f8c41be72f61da2a13a5b2)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/ProjectList:** make archived projects read only except for deletion of data transfers and projects for staff ([910fac0](https://gitlab.com/biomedit/portal/commit/910fac04fd12cb834e49a4f9444bf3f7f0561615)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/ProjectList:** show confirmation dialog upon archival of a project ([438c8cb](https://gitlab.com/biomedit/portal/commit/438c8cbd5323e9cd51b9cb30a8d543b966981524)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend:** require typing the item name in the deletion dialog ([9a0f9e0](https://gitlab.com/biomedit/portal/commit/9a0f9e0db023e977f9d9cb27ce3a4f3ff8bfaa7e)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **projects:** allow PA's and NA's to archive and unarchive projects ([59ad86a](https://gitlab.com/biomedit/portal/commit/59ad86a9c6e227236515c82ef14575a74ac1def4)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)

### [3.9.1-dev.1](https://gitlab.com/biomedit/portal/compare/3.9.1-dev.0...3.9.1-dev.1) (2021-09-15)


### Bug Fixes

* **frontend/Home:** replace deprecated justify prop with its newer equivalent ([a3d6315](https://gitlab.com/biomedit/portal/commit/a3d6315fea3fffa9ed482d2d08898bc6e5b81289))
* **frontend/Home:** use GridItem special key prop properly. ([3d6fc59](https://gitlab.com/biomedit/portal/commit/3d6fc592c321e7e93f9d053f29d35b4a64537c6c))

### [3.9.1-dev.0](https://gitlab.com/biomedit/portal/compare/3.9.0...3.9.1-dev.0) (2021-08-25)


### Bug Fixes

* **frontend/Administration:** sort columns of admin table correctly ([2bac0cb](https://gitlab.com/biomedit/portal/commit/2bac0cb4078e0aad931bfadf88a2334b3024b488))
* **frontend/DataTransfer:** sort requestor column of data-transfers correctly ([a9f0946](https://gitlab.com/biomedit/portal/commit/a9f094685a65dd0c4a0598da9210d5c3a47c196a))

## [3.9.0](https://gitlab.com/biomedit/portal/compare/3.9.0-dev.1...3.9.0) (2021-08-20)

## [3.9.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.9.0-dev.0...3.9.0-dev.1) (2021-08-20)


### Bug Fixes

* **filebeat:** decode the message object in logs ([6f84fff](https://gitlab.com/biomedit/portal/commit/6f84fff38e00198a59489007bc560edd64196dfc)), closes [#442](https://gitlab.com/biomedit/portal/issues/442)

## [3.9.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.8.0...3.9.0-dev.0) (2021-08-20)


### Features

* add new Data Provider (object-based) permissions ([6550b1b](https://gitlab.com/biomedit/portal/commit/6550b1b68e52466eaec56b28341367f350087246)), closes [#368](https://gitlab.com/biomedit/portal/issues/368)

## [3.8.0](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.16...3.8.0) (2021-08-20)

## [3.8.0-dev.16](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.15...3.8.0-dev.16) (2021-08-19)


### Bug Fixes

* **docker-compose:** run filebeat with `root` user ([e19eaff](https://gitlab.com/biomedit/portal/commit/e19eafff6b0b655f538013708f410cbfa6586ee4))

## [3.8.0-dev.15](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.14...3.8.0-dev.15) (2021-08-19)


### Features

* Work around docker.sock in filebeat service ([073f5ea](https://gitlab.com/biomedit/portal/commit/073f5ea8ac7889760cf4992b659f1220d5f28f4c))

## [3.8.0-dev.14](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.13...3.8.0-dev.14) (2021-08-18)

## [3.8.0-dev.13](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.12...3.8.0-dev.13) (2021-08-12)


### Features

* **frontend:** show message instead of website if visitor is using an unsupported browser ([4d2ca25](https://gitlab.com/biomedit/portal/commit/4d2ca251328d1d0e64eb4df5a15edce03227d695)), closes [#189](https://gitlab.com/biomedit/portal/issues/189)

## [3.8.0-dev.12](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.11...3.8.0-dev.12) (2021-08-10)


### Features

* podman deployment ([52d759b](https://gitlab.com/biomedit/portal/commit/52d759b8d106289eda0bd8be0697c2c7b75fc1d1))

## [3.8.0-dev.11](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.10...3.8.0-dev.11) (2021-08-10)


### Features

* **security:** Avoid docker.sock in traefik service ([a204728](https://gitlab.com/biomedit/portal/commit/a20472869b3b8ea8771e17ccb45811c157f3ca2b))

## [3.8.0-dev.10](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.9...3.8.0-dev.10) (2021-08-09)


### Bug Fixes

* **docker-compose:** prevent filebeat from running in a restart loop ([ffba1c9](https://gitlab.com/biomedit/portal/commit/ffba1c9518449bb47923ae1d30ca98b85a04b18b))

## [3.8.0-dev.9](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.8...3.8.0-dev.9) (2021-08-09)

## [3.8.0-dev.8](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.7...3.8.0-dev.8) (2021-08-09)

## [3.8.0-dev.7](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.6...3.8.0-dev.7) (2021-08-02)

## [3.8.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.5...3.8.0-dev.6) (2021-08-02)

## [3.8.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.4...3.8.0-dev.5) (2021-07-29)


### Features

* **backend:** add history for changes on `User` ([18d5a05](https://gitlab.com/biomedit/portal/commit/18d5a059b92431227b172f01786c83f6c57a4377)), closes [#422](https://gitlab.com/biomedit/portal/issues/422)

## [3.8.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.3...3.8.0-dev.4) (2021-07-27)

## [3.8.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.2...3.8.0-dev.3) (2021-07-21)


### Features

* **backend/projects:** allow node admins to edit and create projects associated with their node ([9579f31](https://gitlab.com/biomedit/portal/commit/9579f3133e8252f4eba7625cf6f4f6408f5c097a)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend/ProjectForm:** allow node admins to only select nodes they are associated with when creating a new project ([d053ed0](https://gitlab.com/biomedit/portal/commit/d053ed0f30de082970073aa8e8042629355cf749)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend/ProjectForm:** prevent node admins from changing the node of an existing project ([dcfd973](https://gitlab.com/biomedit/portal/commit/dcfd973636bdf1bf9dbd45d68179c7d33ded5b20)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend/Projects:** allow node admins to edit and create projects associated with their node ([2591ae4](https://gitlab.com/biomedit/portal/commit/2591ae4b45ff10aefea6925429b6664248de0175)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)

## [3.8.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.1...3.8.0-dev.2) (2021-07-21)

## [3.8.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.0...3.8.0-dev.1) (2021-07-18)


### Features

* **frontend:** display groups that a user belongs to in userinfo box. ([c1a11ce](https://gitlab.com/biomedit/portal/commit/c1a11ced6eaaa47b0e60fd4c27ee1166a1b592ae)), closes [#370](https://gitlab.com/biomedit/portal/issues/370)

## [3.8.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.7.1-dev.1...3.8.0-dev.0) (2021-07-16)


### Features

* **frontend/admin/user:** display user flags in username tooltip ([d886aa3](https://gitlab.com/biomedit/portal/commit/d886aa3aab342ee95830f513abcd998918c65dab)), closes [#414](https://gitlab.com/biomedit/portal/issues/414)

### [3.7.1-dev.1](https://gitlab.com/biomedit/portal/compare/3.7.1-dev.0...3.7.1-dev.1) (2021-07-14)

### [3.7.1-dev.0](https://gitlab.com/biomedit/portal/compare/3.7.0...3.7.1-dev.0) (2021-07-13)


### Bug Fixes

* **backend/unique_check:** log `is not unique` responses from a unique check as `warn` instead of `error` ([4f615d4](https://gitlab.com/biomedit/portal/commit/4f615d4dc9f8eb9df8d3dc40f3fff80d357e8b83)), closes [#395](https://gitlab.com/biomedit/portal/issues/395)

## [3.7.0](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.17...3.7.0) (2021-07-13)

## [3.7.0-dev.17](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.16...3.7.0-dev.17) (2021-07-12)


### Bug Fixes

* **frontend/Administration/Users:** fix issue where sorting by username would result in an exception ([0cc2030](https://gitlab.com/biomedit/portal/commit/0cc20302259c1849ef0712d946b79a6e504bf65d)), closes [#418](https://gitlab.com/biomedit/portal/issues/418)

## [3.7.0-dev.16](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.15...3.7.0-dev.16) (2021-07-09)

## [3.7.0-dev.15](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.14...3.7.0-dev.15) (2021-07-08)

## [3.7.0-dev.14](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.13...3.7.0-dev.14) (2021-07-07)

## [3.7.0-dev.13](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.12...3.7.0-dev.13) (2021-07-07)

## [3.7.0-dev.12](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.11...3.7.0-dev.12) (2021-07-07)


### Features

* **backend/User:** add possibility to change/update 'uid', 'flags' and 'local_username' through the REST API ([37b8f33](https://gitlab.com/biomedit/portal/commit/37b8f33bb3557088fcc67acf6f0a026e4ebba9f9)), closes [#13](https://gitlab.com/biomedit/portal/issues/13)

## [3.7.0-dev.11](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.10...3.7.0-dev.11) (2021-07-05)


### Features

* **frontend/Home:** make longer titles possible in quick access tiles ([26a7df6](https://gitlab.com/biomedit/portal/commit/26a7df62d35438c2c224a093170bc1ff0207b6d8)), closes [#412](https://gitlab.com/biomedit/portal/issues/412)

## [3.7.0-dev.10](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.9...3.7.0-dev.10) (2021-07-01)

## [3.7.0-dev.9](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.8...3.7.0-dev.9) (2021-07-01)

## [3.7.0-dev.8](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.7...3.7.0-dev.8) (2021-06-28)


### Bug Fixes

* **frontend:** add `UserMenu` back ([9fb7fee](https://gitlab.com/biomedit/portal/commit/9fb7fee6fade425b91c0f746ee2d4980da90cb41)), closes [#411](https://gitlab.com/biomedit/portal/issues/411)
* **frontend:** fix weird styling in frontend production builds ([09ef4e6](https://gitlab.com/biomedit/portal/commit/09ef4e65c2e9afc64215b98808775df1b5553381)), closes [#411](https://gitlab.com/biomedit/portal/issues/411)

## [3.7.0-dev.7](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.6...3.7.0-dev.7) (2021-06-23)

## [3.7.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.5...3.7.0-dev.6) (2021-06-22)

## [3.7.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.4...3.7.0-dev.5) (2021-06-18)


### Features

* **frontend/Home:** display quick access tiles specified in the database ([9d987e7](https://gitlab.com/biomedit/portal/commit/9d987e7524b95e4fc5cfad16f3d49906b240dd27)), closes [#393](https://gitlab.com/biomedit/portal/issues/393)
* **quick accesses:** move quick access tiles administration to database ([52e0560](https://gitlab.com/biomedit/portal/commit/52e05604a332599b49592265d59f627de2848936)), closes [#393](https://gitlab.com/biomedit/portal/issues/393)

## [3.7.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.3...3.7.0-dev.4) (2021-06-18)


### Features

* **backend/DataPackageLogViewSet:** accept model permissions on the view ([16fa701](https://gitlab.com/biomedit/portal/commit/16fa701695ef34a1d7a2d45f4012763001d90f68)), closes [#405](https://gitlab.com/biomedit/portal/issues/405)

## [3.7.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.2...3.7.0-dev.3) (2021-06-17)


### Features

* **backend/logging:** add app name to request logging ([e91449f](https://gitlab.com/biomedit/portal/commit/e91449f931a1d8c36f94fad574a641d4b0fa365b)), closes [#397](https://gitlab.com/biomedit/portal/issues/397)

## [3.7.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.1...3.7.0-dev.2) (2021-06-16)

## [3.7.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.0...3.7.0-dev.1) (2021-06-16)

## [3.7.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.6.0...3.7.0-dev.0) (2021-06-15)


### Features

* **frontend/project:** add 'contact' to project resource ([e34cbce](https://gitlab.com/biomedit/portal/commit/e34cbce3545b6504a1a9277a9acfe9c8f026aa5b)), closes [#321](https://gitlab.com/biomedit/portal/issues/321)
* **frontend/project:** change resources management in project space ([105dd9a](https://gitlab.com/biomedit/portal/commit/105dd9a5b84f3a5237aafe2328f4da24b1ae44d0)), closes [#321](https://gitlab.com/biomedit/portal/issues/321)

## [3.6.0](https://gitlab.com/biomedit/portal/compare/3.6.0-dev.6...3.6.0) (2021-06-15)

## [3.6.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.6.0-dev.5...3.6.0-dev.6) (2021-06-14)

## [3.6.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.6.0-dev.4...3.6.0-dev.5) (2021-06-14)


### Bug Fixes

* **backend/logging:** log requests in `identities` app ([4f9f337](https://gitlab.com/biomedit/portal/commit/4f9f3378907f9daba1c0f9ed48735e735681c3d2)), closes [#394](https://gitlab.com/biomedit/portal/issues/394)

## [3.6.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.6.0-dev.3...3.6.0-dev.4) (2021-06-10)


### Bug Fixes

* **frontend/admin/users:** label IS the code for flags on users panel ([bdc97db](https://gitlab.com/biomedit/portal/commit/bdc97dbbb4d372103f761f5b75f8333fa9845a4a)), closes [#399](https://gitlab.com/biomedit/portal/issues/399)

## [3.6.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.6.0-dev.2...3.6.0-dev.3) (2021-06-10)


### Features

* **backend/permission:** allow node viewer/manager more read permissions ([2d63281](https://gitlab.com/biomedit/portal/commit/2d63281f6be66d3a07f9f1f079099dbbec81d8c9))

## [3.6.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.6.0-dev.1...3.6.0-dev.2) (2021-06-09)

## [3.6.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.6.0-dev.0...3.6.0-dev.1) (2021-06-04)

## [3.6.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.5.1-dev.3...3.6.0-dev.0) (2021-06-03)


### Features

* **frontend/UserManage:** show affiliations tooltip on username ([ba68082](https://gitlab.com/biomedit/portal/commit/ba6808207adbbe2a618e2d66f374b0951d661c92)), closes [#361](https://gitlab.com/biomedit/portal/issues/361)

### [3.5.1-dev.3](https://gitlab.com/biomedit/portal/compare/3.5.1-dev.2...3.5.1-dev.3) (2021-06-03)

### [3.5.1-dev.2](https://gitlab.com/biomedit/portal/compare/3.5.1-dev.1...3.5.1-dev.2) (2021-05-28)

### [3.5.1-dev.1](https://gitlab.com/biomedit/portal/compare/3.5.1-dev.0...3.5.1-dev.1) (2021-05-28)


### Bug Fixes

* **backend/tests:** fix tests not being executed which are in a class whose name is not prefixed by `Test` ([49d4de6](https://gitlab.com/biomedit/portal/commit/49d4de63dd26b8266ac7547150f157112244928e))

### [3.5.1-dev.0](https://gitlab.com/biomedit/portal/compare/3.5.0...3.5.1-dev.0) (2021-05-27)

## [3.5.0](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.12...3.5.0) (2021-05-25)

## [3.5.0-dev.12](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.11...3.5.0-dev.12) (2021-05-21)


### Bug Fixes

* **frontend/GroupManageForm:** fix a bug where in some cases the objects field in the object permissions was empty on edit ([c89080a](https://gitlab.com/biomedit/portal/commit/c89080a4aa36896b6b5b6a6238902ad2d09f8005)), closes [#381](https://gitlab.com/biomedit/portal/issues/381), relates to [/github.com/react-hook-form/react-hook-form/issues/1558#issuecomment-623196472](https://github.com/react-hook-form/react-hook-form/issues/1558/issues/issuecomment-623196472)
* **frontend/NodeManage:** fix bug where navigating from `Projects` to the node administration will always show all nodes ([3145f1a](https://gitlab.com/biomedit/portal/commit/3145f1ac7880894ef78016d03ad86684de1c0d8b)), closes [#381](https://gitlab.com/biomedit/portal/issues/381)

## [3.5.0-dev.11](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.10...3.5.0-dev.11) (2021-05-20)


### Features

* **backend/user:** projects API view should display 'affiliation_id' of users as well ([2382c94](https://gitlab.com/biomedit/portal/commit/2382c94d1fedfbdd6e6e5a68331bc02ce195b629))

## [3.5.0-dev.10](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.9...3.5.0-dev.10) (2021-05-20)


### Features

* **backend/user:** implement 'affiliation_id' based on 'linkedAffiliationUniqueID' ([3a7956b](https://gitlab.com/biomedit/portal/commit/3a7956b5425ea99f3507e63db7214fcc9b6e3b6b)), closes [#337](https://gitlab.com/biomedit/portal/issues/337)

## [3.5.0-dev.9](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.8...3.5.0-dev.9) (2021-05-20)


### Bug Fixes

* **frontend/Admin:** make 'isActive' property of user filterable and sortable ([e4f0caa](https://gitlab.com/biomedit/portal/commit/e4f0caa0bcdab5bb0c2639e8efa4ceed0b62cf68)), closes [#385](https://gitlab.com/biomedit/portal/issues/385)

## [3.5.0-dev.8](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.7...3.5.0-dev.8) (2021-05-20)


### Features

* **backend:** remove LDAP support ([a893875](https://gitlab.com/biomedit/portal/commit/a893875dd1da6c3b8ba1d6c2df10e3725617608f)), closes [#388](https://gitlab.com/biomedit/portal/issues/388)

## [3.5.0-dev.7](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.6...3.5.0-dev.7) (2021-05-18)

## [3.5.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.5...3.5.0-dev.6) (2021-05-18)


### Features

* **frontend:** show redux dev tools outside of local development if the environment variable `NEXT_USE_REDUX_DEV_TOOLS` is set to `true` ([a9fa207](https://gitlab.com/biomedit/portal/commit/a9fa207080314c508bde7723b6e9ed6721309179))

## [3.5.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.4...3.5.0-dev.5) (2021-05-18)

## [3.5.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.3...3.5.0-dev.4) (2021-05-17)

## [3.5.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.2...3.5.0-dev.3) (2021-05-17)

## [3.5.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.1...3.5.0-dev.2) (2021-05-17)

## [3.5.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.0...3.5.0-dev.1) (2021-05-12)


### Features

* **contact:** add contact form ([0682a64](https://gitlab.com/biomedit/portal/commit/0682a644cd99031931a5e37be635e24401c96702)), closes [#247](https://gitlab.com/biomedit/portal/issues/247)


### Bug Fixes

* **frontend/LoadingButton:** make sure spinner in form submit buttons always stays within the boundaries of the button ([85ce44b](https://gitlab.com/biomedit/portal/commit/85ce44b2960ce9c1dc20a2427706fb9f61e930bd))

## [3.5.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.4.1...3.5.0-dev.0) (2021-05-12)


### Features

* **backend/permissions:** add custom permission `admin_node` ([23b03bc](https://gitlab.com/biomedit/portal/commit/23b03bc96cfa51f094771cbe3583c9f495541d34)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **backend/permissions:** allow creating groups with permissions to access or modify permissions or groups ([28f7dbd](https://gitlab.com/biomedit/portal/commit/28f7dbdccfb62d2721b6f5529d4a9bae6214fad0)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **frontend/manage nodes:** make nodes editable by node admins ([0e41945](https://gitlab.com/biomedit/portal/commit/0e41945578a642eefbefb09464a9e1306eda0d53)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **frontend/NodeManagerForm:** make node id only editable by staff ([1a26f0a](https://gitlab.com/biomedit/portal/commit/1a26f0a3bd7d507b75b060f7a2cb34fd33d0ae65)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **group management:** allow Node Managers to edit `users` on groups ([8192569](https://gitlab.com/biomedit/portal/commit/81925696afd17df7d819def1eb41d2d4937ceee2)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **node:** remove `users` from `Node` ([aab1628](https://gitlab.com/biomedit/portal/commit/aab1628dabbff941d1c8e149ad7453b7667ee769)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **node permissions:** show `Administration` menu option and `Node` tabs for Node Admins and Node Viewers ([d16b155](https://gitlab.com/biomedit/portal/commit/d16b1550aceef0b2537a2eed796c61a04a7f90d5)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)

### [3.4.1](https://gitlab.com/biomedit/portal/compare/3.4.1-dev.0...3.4.1) (2021-05-11)

### [3.4.1-dev.0](https://gitlab.com/biomedit/portal/compare/3.4.0...3.4.1-dev.0) (2021-05-11)


### Bug Fixes

* **backend/notification:** empty affiliation triggers an error ([05c0492](https://gitlab.com/biomedit/portal/commit/05c049290b91592357d8f66dac4da5153fd4aab0))
* **frontend/TextField:** labelled fields do not get properly disabled ([fdbc0cf](https://gitlab.com/biomedit/portal/commit/fdbc0cfcd38cd8dd749bf42cd6746cda7ddb2e6d))

## [3.4.0](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.13...3.4.0) (2021-05-11)

## [3.4.0-dev.13](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.12...3.4.0-dev.13) (2021-05-11)


### Bug Fixes

* **backend/notification:** be smarter while checking user affiliations ([6d74528](https://gitlab.com/biomedit/portal/commit/6d74528a1ee536ebb1f01d3b48cfc598670f49c2))

## [3.4.0-dev.12](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.11...3.4.0-dev.12) (2021-05-10)


### Bug Fixes

* **frontend/AutocompleteField:** attach a hidden field in case a 'AutocompleteField' is disabled ([8b6d840](https://gitlab.com/biomedit/portal/commit/8b6d8408cfb106478f4febcdc795ed8cd160d0e4)), closes [#369](https://gitlab.com/biomedit/portal/issues/369)
* **frontend/LabelledField:** attach a hidden field in case a 'LabelledField' is disabled ([cd4420e](https://gitlab.com/biomedit/portal/commit/cd4420e415c9d1bde07c04570c7eb5808a5fa47f)), closes [#369](https://gitlab.com/biomedit/portal/issues/369)
* **frontend/project:** pass project role via hidden field and remove 'ref' when field is disabled ([a8f4850](https://gitlab.com/biomedit/portal/commit/a8f485006eef0e2f9a8e3ea463e6944ca1610112)), closes [#369](https://gitlab.com/biomedit/portal/issues/369)

## [3.4.0-dev.11](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.10...3.4.0-dev.11) (2021-05-07)


### Bug Fixes

* **frontend/logger:** import 'setimmediate' polyfill for winston logger ([5e1ead9](https://gitlab.com/biomedit/portal/commit/5e1ead940d4c1f0607e98c8756c083c4763691af)), closes [#377](https://gitlab.com/biomedit/portal/issues/377)

## [3.4.0-dev.10](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.9...3.4.0-dev.10) (2021-05-06)

## [3.4.0-dev.9](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.8...3.4.0-dev.9) (2021-05-06)


### Bug Fixes

* **frontend/validation:** fix 'max' vs. 'maxLength' ([8270b91](https://gitlab.com/biomedit/portal/commit/8270b915f7fccdc176730746dace8c86997338f0)), closes [#360](https://gitlab.com/biomedit/portal/issues/360)
* **frontend/validation:** specify a key to each 'validate' definition and use 'deepmerge' for merging the validators ([7c91c2d](https://gitlab.com/biomedit/portal/commit/7c91c2d6d5738587fb2eb8395315f896e01fbaf7)), closes [#360](https://gitlab.com/biomedit/portal/issues/360)

## [3.4.0-dev.8](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.7...3.4.0-dev.8) (2021-04-30)


### Bug Fixes

* **frontend/user:** overwrite 'datetime' as it can't cope with 'null' value ([3b61117](https://gitlab.com/biomedit/portal/commit/3b611177c90ae0073d611b8c131ccf93ffb6eefb))

## [3.4.0-dev.7](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.6...3.4.0-dev.7) (2021-04-29)


### Bug Fixes

* **config:** revert configuration ([c71b369](https://gitlab.com/biomedit/portal/commit/c71b3698c6243d1c7182dab9419eae1502162d17))

## [3.4.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.5...3.4.0-dev.6) (2021-04-28)


### Features

* **frontend/user:** make sorting case- and language-insensitive ([25c4d4f](https://gitlab.com/biomedit/portal/commit/25c4d4f9499935d365aff662d31067500dc102fd)), closes [#357](https://gitlab.com/biomedit/portal/issues/357)


### Bug Fixes

* **frontend/user:** use 'datetime' as sort type for 'lastlogin' column ([c93ee2c](https://gitlab.com/biomedit/portal/commit/c93ee2cb9de92ebe63f8ecab4578926f761b59c2)), closes [#357](https://gitlab.com/biomedit/portal/issues/357)

## [3.4.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.4...3.4.0-dev.5) (2021-04-28)


### Features

* **backend/data_transfer:** prevent data transfers from being created with a requestor that is not a data manager of the project ([1bfbb8d](https://gitlab.com/biomedit/portal/commit/1bfbb8d2e0b8ae5457f5b5d16c4b7be0d61952ca)), closes [#346](https://gitlab.com/biomedit/portal/issues/346)
* **frontend/DataTransferForm:** only show requestors in the dropdown which are data managers of the project ([7b442a9](https://gitlab.com/biomedit/portal/commit/7b442a9e93eeb8dd383b532852e50459e766c898)), closes [#346](https://gitlab.com/biomedit/portal/issues/346)

## [3.4.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.3...3.4.0-dev.4) (2021-04-27)


### Features

* **backend/scheduled job:** send a notification email if a user hasn't logged in for 60 days ([307866e](https://gitlab.com/biomedit/portal/commit/307866e946240853b33d65ba25ddc065f86f72bf)), closes [#333](https://gitlab.com/biomedit/portal/issues/333)

## [3.4.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.2...3.4.0-dev.3) (2021-04-23)


### Features

* **backend/data_package:** prevent a package from getting transferred using sett if it has been transferred before ([8b2a4c9](https://gitlab.com/biomedit/portal/commit/8b2a4c9b7e840e4b47cc96e1d7ad6a3f23e5a240)), closes [#350](https://gitlab.com/biomedit/portal/issues/350)

## [3.4.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.1...3.4.0-dev.2) (2021-04-22)


### Bug Fixes

* **backend/project:** make gid field read-only ([add778f](https://gitlab.com/biomedit/portal/commit/add778f099cdfd3ddaa53898b49c2cc760f9daed))

## [3.4.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.0...3.4.0-dev.1) (2021-04-22)


### Features

* **backend:** add object-level permission backend ([84f1c89](https://gitlab.com/biomedit/portal/commit/84f1c899dffda66848c145f28d2c7d1c5e070d36))
* **backend:** add object-level permission filter and permission verification class ([ecea135](https://gitlab.com/biomedit/portal/commit/ecea1354c9e4181a66fbe5ec302ea5c744e12170))
* **backend:** add object-level permission management endpoints ([b30b868](https://gitlab.com/biomedit/portal/commit/b30b868b83f0a7ea5c6dc9ad79c42a64012c1d91))
* **backend:** allow group management in user endpoint ([f6a39cf](https://gitlab.com/biomedit/portal/commit/f6a39cf56b890fc5571f5177b241a76abbcaeec4))
* **backend/DataProvider:** check object-level permissions ([ca74ab3](https://gitlab.com/biomedit/portal/commit/ca74ab381bda730c1067f589ccf275fc4f1fa927))
* **backend/Node:** check object-level permissions ([8a6bd11](https://gitlab.com/biomedit/portal/commit/8a6bd1144842fa2ce57b0bbada0609eb0c4e51aa))
* **frontend/admin:** add group management tab ([3281914](https://gitlab.com/biomedit/portal/commit/3281914cefa64b2efbc4a4f6ac9678f561710f62))
* **frontend/admin/user:** add group assignment field ([b71b621](https://gitlab.com/biomedit/portal/commit/b71b621b7a23a79ce6bbb870bccc86955318a204))


### Bug Fixes

* **backend/model:** make model str representations more verbose ([3813c3b](https://gitlab.com/biomedit/portal/commit/3813c3b8575bb54c4390596f66d603c1ee197be2))
* **frontend/UserManageForm:** make flags field optional ([d98a768](https://gitlab.com/biomedit/portal/commit/d98a768425f97df662bb28335de6ba5ddb7d5ca5))

## [3.4.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.3.0...3.4.0-dev.0) (2021-04-21)


### Features

* **backend:** make the lowest and highest `uid` and `gid` of users and `gid` of projects configurable in `config.json` ([37f6fc1](https://gitlab.com/biomedit/portal/commit/37f6fc18d640373382447f0245cbb7c3bdf58adf)), closes [#11](https://gitlab.com/biomedit/portal/issues/11)

## [3.3.0](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.12...3.3.0) (2021-04-20)

## [3.3.0-dev.12](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.11...3.3.0-dev.12) (2021-04-19)


### Bug Fixes

* **backend/data_package:** Propagate metadata to the DB ([871254e](https://gitlab.com/biomedit/portal/commit/871254e9d1f235e699a74cd0756c2af3bc3d7ab3))

## [3.3.0-dev.11](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.10...3.3.0-dev.11) (2021-04-19)


### Bug Fixes

* **backend/login:** redirect users trying to log in using a link with an expired session to the login page ([7437526](https://gitlab.com/biomedit/portal/commit/74375262fcfbba08824d7828f0a59f82f1397d38)), closes [#341](https://gitlab.com/biomedit/portal/issues/341)

## [3.3.0-dev.10](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.9...3.3.0-dev.10) (2021-04-16)


### Features

* **frontend/home:** add link to registry and specify 'target="_blank"' for each grid tile ([4370d4f](https://gitlab.com/biomedit/portal/commit/4370d4fd611038e6d79153eb3abcffa60d4bed2f)), closes [#336](https://gitlab.com/biomedit/portal/issues/336) [#343](https://gitlab.com/biomedit/portal/issues/343)

## [3.3.0-dev.9](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.8...3.3.0-dev.9) (2021-04-15)

## [3.3.0-dev.8](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.7...3.3.0-dev.8) (2021-04-14)


### Features

* **backend/user:** send notification ticket on user affiliation change ([3af7c70](https://gitlab.com/biomedit/portal/commit/3af7c70b14dc476fa204db8cc5e34cbd57ef244e)), closes [#332](https://gitlab.com/biomedit/portal/issues/332)

## [3.3.0-dev.7](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.6...3.3.0-dev.7) (2021-04-14)


### Features

* **forms:** add `SelectArrayField` ([db01ca7](https://gitlab.com/biomedit/portal/commit/db01ca71d7b492f0a20e28d941d4f0ff273d04f3)), closes [#338](https://gitlab.com/biomedit/portal/issues/338)
* **frontend/forms:** add `AutocompleteArrayField` ([5d142f4](https://gitlab.com/biomedit/portal/commit/5d142f4aec13ee321d33c06bdcc8f63c8d0fd45e)), closes [#338](https://gitlab.com/biomedit/portal/issues/338)

## [3.3.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.5...3.3.0-dev.6) (2021-04-14)

## [3.3.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.4...3.3.0-dev.5) (2021-04-14)

## [3.3.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.3...3.3.0-dev.4) (2021-04-01)


### Features

* **frontend/DataTransferTables:** only search for numbers in `id` and `maxPackages` ([4f134db](https://gitlab.com/biomedit/portal/commit/4f134dbf1b1a980b48008495660d1629943535a9)), closes [#329](https://gitlab.com/biomedit/portal/issues/329)


### Bug Fixes

* **frontend/DataTransfers:** fix a bug where newly added data transfers in `Projects` would not appear in `Data Transfers` ([21ae295](https://gitlab.com/biomedit/portal/commit/21ae2954d2a859c7d82b8ab0572b4c35602755e0))

## [3.3.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.2...3.3.0-dev.3) (2021-04-01)

## [3.3.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.1...3.3.0-dev.2) (2021-03-29)


### Features

* **user:** display 'local_username' for project users ([ac84f11](https://gitlab.com/biomedit/portal/commit/ac84f1196d321788692358a28b537ee082fbbda7))

## [3.3.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.0...3.3.0-dev.1) (2021-03-24)

## [3.3.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.2.1-dev.2...3.3.0-dev.0) (2021-03-24)


### Features

* **frontend/administration:** prevent the currently logged in user from inactivating themself ([bbee346](https://gitlab.com/biomedit/portal/commit/bbee346cd036447809fd07c49163d679f7c43564)), closes [#328](https://gitlab.com/biomedit/portal/issues/328)
* **frontend/tests:** log all requests on fails associated with `RequestVerifier` ([09a0751](https://gitlab.com/biomedit/portal/commit/09a07514a2822092c167ea2cfd39a3a37f558da3))


### Bug Fixes

* **frontend/renderers:** fix error when rendering a `LabelledControl` with `multiple` and an empty value ([de4d50b](https://gitlab.com/biomedit/portal/commit/de4d50b46e8caf412f629b8236978b23c760bfb1))

### [3.2.1-dev.2](https://gitlab.com/biomedit/portal/compare/3.2.1-dev.1...3.2.1-dev.2) (2021-03-23)

### [3.2.1-dev.1](https://gitlab.com/biomedit/portal/compare/3.2.1-dev.0...3.2.1-dev.1) (2021-03-23)

### [3.2.1-dev.0](https://gitlab.com/biomedit/portal/compare/3.2.0...3.2.1-dev.0) (2021-03-23)

## [3.2.0](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.11...3.2.0) (2021-03-22)

## [3.2.0-dev.11](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.10...3.2.0-dev.11) (2021-03-22)

## [3.2.0-dev.10](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.9...3.2.0-dev.10) (2021-03-19)

## [3.2.0-dev.9](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.8...3.2.0-dev.9) (2021-03-19)

## [3.2.0-dev.8](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.7...3.2.0-dev.8) (2021-03-19)


### Bug Fixes

* **frontend:** catch 'undefined' on value and default value ([a7c293a](https://gitlab.com/biomedit/portal/commit/a7c293a803c063930ccbb6cd54b6630e053811a9)), closes [#324](https://gitlab.com/biomedit/portal/issues/324)
* **frontend:** fix console errors when specifying users in a new project ([de109be](https://gitlab.com/biomedit/portal/commit/de109be9b7b10a1d60c1662a8c2b216716c8ef89)), closes [#324](https://gitlab.com/biomedit/portal/issues/324)

## [3.2.0-dev.7](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.6...3.2.0-dev.7) (2021-03-19)


### Features

* **frontend/navigation:** highlight current route ([df1b46e](https://gitlab.com/biomedit/portal/commit/df1b46e944293b058553eb64883552e7461ef3f8))

## [3.2.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.5...3.2.0-dev.6) (2021-03-18)


### Features

* **frontend/Home:** add easter egg during easter ([1b5ffd2](https://gitlab.com/biomedit/portal/commit/1b5ffd2c8e5d66cb0777d2b3f07358087dd21765)), closes [#304](https://gitlab.com/biomedit/portal/issues/304)
* **frontend/Home:** show easter background during easter ([5c8d48a](https://gitlab.com/biomedit/portal/commit/5c8d48af3fc431ec96caf67f8acb82a3866f8dde)), closes [#304](https://gitlab.com/biomedit/portal/issues/304)


### Bug Fixes

* **frontend/Home:** fix layout issue where quick access links would separate too far apart ([0eb6b62](https://gitlab.com/biomedit/portal/commit/0eb6b62ab1956254e45443adc9d22036e4a8f4a5))

## [3.2.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.4...3.2.0-dev.5) (2021-03-18)


### Features

* **Administration/Users Table:** add column showing if user is active or not ([f162c09](https://gitlab.com/biomedit/portal/commit/f162c09386b8129c791eaa128ae90c8b80edb8d3)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **backend/user:** remove objects related to a user which was made inactive ([8ce40a6](https://gitlab.com/biomedit/portal/commit/8ce40a69b79dd223bec3edf565320e4fc1699fbf)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **user management:** include inactive users in the list of users ([fbf10bd](https://gitlab.com/biomedit/portal/commit/fbf10bd9d3115d8eb9e2700f07f2e8532f07781c)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **User Management:** allow changing the active status of a user from the frontend ([ee87471](https://gitlab.com/biomedit/portal/commit/ee874713d65d6a57e71e7f9de573bdbe81f6920e)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)

## [3.2.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.3...3.2.0-dev.4) (2021-03-17)

## [3.2.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.2...3.2.0-dev.3) (2021-03-15)


### Bug Fixes

* **backend/signals:** remove deprecation warning ([586a458](https://gitlab.com/biomedit/portal/commit/586a4582b2ac20ff25b2397cf598b192b33bb0cd))

## [3.2.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.1...3.2.0-dev.2) (2021-03-12)


### Bug Fixes

* **frontend/DataTransferTable:** fix console error related to updating a component while rendering a different component ([d21374a](https://gitlab.com/biomedit/portal/commit/d21374a8839ecacb2b7d4d3e1a706ada412a8afb)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Home:** fix console error about missing `ref` ([740a572](https://gitlab.com/biomedit/portal/commit/740a57223f2331b64e71f39073b0a4a18ae126f8)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Home:** fix console error related to invalid DOM nesting ([8ffc005](https://gitlab.com/biomedit/portal/commit/8ffc005900837efb25b7febc7ff56de3fa63d1cc)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Projects:** fix console error about `Description` not having unique keys ([c810e66](https://gitlab.com/biomedit/portal/commit/c810e661fcd8d1bd10db7c089de1c0902385da5f)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Projects:** fix console error related to duplicate keys ([013b2ca](https://gitlab.com/biomedit/portal/commit/013b2ca351b2b30420cd0e32f54cf3910a2d9ae1)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Tab:** fix console error related to invalid DOM nesting ([de77b2f](https://gitlab.com/biomedit/portal/commit/de77b2f0ea7972af6225cace7d5eb906cfc9daa4)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Tables:** fix console error related to invalid DOM nesting ([c6738fd](https://gitlab.com/biomedit/portal/commit/c6738fdc2dd434fb8a4c15dc853702d5c1f0291f)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/UserInfoBox:** fix console error when opening the `UserInfoBox` due to a missing forwarded ref ([a5b5b42](https://gitlab.com/biomedit/portal/commit/a5b5b421a1c1b8a1ede57cfd5eeabd85047057e2)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/UserMenu:** fix console error when opening the Messages box ([3e401c4](https://gitlab.com/biomedit/portal/commit/3e401c48b5f9405d7f881efdaed82a458c263434)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)

## [3.2.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.0...3.2.0-dev.1) (2021-03-12)

## [3.2.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.1.0...3.2.0-dev.0) (2021-03-10)


### Features

* **Feed:** add basic markdown parsing for `title` and `message` ([9ba40ce](https://gitlab.com/biomedit/portal/commit/9ba40ce8ee50e8604a16f8c32616d849b4f1010a)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **Feed:** allow specifying a `title` separately from a `message` for `Feed` ([a6d742d](https://gitlab.com/biomedit/portal/commit/a6d742d6914eb8ed05f2d24fc21a2adf1bfcf469)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/Feed:** change icon for `Feed` label `WARN` to a warning symbol ([02f8eb0](https://gitlab.com/biomedit/portal/commit/02f8eb0ebfdb0b27ff2f8f8ccfb8b45d2b890463)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/FeedList:** show empty message when no feeds are present ([ce8c8f9](https://gitlab.com/biomedit/portal/commit/ce8c8f9c9ed810cae3457dc0d6d7d59f9815e4d1)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)


### Bug Fixes

* **frontend/Feed:** fix a bug where a feed with label `Warning` would not show an icon ([af417f7](https://gitlab.com/biomedit/portal/commit/af417f74e8b9981e309257e7edb03cb18391efad)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/Feed:** fix a bug where line breaks in Feed messages were ignored ([5e5bf7f](https://gitlab.com/biomedit/portal/commit/5e5bf7fd838b34ee5b70962a72d1cbc0f9432b24)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/Home:** fix strange layout with longer text in `Feed` ([6e49be1](https://gitlab.com/biomedit/portal/commit/6e49be168b2f2cce3234b81bcbb93a45ba5973e9)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)

## [3.1.0](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.9...3.1.0) (2021-03-09)

## [3.1.0-dev.9](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.8...3.1.0-dev.9) (2021-03-03)


### Bug Fixes

* **frontend/FlagManageForm:** use right backend API for flag uniqueness ([d42c71d](https://gitlab.com/biomedit/portal/commit/d42c71d1f32c5bf180225b14681c88b32772c5d6))

## [3.1.0-dev.8](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.7...3.1.0-dev.8) (2021-03-02)


### Bug Fixes

* **frontend/forms:** fix delay when clicking on the `submit` button in forms ([f94df38](https://gitlab.com/biomedit/portal/commit/f94df38d1b2f09f87bac4a8e6483328017a99bb4)), closes [#280](https://gitlab.com/biomedit/portal/issues/280)

## [3.1.0-dev.7](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.6...3.1.0-dev.7) (2021-03-01)

## [3.1.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.5...3.1.0-dev.6) (2021-03-01)


### Features

* **frontedn/UserInfoBox:** show user flags ([d0a12f9](https://gitlab.com/biomedit/portal/commit/d0a12f93bdae2cb08e3df21d2cb291c30c2bd887)), closes [#250](https://gitlab.com/biomedit/portal/issues/250)

## [3.1.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.4...3.1.0-dev.5) (2021-03-01)


### Features

* **backend/user:** add optional flags specification on user ([21d9e98](https://gitlab.com/biomedit/portal/commit/21d9e980fad2a2c388b447d18214a789f20e6425)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend/flags:** add a new tab for flags administration ([c050f87](https://gitlab.com/biomedit/portal/commit/c050f8706aa91fa11517068b193d4327352561a3)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend/reducers:** introduce reducer for flags ([1f142c4](https://gitlab.com/biomedit/portal/commit/1f142c4f953682860cfe0ac8e1e49e171c739fbd)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend/user form:** display the flags in the user form ([9d677b6](https://gitlab.com/biomedit/portal/commit/9d677b6a72398d9cda0a40bc2b430fddf4b0f300)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)


### Bug Fixes

* **frontend/affiliations:** nicely display the affiliations ([dedc85d](https://gitlab.com/biomedit/portal/commit/dedc85dc033fa14a20034625f826f3caa203a56e))

## [3.1.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.3...3.1.0-dev.4) (2021-02-25)


### Bug Fixes

* **backend/exception_handler:** log exceptions where a user is not logged in as `INFO` instead of `ERROR` ([6fc62b5](https://gitlab.com/biomedit/portal/commit/6fc62b5f3fe70a419ddd9ee3e67424e492628f30))

## [3.1.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.2...3.1.0-dev.3) (2021-02-25)


### Features

* **frontend:** make docs url configurable in env vars ([4ed892c](https://gitlab.com/biomedit/portal/commit/4ed892c48c5273bb95a8b306b07effcd86166de2)), closes [#299](https://gitlab.com/biomedit/portal/issues/299)

## [3.1.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.1...3.1.0-dev.2) (2021-02-24)

## [3.1.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.0...3.1.0-dev.1) (2021-02-23)


### Features

* **maxPackages:** enforce 'maxPackages' specification (no longer optional) ([95d0bb7](https://gitlab.com/biomedit/portal/commit/95d0bb7783f0a5aa01d684fa340ef0d0f108627a))


### Bug Fixes

* **frontend/maxPackages:** show unlimited DTR's when filtering data transfer tables for 'unlimited' ([eee6856](https://gitlab.com/biomedit/portal/commit/eee685643cd1eef277086e7c825b68d75edc0e3b)), closes [#249](https://gitlab.com/biomedit/portal/issues/249)

## [3.1.0-dev.0](https://gitlab.com/biomedit/portal/compare/3.0.1-dev.1...3.1.0-dev.0) (2021-02-23)


### Features

* **backend/migrations:** generate migration script ([8d6c9bc](https://gitlab.com/biomedit/portal/commit/8d6c9bcf14dbfdb3eadaf9b623b7489819fb6a30)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)
* **backend/models:** replace user 'status' with flags ([76bb9a1](https://gitlab.com/biomedit/portal/commit/76bb9a1a5d0d5ea4ba65c4fb584e87ca45ad813a)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)
* **frontend/api:** re-generate the API ([6a7350c](https://gitlab.com/biomedit/portal/commit/6a7350c91f632be693c5564aa4d75cb022a29415)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)


### Bug Fixes

* **frontend/user administration:** remove displaying/management of user status ([aacf895](https://gitlab.com/biomedit/portal/commit/aacf895b33b13a4360e726190c1189e0f2429bd0)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)

### [3.0.1-dev.1](https://gitlab.com/biomedit/portal/compare/3.0.1-dev.0...3.0.1-dev.1) (2021-02-23)

### [3.0.1-dev.0](https://gitlab.com/biomedit/portal/compare/3.0.0...3.0.1-dev.0) (2021-02-23)

## [3.0.0](https://gitlab.com/biomedit/portal/compare/3.0.0-dev.6...3.0.0) (2021-02-22)

## [3.0.0-dev.6](https://gitlab.com/biomedit/portal/compare/3.0.0-dev.5...3.0.0-dev.6) (2021-02-22)

## [3.0.0-dev.5](https://gitlab.com/biomedit/portal/compare/3.0.0-dev.4...3.0.0-dev.5) (2021-02-18)

## [3.0.0-dev.4](https://gitlab.com/biomedit/portal/compare/3.0.0-dev.3...3.0.0-dev.4) (2021-02-17)


### Features

* **logging:** unify different logging streams into the console output ([3be84e1](https://gitlab.com/biomedit/portal/commit/3be84e1bb77776278a9d40a653966d5ec6ec8aca))

## [3.0.0-dev.3](https://gitlab.com/biomedit/portal/compare/3.0.0-dev.2...3.0.0-dev.3) (2021-02-17)


### Features

* **frontend:** conditional adding of empty entries for IP ranges and resources ([e15f7f7](https://gitlab.com/biomedit/portal/commit/e15f7f78d559a1de20d7318c951dabb9fba86244))


### Bug Fixes

* **frontend/resources:** improve layout of resources block ([e3c303f](https://gitlab.com/biomedit/portal/commit/e3c303fc9481fbf2005b0e28d3bd2894747b0de1)), closes [#290](https://gitlab.com/biomedit/portal/issues/290)

## [3.0.0-dev.2](https://gitlab.com/biomedit/portal/compare/3.0.0-dev.1...3.0.0-dev.2) (2021-02-12)


### Bug Fixes

* **backend/user status:** remove all consequences linked to user status changes ([42acd87](https://gitlab.com/biomedit/portal/commit/42acd87b9d0ed0725fea7fc3ba2b210e81e1758c)), closes [#219](https://gitlab.com/biomedit/portal/issues/219) [#217](https://gitlab.com/biomedit/portal/issues/217) [#284](https://gitlab.com/biomedit/portal/issues/284)
* **frontend/UsersList:** remove warning icon when user is 'INITIAL' and get rid of 'excludeUnauthorized' ([6692199](https://gitlab.com/biomedit/portal/commit/6692199d3794750330de3b9d63d547ec27fd39cf)), closes [#284](https://gitlab.com/biomedit/portal/issues/284)

## [3.0.0-dev.1](https://gitlab.com/biomedit/portal/compare/3.0.0-dev.0...3.0.0-dev.1) (2021-02-11)


### Bug Fixes

* **frontend/UsersList:** make sure that we are comparing numbers ([7cd17e6](https://gitlab.com/biomedit/portal/commit/7cd17e6d2205a6c125866795b4120258c8c3348e)), closes [#295](https://gitlab.com/biomedit/portal/issues/295) [#279](https://gitlab.com/biomedit/portal/issues/279)

## [3.0.0-dev.0](https://gitlab.com/biomedit/portal/compare/2.3.0-dev.0...3.0.0-dev.0) (2021-02-11)


### ⚠ BREAKING CHANGES

* **frontend:** The restriction of only having environment variables exposed to the frontend
with keys prefixed by `REACT_APP_` came from `create-react-app`.
With the migration to Next.js, we have to use the Next.js specific prefix `NEXT_PUBLIC_`.

Migration Steps: Rename all environment variable key names starting with `REACT_APP_` to start with `NEXT_PUBLIC_` instead.

### Bug Fixes

* **frontend:** prevent errors when accessing `window` / `document` during SSR ([ea821d1](https://gitlab.com/biomedit/portal/commit/ea821d1cffe502bb77c324b5bbdcf950c4d3a724))
* **NotFound:** fix error `<h6> cannot appear as a descendant of <p>` ([f95774a](https://gitlab.com/biomedit/portal/commit/f95774a76f4741146de69db2e97feff660be15cf))


* **frontend:** replace `react-env` mechanism for loading environment variables with Next.js built-in features ([46dcf8d](https://gitlab.com/biomedit/portal/commit/46dcf8d5c7fe3f5892abbd6522dd9f3b83cf25b1))

## [2.3.0-dev.0](https://gitlab.com/biomedit/portal/compare/2.2.0...2.3.0-dev.0) (2021-02-09)


### Features

* **backend/test_node:** ensure that we have the right number of registered node users returned ([b473ee5](https://gitlab.com/biomedit/portal/commit/b473ee518928ff196e01027da8c95a29d149c662))


### Bug Fixes

* **backend/user:** return all active users from '/backend/users' to node managers ([b4a722f](https://gitlab.com/biomedit/portal/commit/b4a722feb2986366b23ed545ab544c7216069439)), closes [#291](https://gitlab.com/biomedit/portal/issues/291)

## [2.2.0](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.12...2.2.0) (2021-02-08)

## [2.2.0-dev.12](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.11...2.2.0-dev.12) (2021-02-08)


### Bug Fixes

* **frontend/ListHooks:** fix repetitive effect calls ([87496b3](https://gitlab.com/biomedit/portal/commit/87496b396589d8bfd017ffbee39a831a8a00f17d))
* **frontend/UsersList:** fix add user not working ([87aa1bb](https://gitlab.com/biomedit/portal/commit/87aa1bbb842a2245b43f2e8328ba00ad20cb19d6))

## [2.2.0-dev.11](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.10...2.2.0-dev.11) (2021-02-05)

## [2.2.0-dev.10](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.9...2.2.0-dev.10) (2021-02-03)

## [2.2.0-dev.9](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.8...2.2.0-dev.9) (2021-02-03)


### Features

* **backend/data_transfer:** completely remove RT ticket generation/email notification on DTR creation ([b8af12e](https://gitlab.com/biomedit/portal/commit/b8af12e6e0ad65e304b276b1ec12fdc3a42fa600)), closes [#276](https://gitlab.com/biomedit/portal/issues/276)
* **frontend:** DM are no longer able to create new DTRs ([260c860](https://gitlab.com/biomedit/portal/commit/260c8606e1b934a360b17690ec5b56c144c291e0)), closes [#276](https://gitlab.com/biomedit/portal/issues/276)

## [2.2.0-dev.8](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.7...2.2.0-dev.8) (2021-02-03)

## [2.2.0-dev.7](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.6...2.2.0-dev.7) (2021-02-03)


### Bug Fixes

* **frontend:** revert cleanup "missing dependencies" linter warnings ([cf42297](https://gitlab.com/biomedit/portal/commit/cf422971267853ab4a50dceb8e94b72e9e727361))

## [2.2.0-dev.6](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.5...2.2.0-dev.6) (2021-02-03)


### Bug Fixes

* **frontend/ProjectList:** only show edit button on projects the user has the permissions to edit ([2141216](https://gitlab.com/biomedit/portal/commit/21412161ba66bef32488ba8e10d161f9f6295111)), closes [#281](https://gitlab.com/biomedit/portal/issues/281)

## [2.2.0-dev.5](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.4...2.2.0-dev.5) (2021-02-02)


### Features

* **frontend/forms:** make all dialogs that can be cancelled closeable by pressing the escape key on the keyboard or clicking outside of the dialog ([0d19c8b](https://gitlab.com/biomedit/portal/commit/0d19c8bdb76d68a6b6eec1dd77d91e9a6a9cb2dd))

## [2.2.0-dev.4](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.3...2.2.0-dev.4) (2021-02-02)


### Features

* **backend:** a node manager can edit his own nodes ([801e859](https://gitlab.com/biomedit/portal/commit/801e859b5029045afc88d64fd885b8f674875740)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **backend/ReadOnly:** 'ReadOnly' should apply to authenticated users only ([10462a5](https://gitlab.com/biomedit/portal/commit/10462a55cc4bd7240c6af2b8a8c8b4b9a743af77))
* **frontend:** polish node management by NM ([ce12192](https://gitlab.com/biomedit/portal/commit/ce1219219607f2b76f2d9063c4f3b27ca1757ed8)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **frontend/navigation:** rename 'Users & Groups' to 'Administration' ([a6a7651](https://gitlab.com/biomedit/portal/commit/a6a76511b3cc58d818291388130f4e13c519245f))
* **frontend/permmissions:** make 'Nodes' tab item accessible to node managers ([d813e85](https://gitlab.com/biomedit/portal/commit/d813e85b66a340136c1c660b4a8a97ea3e3c97c4)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)


### Bug Fixes

* **backend/Userinfo:** make Userinfo ID required as we're only using GET ([dda607d](https://gitlab.com/biomedit/portal/commit/dda607d4e0032b0c4020cb0c556e3db0a9103333))
* **frontend:** make sure that NMs are not editing node IDs ([fcc6a63](https://gitlab.com/biomedit/portal/commit/fcc6a632501b950625c305c57c103e6e2a7927c4)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)

## [2.2.0-dev.3](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.2...2.2.0-dev.3) (2021-02-01)


### Features

* **DataTransfer:** prevent changing purpose on DTR's once a data package was created ([6a1edbf](https://gitlab.com/biomedit/portal/commit/6a1edbf07fb1b8ce8c3cd4be433cf95406a4a23a))

## [2.2.0-dev.2](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.1...2.2.0-dev.2) (2021-02-01)

## [2.2.0-dev.1](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.0...2.2.0-dev.1) (2021-01-27)

## [2.2.0-dev.0](https://gitlab.com/biomedit/portal/compare/2.1.0...2.2.0-dev.0) (2021-01-27)


### Features

* **users:** validate `email` on `User` to be unique ([2968a4d](https://gitlab.com/biomedit/portal/commit/2968a4d2d0477687189c9adfa16fecd7544baf35)), closes [#273](https://gitlab.com/biomedit/portal/issues/273)


### Bug Fixes

* **frontend/empty tables:** fix add button not being visible when table is empty ([5f8ac65](https://gitlab.com/biomedit/portal/commit/5f8ac654960fc986efa1a9898efbf8f324befed5))

## [2.1.0](https://gitlab.com/biomedit/portal/compare/2.1.0-dev.0...2.1.0) (2021-01-26)

## [2.1.0-dev.0](https://gitlab.com/biomedit/portal/compare/2.0.0...2.1.0-dev.0) (2021-01-26)


### Features

* **user:** enforce `email` of users to be unique ([2a51934](https://gitlab.com/biomedit/portal/commit/2a519344f05f998630baf0ea2632a3bad896441b))

## [2.0.0](https://gitlab.com/biomedit/portal/compare/2.0.0-dev.1...2.0.0) (2021-01-26)

## [2.0.0-dev.1](https://gitlab.com/biomedit/portal/compare/2.0.0-dev.0...2.0.0-dev.1) (2021-01-26)


### Bug Fixes

* **user_init:** use `get_user_model` to retrieve the `User` model as defined in `settings.py` ([cd7a6dc](https://gitlab.com/biomedit/portal/commit/cd7a6dc513536887d6e9e5c626bf8aa68f54d9ac))

## [2.0.0-dev.0](https://gitlab.com/biomedit/portal/compare/1.5.0-dev.0...2.0.0-dev.0) (2021-01-26)


### ⚠ BREAKING CHANGES

* **backend:** If you are upgrading an existing `portal` installation, you MUST first deploy this version
and follow the migration steps BEFORE deploying any later version!

Django requires the first migration to be the one that initializes a custom user model
(see https://docs.djangoproject.com/en/3.1/topics/auth/customizing/#changing-to-a-custom-user-model-mid-project)
which is why all existing migrations had to be removed, which requires manual intervention during deployment.

Migration Steps:
- Stop the running docker containers, except for the database container
- Make a backup of the database
- Run the following query on the database: `TRUNCATE django_migrations`
- Deploy the image of EXACTLY this version (don't run the backend server yet)
- Run `./manage.py migrate --fake` on the backend container
- Run the backend server as usual
- From this point on, you can deploy any version later than this one,
using `./manage.py migrate` (without `--fake`) and running the server like usual

* **backend:** use custom user model for authentication ([875c98f](https://gitlab.com/biomedit/portal/commit/875c98f27b7bfd27cfd76a06eb76cbfc01b6b398))

## [1.5.0-dev.0](https://gitlab.com/biomedit/portal/compare/1.4.0...1.5.0-dev.0) (2021-01-25)


### Features

* **frontend/UserManageForm:** add cancel button to user edit form ([f118e18](https://gitlab.com/biomedit/portal/commit/f118e18d6cea66e4761fd56856d95b228a259614))
* **user management:** enable creating local users in the user management in the frontend ([0c7cef7](https://gitlab.com/biomedit/portal/commit/0c7cef78cf4f87f5aee0b5664435196f44786f4a))

## [1.4.0](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.8...1.4.0) (2021-01-25)

## [1.4.0-dev.8](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.7...1.4.0-dev.8) (2021-01-21)


### Bug Fixes

* **backend/update_permissions:** add user status to PL mail ([30518bd](https://gitlab.com/biomedit/portal/commit/30518bd86a0795b5d5bdfaae03d06fccd1a7392a))

## [1.4.0-dev.7](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.6...1.4.0-dev.7) (2021-01-21)


### Features

* **backend/unauthorized users:** prevent the backend from accepting users in `UNAUTHORIZED` status in modifying operations on projects ([0b973cd](https://gitlab.com/biomedit/portal/commit/0b973cd6ec3cad498ffeeb863bfbc90fbe694bb5)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **frontend/ProjectForm:** show warning icon next to users with `INITIAL` state ([8662bfc](https://gitlab.com/biomedit/portal/commit/8662bfcf8e1fb36d57aa1fee091fe35d341477c5)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **frontend/user selection:** hide users in `UNAUTHORIZED` status from user selection in the project form ([7adc655](https://gitlab.com/biomedit/portal/commit/7adc655d4dacc80bba518a484d54828a5a7ff98e)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **Project Resources:** disable resources for users with `INITIAL` status ([c33a55b](https://gitlab.com/biomedit/portal/commit/c33a55b317fc9d11f316185b7434a1cf33e29cea)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **users:** hide users which are not active ([3b494a9](https://gitlab.com/biomedit/portal/commit/3b494a99fe5b56818f9314122d8f968210414ff3)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)


### Bug Fixes

* **frontend/forms:** support nested array fields in forms ([12f41ec](https://gitlab.com/biomedit/portal/commit/12f41eced27af14221945ffb24237d445eecc293))

## [1.4.0-dev.6](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.5...1.4.0-dev.6) (2021-01-20)


### Features

* **backend/project_user_notification:** for each user added/removed, specify the status ([064eb1c](https://gitlab.com/biomedit/portal/commit/064eb1cc4eff99b4cd611d0807e36daffa0e9c35))

## [1.4.0-dev.5](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.4...1.4.0-dev.5) (2021-01-20)

## [1.4.0-dev.4](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.3...1.4.0-dev.4) (2021-01-20)


### Features

* **data package:** Add 'ordering' to '/backend/data-transfer/' ([a569dbc](https://gitlab.com/biomedit/portal/commit/a569dbcd663b65930ccff4b1e7ae38f6189910e8))
* **fontend/DataTransferDetail:** Use file name instead of 'Package X' ([fc78b35](https://gitlab.com/biomedit/portal/commit/fc78b358242404e4c7498eeaecad4f9c2943baf2))
* **serializers/data_transfer:** Sort data packages inside a data transfer by file name ([ac964c1](https://gitlab.com/biomedit/portal/commit/ac964c1482ada3feb7967c8e51871ccf3a2e0618))

## [1.4.0-dev.3](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.2...1.4.0-dev.3) (2021-01-19)


### Features

* **backend:** add name to projects app ([157aa50](https://gitlab.com/biomedit/portal/commit/157aa50dcdd21eacfffecd3733637e189ce8f4f1))
* **logging:** add request logging ([e8302ae](https://gitlab.com/biomedit/portal/commit/e8302ae0af4bfce4a9a5b16b9d4e031e71af9785)), closes [#242](https://gitlab.com/biomedit/portal/issues/242)


### Bug Fixes

* **UserDataPermission:** handle requests without profile field ([ffdf7df](https://gitlab.com/biomedit/portal/commit/ffdf7df3ce6e306290ac1c8ce91277bdb21f4b67))

## [1.4.0-dev.2](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.1...1.4.0-dev.2) (2021-01-18)

## [1.4.0-dev.1](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.0...1.4.0-dev.1) (2021-01-15)


### Features

* **test:** Improve error message on failing ([499a25c](https://gitlab.com/biomedit/portal/commit/499a25ce3615101d7a3084caa04943637f6ff3b4))

## [1.4.0-dev.0](https://gitlab.com/biomedit/portal/compare/1.3.1-dev.0...1.4.0-dev.0) (2021-01-14)


### Features

* **frontend:** Use 'FixedChildrenHeight' in DP and NM forms ([1332015](https://gitlab.com/biomedit/portal/commit/1332015bb617eabbc61b240b569ecfe90f2576ad))
* **frontend/App:** Use 'Box' with display instead of custom React element ([062f04c](https://gitlab.com/biomedit/portal/commit/062f04c6bfa0ec3e534c74a9dc5c6fbc61057411))
* **frontend/DataTransferForm:** Use 'FixedChildrenHeight' in DT form ([e7f8fcc](https://gitlab.com/biomedit/portal/commit/e7f8fcca7fa9bdf9ea8d04a05138fd4e25d6036f))
* **frontend/ProjectForm:** Add more space between project metadata fields ([5f214ab](https://gitlab.com/biomedit/portal/commit/5f214aba6bb2f75fa2b4465f26ed77de7e961d83))
* **frontend/SetUsernameDialog:** Use 'fullWidth' for input field ([46d73cb](https://gitlab.com/biomedit/portal/commit/46d73cb4142944cfe56d7d0128d365026282c2bb))

### [1.3.1-dev.0](https://gitlab.com/biomedit/portal/compare/1.3.0...1.3.1-dev.0) (2021-01-14)

## [1.3.0](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.12...1.3.0) (2021-01-11)

## [1.3.0-dev.12](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.11...1.3.0-dev.12) (2021-01-11)


### Bug Fixes

* **tables:** navigate to first page when search query is changed ([c49d1e3](https://gitlab.com/biomedit/portal/commit/c49d1e366a8d17b5578c6fa18fcb8a955296327c)), closes [#256](https://gitlab.com/biomedit/portal/issues/256)

## [1.3.0-dev.11](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.10...1.3.0-dev.11) (2021-01-11)

## [1.3.0-dev.10](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.9...1.3.0-dev.10) (2021-01-08)


### Bug Fixes

* **username form:** correct validation error to mention that the username must start with a letter ([93e06fc](https://gitlab.com/biomedit/portal/commit/93e06fc155966f3238fc28fa4298fb3871229c78)), closes [#259](https://gitlab.com/biomedit/portal/issues/259)

## [1.3.0-dev.9](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.8...1.3.0-dev.9) (2021-01-08)


### Features

* **projects:** only send email notification to ticketing system when AUTHORIZED users are added/removed to/from a project ([df56f4d](https://gitlab.com/biomedit/portal/commit/df56f4d1e28f25e4d77bf7d8180b90c989468e23)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **projects:** remove all project permission from users when their status is changed from AUTHORIZED to INITIAL or UNAUTHORIZED and send email notification ([519d3ba](https://gitlab.com/biomedit/portal/commit/519d3babdf7534c09f208983367d1c684a00d2ea)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **projects:** send email notification to ticketing system when status of user is set to AUTHORIZED ([39a4a9f](https://gitlab.com/biomedit/portal/commit/39a4a9fe3885ceb33f9c246d49d3bb160010e571)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **projects/users:** only return users with AUTHORIZED status when users of a project are retrieved by permission ([c48b8c3](https://gitlab.com/biomedit/portal/commit/c48b8c3eccdd5dd98d4fc6c560aede21c333b18f)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)

## [1.3.0-dev.8](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.7...1.3.0-dev.8) (2021-01-07)


### Features

* **backend:** Add key_length to PgpKey model ([b1dd5d6](https://gitlab.com/biomedit/portal/commit/b1dd5d6157f10c0b37866398bf9ba2b2627087ef))
* **backend:** key sign request: check for minimal key length ([7da8401](https://gitlab.com/biomedit/portal/commit/7da8401c801972489b43b81075dc4c60ed81bb43))

## [1.3.0-dev.7](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.6...1.3.0-dev.7) (2021-01-06)


### Features

* **backend/OIDCAuth:** Adapt 'OIDCAuth' to support local -> federated user migration ([5e5d727](https://gitlab.com/biomedit/portal/commit/5e5d7270e0137247b4fb6eec4911719ea10fbc80))
* **frontend/ProjectForm:** Check whether at least one PL has been assigned to the project ([2f31f01](https://gitlab.com/biomedit/portal/commit/2f31f01600243b530e81fc1c79bea472c09f8258))


### Bug Fixes

* **backend/project:** `send_user_emails` should be invoked on project creation/deletion as well ([4748a9f](https://gitlab.com/biomedit/portal/commit/4748a9fe0bd51f66532d4c3cab905c108387c0bb))

## [1.3.0-dev.6](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.5...1.3.0-dev.6) (2021-01-06)


### Bug Fixes

* **node:** fix unique check for nodes ([ed07c0a](https://gitlab.com/biomedit/portal/commit/ed07c0adab6eb4078e58b07231784fa21ae37605))
* **username validation:** fix incorrect validation error message for usernames ([32c2210](https://gitlab.com/biomedit/portal/commit/32c2210f7341aaf5841a4598d5e842ba8a35939e))

## [1.3.0-dev.5](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.4...1.3.0-dev.5) (2021-01-05)


### Bug Fixes

* **frontend:** fix unique check not working in production builds ([891c6bc](https://gitlab.com/biomedit/portal/commit/891c6bcece7733c5ea2a9c3cd6110f74ef759ef8)), relates to [/github.com/facebook/create-react-app/blob/master/packages/react-scripts/config/webpack.config.js#L272](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/config/webpack.config.js/issues/L272)
* **frontend:** when editing an object, changing a text field's value back to its initial value will not show a unique validation error ([d961e50](https://gitlab.com/biomedit/portal/commit/d961e50ccb781f384ffcff761ca882986b36ebbf))

## [1.3.0-dev.4](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.3...1.3.0-dev.4) (2021-01-04)


### Bug Fixes

* **frontend/projects:** Revert Display dedicated "empty project" message for initial users ([d71a91a](https://gitlab.com/biomedit/portal/commit/d71a91a1bfcbfdc9affe12c0477a24e33b5723b2))

## [1.3.0-dev.3](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.2...1.3.0-dev.3) (2020-12-18)


### Features

* **data providers:** show unique validation errors for name and id ([cdd9831](https://gitlab.com/biomedit/portal/commit/cdd983188431ede5dbde6f2edb86aec4ae24ee5b))
* **frontend:** prevent repeated calls to the backend during uniqueness check if keystrokes are less than 750ms apart ([b626fcc](https://gitlab.com/biomedit/portal/commit/b626fcc6d129dbebaccfaeec46c3c0551a511567))
* **nodes:** show unique validation errors for name and id ([20b6bc1](https://gitlab.com/biomedit/portal/commit/20b6bc1d9ea882217c4775e6e1798a5f97fb5cc7))
* **projects:** show unique validation errors for name and id ([a6f06f6](https://gitlab.com/biomedit/portal/commit/a6f06f63cbfd9c33de768c1e0687a4fa8a0117a6))

## [1.3.0-dev.2](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.1...1.3.0-dev.2) (2020-12-18)


### Bug Fixes

* **frontend:** remove leading dollar sign in correlationId's ([eac12c6](https://gitlab.com/biomedit/portal/commit/eac12c64b02f89a74a16ce3a2d0fb003df129f43))

## [1.3.0-dev.1](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.0...1.3.0-dev.1) (2020-12-18)


### Bug Fixes

* **frontend:** fix toasts not appearing on errors ([bc9bd71](https://gitlab.com/biomedit/portal/commit/bc9bd7163629a8ad2ec80fb99bcff7e70b726357))

## [1.3.0-dev.0](https://gitlab.com/biomedit/portal/compare/1.2.0...1.3.0-dev.0) (2020-12-18)


### Features

* **backend:** send all logs to ELK ([2f59b3a](https://gitlab.com/biomedit/portal/commit/2f59b3a4efa2976739fbc4d0340ee4e8d27f4867))

## [1.2.0](https://gitlab.com/biomedit/portal/compare/1.2.0-dev.4...1.2.0) (2020-12-14)

## [1.2.0-dev.4](https://gitlab.com/biomedit/portal/compare/1.2.0-dev.3...1.2.0-dev.4) (2020-12-14)

## [1.2.0-dev.3](https://gitlab.com/biomedit/portal/compare/1.2.0-dev.2...1.2.0-dev.3) (2020-12-11)


### Features

* **frontend/projects:** Display dedicated "empty project" message for initial users ([7410716](https://gitlab.com/biomedit/portal/commit/74107162e891cad4249937df6fa8c6d730a93e5c))

## [1.2.0-dev.2](https://gitlab.com/biomedit/portal/compare/1.2.0-dev.1...1.2.0-dev.2) (2020-12-11)

## [1.2.0-dev.1](https://gitlab.com/biomedit/portal/compare/1.1.0...1.2.0-dev.1) (2020-12-11)


### Features

* **frontend/users:** show user status in table under `Users & Groups` ([ca93594](https://gitlab.com/biomedit/portal/commit/ca935941722721e48560fcf5f02c862fd9086f28)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **user:** add new property `status` to user profile ([c4fa04a](https://gitlab.com/biomedit/portal/commit/c4fa04a1b675112e1da6e87c4e3ba37f8d56cb67)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **users:** allow changing user status on the frontend ([0d22e13](https://gitlab.com/biomedit/portal/commit/0d22e13fc79278b7f96d6176b541bdda901c8796)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)


### Bug Fixes

* **frontend/fields:** support retrieving initial values of fields which are nested ([2dbaaa8](https://gitlab.com/biomedit/portal/commit/2dbaaa82e72e27acf180e26f879b073dd7c4344c))

## [1.2.0-dev.0](https://gitlab.com/biomedit/portal/compare/1.1.0...1.2.0-dev.0) (2020-12-10)


### Features

* **frontend/users:** show user status in table under `Users & Groups` ([ca93594](https://gitlab.com/biomedit/portal/commit/ca935941722721e48560fcf5f02c862fd9086f28)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **user:** add new property `status` to user profile ([c4fa04a](https://gitlab.com/biomedit/portal/commit/c4fa04a1b675112e1da6e87c4e3ba37f8d56cb67)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **users:** allow changing user status on the frontend ([b16d34c](https://gitlab.com/biomedit/portal/commit/b16d34cfcc05c627c68ce856224a1384b178cdd8)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)


### Bug Fixes

* **frontend/fields:** support retrieving initial values of fields which are nested ([2dbaaa8](https://gitlab.com/biomedit/portal/commit/2dbaaa82e72e27acf180e26f879b073dd7c4344c))

## [1.1.0](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.10...1.1.0) (2020-12-08)

## [1.1.0-dev.10](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.9...1.1.0-dev.10) (2020-12-08)

## [1.1.0-dev.9](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.8...1.1.0-dev.9) (2020-12-08)


### Features

* allow "'" in names ([69704d7](https://gitlab.com/biomedit/portal/commit/69704d79486ab9c5949685552e6dc28f0c6f2b04))

## [1.1.0-dev.8](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.7...1.1.0-dev.8) (2020-12-07)


### Features

* **frontend/Home:** show christmas wishes during christmas ([f34a530](https://gitlab.com/biomedit/portal/commit/f34a5300c70710fee93d3d803bdfe4c16aec11cd))
* **frontend/UserMenu:** show christmas hat on user icon during christmas ([659bf33](https://gitlab.com/biomedit/portal/commit/659bf33db993fe927812e558b8521cbb3e3778ce))
* **frontend/UserMenu:** show snow background during christmas ([dc9d081](https://gitlab.com/biomedit/portal/commit/dc9d08162e36ad27aab2bf62513917dfae57bbf0))

## [1.1.0-dev.7](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.6...1.1.0-dev.7) (2020-12-07)


### Bug Fixes

* **backend/dtr_creation:** only create child tickets if we have a 'complete' ticket body ([f21a021](https://gitlab.com/biomedit/portal/commit/f21a0216609d1ead2bfac786607bee8b09c9f1ea))

## [1.1.0-dev.6](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.5...1.1.0-dev.6) (2020-12-03)

## [1.1.0-dev.5](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.4...1.1.0-dev.5) (2020-12-03)

## [1.1.0-dev.4](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.2...1.1.0-dev.4) (2020-12-03)


### Bug Fixes

* **frontend/ProjectForm:** make project name wider ([de50406](https://gitlab.com/biomedit/portal/commit/de50406a5b998a5845bb2ad1de236d48bb6b91ff))

## [1.1.0-dev.3](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.2...1.1.0-dev.3) (2020-12-02)

## [1.1.0-dev.2](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.1...1.1.0-dev.2) (2020-12-02)

## [1.1.0-dev.1](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.0...1.1.0-dev.1) (2020-12-02)


### Bug Fixes

* **backend/dtr_creation:** remove unused method ([aed9901](https://gitlab.com/biomedit/portal/commit/aed9901ef200b0f1e17d4e87179de1fefc8110c7))
* **backend/dtr_creation:** switch log level from info to debug ([d8d54ad](https://gitlab.com/biomedit/portal/commit/d8d54ada88a59fe1bc8e153c4477b44e3b6dfba8))
* **backend/dtr_creation:** wrong salutation for node managers ([0a1837a](https://gitlab.com/biomedit/portal/commit/0a1837acf6aec233bad8f17e5cdb5cec210821ac))

## [1.1.0-dev.0](https://gitlab.com/biomedit/portal/compare/1.0.0...1.1.0-dev.0) (2020-12-02)


### Features

* **frontend/nav:** show current portal version ([ecda227](https://gitlab.com/biomedit/portal/commit/ecda227091a603b657cafd3875d58abd3d03c6de))

## 1.0.0 (2020-12-02)

### Bug Fixes

- make ip_address_ranges consistent in /backend/projects/ ([f87bd93](https://gitlab.com/biomedit/portal/commit/f87bd93d99bd6eece96968f0209c1dedadcb8393))
