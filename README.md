[![pipeline status](https://gitlab.com/biomedit/portal/badges/dev/pipeline.svg)](https://gitlab.com/biomedit/portal/-/commits/master)
[![frontend unit test coverage](https://gitlab.com/biomedit/portal/badges/master/coverage.svg?job=frontend-unit-test&key_text=frontend+unit+test&key_width=110)](https://gitlab.com/biomedit/portal/-/commits/master)
[![frontend type coverage](https://gitlab.com/biomedit/portal/badges/master/coverage.svg?job=frontend-type-coverage&key_text=frontend+type&key_width=90)](https://gitlab.com/biomedit/portal/-/commits/master)
[![backend coverage](https://gitlab.com/biomedit/portal/badges/master/coverage.svg?job=backend-unit-test-coverage&key_text=backend)](https://gitlab.com/biomedit/portal/-/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![license](https://img.shields.io/badge/License-AGPLv3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

# BioMedIT Portal

## Releases

This project follows the [semantic versioning specification](https://semver.org/) for its [releases](https://gitlab.com/biomedit/portal/-/releases).

**NOTE:** Releases with a version containing `-dev` are not considered to be stable and should **NOT** be used in production.
Docker images of releases with a version containing `-dev` are automatically removed after 30 days.

## Development

### Requirements

- Python >=3.9
- NodeJS >=16
- Docker Engine >=17.04.0

### Installing a local instance of the portal

- Install [nodejs >= 16](https://nodejs.org).
- Install backend: see [instructions below](#Setting up the backend).
- Install frontend: see [`frontend/README.md`](frontend/README.md).
- Start the backend virtualenv: `source venv/bin/activate`.
- Start the backend: `cd web && python3 ./manage.py runserver localhost:8000`.
- Start the front-end: `cd frontend & npm run start`. If this fails, try to re-run `npm install`.
- The portal front-end should now be up on `localhost:3000`.
- Go to the backend admin `localhost:8000/backend/admin/`, where you can now add nodes and users.

### Setting up the backend

- Enter the the `web` directory: `cd web`.
- Copy `.config.json.sample` to `.config.json` and edit the settings to
  match the OIDC identity provider service configuration:
  - Replace `client_secret` in the `oidc: client_secret` entry with the keycloak
    client OIDC secret token. This is the token that is used to authenticate with
    the keycloak service.
  - If you intend to use the test keycloak instance, replace `auth` with
    `keycloak-test` in the `config_url`.
- Create and activate a python3 `venv`:
  `python3 -m venv ./venv && source venv/bin/activate`.
- Install requirements: `pip install -r requirements-dev.txt`.
- Install git hooks to automatically format code using black with `pre-commit install`
- Run database migrations `python3 ./manage.py migrate`.
- Create an admin user for the local backend: `python3 ./manage.py createsuperuser`.
  This is the user needed for `localhost:8000/backend/admin/`.
- Run the service `./manage.py runserver localhost:8000`.
- The backend service can be accessed at `localhost:8000/backend/`.
- The admin panel can be accessed at `localhost:8000/backend/admin/`.

### Running backend tests

- Install `tox`.
- Run `tox` to execute tests with the default configuration.
- Customize test runtime. For example:

  - Run tests from a specific file/directory in the Python 3.9 environment
    using 8 parallel processes.

    ```bash
    tox -e py38 -- -n 8 projects/tests/views
    ```

  - Filter by name to only run specific tests

    ```bash
    tox -e py38 -- -k test_get_unauthenticated
    # Name filter can also be combined with path
    tox -e py38 -- projects/tests/views -k test_get_unauthenticated
    ```

  - Run tests in a watch mode. Failing tests are re-run when a file in the project
    changes. A full test run is performed when all previously failing tests are fixed.

    ```bash
    tox -e py38 -- --looponfail
    ```

  Note: arguments passed after `--` are passed directly to `pytest` not `tox`.

### Setting-up the frontend

- Go to the `frontend` directory.
- Copy `.env.sample` to `.env` and edit the values to match your application.
- Run:

  ```bash
  npm install
  npm start
  ```

A new browser tab should automatically open at `localhost:3000`

Detailed instructions can be found in [frontend documentation](frontend/README.md).

### Starting the portal service

- Start the backend service

  ```bash
  cd web
  ./manage.py runserver localhost:8000
  ```

- Start the fronend service

  ```bash
  cd frontend
  npm start
  ```

### Generating the backend API in the frontend based on the backend

Every time you change something in the backend that involves changes in the API, you also need to re-generate the
backend API in the frontend.

- Re-generate the backend API in the frontend:

  ```bash
  cd frontend
  npm run generate-api
  ```

- Commit the changes inside `frontend/src/api/generated` in the same commit as your change in the backend

## Production

If you want to deploy to k8s: See [k8s/](k8s/README.md).

- Create `.env`, `frontend/.env`, `web/.config.json` (check `.env.sample`, `frontend/.env.sample`,
  `web/.config.json.sample`) files, which match your service configuration.
- Run:

  ```bash
  docker-compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.prod.yml build
  docker-compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.prod.yml up -d
  docker-compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.prod.yml exec -T web python manage.py migrate
  docker-compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.prod.yml exec -T web python user_init.py
  ```

- Access service at your domain name.

Note: the build command creates two images, one for `frontend` and one for `web`. Their registry prefixes are defined
by the `REGISTRY` environment variable (see `.env.sample`).

### Running without traefik / filebeat

If you have a system with pre deployed traefik, like
[here](https://framagit.org/oxyta.net/proxyta.net), then just omit `-f compose/traefik.yml` in all `docker-compose` invocations.

Similarly, if you dont need filebeat, simply omit the
`compose/filebeat.yml` file.

### Production-like local deployment

You can set up a local environment resembling production by using
`docker-compose.local-prod.yml` instead of `docker-compose.prod.yml`.
The `local-prod` deployment runs over plain HTTP and exposes the service
monitoring system at <http://localhost:8080>

```bash
docker-compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.local-prod.yml build
docker-compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.local-prod.yml up -d
docker-compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.local-prod.yml exec -T web python manage.py migrate
docker-compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.local-prod.yml exec -T web python user_init.py
```

## Usage

The service can be explored through a web browser or accessed directly
through the API; `biomedit_users.py` shows an example of a script that uses
the API. API endpoints are available at `/backend/` and a detailed API schema
at `/backend/schema/`.

**NOTE:** Most actions should be done via the frontend, e.g. adding projects,
managing user roles, adding project resources.

### Adding users

User accounts are automatically created, based on the OIDC account information,
on the first login.

It is also possible to create local user accounts (e.g. service or test
users) using the admin panel (`/backend/admin/auth/user/`).

**NOTE:** When creating a local user account, you must create a new account
with an empty profile first
(see the [issue](https://gitlab.com/biomedit/portal/-/issues/2)).
You can edit the profile information once the new account has been created.

### Permissions

Only authenticated users can see any data. Access to data (both read and write)
is further restricted with specific permission. Global permissions like
"staff" (can read and write all data) can be configured using the Django
admin panel (/backend/admin/).

Custom permissions (model/object-based) can be assigned to groups.
Users can be added to these groups to gain the corresponding group permissions.

Local permission (role) can be assigned per project.

- "Project Leader"
- "Permission Manager"
- "Data Manager"
- "User"

### Customize frontend text

To change text used in the frontend, copy the files in `frontend/public/locales` and mount them into the `frontend`
container in your `docker-compose.yml`.

Assuming you have a folder `locales` on the same level as your `docker-compose.yml` file, your `docker-compose.yml`
entry for the `frontend` container needs to look like this:

```yaml
  frontend:
    ...
    volumes:
      - ./locales:/usr/app/public/locales:ro
    ...
```

### Update supported browsers

When a visitor uses an unsupported browser, they see a message instead of the website, telling them to use a supported
browser along with which browsers are supported.

Should this change at some point (f. e. because of new JavaScript features which are used and not polyfilled), it's
necessary to update the supported browsers in the code.

To do this, follow those steps:

1. Adapt the `browserslist` for `production` in `package.json` to the browsers that should be newly supported.
   - You can use https://browserslist.dev/ to check if your `browserslist` is correct and which browsers are included.
     Just make sure instead of using the array on the website directly, join all array entries with a comma, followed
     by a space, instead.
2. Run `npm run supportedBrowsers`
3. Change `unsupportedBrowser` in `public/locales/en/common.json` to explain which browsers we now support.
