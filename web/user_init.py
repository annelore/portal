#!/usr/bin/env python

import os

import django
from django.contrib.auth import get_user_model

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "portal.settings")
django.setup()

User = get_user_model()

if not User.objects.filter(username="admin").exists():
    User.objects.create_superuser(
        "admin", os.environ.get("ADMIN_EMAIL", "").split(",")[0], "bRZYu33DwXc8"
    )
