from django.apps import AppConfig

APP_NAME = "identities"


class IdentitiesConfig(AppConfig):
    name = APP_NAME
