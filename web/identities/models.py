from django.contrib.auth.models import AbstractUser
from django.db import models
from guardian.mixins import GuardianUserMixin
from simple_history.models import HistoricalRecords


class User(AbstractUser, GuardianUserMixin):
    email = models.EmailField(
        blank=True, max_length=254, unique=True, verbose_name="email address"
    )
    history = HistoricalRecords()


def get_anonymous_user_instance(UserModel):
    return UserModel(username="AnonymousUser", email="anonymous_user@localhost")
