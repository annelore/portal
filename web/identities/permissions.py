from typing import Optional

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db.models import QuerySet
from guardian.shortcuts import get_objects_for_user
from rest_framework import permissions

User = get_user_model()

perm_group_manager = "auth.change_group"
perm_change_user = "identities.change_user"


def has_change_user_permission(user: User) -> bool:
    return user.has_perm(perm_change_user)


def get_group_manager_groups(user: User) -> QuerySet[Group]:
    return get_objects_for_user(user, perm_group_manager)


def has_group_manager_groups(user: User) -> bool:
    return get_group_manager_groups(user).exists()


def has_group_manager_permission(user: User, group: Optional[Group] = None) -> bool:
    return user.has_perm(perm_group_manager, group)


class IsGroupManagerRead(permissions.BasePermission):
    """Group Manager can modify a Group"""

    def has_permission(self, request, view):
        user = request.user
        return (
            user
            and user.is_authenticated
            and has_group_manager_groups(user)
            and request.method in ("GET", "HEAD")
        )

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view) and has_group_manager_permission(
            request.user, obj
        )


class IsGroupManagerWrite(permissions.BasePermission):
    """Group Manager can edit a Group"""

    def has_permission(self, request, view):
        user = request.user
        return (
            user
            and user.is_authenticated
            and has_group_manager_groups(user)
            and request.method in ("PUT", "PATCH")
        )

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view) and has_group_manager_permission(
            request.user, obj
        )
