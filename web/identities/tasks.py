from datetime import timedelta, datetime

import logging
import pytz

from celery import shared_task
from django.contrib.auth import get_user_model
from django.conf import settings
from django_drf_utils.email import sendmail

from identities.mail_templates.user_info import user_info_body
from projects.models import PeriodicTaskRun
from projects.utils.task import get_last_run

User = get_user_model()

logger = logging.getLogger(__name__)


# Remember to migrate `PeriodicTask` and `PeriodicTaskRun` when you move / rename this method!
@shared_task(bind=True)
def user_last_logged_in_notification(self):
    now = datetime.now(pytz.utc)
    # pylint: disable=no-member
    threshold_days = settings.CONFIG.notification.user_last_logged_in_days
    threshold_difference = timedelta(days=threshold_days)
    kwargs = {"is_active": True}

    last_run = get_last_run(self)
    if last_run:
        kwargs["last_login__gt"] = last_run - threshold_difference
    kwargs["last_login__lte"] = now - threshold_difference
    users = User.objects.filter(**kwargs)
    user: User
    for user in users:
        # pylint: disable=no-member
        sendmail(
            subject=f"User {user.username} has last logged in {threshold_days} days ago",
            body=user_info_body(user),
            recipients=(settings.CONFIG.notification.ticket_mail,),
            email_cfg=settings.CONFIG.email,
        )

    PeriodicTaskRun.objects.create(task=self.name, created_at=now)
    logger.info(
        "Successfully sent notification emails for %s users which have last logged in %s days ago "
        "since the last run on %s.",
        len(users),
        threshold_days,
        last_run,
    )
