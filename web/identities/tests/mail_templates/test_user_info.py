from datetime import datetime

import pytest
from django.test import TestCase

from identities.mail_templates.user_info import user_info_body
from projects.tests.views import UserFactory
from projects.tests.factories import FlagFactory
from projects.models import Project, ProjectUserRole, ProjectRole


class TestUserInfo(TestCase):
    def setUp(self):
        self.p1 = Project.objects.create(
            gid=1500001, name="Project 1", code="project_1"
        )
        self.p2 = Project.objects.create(
            gid=1500002, name="Project 2", code="project_2"
        )
        self.u1 = UserFactory.create_basic()
        self.u1.profile.affiliation = "staff@uzh.ch,member@uzh.ch"
        self.u1.profile.affiliation_id = "123456@uzh.ch,987654@ethz.ch"
        self.u1.last_login = datetime(2018, 12, 7, 3, 0, 0, 0)

        self.u2 = UserFactory.create_staff()
        ProjectUserRole.objects.create(
            project=self.p1,
            user=self.u1,
            role=ProjectRole.DM.value,
        )
        ProjectUserRole.objects.create(
            project=self.p1,
            user=self.u1,
            role=ProjectRole.PM.value,
        )
        ProjectUserRole.objects.create(
            project=self.p1,
            user=self.u2,
            role=ProjectRole.PM.value,
        )
        ProjectUserRole.objects.create(
            project=self.p2,
            user=self.u1,
            role=ProjectRole.USER.value,
        )
        ProjectUserRole.objects.create(
            project=self.p2,
            user=self.u1,
            role=ProjectRole.PM.value,
        )

    @pytest.mark.django_db
    def test_user_info_body(self):
        assert (
            user_info_body(self.u1) == f"User ID: {self.u1.id}\n"
            f"Display Name: {self.u1.profile.display_name}\n"
            f"Last Login: {self.u1.last_login}\n"
            f"Affiliation: {self.u1.profile.affiliation}\n"
            f"LinkedAffiliationUniqueID: {self.u1.profile.affiliation_id}\n"
            "Flags: -\n"
            "\n"
            "Projects and Roles:\n"
            f"{self.p1.code}: DM, PM\n"
            f"{self.p2.code}: USER, PM"
        )

    @pytest.mark.django_db
    def test_user_info_body_with_flags(self):
        self.flag = FlagFactory.create()
        self.u1.flags.set([self.flag])
        assert f"Flags: {self.flag.code}" in user_info_body(self.u1)
