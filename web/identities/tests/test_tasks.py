from unittest.mock import patch, Mock

from datetime import datetime
import pytest

import pytz
from django.contrib.auth import get_user_model
from django.core import mail

import identities
from identities.tasks import user_last_logged_in_notification
from projects.tests.views.test_pgpkey import TEST_CONFIG

User = get_user_model()


class TestTasks:
    # pylint: disable=no-self-use
    @pytest.fixture(autouse=True)
    def apply_test_settings(self, settings):
        settings.CONFIG = TEST_CONFIG
        return settings

    utc = pytz.timezone("UTC")

    @patch.object(identities.tasks, "datetime", Mock(wraps=datetime))
    @pytest.mark.django_db
    def test_user_last_logged_in_notification(self, apply_test_settings):
        subject = (
            "Portal: User {} has last logged in "
            f"{apply_test_settings.CONFIG.notification.user_last_logged_in_days} days ago"
        )

        day1 = datetime(2020, 2, 1, 3, 0, 0, 0, self.utc)
        day2 = datetime(2020, 2, 2, 3, 0, 0, 0, self.utc)
        day3 = datetime(2020, 2, 3, 3, 0, 0, 0, self.utc)

        # setup day 1
        day1_user1 = User.objects.create_user(
            username="day1_user1", email="day1_user1@example.org"
        )
        day1_user1.last_login = datetime(2000, 12, 3, 13, 0, 0, 0, self.utc)
        day1_user1.save()

        day1_user2 = User.objects.create_user(
            username="day1_user2", email="day1_user2@example.org"
        )
        day1_user2.last_login = datetime(2019, 12, 3, 3, 0, 0, 0, self.utc)
        day1_user2.save()

        day1_inactive = User.objects.create_user(
            username="day1_inactive", email="day1_inactive@example.org"
        )
        day1_inactive.last_login = day1_user1.last_login
        day1_inactive.is_active = False
        day1_inactive.save()

        # setup day 2
        day2_user1 = User.objects.create_user(
            username="day2_user1", email="day2_user1@example.org"
        )
        day2_user1.last_login = datetime(2019, 12, 3, 3, 0, 0, 1, self.utc)
        day2_user1.save()

        day2_user2 = User.objects.create_user(
            username="day2_user2", email="day2_user2@example.org"
        )
        day2_user2.last_login = datetime(2019, 12, 4, 3, 0, 0, 0, self.utc)
        day2_user2.save()

        # setup day 3
        day3_user1 = User.objects.create_user(
            username="day3_user1", email="day3_user1@example.org"
        )
        day3_user1.last_login = datetime(2019, 12, 5, 3, 0, 0, 1, self.utc)
        day3_user1.save()

        # test day 1
        identities.tasks.datetime.now.return_value = day1

        # pylint: disable=no-value-for-parameter
        user_last_logged_in_notification()

        assert len(mail.outbox) == 2, "Sent two emails"
        email1 = mail.outbox[0]
        assert email1.subject == subject.format(day1_user1.username)
        assert f"Display Name: {day1_user1.profile.display_name}" in email1.body

        email2 = mail.outbox[1]
        assert email2.subject == subject.format(day1_user2.username)

        # test day 2
        identities.tasks.datetime.now.return_value = day2

        # pylint: disable=no-value-for-parameter
        user_last_logged_in_notification()

        assert len(mail.outbox) == 4, "Sent two more emails"
        email3 = mail.outbox[2]
        assert email3.subject == subject.format(day2_user1.username)

        email4 = mail.outbox[3]
        assert email4.subject == subject.format(day2_user2.username)

        # test day 3
        identities.tasks.datetime.now.return_value = day3

        # pylint: disable=no-value-for-parameter
        user_last_logged_in_notification()

        assert len(mail.outbox) == 4, "No emails are sent"
