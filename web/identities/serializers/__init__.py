# pylint: disable=unused-import
from identities.serializers.permission import (
    AnyObjectSerializer,
    AnyObjectByPermissionSerializer,
    PermissionSerializer,
)
from identities.serializers.group import GroupSerializer
