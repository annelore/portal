import enum
from dataclasses import dataclass

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core import validators
from django.db import models
from django.dispatch import receiver
from django_drf_utils.models import CodeField, NameField, CreatedMixin, EnumField
from simple_history.models import HistoricalRecords

from . import permissions

User = get_user_model()

PERMISSION_NODE_ADMIN = "admin_node"
PERMISSION_DATA_PROVIDER_ADMIN = "admin_dataprovider"


class UserNamespace(models.Model):
    name = models.CharField(
        max_length=4,
        unique=True,
        validators=(
            validators.MinLengthValidator(2),
            validators.RegexValidator(
                regex=r"^[a-z]{2,4}$",
                message='Enter a valid value. Allowed characters: "a-z"',
            ),
        ),
    )

    def __str__(self):
        return f"Namespace ({self.name})"


class Flag(models.Model):
    code = CodeField()
    # Description could be empty
    description = models.TextField(blank=True)
    users = models.ManyToManyField(User, related_name="flags", blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.code)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    affiliation = models.CharField(max_length=512, blank=True)
    affiliation_id = models.CharField(max_length=512, blank=True)
    local_username = models.CharField(
        blank=True,
        null=True,
        max_length=16,
        validators=(
            validators.MaxLengthValidator(16),
            validators.MinLengthValidator(6),
            validators.RegexValidator(
                regex=r"^[a-z][a-z0-9_]{5,15}$",
                message=(
                    "Enter a valid value. "
                    'Allowed characters: "a-z", "0-9", "_". '
                    "Must start with a lowercase letter."
                ),
            ),
        ),
    )
    namespace = models.ForeignKey(
        UserNamespace, on_delete=models.SET_NULL, blank=True, null=True
    )
    uid = models.PositiveIntegerField(blank=True, null=True, unique=True)
    gid = models.PositiveIntegerField(blank=True, null=True, unique=True)

    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["namespace", "local_username"], name="unique username"
            )
        ]

    def __str__(self):
        return f"Profile ({self.user.username})"

    @property
    def display_id(self):
        return (
            f"ID: {self.user.username.split('@')[0]}"
            if "@" in self.user.username
            else None
        )

    @property
    def display_name(self):
        if self.user.first_name or self.user.last_name:
            name = " ".join(filter(bool, (self.user.first_name, self.user.last_name)))
        else:
            name = self.local_username or self.user.username
        display_id = self.display_id and f"({self.display_id})"
        display_email = self.user.email and f"({self.user.email})"
        return " ".join(filter(bool, (f"{name}", display_id, display_email)))

    @property
    def display_local_username(self):
        return "_".join(
            filter(None, (self.namespace and self.namespace.name, self.local_username))
        )


@receiver(models.signals.post_save, sender=User)
def create_or_update_user_profile(
    sender, instance, created, **kwargs
):  # pylint: disable=unused-argument
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class Node(CreatedMixin):
    STATUS_ONLINE = "ON"
    STATUS_OFFLINE = "OFF"
    STATUS_CHOICES = ((STATUS_ONLINE, "Online"), (STATUS_OFFLINE, "Offline"))
    code = CodeField()
    name = NameField()
    ticketing_system_email = models.EmailField(null=False, blank=False)
    node_status = models.CharField(max_length=4, choices=STATUS_CHOICES)

    def __str__(self) -> str:
        return f"{self.name} ({self.code})"

    class Meta:
        permissions = [
            (
                PERMISSION_NODE_ADMIN,
                "Can edit a node (except for Code)",
            ),
        ]


class ProjectRole(enum.Enum):
    USER: "User" = 1
    DM: "Data Manager" = 2
    PM: "Permission Manager" = 3
    PL: "Project Leader" = 4

    @property
    def label(self) -> str:
        # pylint: disable=no-member
        return type(self).__annotations__[self.name]

    @classmethod
    def choices(cls) -> list[tuple[int, str]]:
        # pylint: disable=no-member
        return [(member.value, cls.__annotations__[member.name]) for member in cls]


class Project(CreatedMixin):
    gid = models.PositiveIntegerField(null=True, unique=True)
    code = CodeField()
    name = NameField()
    destination = models.ForeignKey(
        Node, on_delete=models.SET_NULL, null=True, related_name="projects"
    )
    archived = models.BooleanField(default=False)
    history = HistoricalRecords()

    class Meta:
        ordering = ("-created",)

    def __str__(self) -> str:
        return f"{self.name} ({self.code})"

    def users_by_role(self, role: ProjectRole):
        return (
            project_user_role.user
            for project_user_role in self.users.filter(role=role.value)
        )


class ProjectUserRole(CreatedMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="users")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="+")
    role = models.SmallIntegerField(choices=ProjectRole.choices())

    class Meta:
        unique_together = [["project", "user", "role"]]


class ProjectUserRoleHistory(CreatedMixin):
    project = models.ForeignKey(
        Project,
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
        related_query_name="project_user_role_project",
    )
    project_str = models.CharField(max_length=32)
    changed_by = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="+",
        related_query_name="project_user_role_changed_by",
    )
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="+",
        related_query_name="project_user_role_user",
    )
    role = models.SmallIntegerField(choices=ProjectRole.choices())
    enabled = models.BooleanField()

    @property
    def display_changed_by(self):
        return " ".join(
            filter(
                None,
                (
                    self.changed_by.first_name,
                    self.changed_by.last_name,
                    f"({self.changed_by.username})",
                ),
            )
        )

    @property
    def display_user(self):
        return " ".join(
            filter(
                None,
                (self.user.first_name, self.user.last_name, f"({self.user.username})"),
            )
        )

    @property
    def display_permission(self):
        return " ".join(
            map(
                str.capitalize,
                ProjectRole(self.role).name.split("_"),
            )
        )


class ProjectIPAddresses(models.Model):
    project = models.ForeignKey(
        Project, related_name="ip_address_ranges", on_delete=models.CASCADE
    )
    ip_address = models.GenericIPAddressField()
    mask = models.PositiveSmallIntegerField(
        validators=(validators.MaxValueValidator(32),)
    )
    history = HistoricalRecords()

    class Meta:
        unique_together = ("project", "ip_address", "mask")
        ordering = ("project", "ip_address", "mask")

    def __str__(self):
        return f"{self.ip_address}/{self.mask}"


class CustomUrlFormField(forms.fields.URLField):
    default_validators = [
        validators.URLValidator(schemes=["http", "https", "ftp", "ftps", "ssh"])
    ]


class CustomUrlModelField(models.URLField):
    default_validators = [
        validators.URLValidator(schemes=["http", "https", "ftp", "ftps", "ssh"])
    ]

    def formfield(self, **kwargs):
        return super().formfield(form_class=CustomUrlFormField, **kwargs)


class Resource(models.Model):
    project = models.ForeignKey(
        Project, related_name="resources", on_delete=models.CASCADE
    )
    name = models.CharField(max_length=512)
    location = CustomUrlModelField()
    description = models.TextField(blank=True)
    contact = models.EmailField(null=False, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.location)


class PgpKeySignRequest(CreatedMixin):
    pgpkey_id = models.CharField(max_length=16)
    signing_pgpkey_id = models.CharField(max_length=16)

    class Meta:
        unique_together = [["pgpkey_id", "signing_pgpkey_id"]]


class PgpKeySignStatus(enum.Enum):
    SIGNED = "SIGNED"
    UNSIGNED = "UNSIGNED"
    PENDING = "PENDING"
    REVOKED = "REVOKED"


@dataclass
class PgpKey:
    """Foreign schema for keys from the keyserver"""

    pgpkey_id: str
    fingerprint: str
    key_name: str
    email: str
    sign_status: PgpKeySignStatus
    key_length: int


class Message(CreatedMixin):
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="messages"
    )
    tool = models.CharField(max_length=32)
    data = models.TextField()


class Feed(CreatedMixin):
    INFO = "INFO"
    WARNING = "WARN"
    LABELS = (
        (INFO, "Info"),
        (WARNING, "Warning"),
    )
    label = models.CharField(max_length=4, choices=LABELS)
    title = models.TextField(blank=True)
    message = models.TextField(blank=True)

    def __str__(self) -> str:
        return f"{self.label}: {self.title}"


class DataProvider(models.Model):
    code = CodeField()
    name = NameField()
    node = models.ForeignKey(Node, on_delete=models.SET_NULL, null=True, blank=False)
    users = models.ManyToManyField(User, related_name="data_provider", blank=True)
    enabled = models.BooleanField(default=True)

    class Meta:
        permissions = [
            (
                PERMISSION_DATA_PROVIDER_ADMIN,
                "Can edit a data provider (except for Code and Node)",
            ),
        ]

    def __str__(self) -> str:
        return f"{self.name} ({self.code})"


class DataTransfer(models.Model):
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="data_transfers"
    )
    max_packages = models.IntegerField(
        validators=(validators.MinValueValidator(-1),), default=1
    )

    INITIAL = "INITIAL"
    AUTHORIZED = "AUTHORIZED"
    UNAUTHORIZED = "UNAUTHORIZED"
    EXPIRED = "EXPIRED"
    LABELS = (
        (INITIAL, "Initial"),
        (AUTHORIZED, "Authorized"),
        (UNAUTHORIZED, "Unauthorized"),
        (EXPIRED, "Expired"),
    )

    status = EnumField(choices=LABELS, default=INITIAL)
    data_provider = models.ForeignKey(
        DataProvider, on_delete=models.CASCADE, related_name="data_transfers"
    )
    requestor = models.ForeignKey(User, on_delete=models.CASCADE)
    requestor_pgp_key_fp = models.CharField(
        max_length=40,
        blank=True,
        help_text="Requestor PGP Key Fingerprint",
        validators=(
            validators.RegexValidator(
                regex=r"^[0-9A-F]+$", message='Allowed characters: "A-F", "0-9"'
            ),
        ),
    )
    PRODUCTION = "PRODUCTION"
    TEST = "TEST"
    PURPOSE_LABELS = (
        (PRODUCTION, "Production"),
        (TEST, "Test"),
    )
    purpose = EnumField(PURPOSE_LABELS)
    history = HistoricalRecords()

    @property
    def requestor_name(self):
        return self.requestor.profile.display_name

    def __str__(self) -> str:
        return f"DTR {self.id} ({self.purpose}, {self.status})"


class DataPackage(models.Model):
    metadata_hash = models.CharField(max_length=256, unique=True)
    data_transfer = models.ForeignKey(
        DataTransfer, on_delete=models.CASCADE, related_name="packages"
    )
    file_name = models.TextField()
    metadata = models.TextField()
    PRODUCTION = "PRODUCTION"
    TEST = "TEST"
    PURPOSE_LABELS = (
        (PRODUCTION, "Production"),
        (TEST, "Test"),
    )
    purpose = EnumField(PURPOSE_LABELS)
    history = HistoricalRecords()


class DataPackageTrace(models.Model):
    """Trace of a data packages (timestamps of successful processings by lz's of that package)"""

    data_package = models.ForeignKey(
        DataPackage, on_delete=models.CASCADE, related_name="trace"
    )
    node = models.ForeignKey(Node, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    ENTER = "ENTER"
    PROCESSED = "PROCESSED"
    ERROR = "ERROR"
    STATUS_LABELS = ((ENTER, "Enter"), (PROCESSED, "Processed"), (ERROR, "Error"))
    status = EnumField(STATUS_LABELS)


# using attribute `last_run_at` in `PeriodicTask` from `django_celery_beat` is not reliable, see:
# https://dev.to/vergeev/django-celery-beat-how-to-get-the-last-time-a-periodictask-was-run-39k9
class PeriodicTaskRun(models.Model):
    task = models.CharField(max_length=512, verbose_name="Task Name")
    created_at = models.DateTimeField()

    class Meta:
        get_latest_by = "created_at"

    def __str__(self) -> str:
        return f"{self.task} ({self.created_at})"


QUICK_ACCESS_TILE_URL_SCHEMES = ["http", "https"]


class QuickAccessTileUrlFormField(forms.fields.URLField):
    default_validators = [
        validators.URLValidator(schemes=QUICK_ACCESS_TILE_URL_SCHEMES)
    ]


class QuickAccessTileUrlModelField(models.URLField):
    default_validators = [
        validators.URLValidator(schemes=QUICK_ACCESS_TILE_URL_SCHEMES)
    ]

    def formfield(self, **kwargs):
        return super().formfield(form_class=QuickAccessTileUrlFormField, **kwargs)


class QuickAccessTile(CreatedMixin):
    title = models.CharField(max_length=128)
    description = models.TextField(blank=True)
    url = QuickAccessTileUrlModelField()
    image = models.ImageField(upload_to="tiles")
    order = models.SmallIntegerField(
        blank=True,
        null=True,
        help_text="The order of this quick access tile (for sorting).",
    )
    flag = models.ForeignKey(Flag, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self) -> str:
        return f"{self.title}: {self.url}"


# pylint: disable=unused-argument
@receiver(models.signals.post_save, sender=Node)
def create_node_groups(sender, instance, created, *args, **kwargs):
    if created:
        node_viewer = Group.objects.create(name=f"{instance.name} Node Viewer")
        permissions.node.assign_node_viewer(node_viewer, instance)

        node_admin = Group.objects.create(name=f"{instance.name} Node Admin")
        permissions.node.assign_node_admin(node_admin, instance)

        node_manager = Group.objects.create(name=f"{instance.name} Node Manager")
        permissions.node.assign_node_manager(node_manager, node_viewer, node_admin)


# pylint: disable=unused-argument
@receiver(models.signals.post_save, sender=DataProvider)
def create_dp_groups(sender, instance, created, *args, **kwargs):
    if created:
        dp_ta = Group.objects.create(name=f"{instance.code} DP Technical Admin")
        permissions.data_provider.assign_dp_technical_admin(dp_ta, instance)

        dp_viewer = Group.objects.create(name=f"{instance.code} DP Viewer")
        permissions.data_provider.assign_dp_viewer(dp_viewer, instance)

        dp_manager = Group.objects.create(name=f"{instance.code} DP Manager")
        dp_coordinator = Group.objects.create(name=f"{instance.code} DP Coordinator")
        permissions.data_provider.assign_dp_manager(
            dp_manager, dp_ta, dp_viewer, dp_coordinator
        )
        permissions.data_provider.assign_dp_coordinator(
            dp_coordinator, dp_ta, dp_viewer, dp_manager
        )
