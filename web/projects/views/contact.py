import logging

from rest_framework import viewsets, permissions, mixins
from rest_framework.throttling import AnonRateThrottle

from projects.serializers import ContactSerializer

logger = logging.getLogger(__name__)


class ContactView(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = ContactSerializer
    permission_classes = (permissions.AllowAny,)
    throttle_classes = [AnonRateThrottle]
