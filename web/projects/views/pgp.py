from django.conf import settings
from django_drf_utils.exceptions import Unimplemented
from django_drf_utils.views.utils import DetailedResponse
from rest_framework import mixins, permissions, status, response, viewsets
from rest_framework.schemas.openapi import AutoSchema

from projects.serializers import PgpKeySignRequestSerializer, PgpKeySerializer
from projects.utils.gpg import fetch_pgp_keys
from projects.models import PgpKeySignStatus, PgpKeySignRequest


class PgpKeyViewSchema(AutoSchema):
    def get_filter_parameters(self, path, method):
        if method == "GET":
            return [
                {
                    "name": "email",
                    "in": "query",
                    "required": False,
                    "description": "Email address from which to return the pgp keys from",
                    "schema": {"type": "string"},
                },
                {
                    "name": "sign_status",
                    "in": "query",
                    "required": False,
                    "description": "Keys to return which match the signing status.",
                    "schema": {
                        "type": "string",
                        "enum": [s.name for s in PgpKeySignStatus],
                    },
                },
            ]

        return []

    def get_components(self, path, method):
        responses = super().get_components(path, method)
        response_model = responses.get("PgpKey")
        if response_model:
            response_model["properties"]["sign_status"]["enum"] = [
                s.name for s in PgpKeySignStatus
            ]
        return responses


# pylint: disable=too-many-ancestors
class PgpKeyViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny,)
    serializer_class = PgpKeySerializer
    queryset = []
    schema = PgpKeyViewSchema()

    def list(self, request, *_args, **_kwargs):
        """List PGP keys associated with an email address."""
        email = request.query_params.get("email", getattr(request.user, "email", None))
        if email is None:
            return response.Response([])
        try:
            sign_status_filter = request.query_params.get("sign_status", None)
            # pylint: disable=no-member
            keys = fetch_pgp_keys(
                email,
                settings.CONFIG.pgp.keyserver,
                settings.CONFIG.pgp.signing_pgpkey_id,
                sign_status_filter,
            )
        except RuntimeError as e:
            return DetailedResponse(
                format(e), status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        keys = join_with_sign_requests(list(keys))
        serializer = self.serializer_class(keys, many=True)
        return response.Response(serializer.data)


class PgpKeySignRequestViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny,)
    serializer_class = PgpKeySignRequestSerializer
    queryset = []

    def create(self, request, *args, **kwargs):  # pylint: disable=unused-argument
        """Request PGP key signature."""
        # pylint: disable=no-member
        if not (settings.CONFIG.pgp and settings.CONFIG.pgp.keyserver):
            return DetailedResponse(
                Unimplemented.default_detail, status_code=Unimplemented.status_code
            )
        return super().create(request)


def join_with_sign_requests(keys):
    key_ids = {key.pgpkey_id for key in keys}
    # pylint: disable=no-member
    signing_pgpkey_id = settings.CONFIG.pgp.signing_pgpkey_id
    sign_requests = {
        req.pgpkey_id
        for req in PgpKeySignRequest.objects.filter(
            pgpkey_id__in=key_ids, signing_pgpkey_id=signing_pgpkey_id
        )
    }
    for key in keys:
        if (
            key.sign_status is not PgpKeySignStatus.SIGNED
            and key.sign_status is not PgpKeySignStatus.REVOKED
        ) and key.pgpkey_id in sign_requests:
            key.sign_status = PgpKeySignStatus.PENDING
    return keys
