from django_drf_utils.views.utils import unique_check
from rest_framework import viewsets

from projects.filters import NodePermissionsFilter
from projects.models import Node
from projects.permissions import (
    IsAuthenticatedAndUniqueCheck,
    IsStaff,
    ObjectPermission,
    ReadOnly,
)
from projects.permissions.node import IsNodeAdmin
from projects.serializers import NodeSerializer


@unique_check((IsAuthenticatedAndUniqueCheck,))
class NodeViewSet(viewsets.ModelViewSet):  # pylint: disable=too-many-ancestors
    """Authenticated users can list, node admins can update
    their nodes and staff can create and update any node."""

    filter_backends = (NodePermissionsFilter,)
    search_fields = ("username",)

    serializer_class = NodeSerializer
    # pylint: disable=unsupported-binary-operation
    permission_classes = (IsStaff | IsNodeAdmin | ObjectPermission | ReadOnly,)
    queryset = Node.objects.all()
