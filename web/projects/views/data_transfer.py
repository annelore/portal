from django.db.models import Q
from rest_framework import viewsets

from projects.models import DataTransfer, ProjectRole
from projects.permissions import IsStaff, ReadOnly
from projects.permissions.node import get_node_permission_nodes
from projects.serializers import DataTransferSerializer


# pylint: disable=too-many-ancestors
class DataTransferViewSet(viewsets.ModelViewSet):
    # pylint: disable=unsupported-binary-operation
    permission_classes = (ReadOnly | IsStaff,)
    serializer_class = DataTransferSerializer
    lookup_field = "id"

    def get_queryset(self):
        requestor = self.request.user
        user_nodes = get_node_permission_nodes(requestor)
        if requestor.is_staff:
            return DataTransfer.objects.all()
        return DataTransfer.objects.filter(
            Q(
                project__users__user=requestor,
                project__users__role__in=(ProjectRole.DM.value, ProjectRole.PL.value),
            )
            | Q(data_provider__users=requestor)
            |
            # Node admins and node viewers
            (
                Q(project__destination__in=user_nodes)
                | Q(data_provider__node__in=user_nodes)
            )
        ).distinct()
