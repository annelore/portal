from django.db.models import Q
from rest_framework import mixins, viewsets
from rest_framework.filters import SearchFilter

from projects.models import Message, ProjectRole
from projects.permissions import ReadOnly
from projects.serializers import MessageSerializer


class MessageViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = MessageSerializer
    permission_classes = (ReadOnly,)
    filter_backends = (SearchFilter,)
    search_fields = ("tool", "project__code")

    def get_queryset(self):
        if self.request.user.is_staff:
            return Message.objects.all()
        projects = Message.objects.filter(
            (
                Q(project__users__user__username=self.request.user.username)
                & Q(project__users__role=ProjectRole.USER.value)
            )
            | (
                Q(project__users__user__username=self.request.user.username)
                & Q(project__users__role=ProjectRole.PM.value)
            )
        ).distinct()
        return projects
