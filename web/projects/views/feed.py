from rest_framework import viewsets

from projects.models import Feed
from projects.permissions import IsStaffOrReadOnly
from projects.serializers import FeedSerializer


# pylint: disable=too-many-ancestors
class FeedViewSet(viewsets.ModelViewSet):
    queryset = Feed.objects.all()
    permission_classes = (IsStaffOrReadOnly,)
    serializer_class = FeedSerializer
