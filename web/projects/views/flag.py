from django_drf_utils.views.utils import unique_check
from rest_framework import viewsets

from projects.models import Flag
from projects.permissions import IsStaffOrReadOnly, IsAuthenticatedAndUniqueCheck
from projects.serializers.user import FlagSerializer


@unique_check((IsAuthenticatedAndUniqueCheck,))
class FlagViewSet(viewsets.ModelViewSet):  # pylint: disable=too-many-ancestors

    serializer_class = FlagSerializer
    permission_classes = (IsStaffOrReadOnly,)
    queryset = Flag.objects.all()
