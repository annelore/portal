from django_drf_utils.views.utils import unique_check
from rest_framework import viewsets

from projects.filters import DataProviderPermissionsFilter
from projects.models import DataProvider
from projects.permissions import (
    IsAuthenticatedAndUniqueCheck,
    IsStaff,
    ObjectPermission,
    ReadOnly,
    is_staff,
)
from projects.permissions.data_provider import (
    IsDataProviderAdmin,
    has_any_data_provider_permissions,
)
from projects.serializers import DataProviderSerializer


@unique_check((IsAuthenticatedAndUniqueCheck,))
class DataProviderViewSet(viewsets.ModelViewSet):  # pylint: disable=too-many-ancestors
    """Authenticated users can list, data provider admins can update
    their data providers and staff can create and update any data provider."""

    filter_backends = (DataProviderPermissionsFilter,)
    search_fields = ("username",)

    serializer_class = DataProviderSerializer
    # pylint: disable=unsupported-binary-operation
    permission_classes = (IsStaff | IsDataProviderAdmin | ObjectPermission | ReadOnly,)

    def get_queryset(self):
        if self.request and (
            is_staff(self.request.user)
            or has_any_data_provider_permissions(self.request.user)
        ):
            return DataProvider.objects.all()
        return DataProvider.objects.filter(enabled=True)
