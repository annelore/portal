from rest_framework import mixins, permissions, viewsets

from projects.serializers import NotifySerializer


class NotifyViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = NotifySerializer
    permission_classes = (permissions.AllowAny,)
