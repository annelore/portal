from django.db.models import Q
from django_drf_utils.views.utils import DetailedResponse
from rest_framework import status, viewsets, mixins, response, permissions

from projects.filters import CaseInsensitiveOrderingFilter
from projects.models import DataPackage, ProjectRole
from projects.models import DataPackageTrace
from projects.permissions import is_staff, IsStaff
from projects.permissions.node import get_node_permission_nodes
from projects.serializers import (
    DataPackageSerializer,
    DataPackageCheckSerializer,
    DataPackageLogSerializer,
)


# pylint: disable=too-many-ancestors


class DataPackageCheckViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = DataPackage.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = DataPackageCheckSerializer

    def create(self, request, *args, **kwargs):
        r = super().create(request, *args, **kwargs)
        r.status_code = status.HTTP_200_OK
        return r


class DataPackageLogViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    # pylint: disable=unsupported-binary-operation
    permission_classes = (IsStaff | permissions.DjangoModelPermissions,)
    serializer_class = DataPackageLogSerializer
    queryset = DataPackageTrace.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = DataPackageLogSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        dpkg = serializer.save()
        node = serializer.validated_data["node"]
        trace_status = serializer.validated_data["status"]

        # Block logging from other nodes while pkg is being processed at a node:
        processing_node = get_processing_node_code(dpkg)
        if processing_node is not None and processing_node != node.code:
            return DetailedResponse(
                f"PKG is processed by {processing_node}. Logging blocked for other nodes",
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
            )

        # After PROCESSED, forbid further processing on the same node:
        if DataPackageTrace.objects.filter(
            node=node, data_package=dpkg, status=DataPackageTrace.PROCESSED
        ).exists():
            return DetailedResponse(
                f"PKG {dpkg.file_name} already processed on node {node}",
                status_code=status.HTTP_409_CONFLICT,
            )
        # Prevent duplicate ENTER events for the same node:
        if not (
            trace_status == DataPackageTrace.ENTER
            and DataPackageTrace.objects.filter(
                node=node, data_package=dpkg, status=trace_status
            ).exists()
        ):
            DataPackageTrace.objects.create(
                node=node, data_package=dpkg, status=trace_status
            )
            dpkg.refresh_from_db()

        return response.Response(
            DataPackageSerializer(dpkg).data, status.HTTP_201_CREATED
        )


def get_processing_node_code(dpkg):
    try:
        obj = DataPackageTrace.objects.filter(data_package=dpkg).latest("timestamp")
    except DataPackageTrace.DoesNotExist:
        return None
    if obj.status != DataPackageTrace.PROCESSED:
        return obj.node.code
    return None


def feed_serializer(serializer_class, data):
    serializer = serializer_class(data=data)
    serializer.is_valid(raise_exception=True)
    return serializer


class DataPackageViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = DataPackageSerializer
    filter_backends = (CaseInsensitiveOrderingFilter,)
    ordering_fields = ("file_name",)

    def get_queryset(self):
        if is_staff(self.request.user):
            return DataPackage.objects.all()
        users_pkgs = Q(
            data_transfer__project__users__user__username=self.request.user.username,
            data_transfer__project__users__role=ProjectRole.DM.value,
        )
        node_permission_pkgs = Q(
            data_transfer__project__destination__in=get_node_permission_nodes(
                self.request.user
            ).all()
        )
        data_provider_pkgs = Q(data_transfer__data_provider__users=self.request.user)
        return DataPackage.objects.filter(
            users_pkgs | node_permission_pkgs | data_provider_pkgs
        ).distinct()
