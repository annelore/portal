import re
from functools import lru_cache
from typing import Optional
from collections.abc import Iterable

import requests
from django.conf import settings
from rest_framework import status

from projects.models import PgpKey, PgpKeySignStatus


def fetch_pgp_keys(
    email: str,
    keyserver: str,
    signing_pgpkey_id: str,
    sign_status_filter: Optional[PgpKeySignStatus] = None,
) -> Iterable[PgpKey]:
    ks_response = requests.get(keyserver_query(keyserver, email))
    if ks_response.status_code == status.HTTP_404_NOT_FOUND:
        return []
    if not ks_response.ok:
        raise RuntimeError("Failed to fetch keys from the keyserver.")
    return filter(
        None,
        (
            parse_key(key_text, signing_pgpkey_id, sign_status_filter)
            for key_text in re.findall(
                r">pub<.*?</pre>", ks_response.text.replace("\n", " ")
            )
        ),
    )


def fetch_pgp_key_by_fingerprint(
    fingerprint: str,
    keyserver: str,
    signing_pgpkey_id: str,
    sign_status_filter: Optional[PgpKeySignStatus] = None,
) -> Optional[PgpKey]:
    keys = iter(
        fetch_pgp_keys(
            f"0x{fingerprint}", keyserver, signing_pgpkey_id, sign_status_filter
        )
    )
    try:
        return next(keys)
    except StopIteration:
        return None


def fetch_pgp_keys_by_id(
    key_id: str,
    keyserver: Optional[str] = None,
    signing_pgpkey_id: Optional[str] = None,
) -> Optional[PgpKey]:
    if keyserver is None:
        keyserver = settings.CONFIG.pgp.keyserver  # pylint: disable=no-member
    if signing_pgpkey_id is None:
        signing_pgpkey_id = (
            settings.CONFIG.pgp.signing_pgpkey_id  # pylint: disable=no-member
        )
    ks_response = requests.get(keyserver_query_fpr(keyserver, key_id))
    if not ks_response.ok:
        raise RuntimeError("Key has not been found on the keyserver.")
    return parse_key(ks_response.text.replace("\n", " "), signing_pgpkey_id.upper())


fetch_pgp_keys_by_id_cached = lru_cache()(fetch_pgp_keys_by_id)


def keyserver_query(keyserver: str, query: str) -> str:
    return f"{keyserver}/pks/lookup?search={query}&op=vindex&fingerprint=on"


def keyserver_query_fpr(keyserver: str, key_id: str) -> str:
    return keyserver_query(keyserver, f"0x{key_id.upper()}")


def parse_key(
    key, signing_pgpkey_id, sign_status_filter: Optional[PgpKeySignStatus] = None
) -> Optional[PgpKey]:
    r_key_info = (
        r".*?>pub</strong> *(?P<key_length>[0-9]+)"
        r".*?=0x(?P<pgpkey_id>\w+)"
        r".*?Fingerprint=(?P<fingerprint>[\w\s]+)"
        r'.*?"uid">(?P<key_name>.*?)<'
    )
    r_key_sig = r"sig3\s+<a\s+href\S+" + signing_pgpkey_id

    pub_key_end_index = key.index("<strong>uid</strong>")
    revok_index = key.find('sig <span class="warn">revok')
    if revok_index != -1:
        owner_revoked = revok_index < pub_key_end_index
        if not owner_revoked:
            # if it fails here, then the format of "key" is wrong
            revok_end_index = key.index("\r", revok_index)
            # if False, revoked by someone other than owner or signing authority
            signing_authority_revoked = (
                key.find(signing_pgpkey_id, revok_index, revok_end_index) != -1
            )
        else:
            signing_authority_revoked = False
    else:
        owner_revoked = False
        signing_authority_revoked = False

    if owner_revoked or signing_authority_revoked:
        sign_status = PgpKeySignStatus.REVOKED
    elif bool(re.search(r_key_sig, key)):
        sign_status = PgpKeySignStatus.SIGNED
    else:
        sign_status = PgpKeySignStatus.UNSIGNED

    if sign_status_filter and sign_status_filter != sign_status.name:
        return None

    m = re.match(r_key_info, key)
    return m and PgpKey(
        pgpkey_id=m.group("pgpkey_id"),
        fingerprint=m.group("fingerprint").strip().replace(" ", ""),
        key_name=m.group("key_name").replace("&lt;", "<").replace("&gt;", ">"),
        email=m.group("key_name").split("&lt;", 1)[-1].split("&gt;", 1)[0],
        sign_status=sign_status,
        key_length=int(m.group("key_length")),
    )
