from typing import Optional

from django.contrib.auth import get_user_model
from django.db.models.functions import Lower
from guardian.shortcuts import get_objects_for_user
from rest_framework.filters import BaseFilterBackend, OrderingFilter

from projects.models import ProjectRole, ProjectUserRole
from projects.permissions.data_provider import (
    get_data_provider_permission_data_providers,
)
from projects.permissions.node import get_node_viewer_nodes, get_node_admin_nodes

User = get_user_model()


class NodePermissionsFilter(BaseFilterBackend):
    """Filters out all nodes the user has permission to"""

    param = "username"

    def filter_queryset(self, request, queryset, view):
        username = request.query_params.get(self.param)
        if username:
            user = User.objects.get(username=username)
            return get_node_viewer_nodes(user) | get_node_admin_nodes(user)
        return queryset.all()

    def get_schema_operation_parameters(self, view):
        return (
            {
                "name": self.param,
                "required": False,
                "in": "query",
                "description": "Only returns nodes which the user has permission to view or edit.",
                "schema": {"type": "string"},
            },
        )


class DataProviderPermissionsFilter(BaseFilterBackend):
    """Filters out all data providers the user has permission to"""

    param = "username"

    def filter_queryset(self, request, queryset, view):
        username = request.query_params.get(self.param)
        if username:
            return get_data_provider_permission_data_providers(
                User.objects.get(username=username)
            )
        return queryset.all()

    def get_schema_operation_parameters(self, view):
        return (
            {
                "name": self.param,
                "required": False,
                "in": "query",
                "description": "Only returns data providers which the user has permission to view or edit.",
                "schema": {"type": "string"},
            },
        )


class ProjectsPermissionFilter(BaseFilterBackend):
    """Filter projects by user permissions."""

    def filter_queryset(self, request, queryset, view):
        f = {}
        username = request.query_params.get("username")
        if username:
            f["users__user__username"] = username
            f["users__role"] = ProjectRole.USER.value
        manager = request.query_params.get("manager")
        if manager:
            f["users__user__username"] = manager
            f["users__role__in"] = (
                ProjectRole.PM.value,
                ProjectRole.PL.value,
            )
        return queryset.filter(**f).distinct()

    def get_schema_operation_parameters(self, view):
        params = []
        for name, description in (
            ("username", "Username for the User Role"),
            ("manager", "Username for the PL/PM Role"),
        ):
            params.append(
                {
                    "name": name,
                    "required": False,
                    "in": "query",
                    "description": description,
                    "schema": {"type": "string"},
                }
            )
        return params


class UserActiveFilter(BaseFilterBackend):
    """Filter projects depending on the user being active (`is_active` is `True`)."""

    param = "include_inactive"

    def filter_queryset(self, request, queryset, view):
        if request.method != "GET" or request.query_params.get(self.param):
            # pylint: disable=no-member
            return queryset.all()
        # exclude inactive users by default
        return queryset.exclude(is_active=False)

    def get_schema_operation_parameters(self, view):
        return (
            {
                "name": self.param,
                "required": False,
                "in": "query",
                "description": "Includes users which are not active (`is_active` is `False`)",
                "schema": {"type": "boolean"},
            },
        )


class UserProjectRoleFilter(BaseFilterBackend):
    """
    Return users which have the specified role in the specified project.
    If no role is specified, all users with any role in the specified project are returned.
    """

    projectIdParam = "project_id"
    roleParam = "role"

    def filter_queryset(self, request, queryset, view):
        project_id: Optional[str] = request.query_params.get(self.projectIdParam)
        role: Optional[str] = request.query_params.get(self.roleParam)
        kwargs = {}
        if role:
            kwargs["role"] = ProjectRole[role].value
        if project_id:
            kwargs["project__id"] = project_id
        if kwargs:
            return queryset.filter(
                id__in=(pur.user.id for pur in ProjectUserRole.objects.filter(**kwargs))
            )
        return queryset.all()

    def get_schema_operation_parameters(self, view):
        return (
            {
                "name": self.projectIdParam,
                "required": False,
                "in": "query",
                "description": "Only includes users which have at least one role in this project.",
                "schema": {"type": "integer"},
            },
            {
                "name": self.roleParam,
                "required": False,
                "in": "query",
                "description": "Only includes users which have at least this role in any project.",
                "schema": {"type": "string", "enum": [p.name for p in ProjectRole]},
            },
        )


class CaseInsensitiveOrderingFilter(OrderingFilter):
    def filter_queryset(self, request, queryset, view):
        ordering = self.get_ordering(request, queryset, view)
        if ordering:
            return queryset.order_by(
                *[
                    Lower(f[1:]).desc() if f.startswith("-") else Lower(f)
                    for f in ordering
                ]
            )
        return queryset


class ObjectPermissionsFilter(BaseFilterBackend):
    """Filter objects which user has object-level view permission"""

    accept_global_permissions = False
    superuser_show_all = True

    def filter_queryset(self, request, queryset, view):
        app_label = queryset.model._meta.app_label
        model_name = queryset.model._meta.model_name
        obj = get_objects_for_user(
            request.user,
            f"{app_label}.view_{model_name}",
            queryset,
            accept_global_perms=self.accept_global_permissions,
            with_superuser=self.superuser_show_all,
        )
        return obj
