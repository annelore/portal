from django.contrib.auth import get_user_model

User = get_user_model()


def affiliation_change_body(
    user: User, old_affiliation: str, new_affiliation: str
) -> str:
    def display_affiliation(affiliation: str):
        return ", ".join(term.strip() for term in affiliation.split(","))

    body = (f"Display name: {user.profile.display_name}",)
    body += (f"Old affiliation: {display_affiliation(old_affiliation)}",)
    body += (f"New affiliation: {display_affiliation(new_affiliation)}",)
    return "\n".join(body)
