from projects.models import ProjectRole, User


PermissionChanges = set[tuple[User, ProjectRole]]


def update_permissions_notification(
    project_name: str,
    requestor_name: str,
    created_permissions: PermissionChanges,
    deleted_permissions: PermissionChanges,
) -> str:
    def pretty_print_permission_changes(permission_changes: PermissionChanges):
        return "\n".join(
            [
                (
                    f"User: {user.profile.display_name}\t "
                    f"Permission: {permission.label}"
                )
                for (user, permission) in permission_changes
            ]
        )

    message = [
        f"Project name: {project_name}",
        f"Changed by: {requestor_name}\n",
    ]

    if created_permissions:
        message.append(
            "Added permissions:\n"
            f"{pretty_print_permission_changes(created_permissions)}"
        )

    if created_permissions and deleted_permissions:
        message.append("\n")

    if deleted_permissions:
        message.append(
            "Deleted permissions:\n"
            f"{pretty_print_permission_changes(deleted_permissions)}"
        )
    return "\n".join(message)
