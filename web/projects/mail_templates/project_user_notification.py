from collections.abc import Collection

from projects.models import User, Profile


def project_user_notification(
    project_name: str, new_users: Collection[User], del_users: Collection[User]
) -> str:
    body = (f"Project name: {project_name}",)

    def display_profile(profile: Profile) -> str:
        return f"{profile.display_name}"

    if new_users:
        body += (
            f"Added users: {', '.join(display_profile(u.profile) for u in new_users)}",
        )
    if del_users:
        body += (
            f"Removed users: {', '.join(display_profile(u.profile) for u in del_users)}",
        )
    return "\n".join(body)
