def gpg_key_sign_request_notification(
    key_name: str,
    key_fingerprint: str,
    keyserver_query_url: str,
    signing_pgpkey_id: str,
) -> str:
    return (
        f"PGP key signature request for {key_name}\n"
        f"Key fingerprint {key_fingerprint}\n"
        f"{keyserver_query_url}\n\n"
        f"The key specified above should be signed with "
        f"{signing_pgpkey_id}"
    )
