# Generated by Django 3.1.7 on 2021-05-07 09:23

from django.db import migrations


class Migration(migrations.Migration):

    replaces = [
        ("projects", "0008_add_admin_node_permission"),
        ("projects", "0009_remove_node_users"),
        ("projects", "0010_rename_admin_node_description"),
    ]

    dependencies = [
        ("projects", "0007_periodictaskrun"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="node",
            options={
                "permissions": [
                    (
                        "admin_node",
                        "Can edit a node (without Node ID and `users` associated to it)",
                    )
                ]
            },
        ),
        migrations.RemoveField(
            model_name="node",
            name="users",
        ),
        migrations.AlterModelOptions(
            name="node",
            options={
                "permissions": [("admin_node", "Can edit a node (without Node ID)")]
            },
        ),
    ]
