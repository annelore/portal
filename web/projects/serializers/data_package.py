import hashlib
import json
import logging

from django.conf import settings
from django.db import transaction
from django.utils import dateparse
from django_drf_utils.serializers.utils import DetailedValidationError
from rest_framework import serializers

from projects.models import (
    DataPackage,
    DataPackageTrace,
    DataTransfer,
    ProjectRole,
    Node,
    PgpKeySignStatus,
)
from projects.utils.gpg import fetch_pgp_keys_by_id_cached, fetch_pgp_key_by_fingerprint

logger = logging.getLogger(__name__)


class NodeField(serializers.Field):
    """Field for deserializing a node code into a Node object"""

    def to_representation(self, value):
        return value.code

    def to_internal_value(self, data):
        try:
            return Node.objects.get(code=data)
        except ValueError:
            raise DetailedValidationError(
                f"Invalid node: {data}", field=self.field_name
            ) from None


class DataPackageCheckSerializer(serializers.ModelSerializer):
    """Check PKG integrity (read-only POST)"""

    project_code = serializers.CharField(
        source="data_transfer.project.code", read_only=True
    )

    class Meta:
        model = DataPackage
        read_only_fields = (
            "metadata_hash",
            "data_transfer",
            "purpose",
            "project_code",
        )
        fields = read_only_fields + ("file_name", "metadata")

    def create(self, validated_data):
        _, data = check_dpkg(**validated_data)
        return DataPackage(**data)


def check_dpkg(metadata, file_name, read_only: bool = True):
    """Check if DataPackage is valid.

    validated_data: request data
    read_only: If True, the check will always fail for status != AUTHORIZED.
        If False, the check also succeeds if status is EXPIRED and
        DataPackage with the given metadata_hash already exists.
        This flag is necessary to differentiate between the following two cases:
          1. `read_only` is `True`: this function is called from sett through the `check` endpoint
              when attempting to transfer a data package. sett only checks if the package is valid
              and doesn't perform any modifying operations (is `read_only`).
          2. `read_only` is `False`: this function is called implicitly in the
             `data-package/log` endpoint to check if the data package data is valid before creating
             a data package, which is a modifying operation (is NOT `read_only`).
    """
    metadata_str = metadata
    try:
        metadata = json.loads(metadata)
    except json.decoder.JSONDecodeError as e:
        raise DetailedValidationError(f"Invalid metadata: {e}") from e
    try:
        metadata_hash = hash_metadata(metadata)
        transfer_id = metadata["transfer_id"]
        purpose = metadata["purpose"]
    except KeyError as e:
        raise DetailedValidationError(f"metadata is missing field '{e}'") from e
    try:
        transfer = DataTransfer.objects.get(id=transfer_id)
    except (ValueError, DataTransfer.DoesNotExist):
        raise DetailedValidationError(f"Invalid transfer: {transfer_id}") from None
    dpkg_exists = DataPackage.objects.filter(metadata_hash=metadata_hash).exists()
    if transfer.status != DataTransfer.AUTHORIZED:
        if read_only or not (transfer.status == DataTransfer.EXPIRED and dpkg_exists):
            raise DetailedValidationError(
                "Cannot create data package for transfer with "
                f"ID '{transfer_id}' in state '{transfer.status}'"
            )
    if read_only and dpkg_exists:
        raise DetailedValidationError(
            "Data package already exists for transfer with "
            f"ID '{transfer_id}' with the same metadata '{metadata_str}'"
        )
    if not transfer.data_provider.enabled:
        raise DetailedValidationError(
            f"Data Provider: {transfer.data_provider.code} is currently "
            f"not able to send data for transfer with ID '{transfer_id}'"
        )
    if transfer.project.destination.node_status != Node.STATUS_ONLINE:
        raise DetailedValidationError(
            f"Node: {transfer.project.destination.name} is not online"
        )
    check_user_permissions(transfer, metadata["recipients"])
    if purpose != transfer.purpose:
        raise DetailedValidationError(
            f"Got a '{purpose}' package for a '{transfer.purpose}' "
            f"data transfer with ID '{transfer_id}'"
        )

    return (
        transfer,
        dict(
            metadata_hash=metadata_hash,
            purpose=purpose,
            data_transfer=transfer,
            file_name=file_name,
            metadata=metadata_str,
        ),
    )


def check_user_permissions(transfer, user_fingerprints):
    project = transfer.project
    email_addresses = {
        p.user.email for p in project.users.filter(role=ProjectRole.DM.value)
    }
    # pylint: disable=no-member
    keys = [
        fetch_pgp_key_by_fingerprint(
            fpr,
            settings.CONFIG.pgp.keyserver,
            settings.CONFIG.pgp.signing_pgpkey_id,
        )
        for fpr in user_fingerprints
    ]
    keys = [key for key in keys if key is not None]
    unsigned_fingerprints = {
        key.fingerprint
        for key in keys
        if key.sign_status is not PgpKeySignStatus.SIGNED
    }
    unauthorized_fingerprints = {
        key.fingerprint for key in keys if key.email not in email_addresses
    }
    missing_fingerprints = set(user_fingerprints) - {key.fingerprint for key in keys}
    violations = (
        [(fpr, "unsigned") for fpr in unsigned_fingerprints]
        + [
            (fpr, f"not a data manager of project {transfer.project}")
            for fpr in unauthorized_fingerprints
        ]
        + [(fpr, "Not found on keyserver") for fpr in missing_fingerprints]
    )
    if violations:
        raise DetailedValidationError(
            "The following keys are invalid:"
            + ", ".join(f"{fpr} ({reason})" for fpr, reason in violations)
        )


class DataPackageTraceSerializer(serializers.ModelSerializer):
    node = NodeField()

    class Meta:
        model = DataPackageTrace
        fields = ("node", "timestamp", "status")


class DataPackageSerializer(DataPackageCheckSerializer):
    """Serializer to list data packages"""

    destination_node = serializers.CharField(
        source="data_transfer.project.destination.code", read_only=True
    )
    trace = DataPackageTraceSerializer(read_only=True, many=True)
    data_provider = serializers.CharField(
        source="data_transfer.data_provider.code", read_only=True
    )
    sender_pgp_key_info = serializers.SerializerMethodField()

    class Meta(DataPackageCheckSerializer.Meta):
        read_only_fields = DataPackageCheckSerializer.Meta.read_only_fields + (
            "id",
            "trace",
            "destination_node",
            "data_provider",
            "sender_pgp_key_info",
        )
        fields = DataPackageCheckSerializer.Meta.fields + (
            "id",
            "trace",
            "destination_node",
            "data_provider",
            "sender_pgp_key_info",
        )

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["trace"] = sorted(
            response["trace"], key=lambda x: dateparse.parse_datetime(x["timestamp"])
        )
        return response

    def validate(self, attrs):
        _, validated_data = check_dpkg(**attrs)
        return validated_data

    def get_sender_pgp_key_info(  # pylint: disable=no-self-use
        self, obj: DataPackage
    ) -> str:
        try:
            sender = json.loads(obj.metadata).get("sender")
            if sender:
                try:
                    key = fetch_pgp_keys_by_id_cached(sender)
                    if key:
                        return f"{key.key_name} {key.fingerprint}"
                except RuntimeError as e:
                    return str(e)
                return "Sender PGP key not found"
            logger.debug("Sender not found for DataPackage pk=%s", obj.pk)
        except json.decoder.JSONDecodeError:
            logger.debug("Failed metadata parsing for DataPackage pk=%s", obj.pk)
        return "Sender not found"


class DataPackageLogSerializer(DataPackageTraceSerializer):
    """Serializer for logging data packages reported by a landing zone (involves metadata checks)"""

    file_name = serializers.CharField()
    metadata = serializers.CharField()

    class Meta:
        model = DataPackageTrace
        fields = ("node", "status", "file_name", "metadata")

    @transaction.atomic
    def create(self, validated_data):
        transfer, data = check_dpkg(
            validated_data["metadata"], validated_data["file_name"], read_only=False
        )
        matadata_hash = data.pop("metadata_hash")
        dpkg, created = DataPackage.objects.get_or_create(
            metadata_hash=matadata_hash, defaults=data
        )
        if (
            created
            and transfer.max_packages > 0
            and transfer.packages.count() >= transfer.max_packages
        ):
            transfer.status = DataTransfer.EXPIRED
            transfer.save()
        return dpkg


def hash_metadata(metadata: dict) -> str:
    return hashlib.sha3_256(
        repr(
            (
                metadata["sender"],
                metadata["recipients"],
                metadata["transfer_id"],
                metadata["timestamp"],
                metadata["checksum"],
            )
        ).encode()
    ).hexdigest()
