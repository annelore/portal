from django.conf import settings
from django_drf_utils.email import sendmail
from django_drf_utils.serializers.utils import get_request_username
from rest_framework import serializers

from projects.mail_templates.contact_form import contact_form

UNAUTHENTICATED_MISSING_USER_INFO_ERROR = (
    "'first_name', 'last_name' and 'email' are required."
)

CONTACT_FORM_SUBJECT_PREFIX = "Contact Form - "


class ContactSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    first_name = serializers.CharField(
        required=False,
    )
    last_name = serializers.CharField(
        required=False,
    )
    email = serializers.EmailField(
        required=False,
    )
    subject = serializers.CharField()
    message = serializers.CharField()
    send_copy = serializers.BooleanField()

    def validate(self, attrs):  # pylint: disable=no-self-use
        data = super().validate(attrs)
        request_user = get_request_username(self)
        if request_user and request_user.is_authenticated:
            data["first_name"] = request_user.first_name
            data["last_name"] = request_user.last_name
            data["email"] = request_user.email
        elif any(key not in data for key in ("first_name", "last_name", "email")):
            raise serializers.ValidationError(UNAUTHENTICATED_MISSING_USER_INFO_ERROR)
        return data

    def create(self, validated_data):
        """
        :raises: ServiceUnavailable
        """
        sender_email_address = validated_data["email"]
        kwargs = {
            "subject": f'{CONTACT_FORM_SUBJECT_PREFIX}{validated_data["subject"]}',
            "body": contact_form(
                validated_data["first_name"],
                validated_data["last_name"],
                sender_email_address,
                validated_data["message"],
            ),
            # pylint: disable=no-member
            "recipients": (settings.CONFIG.notification.contact_form_recipient,),
            "email_cfg": settings.CONFIG.email,
            "reply_to": (sender_email_address,),
        }
        if "send_copy" in validated_data:
            kwargs["cc"] = (sender_email_address,)
        sendmail(**kwargs)
        return validated_data
