import logging

from django.conf import settings
from django.db import transaction
from django_drf_utils.serializers.utils import (
    DetailedValidationError,
    get_request_username,
)
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from projects.models import (
    DataProvider,
    DataTransfer,
    PgpKeySignStatus,
    DataPackage,
    ProjectUserRole,
    ProjectRole,
)
from projects.permissions import is_staff
from projects.serializers.data_package import DataPackageSerializer
from projects.utils.gpg import fetch_pgp_key_by_fingerprint

CREATE_MESSAGE_ARCHIVED_PROJECT = "Cannot create data transfer for an archived project."
UPDATE_MESSAGE_ARCHIVED_PROJECT = "Cannot update data transfer of an archived project."

logger = logging.getLogger(__name__)


class DataTransferSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataTransfer
        read_only_fields = (
            "id",
            "packages",
            "requestor_name",
            "requestor_display_id",
            "requestor_first_name",
            "requestor_last_name",
            "requestor_email",
            "project_name",
            "project_archived",
        )
        fields = read_only_fields + (
            "project",
            "max_packages",
            "status",
            "data_provider",
            "requestor",
            "purpose",
            "requestor_pgp_key_fp",
        )
        lookup_field = "id"
        extra_kwargs = {"requestor": {"required": False}}

    # Sets `required: true` in the OpenAPI schema
    max_packages = serializers.IntegerField(required=True)
    data_provider = serializers.SlugRelatedField(
        queryset=DataProvider.objects.all(),
        read_only=False,
        slug_field="code",
        required=True,
    )

    packages = DataPackageSerializer(
        many=True,
        read_only=True,
    )

    requestor_pgp_key_fp = serializers.CharField(
        required=True,
        help_text=DataTransfer.requestor_pgp_key_fp.field.help_text,  # pylint: disable=no-member
    )

    requestor_display_id = serializers.CharField(
        source="requestor.profile.display_id", read_only=True
    )
    requestor_first_name = serializers.CharField(
        source="requestor.first_name", read_only=True
    )
    requestor_last_name = serializers.CharField(
        source="requestor.last_name", read_only=True
    )
    requestor_email = serializers.CharField(source="requestor.email", read_only=True)

    project_name = serializers.CharField(source="project.name", read_only=True)

    project_archived = serializers.BooleanField(
        source="project.archived", read_only=True
    )

    @transaction.atomic
    def create(self, validated_data):
        if validated_data["project"].archived:
            raise ValidationError(CREATE_MESSAGE_ARCHIVED_PROJECT)
        if validated_data.get("status", DataTransfer.INITIAL) != DataTransfer.INITIAL:
            raise DetailedValidationError(
                "Cannot create new transfer in state different than 'initial'",
                field="status",
            )
        request_user = get_request_username(self)
        requestor = validated_data.setdefault("requestor", request_user)
        if requestor != request_user and not is_staff(request_user):
            raise DetailedValidationError(
                "Not allowed to specify requestor", field="requestor"
            )
        if not ProjectUserRole.objects.filter(
            project=validated_data["project"], user=requestor, role=ProjectRole.DM.value
        ).exists():
            raise DetailedValidationError(
                "Requestor is not a data manager of the project", field="requestor"
            )
        fingerprint = validated_data.get("requestor_pgp_key_fp", None)
        if not (fingerprint and self.verify_pgp_key_signed(fingerprint)):
            raise DetailedValidationError(
                "Cannot create new transfer with a key that is not signed",
                field="requestor_pgp_key_fp",
            )
        return super().create(validated_data)

    @transaction.atomic
    def update(self, instance, validated_data):
        if instance.project.archived:
            raise ValidationError(UPDATE_MESSAGE_ARCHIVED_PROJECT)
        if (
            DataPackage.objects.filter(data_transfer__id=instance.id).exists()
            and validated_data.get("purpose", instance.purpose) != instance.purpose
        ):
            raise DetailedValidationError(
                "Cannot update data transfer's purpose once a data package was created",
                field="purpose",
            )
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["packages"] = sorted(
            response["packages"], key=lambda x: x["file_name"]
        )
        return response

    @staticmethod
    def verify_pgp_key_signed(fingerprint: str) -> bool:
        # pylint: disable=no-member
        key = fetch_pgp_key_by_fingerprint(
            fingerprint,
            settings.CONFIG.pgp.keyserver,
            settings.CONFIG.pgp.signing_pgpkey_id,
        )
        return key and key.sign_status == PgpKeySignStatus.SIGNED
