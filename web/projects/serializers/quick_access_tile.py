from rest_framework import serializers

from projects.models import QuickAccessTile


class QuickAccessTileSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuickAccessTile
        read_only_fields = ("id",)
        fields = read_only_fields + ("title", "description", "url", "image", "order")
