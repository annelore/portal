from rest_framework import serializers

from projects.models import UserNamespace


class UserNamespaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserNamespace
        fields = ("id", "name")
