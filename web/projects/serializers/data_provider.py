from rest_framework import serializers

from projects.models import DataProvider, Node


class DataProviderSerializer(serializers.ModelSerializer):
    node = serializers.SlugRelatedField(
        slug_field="code",
        queryset=Node.objects.all(),
    )

    class Meta:
        model = DataProvider
        read_only_fields = ("id",)
        fields = read_only_fields + (
            "code",
            "name",
            "enabled",
            "node",
            "users",
        )
