import logging

from django.conf import settings
from django.contrib.auth.models import Group
from django.core import validators
from django.db import models, transaction
from django_drf_utils.serializers.utils import get_request_username
from django_drf_utils.serializers.validators import RaiseConflictUniqueTogetherValidator
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed

from identities.permissions import has_group_manager_groups
from projects.models import (
    User,
    Profile,
    UserNamespace,
    Flag,
    ProjectUserRole,
    ProjectRole,
    Project,
)
from projects.permissions.data_provider import (
    managed_data_providers,
    is_data_provider,
    has_data_provider_admin_data_providers,
    has_data_provider_viewer_data_providers,
    get_data_provider_admin_data_providers,
)
from projects.permissions.node import has_node_viewer_nodes, get_node_admin_nodes
from projects.permissions.project import (
    is_manager,
    is_data_manager,
    has_projects,
    has_node_admin_nodes,
    is_project_leader,
)
from projects.signals.project import (
    update_permissions_signal,
    DictUserRole,
)

logger = logging.getLogger(__name__)


class FlagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flag
        read_only_fields = ("id",)
        fields = read_only_fields + ("code", "description", "users")


class ProfileSerializer(serializers.ModelSerializer):
    namespace = serializers.SlugRelatedField(slug_field="name", read_only=True)
    local_username = serializers.CharField(
        required=True,
        allow_blank=False,
        validators=Profile.local_username.field.validators,  # pylint: disable=no-member
    )
    # pylint: disable=no-member
    uid = serializers.IntegerField(
        required=False,
        validators=(
            validators.MinValueValidator(settings.CONFIG.user.uid.min),
            validators.MaxValueValidator(settings.CONFIG.user.uid.max),
        ),
    )
    # pylint: disable=no-member
    gid = serializers.IntegerField(
        required=False,
        validators=(
            validators.MinValueValidator(settings.CONFIG.user.gid.min),
            validators.MaxValueValidator(settings.CONFIG.user.gid.max),
        ),
    )

    class Meta:
        model = Profile
        read_only_fields = ("affiliation", "affiliation_id", "uid", "gid")
        fields = read_only_fields + (
            "local_username",
            "display_name",
            "display_id",
            "namespace",
            "display_local_username",
        )
        validators = [
            RaiseConflictUniqueTogetherValidator(
                model=Profile,
                message="A user with the specified username already exists",
            )
        ]


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(required=False)
    flags = serializers.SlugRelatedField(
        slug_field="code",
        read_only=False,
        many=True,
        required=False,
        queryset=Flag.objects.all(),
    )
    groups = serializers.SlugRelatedField(
        slug_field="name",
        read_only=False,
        many=True,
        required=False,
        queryset=Group.objects.all(),
    )

    class Meta:
        model = User
        read_only_fields = (
            "id",
            "username",
            "last_login",
        )
        fields = read_only_fields + (
            "profile",
            "email",
            "first_name",
            "last_name",
            "groups",
            "flags",
            "is_active",
        )

    @staticmethod
    def _set_username(profile: Profile, new_local_username: str):
        profile.local_username = new_local_username
        # set default user namespace
        # pylint: disable=no-member
        namespace, _ = UserNamespace.objects.get_or_create(
            name=settings.CONFIG.user.default_namespace
        )
        profile.namespace = namespace

        if not (profile.uid and profile.gid):
            uid_max = Profile.objects.aggregate(models.Max("uid"))["uid__max"]
            if uid_max:
                profile.uid = uid_max + 1
                profile.gid = uid_max + 1
            else:
                profile.uid = settings.CONFIG.user.uid.min
                profile.gid = settings.CONFIG.user.gid.min

    def _deactivate_user(self, instance: User):
        # remove all associations to user
        project_user_roles = ProjectUserRole.objects.all()
        user_roles_by_project: dict[Project, list[DictUserRole]] = {}
        for pur in project_user_roles:
            if pur.project not in user_roles_by_project:
                user_roles_by_project[pur.project] = []
            if pur.user.id != instance.id:
                user_roles_by_project[pur.project].append(
                    {"user": pur.user, "role": ProjectRole(pur.role)}
                )
        for (project, user_roles) in user_roles_by_project.items():
            update_permissions_signal.send(
                sender=project,
                user=get_request_username(self),
                user_roles=user_roles,
            )

        # remove all permissions
        instance.groups.clear()
        instance.user_permissions.clear()

        instance.data_provider.set([])
        instance.flags.set([])

    @transaction.atomic
    def update(self, instance, validated_data):
        if "profile" in validated_data:
            request_profile = validated_data["profile"]
            if "local_username" in request_profile:
                self._set_username(instance.profile, request_profile["local_username"])
            if "uid" in request_profile:
                instance.profile.uid = request_profile["uid"]
        for field in ("flags", "groups"):
            if field in validated_data:
                getattr(instance, field).set(validated_data[field])
        if (
            "is_active" in validated_data
            and validated_data["is_active"] != instance.is_active
        ):
            request_user = get_request_username(self)
            logger.info(
                "User (ID: %s) changed `is_active` of user (ID: %s) from %s to %s",
                request_user.id,
                instance.id,
                instance.is_active,
                validated_data["is_active"],
            )
            instance.is_active = validated_data["is_active"]
            if not instance.is_active:
                self._deactivate_user(instance)

        instance.save()
        return instance

    def create(self, validated_data):
        if "profile" in validated_data:
            del validated_data["profile"]
        validated_data["username"] = validated_data["email"]
        return super().create(validated_data)


class UserShortSerializer(serializers.ModelSerializer):
    """A read-only user serializer."""

    display_local_username = serializers.CharField(
        source="profile.display_local_username", read_only=True
    )
    local_username = serializers.CharField(
        source="profile.local_username", read_only=True
    )
    affiliation = serializers.CharField(source="profile.affiliation", read_only=True)
    affiliation_id = serializers.CharField(
        source="profile.affiliation_id", read_only=True
    )
    uid = serializers.IntegerField(source="profile.uid", read_only=True)
    gid = serializers.IntegerField(source="profile.gid", read_only=True)
    flags = serializers.SlugRelatedField(
        slug_field="code",
        read_only=True,
        many=True,
        required=False,
    )

    class Meta:
        model = User
        read_only_fields = (
            "username",
            "email",
            "last_name",
            "first_name",
            "local_username",
            "affiliation",
            "affiliation_id",
            "display_local_username",
            "uid",
            "gid",
            "flags",
        )
        fields = read_only_fields

    # Make sure that this serializer is NOT used for POST resp. PUT requests

    def create(self, validated_data):
        raise MethodNotAllowed(method="POST")

    def update(self, instance, validated_data):
        raise MethodNotAllowed(method="PUT")


class UserinfoSerializer(serializers.ModelSerializer):
    """Serializer that extracts data used to display the user infobox in the
    portal.
    """

    # Sets `required: true` in the OpenAPI schema.
    id = serializers.IntegerField(required=True)
    profile = ProfileSerializer(read_only=True)
    ip_address = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField()
    manages = serializers.SerializerMethodField()

    groups = serializers.SlugRelatedField(
        slug_field="name",
        read_only=True,
        many=True,
        required=False,
    )

    flags = serializers.SlugRelatedField(
        slug_field="code",
        read_only=True,
        many=True,
        required=False,
    )

    class Meta:
        """List of all fields returned by the serializer"""

        model = User
        read_only_fields = ("ip_address", "permissions", "manages", "flags", "groups")
        fields = read_only_fields + (
            "username",
            "email",
            "first_name",
            "last_name",
            "profile",
            "id",
        )

    def get_ip_address(self, _: User):
        request = self.context["request"]
        x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
        if x_forwarded_for:
            return x_forwarded_for.split(",")[0]
        return request.META.get("REMOTE_ADDR")

    def get_permissions(self, _: User):
        request = self.context["request"]
        return {
            "manager": is_manager(request.user),
            "staff": request.user.is_staff,
            "data_manager": is_data_manager(request.user),
            "project_leader": is_project_leader(request.user),
            "data_provider": is_data_provider(request.user),
            "data_provider_admin": has_data_provider_admin_data_providers(request.user),
            "data_provider_viewer": has_data_provider_viewer_data_providers(
                request.user
            ),
            "node_admin": has_node_admin_nodes(request.user),
            "node_viewer": has_node_viewer_nodes(request.user),
            "group_manager": has_group_manager_groups(request.user),
            "has_projects": has_projects(request.user),
        }

    def get_manages(self, _: User):
        request = self.context["request"]
        return {
            "data_providers": managed_data_providers(request.user).values(),
            "data_provider_admin": get_data_provider_admin_data_providers(
                request.user
            ).values(),
            "node_admin": get_node_admin_nodes(request.user).values(),
        }
