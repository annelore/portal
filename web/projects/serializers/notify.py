from rest_framework import serializers

from projects.models import Project, Message


class NotifySerializer(serializers.ModelSerializer):
    project = serializers.SlugRelatedField(
        slug_field="code", queryset=Project.objects.all()
    )

    class Meta:
        model = Message
        fields = ("tool", "data", "project", "created")
