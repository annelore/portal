import logging

from django.conf import settings
from django_drf_utils.email import sendmail
from django_drf_utils.exceptions import Unimplemented
from django_drf_utils.serializers.fields import EnumChoiceField
from django_drf_utils.serializers.utils import DetailedValidationError
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed

from projects.models import PgpKeySignRequest, PgpKeySignStatus, PgpKey
from projects.utils.gpg import fetch_pgp_keys_by_id, keyserver_query_fpr
from projects.mail_templates.gpg_signature_request import (
    gpg_key_sign_request_notification,
)

logger = logging.getLogger(__name__)


class PgpKeySerializer(serializers.Serializer):  # pylint: disable=abstract-method
    pgpkey_id = serializers.CharField(
        validators=PgpKeySignRequest.pgpkey_id.field.validators  # pylint: disable=no-member
    )
    fingerprint = serializers.CharField(read_only=True)
    key_name = serializers.CharField(read_only=True)
    sign_status = EnumChoiceField(
        choices=PgpKeySignStatus, use_enum=True, read_only=True
    )
    link = serializers.SerializerMethodField()

    @staticmethod
    def get_link(key: PgpKey):
        # pylint: disable=no-member
        return f"{settings.CONFIG.pgp.keyserver}/pks/lookup?op=get&search=0x{key.pgpkey_id}"


class PgpKeySignRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = PgpKeySignRequest
        read_only_fields = ("signing_pgpkey_id", "created")
        fields = ("pgpkey_id",) + read_only_fields

    def create(self, validated_data):
        # pylint: disable=no-member
        if not (settings.CONFIG.pgp and settings.CONFIG.pgp.keyserver):
            raise Unimplemented()
        signing_pgpkey_id = settings.CONFIG.pgp.signing_pgpkey_id
        pgpkey_id = validated_data.get("pgpkey_id", "").upper()
        try:
            key = fetch_pgp_keys_by_id(
                key_id=pgpkey_id,
                keyserver=settings.CONFIG.pgp.keyserver,
                signing_pgpkey_id=signing_pgpkey_id,
            )
        except RuntimeError as e:
            raise DetailedValidationError(format(e)) from e
        if key.sign_status is PgpKeySignStatus.SIGNED:
            raise DetailedValidationError("Key has already been signed.")
        if key.key_length < settings.CONFIG.pgp.minimal_key_length:
            raise DetailedValidationError(
                f"Key length must be greater or equal to {settings.CONFIG.pgp.minimal_key_length}"
            )
        if PgpKeySignRequest.objects.filter(
            pgpkey_id=pgpkey_id, signing_pgpkey_id=signing_pgpkey_id
        ).exists():
            raise DetailedValidationError(
                "Key signature request has already been submitted."
            )
        recipient = settings.CONFIG.notification.ticket_mail
        logger.debug(
            "Sending key signature request for '%s' to '%s'.", pgpkey_id, recipient
        )
        sendmail(
            subject=f"PGP key signature request for {pgpkey_id}",
            body=gpg_key_sign_request_notification(
                key.key_name,
                key.fingerprint,
                keyserver_query_fpr(settings.CONFIG.pgp.keyserver, pgpkey_id),
                signing_pgpkey_id,
            ),
            recipients=[recipient],
            email_cfg=settings.CONFIG.email,
        )
        return super().create(
            {
                **validated_data,
                "pgpkey_id": pgpkey_id,
                "signing_pgpkey_id": settings.CONFIG.pgp.signing_pgpkey_id.upper(),
            }
        )

    def update(self, instance, validated_data):
        raise MethodNotAllowed(method="PUT")
