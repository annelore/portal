from rest_framework import serializers

from projects.models import Message
from projects.serializers.project import ProjectInfoSerializer


class MessageSerializer(serializers.ModelSerializer):
    project = ProjectInfoSerializer(read_only=True)

    class Meta:
        model = Message
        fields = ("id", "tool", "data", "project", "created")
