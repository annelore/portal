from django.conf.urls import include
from django.urls import re_path
from rest_framework.routers import DefaultRouter

from projects import views
from projects.apps import APP_NAME


router = DefaultRouter()
router.register(r"nodes", views.NodeViewSet, "node")
router.register(r"notify", views.NotifyViewSet, "notify")
router.register(r"projects", views.ProjectViewSet, "project")
router.register(r"projects-info", views.ProjectInfoViewSet, "projectinfo")
router.register(r"project-messages", views.MessageViewSet, "projectmessage")
router.register(r"users", views.UserViewSet, "user")
router.register(r"user-namespaces", views.UserNamespaceViewSet, "user-namespace")
router.register(
    r"pgpkey/sign-request", views.PgpKeySignRequestViewSet, "pgpkeysignrequest"
)
router.register(r"pgpkey", views.PgpKeyViewSet, "pgpkey")
router.register(
    r"audit/project-permission",
    views.ProjectUserRoleHistoryViewSet,
    "audit-project-permission",
)
router.register(r"feed", views.FeedViewSet, "feed")
router.register(r"data-provider", views.DataProviderViewSet, "dataprovider")
router.register(r"data-package/log", views.DataPackageLogViewSet, "datapackagelog")
router.register(
    r"data-package/check", views.DataPackageCheckViewSet, "datapackagecheck"
)
router.register(r"data-package", views.DataPackageViewSet, "datapackage")
router.register(r"data-transfer", views.DataTransferViewSet, "datatransfer")
router.register(r"log", views.LogView, "log")
router.register(r"contact", views.ContactView, "contact")
router.register(r"flags", views.FlagViewSet, "flag")
router.register(r"quick-accesses", views.QuickAccessTileViewSet, "quickaccess")

# The API URLs are now determined automatically by the router.
app_name = APP_NAME
urlpatterns = [
    re_path(r"^", include(router.urls)),
    re_path(r"^userinfo/$", views.UserinfoView.as_view(), name="userinfo"),
]
