from dataclasses import dataclass
from collections.abc import Iterable, Collection

from django.conf import settings
from django.dispatch import Signal, receiver
from django_drf_utils.email import sendmail
from rest_framework.exceptions import PermissionDenied

from projects.mail_templates.project_user_notification import project_user_notification
from projects.mail_templates.update_permissions import (
    PermissionChanges,
    update_permissions_notification,
)
from projects.models import (
    Project,
    User,
    ProjectRole,
    ProjectUserRole,
    ProjectUserRoleHistory,
)
from projects.permissions import is_staff
from projects.permissions.project import has_project_role
from projects.permissions.node import has_node_admin_permission

# providing_args=["user", "user_roles"]
update_permissions_signal = Signal()


def get_project_change_roles(user: User, obj: Project) -> set[ProjectRole]:
    can_change_pl = is_staff(user) or has_node_admin_permission(user, obj.destination)
    can_change_pm = can_change_pl or has_project_role(user, obj, ProjectRole.PL)
    can_change_dm_user = can_change_pm or has_project_role(user, obj, ProjectRole.PM)
    return {
        role
        for condition, role in (
            (can_change_pl, ProjectRole.PL),
            (can_change_pm, ProjectRole.PM),
            (can_change_dm_user, ProjectRole.DM),
            (can_change_dm_user, ProjectRole.USER),
        )
        if condition
    }


def send_user_emails(
    project: Project, new_users: Collection[User], del_users: Collection[User]
):
    """Notify nodes about changes in project users"""
    if not new_users and not del_users:
        return
    sendmail(
        subject=f"User(s) added to/removed from project '{project.name}'",
        body=project_user_notification(project.name, new_users, del_users),
        recipients=[project.destination.ticketing_system_email],
        email_cfg=settings.CONFIG.email,  # pylint: disable=no-member
    )


def check_can_change_roles(user: User, instance: Project, roles: Iterable[ProjectRole]):
    """Check if the current user can change selected roles"""
    r_edit = get_project_change_roles(user, instance)
    r_diff = set(roles) - r_edit
    if r_diff:
        r_diff = (r.name for r in r_diff)
        raise PermissionDenied(f"Forbidden to change roles: {', '.join(r_diff)}")


def save_user_permission_change(
    user: User, permissions: Iterable[ProjectUserRole], enabled: bool
):
    members = [
        ProjectUserRoleHistory(
            changed_by=user,
            user=permission.user,
            project=permission.project,
            project_str=permission.project.code,
            role=permission.role,
            enabled=enabled,
        )
        for permission in permissions
    ]
    if members:
        ProjectUserRoleHistory.objects.bulk_create(members)


def send_permissions_update_emails(
    user: User,
    project: Project,
    created_permissions: PermissionChanges,
    deleted_permissions: PermissionChanges,
    recipients: list[str],
):
    if not created_permissions and not deleted_permissions:
        return

    requestor_name = user.profile.display_name
    project_name = project.name

    sendmail(
        subject=f"Permissions changed in '{project_name}'",
        body=update_permissions_notification(
            project_name, requestor_name, created_permissions, deleted_permissions
        ),
        recipients=recipients,
        email_cfg=settings.CONFIG.email,  # pylint: disable=no-member
    )


@dataclass
class DictUserRole:
    user: User
    role: ProjectRole


TupleUserRole = tuple[User, ProjectRole]


@receiver(update_permissions_signal)
def update_permissions(
    sender: Project, user: User, user_roles: list[DictUserRole], **kwargs
):  # pylint: disable=unused-argument
    """
    :raises: ServiceUnavailable
    """
    p_old: dict[TupleUserRole, ProjectUserRole] = {
        (p.user, ProjectRole(p.role)): p for p in sender.users.all()
    }
    p_new: set[TupleUserRole] = {(ur["user"], ur["role"]) for ur in user_roles}
    roles_to_create: set[TupleUserRole] = p_new - set(p_old.keys())
    roles_to_delete: set[TupleUserRole] = set(p_old.keys()) - p_new
    check_can_change_roles(
        user, sender, (p for _, p in roles_to_create | roles_to_delete)
    )

    project_leaders_email = [
        user.email for user in sender.users_by_role(ProjectRole.PL)
    ]

    user_roles_created: list[ProjectUserRole] = [
        ProjectUserRole.objects.create(project=sender, user=user, role=r_type.value)
        for user, r_type in roles_to_create
    ]
    save_user_permission_change(user, user_roles_created, enabled=True)

    user_roles_deleted: list[ProjectUserRole] = [p_old[r] for r in roles_to_delete]
    save_user_permission_change(user, user_roles_deleted, enabled=False)
    ProjectUserRole.objects.filter(pk__in=[p.id for p in user_roles_deleted]).delete()

    send_permissions_update_emails(
        user,
        sender,
        roles_to_create,
        roles_to_delete,
        project_leaders_email,
    )

    new_users = {u for u, _ in p_new} - {u for u, _ in p_old}
    del_users = {u for u, _ in p_old} - {u for u, _ in p_new}

    send_user_emails(sender, new_users, del_users)
