# pylint: disable=redefined-outer-name
import pytest

from identities.models import User
from projects.mail_templates import affiliation_change_body
from projects.models import Profile


@pytest.fixture
def user():
    user = User(
        first_name="Peter",
        last_name="Müller",
        username="102715327534@eduid.ch",
        email="peter@mueller.ch",
    )
    user.profile = Profile()
    return user


def test_affiliation_change_body(user):
    expected_body = (
        "Display name: Peter Müller (ID: 102715327534) (peter@mueller.ch)\n"
        "Old affiliation: member@fhnw.ch, staff@fhnw.ch\n"
        "New affiliation: member@chuv.ch, staff@chuv.ch"
    )
    assert (
        affiliation_change_body(
            user, "member@fhnw.ch,staff@fhnw.ch", "member@chuv.ch,staff@chuv.ch"
        )
        == expected_body
    )
