# pylint: disable=redefined-outer-name
import pytest

from projects.mail_templates.project_user_notification import project_user_notification
from projects.models import User, Profile

TEST_PROJECT_NAME = "Project name: Doghouse"


@pytest.fixture
def user_list():
    def make_user(first_name: str, last_name: str):
        user = User(first_name=first_name, last_name=last_name)
        user.profile = Profile()
        return user

    return [
        make_user("Walter", "White"),
        make_user("Jesse", "Pinkman"),
    ]


def test_remove_project_user_notification(user_list):
    lines = (
        TEST_PROJECT_NAME,
        "Removed users: Walter White, Jesse Pinkman",
    )
    assert project_user_notification("Doghouse", [], user_list) == "\n".join(lines)


def test_add_project_user_notification(user_list):
    lines = (
        TEST_PROJECT_NAME,
        "Added users: Walter White, Jesse Pinkman",
    )
    assert project_user_notification("Doghouse", user_list, []) == "\n".join(lines)


def test_project_user_notification(user_list):
    lines = (
        TEST_PROJECT_NAME,
        "Added users: Walter White, Jesse Pinkman",
        "Removed users: Walter White, Jesse Pinkman",
    )
    assert project_user_notification("Doghouse", user_list, user_list) == "\n".join(
        lines
    )
