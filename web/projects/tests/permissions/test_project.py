import pytest
from assertpy import assert_that

from projects.models import ProjectRole
from projects.permissions.project import (
    is_manager,
    has_project_role,
    is_project_leader,
    is_data_manager,
)


@pytest.fixture
def project_basic_user_role(user_factory, project_factory, project_user_role_factory):
    user = user_factory(basic=True)
    project = project_factory()

    def create(role: ProjectRole):
        return project_user_role_factory(user=user, project=project, role=role.value)

    return create


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,manager,true_callable",
    (
        (ProjectRole.PL, True, is_project_leader),
        (ProjectRole.DM, False, is_data_manager),
        (ProjectRole.PM, True, None),
    ),
)
def test_has_any_project_role(
    project_basic_user_role,  # pylint: disable=redefined-outer-name
    role,
    manager,
    true_callable,
):
    project_user_role = project_basic_user_role(role=role)
    assert_that(
        is_manager(project_user_role.user),
    ).is_equal_to(manager)
    if true_callable:
        assert_that(true_callable(project_user_role.user)).is_true()


@pytest.mark.django_db
def test_is_manager_other(user_factory, project_factory, project_user_role_factory):
    user = user_factory(basic=True)
    project = project_factory()
    # No project permission
    assert_that(is_manager(user)).is_false()
    # USER
    project_user_role_factory(user=user, project=project, role=ProjectRole.USER.value)
    assert_that(is_manager(user)).is_false()
    # DM
    project_user_role_factory(user=user, project=project, role=ProjectRole.DM.value)
    assert_that(is_manager(user)).is_false()


@pytest.mark.django_db
def test_has_project_role(
    project_basic_user_role,
):  # pylint: disable=redefined-outer-name
    pl_role = ProjectRole.PL
    project_user_role = project_basic_user_role(role=pl_role)
    assert_that(
        has_project_role(project_user_role.user, project_user_role.project, pl_role)
    ).is_true()
    for role in (
        ProjectRole.PM,
        ProjectRole.DM,
        ProjectRole.USER,
    ):
        assert_that(
            has_project_role(project_user_role.user, project_user_role.project, role)
        ).is_false()
