from unittest.mock import Mock

import pytest
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase
from rest_framework import permissions
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.test import APIRequestFactory

from projects.apps import APP_NAME
from projects.models import Project
from projects.permissions import IsStaff, IsStaffOrReadOnly, ObjectPermission, ReadOnly
from projects.tests.views import UserFactory

NONSAFE_METHODS = ("POST", "PUT", "PATCH", "DELETE")
factory = APIRequestFactory()


class TestIsStaff(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = UserFactory.create_basic()
        self.staff = UserFactory.create_staff()
        self.staff.is_staff = True
        self.permission_obj = IsStaff()

    def test_anonymous(self):
        request = self.factory.request()
        request.user = AnonymousUser()
        self.assertFalse(self.permission_obj.has_permission(request, None))

    def test_user(self):
        request = self.factory.request()
        request.user = self.user
        self.assertFalse(self.permission_obj.has_permission(request, None))

    def test_staff(self):
        request = self.factory.request()
        request.user = self.staff
        self.assertTrue(self.permission_obj.has_permission(request, None))


class TestIsStaffOrReadOnly(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = UserFactory.create_basic()
        self.staff = UserFactory.create_staff()
        self.permission_obj = IsStaffOrReadOnly()
        self.request = self.factory.request()

    def test_anonymous(self):
        self.request.user = AnonymousUser()
        self.assertFalse(self.permission_obj.has_permission(self.request, None))

    def test_user(self):
        self.request.user = self.user
        for method in permissions.SAFE_METHODS:
            self.request.method = method
            self.assertTrue(self.permission_obj.has_permission(self.request, None))
        for method in NONSAFE_METHODS:
            self.request.method = method
            self.assertFalse(self.permission_obj.has_permission(self.request, None))

    def test_staff(self):
        self.request.user = self.staff
        for method in permissions.SAFE_METHODS + NONSAFE_METHODS:
            self.request.method = method
            self.assertTrue(self.permission_obj.has_permission(self.request, None))


@pytest.mark.parametrize(
    "test_input, expected",
    (
        (permissions.SAFE_METHODS, True),
        (NONSAFE_METHODS, False),
    ),
)
def test_readonly(rf, test_input, expected, basic_user_auth):
    permission_obj = ReadOnly()
    request = rf.request()
    request.user = basic_user_auth
    for method in test_input:
        request.method = method
        assert permission_obj.has_permission(request, None) is expected


# pylint: disable=no-self-use
class TestObjectPermission:
    @pytest.mark.parametrize(
        "method", ("GET", "OPTIONS", "HEAD", "PUT", "PATCH", "DELETE")
    )
    def test_get_required_object_permissions(self, method):
        ObjectPermission().get_required_object_permissions(method, Project)

    @pytest.mark.parametrize("method", ("POST", "FOO", "get"))
    def test_get_required_object_permissions_not_allowed_method(self, method):
        with pytest.raises(MethodNotAllowed):
            ObjectPermission().get_required_object_permissions(method, Project)

    def test_has_permission_anonymous(self):
        request = factory.get("/")
        request.user = None
        assert ObjectPermission().has_permission(request, None) is False

    def test_has_permission_method_not_allowed(self, user_factory):
        request = factory.post("/")
        request.user = user_factory()
        with pytest.raises(MethodNotAllowed):
            ObjectPermission().has_permission(request, None)

    @pytest.mark.parametrize(
        "method", ("get", "options", "head", "put", "patch", "delete")
    )
    def test_has_permission_method_ok(self, user_factory, method):
        request = getattr(factory, method)("/")
        request.user = user_factory()
        assert ObjectPermission().has_permission(request, None) is True

    def test_has_object_permission(self):
        request = factory.get("/")
        mock_user = Mock()
        request.user = mock_user
        project = Project(gid=1, name="new", code="foo")
        ObjectPermission().has_object_permission(request, None, project)
        mock_user.has_perms.assert_called_once_with(
            (f"{APP_NAME}.view_{project.__class__._meta.model_name}",), project
        )
