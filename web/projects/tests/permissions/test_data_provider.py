from assertpy import assert_that
from django.contrib.auth.models import AnonymousUser, Group
from django.test import RequestFactory, TestCase
from guardian.shortcuts import assign_perm

from projects.permissions import UPDATE_METHODS
from projects.permissions.data_provider import (
    PERM_DATA_PROVIDER_ADMIN,
    PERM_DATA_PROVIDER_VIEWER,
    is_data_provider,
    get_data_provider_admin_data_providers,
    get_data_provider_viewer_data_providers,
    has_data_provider_admin_data_providers,
    has_data_provider_admin_permission,
    has_data_provider_viewer_data_providers,
    has_data_provider_viewer_permission,
    IsDataProviderAdmin,
)
from projects.models import DataProvider, User
from projects.tests.factories import DataProviderFactory, UserFactory


class TestDataProviderPermission(TestCase):
    def setUp(self):
        self.user = UserFactory()
        self.staff = UserFactory(staff=True)
        data_provider = DataProviderFactory()
        data_provider.users.add(self.user)

    def test_is_data_provider(self):
        self.assertTrue(is_data_provider(self.user))
        self.assertFalse(is_data_provider(self.staff))


def make_data_provider_admin(user: User, data_provider: DataProvider):
    group, _ = Group.objects.get_or_create(
        name=f"Data Provider Admin {data_provider.code}"
    )
    group.user_set.add(user)
    assign_perm(PERM_DATA_PROVIDER_ADMIN, group, data_provider)


def make_data_provider_viewer(user: User, data_provider: DataProvider):
    group, _ = Group.objects.get_or_create(
        name=f"Data Provider Viewer {data_provider.code}"
    )
    group.user_set.add(user)
    assign_perm(PERM_DATA_PROVIDER_VIEWER, group, data_provider)


class TestDataProviderViewer(TestCase):
    def setUp(self):
        self.dp = DataProviderFactory()
        self.viewer = UserFactory()
        make_data_provider_viewer(self.viewer, self.dp)
        self.staff = UserFactory(staff=True)

    def test_get_dp_viewer_dps(self):
        assert_that(
            get_data_provider_viewer_data_providers(self.viewer).all()
        ).contains_only(self.dp)

        dp2 = DataProviderFactory()
        make_data_provider_viewer(self.viewer, dp2)
        assert_that(
            get_data_provider_viewer_data_providers(self.viewer).all()
        ).contains_only(self.dp, dp2)

        assert_that(
            get_data_provider_viewer_data_providers(self.staff).all()
        ).is_empty()

    def test_has_dp_viewer_dps(self):
        assert_that(has_data_provider_viewer_data_providers(self.viewer)).is_true()
        assert_that(has_data_provider_viewer_data_providers(self.staff)).is_false()
        make_data_provider_viewer(self.staff, self.dp)
        assert_that(has_data_provider_viewer_data_providers(self.staff)).is_true()

    def test_has_dp_viewer_permission(self):
        # only `dataprovider_viewer` has data provider viewer permission for `dp`
        assert_that(has_data_provider_viewer_permission(self.viewer, self.dp)).is_true()
        assert_that(has_data_provider_viewer_permission(self.staff, self.dp)).is_false()
        # `dataprovider_viewer` does NOT have data provider viewer permission
        # for all data providers
        assert_that(has_data_provider_viewer_permission(self.viewer)).is_false()


class TestDataProviderAdmin(TestCase):
    def setUp(self):
        self.dp = DataProviderFactory()
        self.admin = UserFactory()
        make_data_provider_admin(self.admin, self.dp)
        self.staff = UserFactory(staff=True)

    def test_get_dp_admin_dps(self):
        assert_that(
            get_data_provider_admin_data_providers(self.admin).all()
        ).contains_only(self.dp)

        dp2 = DataProviderFactory()
        make_data_provider_admin(self.admin, dp2)
        assert_that(
            get_data_provider_admin_data_providers(self.admin).all()
        ).contains_only(self.dp, dp2)

        assert_that(get_data_provider_admin_data_providers(self.staff).all()).is_empty()

    def test_has_dp_admin_dps(self):
        assert_that(has_data_provider_admin_data_providers(self.admin)).is_true()
        assert_that(has_data_provider_admin_data_providers(self.staff)).is_false()
        make_data_provider_admin(self.staff, self.dp)
        assert_that(has_data_provider_admin_data_providers(self.staff)).is_true()

    def test_has_dp_admin_permission(self):
        # only `dataprovider_admin` has data provider admin permission for `dp`
        assert_that(has_data_provider_admin_permission(self.admin, self.dp)).is_true()
        assert_that(has_data_provider_admin_permission(self.staff, self.dp)).is_false()
        # `dataprovider_admin` does NOT have data provider admin permission
        # for all data providers
        assert_that(has_data_provider_admin_permission(self.admin)).is_false()


class TestIsDataProviderAdmin(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user, self.admin1, self.admin2 = UserFactory.create_batch(3)
        self.staff = UserFactory(staff=True)
        self.dp1, self.dp2 = DataProviderFactory.create_batch(2)
        make_data_provider_admin(self.admin1, self.dp1)
        make_data_provider_admin(
            self.admin2, self.dp2
        )  # FIXME test probably fails because of reusing the group
        self.permission_obj = IsDataProviderAdmin()
        self.request = self.factory.request()
        self.request.method = "PUT"

    def test_no_permission(self):
        # Anonymous user and user with no data provider permissions
        for user in (AnonymousUser(), self.user):
            self.request.user = user
            assert_that(
                self.permission_obj.has_permission(self.request, None)
            ).is_false()

    def test_staff(self):
        self.request.user = self.staff
        for method in UPDATE_METHODS:
            self.request.method = method
            assert_that(
                self.permission_obj.has_permission(self.request, None)
            ).is_false()

    def test_dp_admin_own_dp(self):
        for method in ("PUT", "PATCH"):
            for user, dp in ((self.admin1, self.dp1), (self.admin2, self.dp2)):
                self.request.user = user
                self.request.method = method
                assert_that(
                    self.permission_obj.has_permission(self.request, None)
                ).is_true()
                assert_that(
                    self.permission_obj.has_object_permission(self.request, None, dp)
                ).is_true()

    def test_dp_admin_other_dp(self):
        for user, node in (
            (self.admin1, self.dp2),
            (self.admin2, self.dp1),
        ):
            self.request.user = user
            assert_that(
                self.permission_obj.has_permission(self.request, None)
            ).is_true()
            assert_that(
                self.permission_obj.has_object_permission(self.request, None, node)
            ).is_false()

    def test_dp_admin_forbidden_methods(self):
        self.request.user = self.admin1
        for method in ("HEAD", "GET", "POST", "DELETE"):
            self.request.method = method
            assert_that(
                self.permission_obj.has_permission(self.request, None)
            ).is_false()
