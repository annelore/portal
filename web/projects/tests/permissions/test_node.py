from assertpy import assert_that
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase

from projects.permissions import UPDATE_METHODS
from projects.permissions.node import (
    get_node_admin_nodes,
    get_node_viewer_nodes,
    has_node_admin_nodes,
    has_node_admin_permission,
    has_node_viewer_nodes,
    has_node_viewer_permission,
    IsNodeAdmin,
)
from projects.tests.factories import NodeFactory
from projects.tests.views import UserFactory, make_node_admin, make_node_viewer


class TestNodeViewer(TestCase):
    def setUp(self):
        self.node = NodeFactory.create()
        self.node_viewer = UserFactory.create_node_viewer("node-viewer", self.node)
        self.staff = UserFactory.create_staff()

    def test_get_node_viewer_nodes(self):
        assert_that(get_node_viewer_nodes(self.node_viewer).all()).contains_only(
            self.node
        )

        node2 = NodeFactory.create()
        make_node_viewer(self.node_viewer, node2)
        assert_that(get_node_viewer_nodes(self.node_viewer).all()).contains_only(
            self.node, node2
        )

        assert_that(get_node_viewer_nodes(self.staff).all()).is_empty()

    def test_has_node_viewer_nodes(self):
        self.assertTrue(has_node_viewer_nodes(self.node_viewer))
        self.assertFalse(has_node_viewer_nodes(self.staff))
        make_node_viewer(self.staff, self.node)
        self.assertTrue(has_node_viewer_nodes(self.staff))

    def test_has_node_viewer_permission(self):
        # only `node_viewer` has node viewer permission for `node`
        self.assertTrue(has_node_viewer_permission(self.node_viewer, self.node))
        self.assertFalse(has_node_viewer_permission(self.staff, self.node))
        # `node_viewer` does NOT have node viewer permission for all nodes
        self.assertFalse(has_node_viewer_permission(self.node_viewer))


class TestNodeAdmin(TestCase):
    def setUp(self):
        self.node = NodeFactory.create()
        self.node_admin = UserFactory.create_node_admin("node-admin", self.node)
        self.staff = UserFactory.create_staff()

    def test_get_node_admin_nodes(self):
        assert_that(get_node_admin_nodes(self.node_admin).all()).contains_only(
            self.node
        )

        node2 = NodeFactory.create()
        make_node_admin(self.node_admin, node2)
        assert_that(get_node_admin_nodes(self.node_admin).all()).contains_only(
            self.node, node2
        )

        assert_that(get_node_admin_nodes(self.staff).all()).is_empty()

    def test_has_node_admin_nodes(self):
        self.assertTrue(has_node_admin_nodes(self.node_admin))
        self.assertFalse(has_node_admin_nodes(self.staff))
        make_node_admin(self.staff, self.node)
        self.assertTrue(has_node_admin_nodes(self.staff))

    def test_has_node_admin_permission(self):
        # only `node_admin` has node admin permission for `node`
        self.assertTrue(has_node_admin_permission(self.node_admin, self.node))
        self.assertFalse(has_node_admin_permission(self.staff, self.node))
        # `node_admin` does NOT have node admin permission for all nodes
        self.assertFalse(has_node_admin_permission(self.node_admin))


class TestIsNodeAdmin(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = UserFactory.create_basic()
        self.staff = UserFactory.create_staff()
        self.node_1, self.node_2 = NodeFactory.create_batch(2)
        self.node_admin_1 = UserFactory.create_node_admin("node-admin-1", self.node_1)
        self.node_admin_2 = UserFactory.create_node_admin("node-admin-2", self.node_2)
        self.permission_obj = IsNodeAdmin()
        self.request = self.factory.request()
        self.request.method = "PUT"

    def test_anonymous(self):
        self.request.user = AnonymousUser()
        self.assertFalse(self.permission_obj.has_permission(self.request, None))

    def test_user(self):
        self.request.user = self.user
        self.assertFalse(self.permission_obj.has_permission(self.request, None))

    def test_node_admin_own_node(self):
        for user, node, method in (
            (self.node_admin_1, self.node_1, "PUT"),
            (self.node_admin_1, self.node_1, "PATCH"),
            (self.node_admin_2, self.node_2, "PUT"),
            (self.node_admin_2, self.node_2, "PATCH"),
        ):
            self.request.user = user
            self.request.method = method
            self.assertTrue(self.permission_obj.has_permission(self.request, None))
            self.assertTrue(
                self.permission_obj.has_object_permission(self.request, None, node)
            )

    def test_node_admin_other_node(self):
        for user, node in (
            (self.node_admin_1, self.node_2),
            (self.node_admin_2, self.node_1),
        ):
            self.request.user = user
            self.assertTrue(self.permission_obj.has_permission(self.request, None))
            self.assertFalse(
                self.permission_obj.has_object_permission(self.request, None, node)
            )

    def test_node_admin_forbidden_methods(self):
        for method in ("HEAD", "GET", "POST", "DELETE"):
            self.request.user = self.node_admin_1
            self.request.method = method
            self.assertFalse(self.permission_obj.has_permission(self.request, None))

    def test_staff(self):
        self.request.user = self.staff
        for method in UPDATE_METHODS:
            self.request.method = method
            self.assertFalse(self.permission_obj.has_permission(self.request, None))
