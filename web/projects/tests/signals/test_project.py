# pylint: disable=no-self-use
from unittest.mock import MagicMock

import pytest
from django.core import mail

from projects.models import User, Profile
from projects.signals.project import send_user_emails


@pytest.fixture
def project():
    project = MagicMock()
    project.name = "Election 2020"
    project.destination.ticketing_system_email = "election@usa.com"
    return project


def user(first_name, last_name):
    user = User(first_name=first_name, last_name=last_name)
    user.profile = Profile()
    return user


def test_send_user_emails_with_empty_lists(project):
    send_user_emails(project, [], [])
    assert len(mail.outbox) == 0


def test_send_user_emails(project):
    added = user("Joe", "Biden")
    removed = user("Donald", "Trump")
    send_user_emails(project, [added], [removed])
    assert len(mail.outbox) > 0
