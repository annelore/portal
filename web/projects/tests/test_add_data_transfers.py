# pylint: disable=redefined-outer-name
import contextlib
import io
from unittest import mock

import pytest


@contextlib.contextmanager
def mock_open_content(for_path, content):
    def open_mock(name):
        assert name == for_path
        return io.StringIO(content)

    with mock.patch("builtins.open", create=True, side_effect=open_mock) as m:
        yield m


@pytest.fixture
def add_users_and_projects(user_factory, project_factory):
    for code in ("swisspkcdw", "psss", "frailty"):
        project_factory.create(code=code)
    for user_email in ("s.o@u.ch", "jk@e.ch", "n.m@u.ch"):
        user_factory.create(
            email=user_email, username=user_email.split("@", maxsplit=1)[0]
        )
