import factory
from django.contrib.auth import get_user_model
from django.db.models import signals

from projects.models import (
    Node,
    Project,
    Profile,
    PgpKeySignRequest,
    DataTransfer,
    DataProvider,
    ProjectRole,
    ProjectUserRole,
    Flag,
    ProjectUserRoleHistory,
    QuickAccessTile,
    PeriodicTaskRun,
)

USER_PASSWORD = "pass"  # nosec


class FlagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Flag
        django_get_or_create = ("code",)

    code = "sphn"
    description = (
        "This flag is set for an SPHN project user, "
        "when the user passes SPHN security exam and "
        "all relevant SPHN user agreements are signed."
    )

    class Params:
        aup_scicore = factory.Trait(
            name="aup_scicore",
            description=(
                "This flag is set when AUP for sciCORE is signed by the User."
            ),
        )


class QuickAccessTileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = QuickAccessTile

    title = "GitLab"
    url = "https://gitlab.com/"
    image = factory.django.ImageField()
    flag = factory.SubFactory(FlagFactory)


@factory.django.mute_signals(signals.post_save)
class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile

    affiliation = factory.Faker("company")


@factory.django.mute_signals(signals.post_save)
class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    username = factory.Sequence(lambda n: f"user_{n}")
    password = factory.PostGenerationMethodCall("set_password", USER_PASSWORD)
    email = factory.Faker("email")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    profile = factory.RelatedFactory(ProfileFactory, "user")

    class Params:
        basic = factory.Trait(
            username="user-basic",
            email="foo@bar.org",
        )
        staff = factory.Trait(
            username="user-staff",
            is_staff=True,
        )

    @factory.post_generation
    def post_flags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            for flag in extracted:
                self.flags.add(flag)


class NodeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Node

    code = factory.Sequence(lambda n: f"node_{n}")
    name = factory.Sequence(lambda n: f"HPC center {n}")
    node_status = Node.STATUS_ONLINE


class ProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Project

    gid = factory.Sequence(lambda n: n)
    code = factory.Sequence(lambda n: f"project_{n}")
    name = factory.Sequence(lambda n: f"Project {n}")
    destination = factory.SubFactory(
        NodeFactory,
        code=factory.Sequence(lambda n: f"project_node_{n}"),
        name=factory.Sequence(lambda n: f"Project Node {n}"),
    )


class ProjectUserRoleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProjectUserRole

    project = factory.SubFactory(ProjectFactory)
    user = factory.SubFactory(UserFactory)
    role = ProjectRole.USER.value


class ProjectUserRoleHistoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProjectUserRoleHistory

    changed_by = factory.SubFactory(UserFactory)
    user = factory.SubFactory(UserFactory)
    project = factory.SubFactory(ProjectFactory)
    project_str = factory.LazyAttribute(lambda o: o.project.code)
    role = ProjectRole.USER.value
    enabled = True


class DataProviderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataProvider

    name = factory.Faker("word")
    code = factory.Faker("sentence")
    node = factory.SubFactory(
        NodeFactory,
        code=factory.Sequence(lambda n: f"data_provider_node_{n}"),
        name=factory.Sequence(lambda n: f"Data Provider Node {n}"),
    )

    @factory.post_generation
    def post_users(self, create, extracted):
        if not create:
            return

        if extracted:
            for user in extracted:
                self.users.add(user)


class DataTransferFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataTransfer

    project = factory.SubFactory(ProjectFactory)
    data_provider = factory.SubFactory(DataProviderFactory)
    requestor = factory.SubFactory(UserFactory)


class PgpKeySignRequestFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PgpKeySignRequest


class PeriodicTaskRunFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PeriodicTaskRun

    created_at = factory.Faker("date_time")
    task = factory.Faker("word")
