from unittest import mock

import pytest
from rest_framework import generics, permissions, serializers
from rest_framework.test import APIRequestFactory

from projects.models import Project
from projects.filters import CaseInsensitiveOrderingFilter, ObjectPermissionsFilter

factory = APIRequestFactory()


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = "__all__"


class OrderingListView(generics.ListAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (permissions.AllowAny,)
    filter_backends = (CaseInsensitiveOrderingFilter,)
    ordering_fields = ("name",)


class TestCaseInsensitiveOrderingFilter:

    names = (
        "aBc",
        "Bcd",
        "cde",
        "AaC",
        "AbC",
        "AbD",
        "-AB",
        "-AC",
        "AA",
        "BB",
        "ÇA",
        "CB",
        "çc",
        "DD",
        "A̧A",
        "B̧B",
        "ḐD",
        "łó",
        "Ęt",
        "abc",
        "Abc",
        "abC",
        "ABC",
        "Test 1",
        "Test 2",
        "Test 3",
    )
    test_input_orderby_expected = (
        (names, "name", sorted(names, key=str.lower)),
        (names, "-name", sorted(names, key=str.lower, reverse=True)),
    )

    # pylint: disable=no-self-use
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "test_input, order_by, expected", test_input_orderby_expected
    )
    def test_ordering(self, test_input, order_by, expected):
        Project.objects.bulk_create(
            Project(code=f"project_{i}", name=name) for i, name in enumerate(test_input)
        )
        view = OrderingListView.as_view()
        request = factory.get("/", {"ordering": order_by})
        response = view(request)
        assert [x["name"] for x in response.data] == expected


def test_object_permission_filter_queryset(user_factory):
    queryset = Project.objects.all()
    user = user_factory()
    request = factory.get("/")
    request.user = user
    with mock.patch("projects.filters.get_objects_for_user") as mock_get_objects:
        ObjectPermissionsFilter().filter_queryset(request, queryset, None)
        mock_get_objects.assert_called_once_with(
            user,
            f"{Project._meta.app_label}.view_{Project._meta.model_name}",
            queryset,
            accept_global_perms=ObjectPermissionsFilter.accept_global_permissions,
            with_superuser=ObjectPermissionsFilter.superuser_show_all,
        )
