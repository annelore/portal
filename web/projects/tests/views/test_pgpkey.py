import importlib
from unittest.mock import Mock

import pytest
from assertpy import assert_that
from django.urls import reverse
from rest_framework import status

from portal.config import (
    Config,
    Pgp as PgpConfig,
    Notification as NotificationConfig,
    Email as EmailConfig,
)
from projects.apps import APP_NAME
from ..factories import PgpKeySignRequestFactory

URL = reverse(f"{APP_NAME}:pgpkey-list")
URL_SIGN_REQUEST = reverse(f"{APP_NAME}:pgpkeysignrequest-list")
TEST_CONFIG = Config(
    oidc=None,
    pgp=PgpConfig(
        keyserver="keyserver_url",
        signing_pgpkey_id="3A6500D5C1DE39AC",
        minimal_key_length=4096,
    ),
    notification=NotificationConfig(
        ticket_mail="to@example.org", contact_form_recipient="contact_form@example.org"
    ),
    email=EmailConfig(
        host="127.0.0.1",
        port=25,
        use_tls=True,
        user="chuck",
        password="norris",
        from_address="from@example.org",
        subject_prefix="Portal: ",
    ),
)
PGP_KEY_ID = "keyid"
POST_DATA = {"pgpkey_id": PGP_KEY_ID}
KEYSERVER_QUERY_GET = (
    f"{TEST_CONFIG.pgp.keyserver}" "/pks/lookup?search={}&op=vindex&fingerprint=on"
)
KEYSERVER_QUERY_POST = (
    f"{TEST_CONFIG.pgp.keyserver}"
    f"/pks/lookup?search=0x{PGP_KEY_ID.upper()}&op=vindex&fingerprint=on"
)


@pytest.fixture
def get_data():
    data_package = importlib.util.resolve_name(".data", __package__)

    def _get_data(file_name: str) -> str:
        text = importlib.resources.read_text(data_package, file_name)
        return text

    return _get_data


def test_list_unauthenticated(client):
    response = client.get(URL)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == []


def test_no_pgp_config(client, patch_request, settings):
    patch_request(Mock(), "post")
    settings.CONFIG = Config.empty()
    assert client.post(URL_SIGN_REQUEST).status_code == status.HTTP_501_NOT_IMPLEMENTED


def list_empty_get(client):
    response = client.get(URL_SIGN_REQUEST)
    assert response.json() == []
    assert response.status == status.HTTP_200_OK


# pylint: disable=no-self-use
class TestWithTestSettings:
    @pytest.fixture
    def apply_test_settings_email(self, settings):
        settings.DEFAULT_FROM_EMAIL = "from@example.org"
        return settings

    @pytest.fixture(autouse=True)
    def apply_test_settings(self, settings):
        settings.CONFIG = TEST_CONFIG
        return settings

    def test_list_authenticated_no_keys(self, basic_user_auth, client, patch_request):
        mock_request = patch_request(
            Mock(ok=False, status_code=status.HTTP_404_NOT_FOUND)
        )
        response = client.get(URL)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == []
        mock_request.assert_called_once_with(
            KEYSERVER_QUERY_GET.format(basic_user_auth.email)
        )

    def test_list_authenticated_failed(self, basic_user_auth, client, patch_request):
        mock_request = patch_request(Mock(ok=False))
        response = client.get(URL)
        assert response.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR
        assert response.json() == {"detail": "Failed to fetch keys from the keyserver."}
        mock_request.assert_called_once_with(
            KEYSERVER_QUERY_GET.format(basic_user_auth.email)
        )

    # pylint: disable=redefined-outer-name
    def test_list_authenticated_ok(
        self, basic_user_auth, client, patch_request, get_data
    ):
        mock_request = patch_request(Mock(ok=True, text=get_data("key_list.html")))
        response = client.get(URL)
        assert response.status_code == status.HTTP_200_OK
        mock_request.assert_called_once_with(
            KEYSERVER_QUERY_GET.format(basic_user_auth.email)
        )
        ref = [
            {
                "pgpkey_id": "8A10E107053D973C",
                "fingerprint": "60E10C3D2A17E52F49938DF28A10E107053D973C",
                "key_name": "Alice Test <alice.test@example.com>",
                "sign_status": "UNSIGNED",
                "link": "keyserver_url/pks/lookup?op=get&search=0x8A10E107053D973C",
            },
            {
                "pgpkey_id": "941A3EC20555F781",
                "fingerprint": "238565936FCFF3F200219990941A3EC20555F781",
                "key_name": "Alice Inwonderland <alice@wonder.com>",
                "sign_status": "SIGNED",
                "link": "keyserver_url/pks/lookup?op=get&search=0x941A3EC20555F781",
            },
        ]
        assert_that(response.json()).contains_only(*ref)

    @pytest.mark.django_db
    def test_list_authenticated_ok_pending(
        self, basic_user_auth, client, patch_request, get_data
    ):
        mock_request = patch_request(Mock(ok=True, text=get_data("key_list.html")))
        PgpKeySignRequestFactory(
            pgpkey_id="8A10E107053D973C",
            signing_pgpkey_id=TEST_CONFIG.pgp.signing_pgpkey_id,
        )
        PgpKeySignRequestFactory(
            pgpkey_id="941A3EC20555F781",
            signing_pgpkey_id=TEST_CONFIG.pgp.signing_pgpkey_id,
        )

        response = client.get(URL)
        assert response.status_code == status.HTTP_200_OK
        mock_request.assert_called_once_with(
            KEYSERVER_QUERY_GET.format(basic_user_auth.email)
        )
        ref = [
            {
                "pgpkey_id": "8A10E107053D973C",
                "fingerprint": "60E10C3D2A17E52F49938DF28A10E107053D973C",
                "key_name": "Alice Test <alice.test@example.com>",
                "sign_status": "PENDING",
                "link": "keyserver_url/pks/lookup?op=get&search=0x8A10E107053D973C",
            },
            {
                "pgpkey_id": "941A3EC20555F781",
                "fingerprint": "238565936FCFF3F200219990941A3EC20555F781",
                "key_name": "Alice Inwonderland <alice@wonder.com>",
                "sign_status": "SIGNED",
                "link": "keyserver_url/pks/lookup?op=get&search=0x941A3EC20555F781",
            },
        ]
        assert_that(response.json()).contains_only(*ref)

    @pytest.mark.django_db
    def test_list_authenticated_ok_query_email(self, client, patch_request, get_data):
        mock_request = patch_request(Mock(ok=True, text=get_data("key_list.html")))
        email = "chuck@norris.kicks"
        response = client.get(URL + "?email=" + email)
        assert response.status_code == status.HTTP_200_OK
        mock_request.assert_called_once_with(KEYSERVER_QUERY_GET.format(email))

    def test_key_not_found(self, client, patch_request):
        mock_request = patch_request(Mock(ok=False))
        response = client.post(
            URL_SIGN_REQUEST, POST_DATA, content_type="application/json"
        )
        mock_request.assert_called_once_with(KEYSERVER_QUERY_POST)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() == {"detail": "Key has not been found on the keyserver."}

    def test_key_signed(self, client, patch_request, get_data):
        mock_request = patch_request(
            Mock(ok=True, text=get_data("key_one_signed.html"))
        )
        response = client.post(
            URL_SIGN_REQUEST, POST_DATA, content_type="application/json"
        )
        mock_request.assert_called_once_with(KEYSERVER_QUERY_POST)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() == {"detail": "Key has already been signed."}

    def test_key_length_too_small(self, client, patch_request, get_data):
        mock_request = patch_request(
            Mock(
                ok=True, text=get_data("key_one_unsigned.html").replace("4096", "2048")
            )
        )
        response = client.post(
            URL_SIGN_REQUEST, POST_DATA, content_type="application/json"
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() == {
            "detail": "Key length must be greater or equal to 4096"
        }
        mock_request.assert_called_once_with(KEYSERVER_QUERY_POST)

    @pytest.mark.django_db
    def test_key_signature_already_requested(self, client, patch_request, get_data):
        PgpKeySignRequestFactory(
            pgpkey_id=PGP_KEY_ID.upper(),
            signing_pgpkey_id=TEST_CONFIG.pgp.signing_pgpkey_id,
        )
        mock_request = patch_request(
            Mock(ok=True, text=get_data("key_one_unsigned.html"))
        )
        response = client.post(
            URL_SIGN_REQUEST, POST_DATA, content_type="application/json"
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() == {
            "detail": "Key signature request has already been submitted."
        }
        mock_request.assert_called_once_with(KEYSERVER_QUERY_POST)

    # pylint: disable=redefined-outer-name
    @pytest.mark.django_db
    def test_send_ok(
        self, client, patch_request, get_data, mailoutbox, apply_test_settings_email
    ):
        mock_request = patch_request(
            Mock(ok=True, text=get_data("key_one_unsigned.html"))
        )
        response = client.post(
            URL_SIGN_REQUEST, POST_DATA, content_type="application/json"
        )
        assert response.status_code == status.HTTP_201_CREATED
        mock_request.assert_called_once_with(KEYSERVER_QUERY_POST)
        assert len(mailoutbox) == 1
        m = mailoutbox[0]
        assert (
            m.subject == f"Portal: PGP key signature request for {PGP_KEY_ID.upper()}"
        )
        assert "The key specified above should be signed with" in m.body
        assert m.from_email == apply_test_settings_email.DEFAULT_FROM_EMAIL
        assert list(m.to) == [TEST_CONFIG.notification.ticket_mail]
