from collections.abc import Iterable

from django.contrib.auth.models import Group
from guardian.shortcuts import assign_perm

from identities.permissions import perm_group_manager
from projects.models import Node, User, ProjectUserRole, ProjectRole
from projects.permissions.node import PERM_NODE_ADMIN, PERM_NODE_VIEWER


def make_node(n=1):
    return Node.objects.create(
        code=f"node{n}",
        name=f"HPC center {n}",
        node_status=Node.STATUS_ONLINE,
        ticketing_system_email=f"{n}@nodes.skynet",
    )


def make_node_admin(user: User, node: Node):
    group, _ = Group.objects.get_or_create(name=f"Node Admin {node.code}")
    group.user_set.add(user)
    assign_perm(PERM_NODE_ADMIN, group, node)


def make_node_viewer(user: User, node: Node):
    group, _ = Group.objects.get_or_create(name=f"Node Viewer {node.code}")
    group.user_set.add(user)
    assign_perm(PERM_NODE_VIEWER, group, node)


def make_group_manager(user: User, managed_group: Group):
    group, _ = Group.objects.get_or_create(name=f"Group Manager {managed_group.name}")
    group.user_set.add(user)
    assign_perm(perm_group_manager, group, managed_group)


class UserFactory:
    # ignore possible hardcoded password warning
    USER_PASSWORD = "pass"  # nosec

    @staticmethod
    def get_email(username):
        return f"{username}@example.org"

    def __init__(self, username, password, first_name=None, last_name=None, email=None):
        if email is None:
            email = self.get_email(username)
        self.user = User.objects.create_user(
            username=username, password=password, email=email
        )
        if first_name:
            self.user.first_name = first_name
        if last_name:
            self.user.last_name = last_name
        self.user.save()

    def add_to_group(self, group):
        group.user_set.add(self.user)

    @classmethod
    def create_user(cls, username, *args, **kwargs):
        return cls(username, cls.USER_PASSWORD, *args, **kwargs).user

    @classmethod
    def create_data_provider(cls, username, data_provider, *args, **kwargs):
        user = cls.create_user(username, *args, **kwargs)
        data_provider.users.add(user)
        return user

    @classmethod
    def create_node_admin(
        cls,
        username,
        node: Node,
        *args,
        **kwargs,
    ):
        user = cls.create_user(username, *args, **kwargs)
        make_node_admin(user, node)
        return user

    @classmethod
    def create_node_viewer(
        cls,
        username,
        node: Node,
        *args,
        **kwargs,
    ):
        user = cls.create_user(username, *args, **kwargs)
        make_node_viewer(user, node)
        return user

    @classmethod
    def create_group_manager(
        cls,
        username,
        group: Group,
        *args,
        **kwargs,
    ):
        user = cls.create_user(username, *args, **kwargs)
        make_group_manager(user, group)
        return user

    @classmethod
    def create_user_with_perms(
        cls, username: str, perms: Iterable[str], *args, **kwargs
    ):
        user = cls.create_user(username, *args, **kwargs)
        for perm in perms:
            assign_perm(perm, user)
        return user

    @classmethod
    def create_user_with_role(cls, project, username, role, *args, **kwargs):
        user = cls.create_user(username, *args, **kwargs)
        cls.add_role(project, user, role)
        return user

    @classmethod
    def add_role(cls, project, user, role):
        ProjectUserRole.objects.create(project=project, user=user, role=role.value)

    @classmethod
    def create_project_user(cls, project, username="project-user"):
        return cls.create_user_with_role(
            project, username, ProjectRole.USER, "User", "Basic"
        )

    @classmethod
    def create_permission_manager(cls, project):
        return cls.create_user_with_role(
            project, "user-permission-manager", ProjectRole.PM
        )

    @classmethod
    def create_data_manager(cls, project, username="user-data-manager", **kwargs):
        return cls.create_user_with_role(project, username, ProjectRole.DM, **kwargs)

    @classmethod
    def create_project_leader(cls, project):
        return cls.create_user_with_role(project, "user-project-leader", ProjectRole.PL)

    @classmethod
    def create_basic(cls):
        return cls("user-basic", cls.USER_PASSWORD, "User", "Basic").user

    @classmethod
    def create_staff(cls):
        user = cls("user-staff", cls.USER_PASSWORD).user
        user.is_staff = True
        user.save()
        return user


APPLICATION_JSON = "application/json"
