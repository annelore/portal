from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.models import Message, Project
from projects.tests.views import make_node, UserFactory


class TestMessageView(TestCase):
    def setUp(self):
        node = make_node()
        project1 = Project.objects.create(
            gid=1500001, name="new", code="foo", destination=node
        )
        self.basic_user = UserFactory.create_project_user(project1)
        self.pm_user = UserFactory.create_permission_manager(project1)
        self.staff = UserFactory.create_staff()
        Message.objects.create(project=project1, tool="tool_id", data="msg")
        project2 = Project.objects.create(
            gid=1500002, name="new2", code="bar", destination=node
        )
        Message.objects.create(project=project2, tool="tool_id", data="msg")

    def test_view_list(self):
        url = reverse(f"{APP_NAME}:projectmessage-list")
        # Unauthenticated is not allowed
        self.assertEqual(self.client.get(url).status_code, status.HTTP_403_FORBIDDEN)
        # Users and managers can see messages from their projects
        for username in (self.basic_user.username, self.pm_user.username):
            self.client.login(username=username, password=UserFactory.USER_PASSWORD)
            r = self.client.get(url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), 1)
            self.client.logout()
        # Staff can view all messages
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        r = self.client.get(url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 2)
        self.client.logout()
        # Test filters
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        r = self.client.get(url, {"search": "foo"})
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 1)
        r = self.client.get(url, {"search": "tool_id"})
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 2)
        self.client.logout()

    def test_create(self):
        # POST is not allowed
        url = reverse(f"{APP_NAME}:projectmessage-list")
        self.assertEqual(self.client.post(url).status_code, status.HTTP_403_FORBIDDEN)
        for username in (
            self.basic_user.username,
            self.pm_user.username,
            self.staff.username,
        ):
            self.client.login(username=username, password=UserFactory.USER_PASSWORD)
            self.assertEqual(
                self.client.post(url, content_type="application/json").status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()
