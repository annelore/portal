from contextlib import contextmanager

from assertpy import assert_that
from django.conf import settings
from django.core import mail
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.mail_templates.project_archival import project_archive_users_notification
from projects.models import (
    ProjectIPAddresses,
    Resource,
    Project,
    ProjectRole,
    User,
    DataTransfer,
)
from projects.serializers import UserShortSerializer
from projects.serializers.project import ARCHIVED_READ_ONLY_MESSAGE
from projects.tests.views import (
    make_node,
    UserFactory,
    APPLICATION_JSON,
    make_node_admin,
)
from projects.tests.factories import DataTransferFactory


def get_project_detail_url(project):
    return reverse(f"{APP_NAME}:project-detail", kwargs={"pk": project.pk})


def get_project_archive_url(project):
    return reverse(f"{APP_NAME}:project-archive", kwargs={"pk": project.pk})


def get_project_unarchive_url(project):
    return reverse(f"{APP_NAME}:project-unarchive", kwargs={"pk": project.pk})


class ProjectViewTestMixin(TestCase):
    def setUp(self):
        self.destination1 = make_node()
        self.project1 = Project.objects.create(
            gid=1500001, name="first", code="p1", destination=self.destination1
        )
        self.basic_user = UserFactory.create_project_user(self.project1)
        self.permission_manager = UserFactory.create_permission_manager(self.project1)
        self.pl_user = UserFactory.create_project_leader(self.project1)
        self.staff = UserFactory.create_staff()
        self.node_viewer = UserFactory.create_node_viewer(
            "user-node-viewer", self.destination1
        )
        self.project2 = Project.objects.create(
            gid=1500002, name="second", code="p2", destination=make_node(2)
        )
        self.destination3 = make_node(3)
        self.project_node_admin = Project.objects.create(
            gid=1500003,
            name="na_project",
            code="p_na",
            destination=self.destination3,
        )
        # Node admin of destination3, user of project1 (destination 1):
        self.node_admin = UserFactory.create_project_user(
            self.project1, username="node_admin"
        )
        make_node_admin(self.node_admin, self.destination3)
        self.ip_address_ranges = [
            {"ip_address": "127.0.0.1", "mask": 32},
            {"ip_address": "127.0.2.1", "mask": 4},
        ]
        self.resources = [
            {
                "name": "Test Resource",
                "description": "Test Resource Description",
                "location": "https://sib.com",
                "contact": "ticketing_system_email@example.org",
            },
            {
                "name": "Test Resource 2",
                "description": "",
                "location": "https://sib2.com",
                "contact": "",
            },
        ]
        self.client.login(
            username=self.permission_manager.username,
            password=UserFactory.USER_PASSWORD,
        )
        url = get_project_detail_url(self.project1)
        self.project_users = [
            {"id": u["id"], "roles": u["roles"]}
            for u in self.client.get(url).json()["users"]
        ]
        self.client.logout()
        self.new_user = User.objects.create_user("Noob", "new_user@example.org")

    def assert_user_has_roles(self, user, roles):
        self.assertEqual(
            {p.role for p in self.project1.users.all() if p.user == user},
            {p.value for p in roles},
        )

    def users_payload(self, roles, user=None, **kwargs):
        if not user:
            user = self.new_user
        return {
            "users": self.project_users
            + [{"id": user.pk, "roles": [p.name for p in roles]}],
            **kwargs,
        }

    def put_expected_roles_to_new_user(self, expected_roles):
        url = get_project_detail_url(self.project1)
        # PM removes DM permission from new_user
        response = self.client.put(
            url,
            self.users_payload(expected_roles),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assert_user_has_roles(self.new_user, expected_roles)

    @contextmanager
    def pm_add_permission_to_user(self):
        try:
            url = get_project_detail_url(self.project1)
            UserFactory.add_role(self.project1, self.new_user, ProjectRole.DM)
            self.client.login(
                username=self.permission_manager.username,
                password=UserFactory.USER_PASSWORD,
            )
            expected_roles = (ProjectRole.USER, ProjectRole.DM)
            # PM adds User permission to new_user
            self.put_expected_roles_to_new_user(expected_roles)
            yield url, expected_roles
        finally:
            self.client.logout()


class TestProjectView(ProjectViewTestMixin):
    def test_view_list(self):
        url = reverse(f"{APP_NAME}:project-list")
        # Unauthenticated is not allowed
        self.assertEqual(self.client.get(url).status_code, status.HTTP_403_FORBIDDEN)
        # Basic authorized user gets an empty list
        self.client.login(
            username=self.basic_user.username, password=UserFactory.USER_PASSWORD
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)
        self.client.logout()
        # Permission-manager gets a list with one project
        self.client.login(
            username=self.permission_manager.username,
            password=UserFactory.USER_PASSWORD,
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]["gid"], self.project1.gid)
        self.assertEqual(response.json()[0]["name"], self.project1.name)
        self.assertEqual(response.json()[0]["code"], self.project1.code)
        self.assertEqual(
            response.json()[0]["destination"], self.project1.destination.code
        )
        self.client.logout()
        # Staff gets a list with 3 projects
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 3)
        # Filter by project name
        response = self.client.get(url, {"search": self.project1.name})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]["code"], "p1")
        # Filter by project manager
        response = self.client.get(url, {"manager": self.permission_manager.username})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]["code"], "p1")
        # Filter by project username
        response = self.client.get(url, {"username": self.basic_user.username})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)
        self.client.logout()
        # Node admin gets a list with their project1 and destination3 project:
        self.client.login(
            username=self.node_admin.username, password=UserFactory.USER_PASSWORD
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        projects = {p["name"] for p in response.json()}
        self.assertEqual(projects, {self.project1.name, self.project_node_admin.name})

    def test_view_details(self):
        # pylint: disable=too-many-statements
        url1 = get_project_detail_url(self.project1)
        url2 = get_project_detail_url(self.project2)
        url_na = get_project_detail_url(self.project_node_admin)
        test_resource_description = "Test description"
        test_resource_location = "https://resource.org"
        Resource.objects.create(
            project=self.project1,
            name="Test resource",
            location=test_resource_location,
            description=test_resource_description,
        )
        expected_permissions_edit = {
            "name": False,
            "code": False,
            "destination": False,
            "users": [],
            "ip_address_ranges": False,
            "resources": False,
        }
        all_roles = [
            ProjectRole.PL.name,
            ProjectRole.PM.name,
            ProjectRole.DM.name,
            ProjectRole.USER.name,
        ]
        with self.subTest("Unauthenticated"):
            # Unauthenticated is not allowed
            self.assertEqual(
                self.client.get(url1).status_code, status.HTTP_403_FORBIDDEN
            )
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_403_FORBIDDEN
            )
        with self.subTest("Basic user"):
            # Basic authorized user can get one project
            self.client.login(
                username=self.basic_user.username, password=UserFactory.USER_PASSWORD
            )
            r = self.client.get(url1)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            response = r.json()
            resource = response["resources"][0]
            self.assertTrue("location" in resource)
            self.assertEqual(resource["description"], test_resource_description)
            r_edit = response["permissions"]["edit"]
            assert_that(r_edit).is_equal_to(expected_permissions_edit)
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_404_NOT_FOUND
            )
            self.client.logout()
        with self.subTest("Permission Manager"):
            # Permission manager can get one project
            self.client.login(
                username=self.permission_manager.username,
                password=UserFactory.USER_PASSWORD,
            )
            r = self.client.get(url1)
            r_edit = r.json()["permissions"]["edit"]
            assert_that(r_edit).is_equal_to(
                expected_permissions_edit,
                ignore="users",
            )
            assert_that(r_edit["users"]).is_length(2).contains_only(
                ProjectRole.DM.name, ProjectRole.USER.name
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_404_NOT_FOUND
            )
            self.client.logout()
        with self.subTest("Project Leader"):
            # Project leader can get one project
            self.client.login(
                username=self.pl_user.username, password=UserFactory.USER_PASSWORD
            )
            r = self.client.get(url1)
            r_permissions = r.json()["permissions"]
            r_edit = r_permissions["edit"]
            assert_that(r_edit).is_equal_to(
                expected_permissions_edit,
                ignore="users",
            )
            assert_that(r_edit["users"]).is_length(3).contains_only(
                ProjectRole.PM.name, ProjectRole.DM.name, ProjectRole.USER.name
            )
            self.assertFalse(r_permissions["archive"])
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_404_NOT_FOUND
            )
            self.client.logout()
        with self.subTest("Node Admin"):
            # Node Admin see the projects assigned to them and the ones associated with their node
            self.client.login(
                username=self.node_admin.username, password=UserFactory.USER_PASSWORD
            )
            r = self.client.get(url_na)
            r_permissions = r.json()["permissions"]
            r_edit = r_permissions["edit"]
            assert_that(r_edit).is_equal_to(
                {
                    "name": True,
                    "code": True,
                    "ip_address_ranges": True,
                    # Node admin cannot edit node of a project
                    "destination": False,
                    "resources": True,
                },
                ignore="users",
            )
            assert_that(r_edit["users"]).is_length(len(all_roles)).contains_only(
                *all_roles
            )
            # can archive projects associated to the node
            assert_that(r_permissions["archive"]).is_equal_to(True)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            r = self.client.get(url1)
            # can NOT archive projects associated to not administered node
            self.assertFalse(r.json()["permissions"]["archive"])
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_404_NOT_FOUND
            )
            self.client.logout()
        with self.subTest("Staff"):
            # Staff can get both projects
            self.client.login(
                username=self.staff.username, password=UserFactory.USER_PASSWORD
            )
            r = self.client.get(url1)
            response = r.json()
            r_edit = response["permissions"]["edit"]
            assert_that(r_edit).is_equal_to(
                {
                    "name": True,
                    "code": True,
                    "ip_address_ranges": True,
                    "destination": True,
                    "resources": True,
                },
                ignore="users",
            )
            assert_that(r_edit["users"]).is_length(len(all_roles)).contains_only(
                *all_roles
            )
            resource = response["resources"][0]
            self.assertEqual(resource["location"], test_resource_location)
            self.assertEqual(resource["description"], test_resource_description)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(self.client.get(url2).status_code, status.HTTP_200_OK)

            # archived project returns read-only permissions
            self.project1.archived = True
            self.project1.save()
            r = self.client.get(url1)
            response = r.json()
            r_edit = response["permissions"]["edit"]
            assert_that(r_edit).is_equal_to(
                {
                    "name": False,
                    "code": False,
                    "ip_address_ranges": False,
                    "destination": False,
                    "resources": False,
                    "users": [],
                }
            )
            assert_that(response["permissions"]["archive"]).is_equal_to(True)
            self.client.logout()
        # pylint: enable=too-many-statements

    def test_view_users(self):
        url = reverse(f"{APP_NAME}:project-users", kwargs={"pk": 1})
        for username in (self.basic_user.username, self.permission_manager.username):
            self.client.login(username=username, password=UserFactory.USER_PASSWORD)
            self.assertEqual(
                self.client.get(url).status_code, status.HTTP_403_FORBIDDEN
            )
            self.client.logout()
        self.client.login(
            username=self.node_viewer.username, password=UserFactory.USER_PASSWORD
        )

        r = self.client.get(url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(r.json()),
            2,
        )
        r = self.client.get(url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        r_json = r.json()
        self.assertEqual(
            len(r_json),
            self.project1.users.filter(role=ProjectRole.USER.value).count(),
        )
        for user in r_json:
            self.assertListEqual(
                list(UserShortSerializer.Meta.read_only_fields),
                list(user.keys()),
            )
        self.assertEqual(r_json[0]["username"], self.basic_user.username)
        self.assertEqual(
            r_json[0]["local_username"], self.basic_user.profile.local_username
        )
        self.assertEqual(r_json[0]["affiliation"], self.basic_user.profile.affiliation)
        self.assertEqual(
            r_json[0]["affiliation_id"], self.basic_user.profile.affiliation_id
        )
        # Disabled users
        r = self.client.get(url, {"disabled": "true"})
        self.assertEqual(
            len(r.json()),
            len(
                set(User.objects.all())
                - {
                    x.user
                    for x in self.project1.users.filter(role=ProjectRole.USER.value)
                }
            ),
        )
        # Long representation
        r = self.client.get(url, {"short": "false"})
        self.assertIn("affiliation", r.json()[0]["profile"])
        self.assertIn("affiliation_id", r.json()[0]["profile"])
        # Select role
        r = self.client.get(url, {"role": ProjectRole.DM.name})
        self.assertEqual(
            len(r.json()),
            self.project1.users.filter(role=ProjectRole.DM.value).count(),
        )
        # Select invalid role
        self.assertEqual(
            self.client.get(url, {"role": "bad_role"}).status_code,
            status.HTTP_400_BAD_REQUEST,
        )

    def test_create_fails_without_staff_rights(self):
        url = reverse(f"{APP_NAME}:project-list")
        data = {
            "name": "test",
            "code": "bar",
            "destination": self.destination1.name,
            "ip_address_ranges": [],
            "resources": [],
            "users": [],
        }
        # Only staff can create new projects
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_403_FORBIDDEN,
        )
        for username in (
            self.basic_user.username,
            self.permission_manager.username,
            self.node_viewer.username,
        ):
            self.client.login(username=username, password=UserFactory.USER_PASSWORD)
            self.assertEqual(
                self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

    def test_create(self):
        url = reverse(f"{APP_NAME}:project-list")
        code = "bar"
        gid = 1000
        data = {
            "gid": gid,  # gid is a read-only field so this value should be ignored
            "name": "test",
            "code": code,
            "destination": self.destination1.code,
            "ip_address_ranges": self.ip_address_ranges,
            "resources": self.resources,
            "users": [
                {
                    "id": self.basic_user.pk,
                    "roles": [
                        ProjectRole.USER.name,
                        ProjectRole.DM.name,
                    ],
                }
            ],
        }
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_201_CREATED,
        )

        model_ip_ranges = [
            {"ip_address": ip.ip_address, "mask": ip.mask}
            for ip in ProjectIPAddresses.objects.all()
        ]
        self.assertEqual(len(model_ip_ranges), len(self.ip_address_ranges))
        assert_that(model_ip_ranges).contains_only(*self.ip_address_ranges)

        model_resources = [
            {
                "name": resource.name,
                "description": resource.description,
                "location": resource.location,
                "contact": resource.contact,
            }
            for resource in Resource.objects.all()
        ]
        self.assertEqual(len(model_resources), len(self.resources))
        assert_that(model_resources).contains_only(*self.resources)
        project = Project.objects.get(code=code)
        # pylint: disable=no-member
        assert_that(project.gid).is_not_equal_to(gid).is_greater_than_or_equal_to(
            settings.CONFIG.project.gid.min
        ).is_less_than_or_equal_to(settings.CONFIG.project.gid.max)

    def test_unique(self):
        url = reverse(f"{APP_NAME}:project-unique")
        for (code, expected_status_code) in (
            (self.project1.code, status.HTTP_409_CONFLICT),
            ("chuck_norris_research", status.HTTP_200_OK),
        ):
            for user in [self.staff, self.node_admin]:
                self.client.login(
                    username=user.username, password=UserFactory.USER_PASSWORD
                )
                self.assertEqual(
                    self.client.post(
                        url, {"code": code}, content_type=APPLICATION_JSON
                    ).status_code,
                    expected_status_code,
                )
                self.client.logout()

    def test_create_same_name_as_code(self):
        url = reverse(f"{APP_NAME}:project-list")
        data = {
            "name": "test",
            "code": "test",
            "destination": self.destination1.name,
            "ip_address_ranges": self.ip_address_ranges,
            "resources": self.resources,
            "users": [
                {
                    "id": self.basic_user.pk,
                    "roles": [
                        ProjectRole.USER.name,
                        ProjectRole.DM.name,
                    ],
                }
            ],
        }
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_400_BAD_REQUEST,
        )

    def test_create_without_ip_and_resources(self):
        url = reverse(f"{APP_NAME}:project-list")
        data = {
            "name": "test",
            "code": "bar",
            "destination": self.destination1.code,
            "users": [
                {
                    "id": self.basic_user.pk,
                    "roles": [
                        ProjectRole.USER.name,
                        ProjectRole.DM.name,
                    ],
                }
            ],
        }
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_201_CREATED,
        )

        ip_address_ranges = ProjectIPAddresses.objects.all()
        if ip_address_ranges:
            self.fail("IP Address Ranges should not be defined!")

        resources = Resource.objects.all()
        if resources:
            self.fail("Resources should not be defined!")

    def test_create_with_node_admin(self):
        url = reverse(f"{APP_NAME}:project-list")
        self.client.login(
            username=self.node_admin.username, password=UserFactory.USER_PASSWORD
        )
        for destination, expected_status_code in (
            (self.destination3, status.HTTP_201_CREATED),
            (self.destination1, status.HTTP_403_FORBIDDEN),
        ):
            data = {
                "name": "test",
                "code": "bar",
                "destination": destination.code,
                "ip_address_ranges": self.ip_address_ranges,
                "resources": self.resources,
                "users": [
                    {
                        "id": self.new_user.pk,
                        "roles": [
                            ProjectRole.USER.name,
                        ],
                    }
                ],
            }
            response = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                response.status_code,
                expected_status_code,
            )
            if expected_status_code == status.HTTP_201_CREATED:
                assert_that(response.json()).has_name(data["name"]).has_code(
                    data["code"]
                )

    def test_update_unauthorized(self):
        url = get_project_detail_url(self.project1)
        self.assertEqual(
            self.client.put(url, {}, content_type=APPLICATION_JSON).status_code,
            status.HTTP_403_FORBIDDEN,
        )

        self.client.login(
            username=self.basic_user.username, password=UserFactory.USER_PASSWORD
        )
        self.assertEqual(
            self.client.put(
                url, {"name": "changed"}, content_type=APPLICATION_JSON
            ).status_code,
            status.HTTP_403_FORBIDDEN,
        )
        self.client.logout()

        self.client.login(
            username=self.node_viewer.username, password=UserFactory.USER_PASSWORD
        )
        self.assertEqual(
            self.client.put(
                url, {"name": "changed"}, content_type=APPLICATION_JSON
            ).status_code,
            status.HTTP_403_FORBIDDEN,
        )
        self.client.logout()

    def test_update_permission_manager(self):
        with self.pm_add_permission_to_user() as (url, expected_roles):
            # PM can't assign the PM role
            response = self.client.put(
                url,
                self.users_payload([ProjectRole.PM]),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            # PM can't assign the PL role
            response = self.client.put(
                url,
                self.users_payload([ProjectRole.PL]),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            # PM can't update other fields than "users"
            response = self.client.put(
                url,
                self.users_payload(expected_roles, name="new name"),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotEqual(response.json()["name"], "new name")
            # PM can't see other projects
            self.assertEqual(
                self.client.put(
                    get_project_detail_url(self.project2),
                    self.users_payload(expected_roles),
                    content_type=APPLICATION_JSON,
                ).status_code,
                status.HTTP_404_NOT_FOUND,
            )

    def test_update_project_leader(self):
        url = get_project_detail_url(self.project1)
        self.client.login(
            username=self.pl_user.username, password=UserFactory.USER_PASSWORD
        )
        expected_roles = (
            ProjectRole.USER,
            ProjectRole.DM,
            ProjectRole.PM,
        )
        response = self.client.put(
            url,
            self.users_payload(expected_roles),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # PL can't assign the PL role
        response = self.client.put(
            url,
            self.users_payload([ProjectRole.PL]),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # PL can't update other fields than "users"
        response = self.client.put(
            url,
            self.users_payload(expected_roles, name="new name"),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.json()["name"], "new name")
        # PL can't see other projects
        self.assertEqual(
            self.client.put(
                get_project_detail_url(self.project2),
                self.users_payload(expected_roles),
                content_type=APPLICATION_JSON,
            ).status_code,
            status.HTTP_404_NOT_FOUND,
        )
        self.client.logout()

    def test_update_staff(self):
        url = get_project_detail_url(self.project1)
        for ip_range in self.ip_address_ranges:
            ProjectIPAddresses.objects.create(project=self.project1, **ip_range)
        for resource in self.resources:
            Resource.objects.create(project=self.project1, **resource)
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        data = self.client.get(url).json()
        # Update project field
        updated_ip_ranges = [
            self.ip_address_ranges[0],
            {"ip_address": "159.124.12.2", "mask": 20},
        ]
        updated_resources = [
            self.resources[0],
            {"name": "New Resource", "location": "https://sib3.com"},
        ]
        data.update(
            {
                "name": "new project name",
                "ip_address_ranges": updated_ip_ranges,
                "resources": updated_resources,
            }
        )
        # Update by adding an user
        data["users"] = self.project_users + [
            {
                "id": self.new_user.pk,
                "roles": [ProjectRole.USER.name],
            }
        ]
        response = self.client.put(url, data, content_type=APPLICATION_JSON)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        model_ip_ranges = [
            {"ip_address": ip.ip_address, "mask": ip.mask}
            for ip in ProjectIPAddresses.objects.all()
        ]
        self.assertEqual(len(model_ip_ranges), len(updated_ip_ranges))
        assert_that(model_ip_ranges).contains_only(*updated_ip_ranges)

        model_resources = [
            {
                "name": resource.name,
                "description": resource.description,
                "location": resource.location,
                "contact": resource.contact,
            }
            for resource in Resource.objects.all()
        ]
        self.assertEqual(len(model_resources), len(updated_resources))
        # Add the missing default field to resources to match model's default
        updated_resources[1]["description"] = ""
        updated_resources[1]["contact"] = ""
        assert_that(model_resources).contains_only(*updated_resources)
        self.client.logout()

    def test_update_same_name_as_code(self):
        url = get_project_detail_url(self.project1)
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        data = self.client.get(url).json()
        # Update project field
        data.update({"name": self.project1.code})
        response = self.client.put(url, data, content_type=APPLICATION_JSON)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.logout()

    def test_update_with_node_admin(self):
        self.client.login(
            username=self.node_admin.username, password=UserFactory.USER_PASSWORD
        )
        node1 = self.destination1.code
        node3 = self.destination3.code
        na_code = self.project_node_admin.code
        counter = 0
        for project, code, destination, expected_status_code in (
            # node admin can update projects they manage
            (self.project_node_admin, na_code, node3, status.HTTP_200_OK),
            # node admin cannot update destination of a project
            (self.project_node_admin, na_code, node1, status.HTTP_403_FORBIDDEN),
            # node admin can update the code of a project
            (self.project_node_admin, "other_code", node3, status.HTTP_200_OK),
            # node admin cannot update the node of another project to their managed node
            (self.project1, self.project1.code, node3, status.HTTP_403_FORBIDDEN),
            # node admin cannot update projects with a different node from their managed node
            (self.project1, self.project1.code, node1, status.HTTP_403_FORBIDDEN),
        ):
            counter += 1
            url = get_project_detail_url(project)
            data = {
                "name": f"test-{counter}",
                "code": code,
                "destination": destination,
                "ip_address_ranges": self.ip_address_ranges,
                "resources": self.resources,
                "users": [
                    {
                        "id": self.new_user.pk,
                        "roles": [
                            ProjectRole.USER.name,
                        ],
                    }
                ],
            }
            response = self.client.put(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                response.status_code,
                expected_status_code,
            )
            if 200 <= expected_status_code < 300:
                assert_that(response.json()).has_name(data["name"]).has_code(
                    data["code"]
                )

    def test_update_archived(self):
        for user, project in (
            (self.node_admin.username, self.project_node_admin),
            (self.staff.username, self.project1),
            (self.pl_user.username, self.project1),
        ):
            self.client.login(username=user, password=UserFactory.USER_PASSWORD)
            project.archived = True
            project.save()
            url = get_project_detail_url(project)
            payload = self.users_payload(
                (
                    ProjectRole.USER,
                    ProjectRole.DM,
                    ProjectRole.PM,
                )
            )
            payload["destination"] = project.destination.code
            payload["name"] = "New Name"
            payload["code"] = project.code
            self.assertContains(
                self.client.put(
                    url,
                    payload,
                    content_type=APPLICATION_JSON,
                ),
                ARCHIVED_READ_ONLY_MESSAGE,
                status_code=status.HTTP_400_BAD_REQUEST,
            )
            self.client.logout()

    def test_assign_inactive_user(self):
        UserFactory.add_role(self.project1, self.new_user, ProjectRole.DM)
        self.client.login(
            username=self.permission_manager.username,
            password=UserFactory.USER_PASSWORD,
        )
        self.new_user.is_active = False
        self.new_user.save()
        url = get_project_detail_url(self.project1)
        response = self.client.put(
            url,
            self.users_payload((ProjectRole.USER,)),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.logout()

    def test_archive_unarchive(self):
        UserFactory.add_role(self.project_node_admin, self.new_user, ProjectRole.DM)
        UserFactory.add_role(self.project_node_admin, self.new_user, ProjectRole.USER)
        UserFactory.add_role(self.project_node_admin, self.pl_user, ProjectRole.PL)
        UserFactory.add_role(self.project_node_admin, self.pl_user, ProjectRole.USER)
        UserFactory.add_role(self.project_node_admin, self.basic_user, ProjectRole.USER)

        archive_url = get_project_archive_url(self.project_node_admin)
        unarchive_url = get_project_unarchive_url(self.project_node_admin)
        for user in (self.staff.username, self.node_admin.username):
            self.client.login(username=user, password=UserFactory.USER_PASSWORD)

            response = self.client.put(archive_url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTrue(response.json()["archived"])
            self.assertTrue(Project.objects.get(pk=self.project_node_admin.pk).archived)

            self.assertEqual(len(mail.outbox), 2)

            assert_that(mail.outbox[0]).has_body(
                project_archive_users_notification(self.project_node_admin.name)
            )
            self.assertEqual(
                sorted(mail.outbox[0].to),
                [
                    "new_user@example.org",
                    "project-user@example.org",
                    "user-project-leader@example.org",
                ],
            )

            assert_that(mail.outbox[1]).has_body(
                "\n".join(
                    (
                        "Project Name: na_project",
                        "Project Code: p_na",
                        "",
                        "Roles and Users:",
                        "PL: user-project-leader (user-project-leader@example.org)",
                        "DM: Noob (new_user@example.org)",
                        "USER: Noob (new_user@example.org), user-project-leader (user-project-leader@example.org), User Basic (project-user@example.org)",
                    )
                )
            ).has_to(["3@nodes.skynet"])
            mail.outbox = []

            response = self.client.put(unarchive_url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertFalse(response.json()["archived"])
            self.assertFalse(
                Project.objects.get(pk=self.project_node_admin.pk).archived
            )

            self.assertEqual(len(mail.outbox), 0)

            self.client.logout()

    def test_archive_unarchive_unauthorized(self):
        archive_url = get_project_archive_url(self.project1)
        unarchive_url = get_project_unarchive_url(self.project1)
        for user in (
            self.basic_user.username,
            self.node_admin.username,  # of a different node
            self.pl_user.username,
        ):
            self.client.login(username=user, password=UserFactory.USER_PASSWORD)

            response = self.client.put(archive_url)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            self.assertFalse(Project.objects.get(pk=self.project1.pk).archived)

            response = self.client.put(unarchive_url)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            self.assertFalse(Project.objects.get(pk=self.project1.pk).archived)

            self.client.logout()

    def test_archive_dtr_expired(self):
        data_transfer = DataTransferFactory.create()
        archive_url = get_project_archive_url(data_transfer.project)
        self.assertEqual(data_transfer.status, DataTransfer.INITIAL)
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        self.client.put(archive_url)
        self.assertEqual(
            DataTransfer.objects.get(pk=data_transfer.pk).status, DataTransfer.EXPIRED
        )
        self.client.logout()
