from assertpy import assert_that
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from identities.permissions import perm_change_user, has_change_user_permission
from projects.apps import APP_NAME
from projects.models import (
    Project,
    UserNamespace,
    ProjectUserRole,
    ProjectRole,
    DataProvider,
    Flag,
)
from projects.permissions.node import has_node_admin_nodes
from projects.tests.factories import NodeFactory
from projects.tests.views import (
    make_node,
    UserFactory,
    APPLICATION_JSON,
    make_node_admin,
)
from projects.tests.views.test_pgpkey import TEST_CONFIG

CLINERION_FLAG_NAME = "clinerion"
User = get_user_model()


class TestUserView(TestCase):
    def setUp(self):
        self.project1 = Project.objects.create(
            gid=1500001, name="Project", code="p1", destination=make_node()
        )
        self.project2 = Project.objects.create(
            gid=1500002,
            name="Project Number 2",
            code="p2",
            destination=make_node(2),
        )
        self.basic_user = UserFactory.create_project_user(self.project1)
        self.staff = UserFactory.create_staff()
        self.pm_user = UserFactory.create_permission_manager(self.project1)
        self.dm_user = UserFactory.create_data_manager(self.project1)
        self.user_with_perm = UserFactory.create_user_with_perms(
            "user-with-perm", (perm_change_user,)
        )
        settings.CONFIG = TEST_CONFIG
        self.flag = Flag.objects.create(code=CLINERION_FLAG_NAME)
        self.flag.save()
        self.node = NodeFactory.create(code="user-node", name="User Node")
        self.node_admin = UserFactory.create_node_admin("user-node-admin", self.node)

    def test_view_list(self):
        url = reverse(f"{APP_NAME}:user-list")
        # Basic user
        self.client.login(
            username=self.basic_user.username, password=UserFactory.USER_PASSWORD
        )
        r = self.client.get(url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 1)
        self.assertEqual(r.json()[0]["username"], self.basic_user.username)
        self.client.logout()
        # Powerusers
        for username in (
            self.staff.username,
            self.user_with_perm.username,
            self.pm_user.username,
            self.node_admin,
        ):
            self.client.login(username=username, password=UserFactory.USER_PASSWORD)
            # All
            r = self.client.get(url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), User.objects.count())
            # Filter by username / email
            r = self.client.get(url, {"search": self.basic_user.username})
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), 1)
            self.client.logout()

    def test_view_list_hide_not_active_users(self):
        url = reverse(f"{APP_NAME}:user-list")
        user_count = User.objects.count()
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )

        self.assertEqual(len(self.client.get(url).json()), user_count)

        # inactive users are hidden by default
        self.basic_user.is_active = False
        self.basic_user.save()
        self.assertEqual(len(self.client.get(url).json()), user_count - 1)

        # adding request parameter includes inactive users as well
        self.assertEqual(
            len(self.client.get(url, {"include_inactive": True}).json()), user_count
        )

        self.client.logout()

    def test_view_list_user_project_role_filter(self):
        url = reverse(f"{APP_NAME}:user-list")
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        ProjectUserRole.objects.create(
            project=self.project1, user=self.staff, role=ProjectRole.USER.value
        )
        ProjectUserRole.objects.create(
            project=self.project2, user=self.staff, role=ProjectRole.USER.value
        )
        Project.objects.create(
            name="Project Number 3",
            code="p3",
            destination=make_node(3),
        )

        user = ProjectRole.USER.name
        dm = ProjectRole.DM.name
        pl = ProjectRole.PL.name

        for (parameters, expected_user_ids) in (
            ({"project_id": 1}, [2, 3, 4, 5]),
            ({"project_id": 2}, [3]),
            ({"project_id": 3}, []),
            ({"role": user}, [2, 3]),
            ({"role": dm}, [5]),
            ({"role": pl}, []),
            ({"project_id": 1, "role": user}, [2, 3]),
            ({"project_id": 2, "role": user}, [3]),
            ({"project_id": 2, "role": dm}, []),
        ):
            response = self.client.get(url, parameters).json()
            assert_that(response).extracting("id").is_equal_to(expected_user_ids)

        self.client.logout()

    def test_update_as_user_with_perm(self):
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.basic_user.pk})

        self.client.login(
            username=self.user_with_perm.username, password=UserFactory.USER_PASSWORD
        )
        self.assertTrue(has_change_user_permission(self.user_with_perm))

        with self.subTest(
            "Changing 'local_username' and/or 'uid' possible if "
            "the user has the permission 'identities.change_user'"
        ):
            for expected_profile in (
                {"uid": 1000020},
                {"local_username": "new_user_local"},
                {"local_username": "cookie_monster", "uid": 1000050},
            ):
                change = {"profile": expected_profile}
                r = self.client.patch(
                    url,
                    change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(r.status_code, status.HTTP_200_OK)
                # Verify that user has been updated
                profile = User.objects.get(pk=self.basic_user.pk).profile
                assert_that(expected_profile).is_subset_of(profile.__dict__)

        with self.subTest(
            "Updating 'flags' possible if the user has the permission 'identities.change_user'"
        ):
            # Ensure flag `CLINERION_FLAG_NAME` does NOT have any user
            flag = Flag.objects.get(code=CLINERION_FLAG_NAME)
            assert_that(flag.users.all()).is_length(0)
            change = {"flags": [CLINERION_FLAG_NAME]}
            r = self.client.patch(
                url,
                change,
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            # Verify that user has been updated
            flag = Flag.objects.get(code=CLINERION_FLAG_NAME)
            users = flag.users.all()
            assert_that(users).is_length(1)
            assert_that(users[0]).is_equal_to(self.basic_user)

        with self.subTest(
            "Even with the permission 'identities.change_user' you can only "
            "update some specific user properties. Everything else will be ignored."
        ):
            for change in (
                # Non-updatable fields
                {"profile": {"affiliation_id": "21501@fhnw.ch"}},
                {"first_name": "Kirk"},
                # Non-existing fields will be ignored
                {"not_a_user_field": 12345},
            ):
                r = self.client.patch(
                    url,
                    change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(r.status_code, status.HTTP_200_OK)
                user = User.objects.get(pk=self.basic_user.pk)
                assert_that(user).is_equal_to(self.basic_user)

            new_local_username = "new_user_local"
            r = self.client.patch(
                url,
                {
                    "profile": {
                        "affiliation_id": "21501@fhnw.ch",
                        "local_username": new_local_username,
                    },
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            profile = User.objects.get(pk=self.basic_user.pk).profile
            assert_that(profile).has_local_username(
                new_local_username
            ).has_affiliation_id(self.basic_user.profile.affiliation_id)

        with self.subTest(
            "Even with the permission 'identities.change_user' you can only "
            "use method PATCH"
        ):
            r = self.client.put(
                url,
                {"profile": {"local_username": "new_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

        self.client.logout()

    def test_update_as_user(self):
        default_namespace = "ch"
        user = User.objects.get(username=self.pm_user.username)
        user.profile.local_username = "user_local"
        user.profile.namespace = UserNamespace.objects.create(name=default_namespace)
        user.profile.save()
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.basic_user.pk})

        self.client.login(
            username=self.basic_user.username, password=UserFactory.USER_PASSWORD
        )
        with self.subTest("local_username is missing"):
            for value in ("", None):
                r = self.client.patch(
                    url,
                    {"profile": {"local_username": value}},
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
            r = self.client.patch(url, {"profile": {}}, content_type=APPLICATION_JSON)
            self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

        with self.subTest(
            "should not be allowed to use modifying methods other than PATCH"
        ):
            r = self.client.put(
                url,
                {"profile": {"local_username": "my_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
            self.assertDictEqual(
                r.json(),
                {"detail": "You do not have permission to perform this action."},
            )

        with self.subTest("local_username and namespace must be unique together"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": user.profile.local_username}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_409_CONFLICT)
            response = r.json()
            self.assertEqual(
                response["detail"], "A user with the specified username already exists"
            )
            r = self.client.patch(
                url,
                {
                    "first_name": "Chuck",
                    "profile": {"local_username": "my_user_local"},
                },
                content_type=APPLICATION_JSON,
            )
            response = r.json()
            self.assertEqual(response["profile"]["namespace"], default_namespace)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(response["profile"]["local_username"], "my_user_local")
            self.assertEqual(response["first_name"], "User")

        with self.subTest("Changing local_username is forbidden"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": "new_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
            self.assertDictEqual(
                r.json(),
                {"detail": "You do not have permission to perform this action."},
            )

        with self.subTest("Ignore explicit namespace"):
            self.client.login(
                username=self.dm_user.username, password=UserFactory.USER_PASSWORD
            )
            r = self.client.patch(
                reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.dm_user.pk}),
                {"profile": {"local_username": "dm_username", "namespace": "foo"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(r.json()["profile"]["namespace"], default_namespace)
            self.assertEqual(r.json()["profile"]["local_username"], "dm_username")

        self.client.logout()

    def test_update_as_staff(self):
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.basic_user.pk})
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )

        with self.subTest("should not be allowed to set username of someone else"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": "my_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
            self.assertDictEqual(
                r.json(),
                {"detail": "You do not have permission to perform this action."},
            )

        with self.subTest(
            "should remove objects associated to the user when setting `is_active` to `False`"
        ):
            p3_node = make_node(3)
            active_user = UserFactory.create_node_admin("active-user", p3_node)

            url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": active_user.pk})

            make_node_admin(self.staff, p3_node)
            users = (active_user, self.staff)

            p3 = Project.objects.create(
                gid=1500003,
                name="Project Number 3",
                code="p3",
                destination=p3_node,
            )
            ProjectUserRole.objects.create(
                project=p3,
                user=active_user,
                role=ProjectRole.USER.value,
            )
            ProjectUserRole.objects.create(
                project=p3,
                user=active_user,
                role=ProjectRole.DM.value,
            )
            p3_staff_pl = ProjectUserRole.objects.create(
                project=p3,
                user=self.staff,
                role=ProjectRole.PL.value,
            )

            p4 = Project.objects.create(
                gid=1500004,
                name="Project Number 4",
                code="p4",
                destination=make_node(4),
            )
            p4_staff_pl = ProjectUserRole.objects.create(
                project=p4,
                user=self.staff,
                role=ProjectRole.PL.value,
            )

            p5 = Project.objects.create(
                gid=1500005,
                name="Project Number 5",
                code="p5",
                destination=make_node(5),
            )
            ProjectUserRole.objects.create(
                project=p5,
                user=active_user,
                role=ProjectRole.USER.value,
            )

            # project without ProjectUserRoles to check for raised exceptions
            Project.objects.create(
                gid=1500006,
                name="Project Number 6",
                code="p6",
                destination=make_node(6),
            )

            data_provider = DataProvider.objects.create(
                name="Provider", code="provider", node=p3_node
            )
            data_provider.users.set(users)
            data_provider.save()

            flag = Flag.objects.create(code="Flag")
            flag.users.set(users)
            flag.save()

            self.assertTrue(has_node_admin_nodes(self.staff))
            self.assertTrue(has_node_admin_nodes(active_user))

            r = self.client.patch(
                url,
                {"is_active": False},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)

            email = mail.outbox

            assert_that(email).is_length(3).extracting("subject").is_equal_to(
                [
                    "Portal: Permissions changed in 'Project Number 3'",
                    "Portal: User(s) added to/removed from project 'Project Number 3'",
                    "Portal: User(s) added to/removed from project 'Project Number 5'",
                ]
            )

            self.assertEqual(
                ProjectUserRole.objects.filter(project=p3).get(), p3_staff_pl
            )
            self.assertEqual(
                ProjectUserRole.objects.filter(project=p4).get(), p4_staff_pl
            )
            self.assertEqual(ProjectUserRole.objects.filter(project=p5).count(), 0)

            self.assertTrue(has_node_admin_nodes(self.staff))
            self.assertFalse(has_node_admin_nodes(active_user))
            self.assertEqual(data_provider.users.get(), self.staff)
            self.assertEqual(flag.users.get(), self.staff)

        with self.subTest("should be able to change `is_active` to `True` again"):
            r = self.client.patch(
                url,
                {"is_active": True},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)

        self.client.logout()

    def test_create_local_user(self):
        url = reverse(f"{APP_NAME}:user-list")
        data = {
            "first_name": "Chuck",
            "last_name": "Norris",
            "email": "chuck@norris.gov",
            "profile": {"local_username": "chuck_norris"},
            "flags": [],
        }

        with self.subTest("Creating local user as non-staff user is forbidden"):
            self.client.login(
                username=self.basic_user.username, password=UserFactory.USER_PASSWORD
            )
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                r.status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

        with self.subTest(
            "Creating local user as user with 'identities.change_user' is forbidden"
        ):
            self.client.login(
                username=self.user_with_perm.username,
                password=UserFactory.USER_PASSWORD,
            )
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                r.status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

        with self.subTest("Create local user as staff"):
            self.client.login(
                username=self.staff.username, password=UserFactory.USER_PASSWORD
            )
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            response = r.json()
            self.assertEqual(r.status_code, status.HTTP_201_CREATED)
            self.assertEqual(response["first_name"], data["first_name"])
            self.assertEqual(response["last_name"], data["last_name"])
            self.assertEqual(response["email"], data["email"])
            self.assertEqual(response["username"], data["email"])
            self.assertIsNone(response["profile"]["local_username"])
            created_local_user = User.objects.get(id=response["id"])
            # make sure user cannot be used to log in
            self.assertFalse(created_local_user.has_usable_password())
            self.client.logout()

        with self.subTest("Cannot create user with non-unique email address"):
            self.client.login(
                username=self.staff.username, password=UserFactory.USER_PASSWORD
            )
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(
                r.json(), {"email": ["user with this email address already exists."]}
            )
            self.client.logout()
