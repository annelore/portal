from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.models import UserNamespace
from projects.tests.views import UserFactory
from projects.tests.views.test_data_transfer import assert_access_forbidden

URL = reverse(f"{APP_NAME}:user-namespace-list")


class TestUserNamespaceView(TestCase):
    def setUp(self):
        self.basic_user = UserFactory.create_basic()
        self.staff = UserFactory.create_staff()
        self.namespace = UserNamespace.objects.create(name="ch")

    def test_view_list(self):
        # Unauthenticated is not allowed
        self.assertEqual(self.client.get(URL).status_code, status.HTTP_403_FORBIDDEN)
        # Normal can see namespaces
        self.client.login(
            username=self.basic_user.username, password=UserFactory.USER_PASSWORD
        )
        r = self.client.get(URL)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 1)
        self.assertDictEqual(
            r.json()[0], {"id": self.namespace.id, "name": self.namespace.name}
        )
        self.client.logout()

    def test_write_methods_not_allowed(self):
        # POST, PUT, DELETE are not allowed
        assert_access_forbidden(
            self,
            url=URL,
            users=[
                (u, status.HTTP_405_METHOD_NOT_ALLOWED)
                for u in (
                    self.basic_user.username,
                    self.staff.username,
                )
            ],
            methods=("post", "put", "delete"),
        )
