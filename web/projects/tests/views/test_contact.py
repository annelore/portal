from typing import Optional

import pytest
from assertpy import assert_that
from django.contrib.auth import get_user_model
from django.core import mail
from django.core.cache import cache
from django.core.mail import EmailMessage
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.serializers.contact import (
    UNAUTHENTICATED_MISSING_USER_INFO_ERROR,
    CONTACT_FORM_SUBJECT_PREFIX,
)
from projects.tests.views.test_pgpkey import TEST_CONFIG

User = get_user_model()

URL = reverse(f"{APP_NAME}:contact-list")

USER_FIRST_NAME = "Chuck"
USER_LAST_NAME = "Norris"
USER_EMAIL = "chuck.norris@roundhouse.kick"

EMAIL_SUBJECT = "Help please?"
EMAIL_MESSAGE = (
    "Just kidding:\nChuck Norris doesn't need help, Chuck Norris helps others."
)

EXPECTED_SUBJECT = f"Portal: {CONTACT_FORM_SUBJECT_PREFIX}{EMAIL_SUBJECT}"
EXPECTED_BODY = (
    f"First Name: {USER_FIRST_NAME}\n"
    f"Last Name: {USER_LAST_NAME}\n"
    f"Email Address: {USER_EMAIL}\n"
    "\n"
    "Message:\n"
    f"{EMAIL_MESSAGE}"
)


def _get_data(
    with_user_data: Optional[bool] = False, send_copy: Optional[bool] = False
):
    data = {
        "subject": EMAIL_SUBJECT,
        "message": EMAIL_MESSAGE,
        "send_copy": send_copy,
    }
    if with_user_data:
        data["first_name"] = USER_FIRST_NAME
        data["last_name"] = USER_LAST_NAME
        data["email"] = USER_EMAIL
    return data


def _assert_email(email: EmailMessage, cc: bool = False):
    assert email.subject == EXPECTED_SUBJECT
    assert email.body == EXPECTED_BODY

    assert email.from_email == TEST_CONFIG.email.from_address
    assert email.to == [TEST_CONFIG.notification.contact_form_recipient]
    assert email.reply_to == [USER_EMAIL]
    if cc:
        assert email.cc == [USER_EMAIL]


# pylint: disable=no-self-use
class TestUnauthenticated:
    @pytest.fixture(autouse=True)
    def apply_test_settings(self, settings):
        settings.CONFIG = TEST_CONFIG
        yield
        # reset throttling between tests
        # caches are not cleared by default:
        # https://docs.djangoproject.com/en/3.2/topics/testing/overview/#other-test-conditions
        cache.clear()

    def test_get_not_allowed(self, client):
        assert client.get(URL).status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_missing_user_info(self, client):
        response = client.post(URL, data=_get_data(), content_type="application/json")
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert_that(response.json()).contains_value(
            [UNAUTHENTICATED_MISSING_USER_INFO_ERROR]
        )
        assert_that(mail.outbox).is_length(0)

    @pytest.mark.django_db
    def test_success(self, client):
        data = _get_data(True, False)
        response = client.post(URL, data=data, content_type="application/json")
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json() == data
        assert_that(mail.outbox).is_length(1)
        _assert_email(mail.outbox[0])

        # unauthenticated users should get throttled
        response = client.post(URL, data=data, content_type="application/json")
        assert response.status_code == status.HTTP_429_TOO_MANY_REQUESTS
        assert "Request was throttled." in response.json()["detail"]
        assert_that(mail.outbox).is_length(1)  # no emails have been sent


class TestWithAuth:
    @pytest.fixture(autouse=True)
    def setup(self, settings, basic_user_auth: User):
        settings.CONFIG = TEST_CONFIG

        basic_user_auth.first_name = USER_FIRST_NAME
        basic_user_auth.last_name = USER_LAST_NAME
        basic_user_auth.email = USER_EMAIL
        basic_user_auth.save()

    def test_get_not_allowed(self, client):
        assert client.get(URL).status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_missing_user_info_success(self, client):
        data = _get_data()
        response = client.post(URL, data=data, content_type="application/json")
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json() == _get_data(True)  # user info should be inferred
        assert_that(mail.outbox).is_length(1)
        _assert_email(mail.outbox[0])

        # authenticated users should NOT get throttled
        response = client.post(URL, data=data, content_type="application/json")
        assert response.status_code == status.HTTP_201_CREATED
        assert_that(mail.outbox).is_length(2)

    def test_send_copy(self, client):
        data = _get_data(False, True)
        response = client.post(URL, data=data, content_type="application/json")
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json() == _get_data(True, True)  # user info should be inferred
        assert_that(mail.outbox).is_length(1)
        _assert_email(mail.outbox[0], True)
