from smtplib import SMTPException
from unittest.mock import patch

from assertpy import assert_that
from django.conf import settings
from django.core import mail
from rest_framework import status

from projects.models import ProjectRole, User
from projects.tests.views import UserFactory, APPLICATION_JSON
from projects.tests.views.test_project import (
    get_project_detail_url,
    ProjectViewTestMixin,
)
from projects.tests.views.test_pgpkey import TEST_CONFIG

EXCEPTION_STACK_TRACE = "Chuck Norris roundhouse kicked the server again."


class TestProjectNotification(ProjectViewTestMixin):
    def setUp(self):
        super().setUp()
        self.new_user_initial = User.objects.create_user(
            "Initial Noob", "new_user_initial@example.org"
        )
        settings.CONFIG = TEST_CONFIG

    def test_add_user_notification(self):
        project = self.project1
        url = get_project_detail_url(project)
        self.client.login(
            username=self.permission_manager.username,
            password=UserFactory.USER_PASSWORD,
        )
        for usr in [self.new_user, self.new_user_initial]:
            assert usr not in {
                p.user for p in project.users.all()
            }, f"'{usr}' already assigned in project '{project}.'"
            expected_roles = (ProjectRole.USER,)
            # PM adds adds permission USER for new_user
            response = self.client.put(
                url,
                self.users_payload(expected_roles, usr),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assert_user_has_roles(usr, expected_roles)
            emails = {(m.subject, tuple(m.to)) for m in mail.outbox}
            assert emails == {
                # email to PL
                (
                    f"Portal: Permissions changed in '{self.project1.name}'",
                    (UserFactory.get_email(self.pl_user.username),),
                ),
                # email to Node
                (
                    f"Portal: User(s) added to/removed from "
                    f"project '{self.project1.name}'",
                    (self.destination1.ticketing_system_email,),
                ),
            }
        self.client.logout()

    def test_update_permission_add_and_delete(self):
        UserFactory.add_role(self.project1, self.new_user, ProjectRole.DM)
        self.client.login(
            username=self.permission_manager.username,
            password=UserFactory.USER_PASSWORD,
        )
        expected_roles = (ProjectRole.USER,)
        self.put_expected_roles_to_new_user(expected_roles)
        assert len(mail.outbox) == 1, "Sent one email"
        email = mail.outbox[0]
        assert email.subject == f"Portal: Permissions changed in '{self.project1.name}'"
        assert email.to == [UserFactory.get_email(self.pl_user.username)]
        pm_email = UserFactory.get_email(self.permission_manager.username)
        assert_that(email.body).contains(
            f"Project name: {self.project1.name}",
            f"Changed by: {self.permission_manager.username} ({pm_email})",
        )
        # TODO: refactor using contains_sequence? https://github.com/assertpy/assertpy/issues/108
        # make sure "Added permissions" are mentioned before "Deleted permissions"
        assert_that(email.body).matches(
            r"Added permissions\:\n"
            fr"User\: {self.new_user.username} \({self.new_user.email}\)\t "
            r"Permission\: User"
            r"[\n\r\s]+"
            r"Deleted permissions\:\n"
            fr"User\: {self.new_user.username} \({self.new_user.email}\)\t "
            r"Permission\: Data Manager"
        )
        self.client.logout()

    def test_update_permission_add(self):
        with self.pm_add_permission_to_user():
            email = mail.outbox[0]
            assert_that(email.body).contains(
                "Added permissions",
                self.new_user.username,
                self.new_user.email,
                "Permission: User",
            ).does_not_contain("Deleted permissions", "Permission: Data Manager")

    def test_update_permission_delete(self):
        UserFactory.add_role(self.project1, self.new_user, ProjectRole.USER)
        UserFactory.add_role(self.project1, self.new_user, ProjectRole.DM)
        self.client.login(
            username=self.permission_manager.username,
            password=UserFactory.USER_PASSWORD,
        )
        expected_roles = (ProjectRole.USER,)
        self.put_expected_roles_to_new_user(expected_roles)
        email = mail.outbox[0]
        assert_that(email.body).contains(
            "Deleted permissions",
            self.new_user.username,
            self.new_user.email,
            "Permission: Data Manager",
        ).does_not_contain("Added permissions", "Permission: User")
        self.client.logout()

    @patch(
        "django.core.mail.EmailMessage.send",
        side_effect=SMTPException(EXCEPTION_STACK_TRACE),
    )
    def test_update_permission_smtp_error(self, _):
        project = self.project1
        url = get_project_detail_url(project)
        self.client.login(
            username=self.permission_manager.username,
            password=UserFactory.USER_PASSWORD,
        )
        expected_roles = (ProjectRole.USER,)
        # sending email will result in SMTPException
        with self.assertLogs("django_drf_utils.email", level="ERROR") as cm:
            response = self.client.put(
                url,
                self.users_payload(expected_roles),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
            assert_that(cm.output[0]).contains(
                "ERROR",
                "Failed to send email notification",
                f"Exception: {EXCEPTION_STACK_TRACE}",
            )
            self.client.logout()

    @patch("projects.views.project.update_permissions_signal.send")
    def test_delete(self, mock):
        url = get_project_detail_url(self.project1)
        # Only staff can delete projects
        self.assertEqual(self.client.delete(url).status_code, status.HTTP_403_FORBIDDEN)
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        self.assertEqual(
            self.client.delete(url).status_code, status.HTTP_204_NO_CONTENT
        )
        self.client.logout()
        # Verify signal was triggered
        mock.assert_called_once()
