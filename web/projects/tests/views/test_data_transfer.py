from typing import Optional
from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.models import (
    Project,
    UserNamespace,
    DataTransfer,
    DataProvider,
    Node,
    User,
    PgpKeySignStatus,
    PgpKey,
    DataPackage,
    ProjectRole,
    ProjectUserRole,
)
from projects.serializers.data_transfer import (
    CREATE_MESSAGE_ARCHIVED_PROJECT,
    UPDATE_MESSAGE_ARCHIVED_PROJECT,
)
from projects.tests.views import make_node, UserFactory


def mock_fetch_pgp_keys(keys_by_searchterm: dict):
    def _fetch_pgp_keys(searchterm, *_):
        ret = keys_by_searchterm.get(searchterm)
        if ret is None:
            return []
        return [ret]

    return _fetch_pgp_keys


class TransferViewTestBase(TestCase):
    def setUp(self):
        self.namespace = UserNamespace.objects.create(name="ch")
        self.dest_node = make_node()
        self.project = Project.objects.create(
            gid=1500001, name="Project X", code="p1", destination=self.dest_node
        )
        self.basic_user = UserFactory.create_project_user(self.project)
        self.pl_user = UserFactory.create_project_leader(self.project)
        self.staff = UserFactory.create_staff()
        ProjectUserRole.objects.create(
            project=self.project, user=self.staff, role=ProjectRole.DM.value
        )
        self.node_viewer = UserFactory.create_node_viewer(
            "user-node-viewer", self.dest_node
        )
        self.data_provider = DataProvider.objects.create(
            name="Provider", code="provider", node=self.dest_node
        )
        self.requestor = User.objects.create_user(
            username="user-requestor", password=UserFactory.USER_PASSWORD
        )
        ProjectUserRole.objects.create(
            project=self.project, user=self.requestor, role=ProjectRole.DM.value
        )
        self.requestor_pgp_key_fp = "A0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9"
        self.purpose = DataTransfer.PRODUCTION
        self.transfer = self.create_transfer(1, self.purpose)
        self.node = Node.objects.create(
            code="area51", name="Area 51", node_status=Node.STATUS_ONLINE
        )

    def create_transfer(self, id_, purpose, project: Optional[Project] = None):
        return DataTransfer.objects.create(
            id=id_,
            project=project or self.project,
            data_provider=self.data_provider,
            requestor=self.requestor,
            requestor_pgp_key_fp=self.requestor_pgp_key_fp,
            status=DataTransfer.AUTHORIZED,
            purpose=purpose,
        )

    def get_key(self, sign_status: PgpKeySignStatus) -> PgpKey:
        return PgpKey(
            fingerprint=self.requestor_pgp_key_fp,
            sign_status=sign_status,
            pgpkey_id="YYYY",
            key_name="name",
            email="requestor@users.org",
            key_length=4096,
        )


class TestDataTransferView(TransferViewTestBase):
    reverse_url = f"{APP_NAME}:datatransfer"
    url = reverse(reverse_url + "-list")

    def response_json(
        self, id_: Optional[int] = None, project: Optional[Project] = None
    ):
        if not project:
            project = self.project
        return {
            "id": id_ or self.transfer.id,
            "project": project.id,
            "project_archived": False,
            "project_name": project.name,
            "max_packages": self.transfer.max_packages,
            "status": self.transfer.status,
            "data_provider": self.data_provider.code,
            "requestor": self.requestor.id,
            "requestor_pgp_key_fp": self.requestor_pgp_key_fp,
            "requestor_name": self.requestor.username,
            "requestor_first_name": self.requestor.first_name,
            "requestor_last_name": self.requestor.last_name,
            "requestor_email": self.requestor.email,
            "requestor_display_id": None,
            "purpose": self.purpose,
            "packages": [],
        }

    def test_view_list(self):
        dm_user = UserFactory.create_data_manager(self.project)
        data_provider = UserFactory.create_data_provider("dp-user", self.data_provider)
        response_json = self.response_json()
        for username, expected_json in (
            (self.staff.username, [response_json]),
            (dm_user.username, [response_json]),
            (self.pl_user.username, [response_json]),
            (self.node_viewer.username, [response_json]),
            (data_provider.username, [response_json]),
        ):
            with self.subTest(username=username):
                self.client.login(username=username, password=UserFactory.USER_PASSWORD)
                r = self.client.get(self.url)
                self.assertEqual(r.status_code, status.HTTP_200_OK)
                self.assertEqual(r.json(), expected_json)
                self.client.logout()
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_list_with_node_admin(self):
        na_user = UserFactory.create_node_admin("user-node-admin", self.dest_node)
        data_provider_node = "data-provider-node"
        UserFactory.create_node_admin(data_provider_node, self.node)
        expected_json = self.response_json()
        # 'self.node' NOT associated to DP yet
        for username, answer_json in (
            (na_user.username, [expected_json]),
            (data_provider_node, []),
        ):
            with self.subTest(username=username):
                self.client.login(username=username, password=UserFactory.USER_PASSWORD)
                r = self.client.get(self.url)
                self.assertEqual(r.status_code, status.HTTP_200_OK)
                self.assertEqual(r.json(), answer_json)
                self.client.logout()
        # 'self.node' associated to DP NOW
        self.data_provider.node = self.node
        self.client.login(
            username=data_provider_node, password=UserFactory.USER_PASSWORD
        )
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.json(), answer_json)
        self.client.logout()

    def test_view_list_with_node_viewer(self):
        nv_user = UserFactory.create_node_viewer("user-node-viewer-2", self.node)
        project_2 = Project.objects.create(
            gid=1500002, name="Project XI", code="p2", destination=self.node
        )
        ProjectUserRole.objects.create(
            project=project_2, user=self.requestor, role=ProjectRole.DM.value
        )
        dtr_id_2 = 2
        self.create_transfer(dtr_id_2, self.purpose, project_2)
        self.client.login(username=nv_user.username, password=UserFactory.USER_PASSWORD)
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        # doesn't include transfer with id 1
        self.assertEqual(r.json(), [self.response_json(dtr_id_2, project_2)])
        self.client.logout()

    def test_write_methods_not_allowed(self):
        # PUT, DELETE are not allowed
        self.assertEqual(
            self.client.post(self.url).status_code, status.HTTP_403_FORBIDDEN
        )
        assert_access_forbidden(
            self,
            self.url,
            (
                (self.basic_user.username, status.HTTP_403_FORBIDDEN),
                (self.staff.username, status.HTTP_405_METHOD_NOT_ALLOWED),
                (self.node_viewer.username, status.HTTP_403_FORBIDDEN),
            ),
            methods=("put", "delete"),
            test_unauthorized=False,
        )

    # TODO: check if can create or delete data transfer of archived project
    def test_create(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "requestor_pgp_key_fp": self.requestor_pgp_key_fp,
            "max_packages": -1,
            "purpose": self.purpose,
        }
        with patch(
            "projects.utils.gpg.fetch_pgp_keys",
            mock_fetch_pgp_keys(
                {
                    ("0x" + self.requestor_pgp_key_fp): self.get_key(
                        PgpKeySignStatus.SIGNED
                    )
                }
            ),
        ):
            dm_user = UserFactory.create_data_manager(self.project)
            check_method(
                self,
                "post",
                self.reverse_url,
                allowed=(self.staff.username,),
                forbidden=(
                    self.basic_user.username,
                    self.node_viewer.username,
                    dm_user.username,
                ),
                data=data,
                content_type="application/json",
            )

    def test_create_with_wrong_status(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "requestor": self.requestor.id,
            "requestor_pgp_key_fp": self.requestor_pgp_key_fp,
            "purpose": self.purpose,
            "max_packages": -1,
            "status": DataTransfer.AUTHORIZED,  # not allowed
        }
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        self.assertEqual(
            self.client.post(
                self.url, data=data, content_type="application/json"
            ).status_code,
            status.HTTP_400_BAD_REQUEST,
        )
        self.client.logout()

    def test_create_requestor_not_dm(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "purpose": self.purpose,
            "requestor": User.objects.create_user(
                username="non-dm-requestor",
                password=UserFactory.USER_PASSWORD,
                email="non@dm.com",
            ).id,
            "requestor_pgp_key_fp": self.requestor_pgp_key_fp,
            "max_packages": -1,
            "status": DataTransfer.INITIAL,
        }
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        with patch(
            "projects.utils.gpg.fetch_pgp_keys",
            mock_fetch_pgp_keys(
                {
                    ("0x" + self.requestor_pgp_key_fp): self.get_key(
                        PgpKeySignStatus.SIGNED
                    )
                }
            ),
        ):
            self.assertContains(
                self.client.post(self.url, data=data, content_type="application/json"),
                "Requestor is not a data manager of the project",
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        self.client.logout()

    def test_create_as_user_with_requestor(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "purpose": self.purpose,
            "requestor_pgp_key_fp": self.requestor_pgp_key_fp,
            "max_packages": -1,
            "status": DataTransfer.INITIAL,
        }
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        with patch(
            "projects.utils.gpg.fetch_pgp_keys",
            mock_fetch_pgp_keys(
                {
                    ("0x" + self.requestor_pgp_key_fp): self.get_key(
                        PgpKeySignStatus.SIGNED
                    )
                }
            ),
        ):
            # allowed:
            response = self.client.post(
                self.url, data=data, content_type="application/json"
            )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            # takes current user as not specified
            self.assertEqual(response.data["requestor"], self.staff.id)
        self.client.logout()

    def test_create_with_not_signed_key(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "requestor": self.requestor.id,
            "requestor_pgp_key_fp": self.requestor_pgp_key_fp,
            "max_packages": -1,
            "status": DataTransfer.INITIAL,
        }
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        with patch(
            "projects.utils.gpg.fetch_pgp_keys",
            mock_fetch_pgp_keys(
                {
                    ("0x" + self.requestor_pgp_key_fp): self.get_key(
                        PgpKeySignStatus.UNSIGNED
                    )
                }
            ),
        ):
            self.assertEqual(
                self.client.post(
                    self.url, data=data, content_type="application/json"
                ).status_code,
                status.HTTP_400_BAD_REQUEST,
            )
        with patch(
            "projects.utils.gpg.fetch_pgp_keys",
            mock_fetch_pgp_keys(
                {
                    ("0x" + self.requestor_pgp_key_fp): self.get_key(
                        PgpKeySignStatus.PENDING
                    )
                }
            ),
        ):
            self.assertEqual(
                self.client.post(
                    self.url, data=data, content_type="application/json"
                ).status_code,
                status.HTTP_400_BAD_REQUEST,
            )
        with patch(
            "projects.utils.gpg.fetch_pgp_keys",
            mock_fetch_pgp_keys(
                {
                    ("0x" + self.requestor_pgp_key_fp): self.get_key(
                        PgpKeySignStatus.REVOKED
                    )
                }
            ),
        ):
            self.assertEqual(
                self.client.post(
                    self.url, data=data, content_type="application/json"
                ).status_code,
                status.HTTP_400_BAD_REQUEST,
            )
        self.client.logout()

    def test_create_for_archived_project(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "purpose": self.purpose,
            "requestor_pgp_key_fp": self.requestor_pgp_key_fp,
            "max_packages": -1,
            "status": DataTransfer.INITIAL,
        }
        self.project.archived = True
        self.project.save()
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        self.assertContains(
            self.client.post(self.url, data=data, content_type="application/json"),
            CREATE_MESSAGE_ARCHIVED_PROJECT,
            status_code=status.HTTP_400_BAD_REQUEST,
        )
        self.client.logout()

    def test_patch(self):
        data = {"status": DataTransfer.AUTHORIZED}
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        url = reverse(self.reverse_url + "-detail", kwargs={"id": self.transfer.id})
        self.assertEqual(
            self.client.patch(
                url, data={**data, "max_packages": -1}, content_type="application/json"
            ).status_code,
            status.HTTP_200_OK,
        )
        self.client.logout()
        check_method(
            self,
            "patch",
            self.reverse_url,
            allowed=(self.staff.username,),
            forbidden=(self.basic_user.username, self.node_viewer.username),
            data=data,
            content_type="application/json",
            lookup_field="id",
            ids=(1,),
        )

    def test_update_after_pkg(self):
        DataPackage.objects.create(
            metadata_hash="hash",
            data_transfer=self.transfer,
            purpose=DataTransfer.PRODUCTION,
        )
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        url = reverse(self.reverse_url + "-detail", kwargs={"id": self.transfer.id})
        for data, expected_status_code in (
            ({"purpose": DataTransfer.TEST}, status.HTTP_400_BAD_REQUEST),
            ({"status": DataTransfer.UNAUTHORIZED}, status.HTTP_200_OK),
            ({"max_packages": 1}, status.HTTP_200_OK),
        ):
            self.assertEqual(
                self.client.patch(
                    url, data={**data}, content_type="application/json"
                ).status_code,
                expected_status_code,
            )
        self.client.logout()

    def test_update_for_archived_project(self):
        self.project.archived = True
        self.project.save()
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        url = reverse(self.reverse_url + "-detail", kwargs={"id": self.transfer.id})
        self.assertContains(
            self.client.patch(
                url,
                data={"status": DataTransfer.UNAUTHORIZED},
                content_type="application/json",
            ),
            UPDATE_MESSAGE_ARCHIVED_PROJECT,
            status_code=status.HTTP_400_BAD_REQUEST,
        )
        self.client.logout()


def assert_access_forbidden(
    test_class,
    url,
    users=(),
    test_unauthorized=True,
    methods=("get",),
    return_code=status.HTTP_403_FORBIDDEN,
):
    for method in methods:
        with test_class.subTest(method=method):
            # Unauthenticated is not allowed
            if test_unauthorized:
                test_class.assertEqual(
                    getattr(test_class.client, method)(url).status_code, return_code
                )
            for username, user_return_code in users:
                with test_class.subTest(user=username):
                    test_class.client.login(
                        username=username, password=UserFactory.USER_PASSWORD
                    )
                    test_class.assertEqual(
                        getattr(test_class.client, method)(url).status_code,
                        user_return_code,
                    )
                    test_class.client.logout()


def check_method(
    test_class,
    method,
    url,
    allowed,
    forbidden,
    data=None,
    ids=(),
    test_unauthorized=True,
    lookup_field="pk",
    **kwargs,
):
    m = getattr(test_class.client, method)
    # Unauthenticated is not allowed
    id_kwargs = ({lookup_field: i} for i in ids)
    args = () if data is None else (data,)
    if not ids:
        id_kwargs = ({},)
    status_code = {
        "post": status.HTTP_201_CREATED,
        "delete": status.HTTP_204_NO_CONTENT,
        "patch": status.HTTP_200_OK,
    }[method]
    url_suffix = {"post": "-list", "delete": "-detail", "patch": "-detail"}[method]
    for i_kwargs in id_kwargs:
        url = reverse(url + url_suffix, kwargs=i_kwargs)
        if test_unauthorized:
            test_class.assertEqual(m(url).status_code, status.HTTP_403_FORBIDDEN)
        for username in forbidden:
            test_class.client.login(
                username=username, password=UserFactory.USER_PASSWORD
            )
            test_class.assertEqual(
                status.HTTP_403_FORBIDDEN,
                m(url, *args, **kwargs).status_code,
                f"Wrong HTTP status for username '{username}'",
            )
            test_class.client.logout()
        for username in allowed:
            test_class.client.login(
                username=username, password=UserFactory.USER_PASSWORD
            )
            r = m(url, *args, **kwargs)
            test_class.assertEqual(r.status_code, status_code)
            test_class.client.logout()
