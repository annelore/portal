from assertpy import assert_that

from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME

URL = reverse(f"{APP_NAME}:projectinfo-list")


# Everyone can read the list
def test_list(client, project_factory):
    projects = project_factory.create_batch(5)
    data_ref = [{"name": p.name, "code": p.code} for p in projects]
    response = client.get(URL)
    data = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert len(data) == len(projects)
    assert_that(data).contains_only(*data_ref)


# POST is not allowed
def test_create_not_allowed(client):
    assert client.post(URL).status_code == status.HTTP_405_METHOD_NOT_ALLOWED
