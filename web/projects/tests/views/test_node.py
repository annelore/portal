from assertpy import assert_that
from django.contrib.auth.models import Group
from django.test import TestCase
from django.urls import reverse
from guardian.shortcuts import get_perms
from rest_framework import status

from projects.apps import APP_NAME
from projects.models import Node
from projects.permissions.node import (
    PERM_NODE_VIEWER,
    PERM_NODE_ADMIN,
    PERM_NODE_MANAGER_CHANGE,
    PERM_NODE_MANAGER_VIEW,
)
from projects.tests.views import UserFactory, APPLICATION_JSON
from projects.tests.views.test_data_transfer import check_method

JSON = "application/json"


class TestNodeView(TestCase):
    def setUp(self):
        self.nodes = []
        for i in [1, 2]:
            self.nodes.append(
                Node.objects.create(
                    code=f"area5{i}",
                    name=f"Area 5{i}",
                    node_status=Node.STATUS_ONLINE,
                )
            )
        self.user = UserFactory.create_basic()
        self.staff = UserFactory.create_staff()
        self.node_viewer = UserFactory.create_node_viewer("node-viewer", self.nodes[1])
        # Same node
        self.node_admin_1 = UserFactory.create_node_admin("node-admin-1", self.nodes[0])
        self.node_admin_3 = UserFactory.create_node_admin("node-admin-3", self.nodes[0])
        # Different node
        self.node_admin_2 = UserFactory.create_node_admin("node-admin-2", self.nodes[1])

    def test_node_view_list(self):
        url = reverse(f"{APP_NAME}:node-list")
        # You have to be authenticated
        self.assertEqual(self.client.get(url).status_code, status.HTTP_403_FORBIDDEN)
        for user, filtered in (
            (self.user, []),
            (self.node_admin_1, [self.nodes[0].code]),
            (self.node_viewer, [self.nodes[1].code]),
            (self.staff, []),
        ):
            self.client.login(
                username=user.username, password=UserFactory.USER_PASSWORD
            )
            r = self.client.get(url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), len(self.nodes))
            # If 'username' is specified
            r = self.client.get(f"{url}?username={user}")
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            r_json = r.json()
            assert_that(r_json).extracting("code").is_equal_to(filtered)
            self.client.logout()

    def test_node_create(self):
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        url = reverse(f"{APP_NAME}:node-list")
        node_name = "Chuck"
        data = {
            "ticketing_system_email": "chuck-admin@chuck.ch",
            "code": "chuck",
            "name": node_name,
            "node_status": Node.STATUS_ONLINE,
        }
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_201_CREATED,
        )
        node = Node.objects.get(name=node_name)
        node_viewer = Group.objects.get(name=f"{node_name} Node Viewer")
        node_admin = Group.objects.get(name=f"{node_name} Node Admin")
        node_manager = Group.objects.get(name=f"{node_name} Node Manager")
        assert_that(PERM_NODE_VIEWER).ends_with(get_perms(node_viewer, node)[0])
        assert_that(PERM_NODE_ADMIN).ends_with(get_perms(node_admin, node)[0])
        for group in (node_viewer, node_admin):
            assert_that(PERM_NODE_MANAGER_CHANGE).ends_with(
                sorted(get_perms(node_manager, group))[0]
            )
            assert_that(PERM_NODE_MANAGER_VIEW).ends_with(
                sorted(get_perms(node_manager, group))[1]
            )
        self.client.logout()

    def test_node_update(self):
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        url = reverse(f"{APP_NAME}:node-detail", kwargs={"pk": 1})
        # Ticketing system email is required
        for val in ("", "    "):
            self.assertEqual(
                self.client.patch(
                    url,
                    data={"ticketing_system_email": val},
                    content_type=JSON,
                ).status_code,
                status.HTTP_400_BAD_REQUEST,
            )
        # Putting something meaningful
        data = {"ticketing_system_email": "scicore-admin@unibas.ch"}
        self.assertEqual(
            self.client.patch(url, data=data, content_type=JSON).status_code,
            status.HTTP_200_OK,
        )
        self.client.logout()
        check_method(
            test_class=self,
            method="patch",
            url=f"{APP_NAME}:node",
            allowed=(self.staff.username, self.node_admin_1),
            forbidden=(self.user.username, self.node_admin_2),
            ids=(1,),
            data=data,
            content_type=JSON,
        )

    def test_code_update_as_na(self):
        url = reverse(f"{APP_NAME}:node-detail", kwargs={"pk": 1})
        for user, http_status in (
            (self.node_admin_1, status.HTTP_400_BAD_REQUEST),
            (self.staff, status.HTTP_200_OK),
        ):
            self.client.login(username=user, password=UserFactory.USER_PASSWORD)
            self.assertEqual(
                http_status,
                self.client.patch(
                    url,
                    data={"code": "bay51"},
                    content_type=JSON,
                ).status_code,
                f"Wrong HTTP status for user '{user}'",
            )
            self.client.logout()
