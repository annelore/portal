import pytest
from assertpy import assert_that
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.tests.views import APPLICATION_JSON


def test_list_anonymous(client):
    url = reverse(f"{APP_NAME}:flag-list")
    response = client.get(url)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("basic_user_auth")
def test_list_authenticated(client):
    url = reverse(f"{APP_NAME}:flag-list")
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.usefixtures("basic_user_auth")
def test_flag_as_non_staff(client, user_factory, flag_factory):
    new_user = user_factory()
    flag = flag_factory()
    assert_that(flag.code).is_equal_to("sphn")
    assert_that(flag.description).starts_with(
        "This flag is set for an SPHN project user"
    )
    url = reverse(f"{APP_NAME}:flag-detail", kwargs={"pk": flag.id})
    r = client.patch(
        url,
        {"code": "sis", "description": "lorem ipsum", "users": [new_user.id]},
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_403_FORBIDDEN
    assert_that(r.json()).is_equal_to(
        {"detail": "You do not have permission to perform this action."}
    )


@pytest.mark.usefixtures("staff_user_auth")
def test_flag_as_staff(client, flag_factory, user_factory):
    new_user = user_factory()
    flag = flag_factory()
    # Edit a flag
    url = reverse(f"{APP_NAME}:flag-detail", kwargs={"pk": flag.id})
    r = client.patch(
        url,
        {"users": [new_user.id]},
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_200_OK
    # Create a flag
    url = reverse(f"{APP_NAME}:flag-list")
    r = client.post(
        url,
        {"code": "sis", "description": "lorem ipsum", "users": [new_user.id]},
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_201_CREATED
