import logging
from unittest import mock

import pytest
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME

URL = reverse(f"{APP_NAME}:log-list")


@pytest.mark.parametrize("method", ("get", "post"))
def test_unauthenticated(client, method):
    assert getattr(client, method)(URL).status_code == status.HTTP_403_FORBIDDEN


# pylint: disable=no-self-use
@pytest.mark.usefixtures("basic_user_auth")
class TestWithAuth:
    def test_get_not_allowed(self, client):
        assert client.get(URL).status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_log_ok(self, client):
        with mock.patch("projects.views.log.logger.log") as log:
            assert client.post(URL).status_code == status.HTTP_200_OK
            log.assert_called_once()
            # extract args using a tuple (Py37 doesn't support call_args.args)
            assert tuple(log.call_args)[0] == (logging.INFO, "Frontend log msg")
