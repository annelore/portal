from assertpy import assert_that
from django.contrib.auth.models import Group
from django.urls import reverse
from guardian.shortcuts import get_perms
from rest_framework import status

from projects.apps import APP_NAME
from projects.models import DataProvider
from projects.permissions.data_provider import (
    PERM_DATA_PROVIDER_ADMIN,
    PERM_DATA_PROVIDER_VIEWER,
    PERM_DATA_PROVIDER_MANAGER_CHANGE,
    PERM_DATA_PROVIDER_MANAGER_VIEW,
    PERM_DATA_PROVIDER_COORDINATOR,
)
from projects.tests.views import UserFactory, APPLICATION_JSON
from projects.tests.views.test_data_transfer import (
    TransferViewTestBase,
    assert_access_forbidden,
)


class TestDataProviderView(TransferViewTestBase):
    url = reverse(f"{APP_NAME}:dataprovider-list")

    def setUp(self):
        super().setUp()
        self.data_provider_2 = DataProvider.objects.create(
            name="Provider 2", code="provider_2", node=self.dest_node
        )

    def test_view_list(self):
        # Unauthenticated cannot see data providers
        assert_access_forbidden(self, self.url)

        # Staff and authenticated users can see data providers
        for username in (self.staff.username, self.basic_user.username):
            self.client.login(username=username, password=UserFactory.USER_PASSWORD)
            r = self.client.get(self.url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            data_provider_list = r.json()
            data_provider_list_expected = [
                {
                    "id": dp.id,
                    "name": dp.name,
                    "code": dp.code,
                    "enabled": dp.enabled,
                    "node": (dp.node.code if dp.node is not None else None),
                    "users": [],
                }
                for dp in DataProvider.objects.all()
            ]
            assert_that(data_provider_list).contains_only(*data_provider_list_expected)
            self.client.logout()

        self.data_provider.enabled = False
        self.data_provider.save()
        self.client.login(
            username=self.basic_user.username, password=UserFactory.USER_PASSWORD
        )
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        assert_that(r.json()).contains_only(
            *(
                dp
                for dp in data_provider_list_expected
                if dp["code"] != self.data_provider.code
            )
        )
        self.client.logout()

    def test_dp_create(self):
        self.client.login(
            username=self.staff.username, password=UserFactory.USER_PASSWORD
        )
        url = reverse(f"{APP_NAME}:dataprovider-list")
        dp_code = "chuck"
        data = {"name": "Chuck", "code": dp_code, "node": self.dest_node.code}
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_201_CREATED,
        )
        dp = DataProvider.objects.get(code=dp_code)
        dp_ta = Group.objects.get(name=f"{dp_code} DP Technical Admin")
        dp_viewer = Group.objects.get(name=f"{dp_code} DP Viewer")
        dp_manager = Group.objects.get(name=f"{dp_code} DP Manager")
        dp_coordinator = Group.objects.get(name=f"{dp_code} DP Coordinator")
        assert_that(PERM_DATA_PROVIDER_ADMIN).ends_with(get_perms(dp_ta, dp)[0])
        assert_that(PERM_DATA_PROVIDER_VIEWER).ends_with(get_perms(dp_viewer, dp)[0])
        # DP Manager
        for group in (dp_ta, dp_viewer, dp_coordinator):
            assert_that(PERM_DATA_PROVIDER_MANAGER_CHANGE).ends_with(
                sorted(get_perms(dp_manager, group))[0]
            )
            assert_that(PERM_DATA_PROVIDER_MANAGER_VIEW).ends_with(
                sorted(get_perms(dp_manager, group))[1]
            )
        # DP Coordinator
        for group in (dp_ta, dp_viewer, dp_manager):
            assert_that(PERM_DATA_PROVIDER_COORDINATOR).ends_with(
                get_perms(dp_coordinator, group)[0]
            )
        self.client.logout()
