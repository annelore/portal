from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.models import Project
from projects.tests.views import make_node


class TestNotifyView(TestCase):
    def test_view_list(self):
        # GET is not allowed
        url = reverse(f"{APP_NAME}:notify-list")
        self.assertEqual(
            self.client.get(url).status_code, status.HTTP_405_METHOD_NOT_ALLOWED
        )

    def test_create(self):
        # Unauthenticated user can create notifications
        Project(gid=1500001, name="new", code="foo", destination=make_node()).save()
        url = reverse(f"{APP_NAME}:notify-list")
        data = {"project": "foo", "tool": "tool_id", "data": "msg"}
        self.assertEqual(
            self.client.post(url, data, content_type="application/json").status_code,
            status.HTTP_201_CREATED,
        )
