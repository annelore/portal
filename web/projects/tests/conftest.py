import pytest

# pylint: disable=unused-import
from django_drf_utils.tests.utils import no_requests, patch_request
from .factories import (
    ProjectFactory,
    UserFactory,
    USER_PASSWORD,
    DataTransferFactory,
    DataProviderFactory,
    ProjectUserRoleFactory,
    FlagFactory,
    QuickAccessTileFactory,
)


@pytest.fixture
def project_factory(db):  # pylint: disable=unused-argument
    return ProjectFactory


@pytest.fixture
def user_factory(db):  # pylint: disable=unused-argument
    return UserFactory


@pytest.fixture
def data_transfer_factory(db):  # pylint: disable=unused-argument
    return DataTransferFactory


@pytest.fixture
def data_provider_factory(db):  # pylint: disable=unused-argument
    return DataProviderFactory


@pytest.fixture
def project_user_role_factory(db):  # pylint: disable=unused-argument
    return ProjectUserRoleFactory


@pytest.fixture
def basic_user_auth(db, client):  # pylint: disable=unused-argument
    """Return authenticated basic user"""
    user = UserFactory(basic=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def staff_user_auth(db, client):  # pylint: disable=unused-argument
    """Return authenticated staff user"""
    user = UserFactory(staff=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def flag_factory(db):  # pylint: disable=unused-argument
    return FlagFactory


@pytest.fixture
def quick_access_tile_factory(db):  # pylint: disable=unused-argument
    return QuickAccessTileFactory
