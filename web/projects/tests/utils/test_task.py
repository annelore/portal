from datetime import datetime

import pytest
import pytz

from django.test import TestCase

from projects.models import PeriodicTaskRun
from projects.utils.task import get_last_run


class TestTaskUtils(TestCase):
    def setUp(self):
        self.name = "test_task"
        self.utc = pytz.timezone("UTC")

    @pytest.mark.django_db
    def test_get_last_run_no_runs(self):
        self.assertIsNone(get_last_run(self))

    @pytest.mark.django_db
    def test_get_last_run_one_run_match(self):
        now = datetime.now(pytz.utc)
        PeriodicTaskRun.objects.create(task=self.name, created_at=now)
        self.assertEqual(get_last_run(self), now)

    @pytest.mark.django_db
    def test_get_last_run_one_run_no_match(self):
        now = datetime.now(pytz.utc)
        PeriodicTaskRun.objects.create(task="other_task", created_at=now)
        self.assertIsNone(get_last_run(self))

    @pytest.mark.django_db
    def test_get_last_run_multiple_runs(self):
        date1 = datetime(2018, 12, 7, 3, 0, 0, 1, self.utc)
        date2 = datetime(2019, 12, 5, 3, 0, 0, 1, self.utc)
        date3 = datetime(2020, 12, 5, 3, 0, 0, 1, self.utc)
        date4 = datetime(2020, 12, 7, 3, 0, 0, 1, self.utc)

        PeriodicTaskRun.objects.create(task=self.name, created_at=date2)
        PeriodicTaskRun.objects.create(task=self.name, created_at=date3)
        PeriodicTaskRun.objects.create(task="other_task", created_at=date4)
        PeriodicTaskRun.objects.create(task=self.name, created_at=date1)

        self.assertEqual(get_last_run(self), date3)
