from assertpy import assert_that

from projects.models import PgpKeySignStatus, PgpKey
from projects.utils.gpg import parse_key

signing_pgpkey_id = "881685B5EE0FCBD3"

key_owner_revoked = (
    ">pub</strong>  4096R/<a "
    'href="/pks/lookup?op=get&amp;search=0xC65B47B9F8B02FD8">F8B02FD8</a> '
    "2020-11-09            \r"
    ' sig <span class="warn">revok </span> <a '
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0xC65B47B9F8B02FD8">F8B02FD8</a> '
    "2020-11-09 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0xC65B47B9F8B02FD8">[selfsig]</a>\r'
    " \t Fingerprint=6AD9 F4E3 C532 C947 97B7  EB34 C65B 47B9 F8B0 2FD8 \r"
    " \r"
    ' <strong>uid</strong> <span class="uid">Jarek Testkey '
    "&lt;test@example.org&gt;</span>\r"
    " sig  sig3  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0xC65B47B9F8B02FD8">F8B02FD8</a> '
    "2020-11-09 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0xC65B47B9F8B02FD8">[selfsig]</a>\r'
    " \r"
    " <strong>sub</strong>  4096R/BF1369D3 2020-11-09            \r"
    " sig sbind  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0xC65B47B9F8B02FD8">F8B02FD8</a> '
    "2020-11-09 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0xC65B47B9F8B02FD8">[]</a>\r'
    " </pre>"
)

key_signing_authority_revoked = (
    ">pub</strong>  2048R/<a "
    'href="/pks/lookup?op=get&amp;search=0x6AF460F4990D4453">990D4453</a> '
    "2020-04-24            \r"
    " \t Fingerprint=B913 358E 114E 6BE8 C3D7  66A9 6AF4 60F4 990D 4453 \r"
    " \r"
    ' <strong>uid</strong> <span class="uid">Chuck Norris (PSSS Data Manager) '
    "&lt;chucknorris@roundhouse.gov&gt;</span>\r"
    " sig  sig3  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0x6AF460F4990D4453">990D4453</a> '
    "2020-04-24 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0x6AF460F4990D4453">[selfsig]</a>\r'
    " sig  sig3  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0x881685B5EE0FCBD3">EE0FCBD3</a> '
    "2020-04-28 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0x881685B5EE0FCBD3">SPHN '
    "Data Coordination Centre &lt;data@coordination.centre&gt;</a>\r"
    ' sig <span class="warn">revok </span> <a '
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0x881685B5EE0FCBD3">EE0FCBD3</a> '
    "2020-04-28 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0x881685B5EE0FCBD3">SPHN '
    "Data Coordination Centre &lt;data@coordination.centre&gt;</a>\r"
    " \r"
    " <strong>sub</strong>  2048R/E83782F7 2020-04-24            \r"
    " sig sbind  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0x6AF460F4990D4453">990D4453</a> '
    "2020-04-24 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0x6AF460F4990D4453">[]</a>\r'
    " </pre>"
)

key_someone_revoked = (
    ">pub</strong>  4096R/<a "
    'href="/pks/lookup?op=get&amp;search=0x5BA5CFA272310BE6">72310BE6</a> '
    "2020-11-10            \r"
    " \t Fingerprint=BF93 D14F C220 D668 0E38  E19E 5BA5 CFA2 7231 0BE6 \r"
    " \r"
    ' <strong>uid</strong> <span class="uid">François 2 Testkey '
    "&lt;francois@example.org&gt;</span>\r"
    " sig  sig3  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0x5BA5CFA272310BE6">72310BE6</a> '
    "2020-11-10 __________ 2024-11-10 <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0x5BA5CFA272310BE6">[selfsig]</a>\r'
    " sig  sig   <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0x31B77BA4E9BB076D">E9BB076D</a> '
    "2020-11-10 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0x31B77BA4E9BB076D">François '
    "Testkey &lt;francois@example.org&gt;</a>\r"
    ' sig <span class="warn">revok </span> <a '
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0x5BA5CFA272310BE6">72310BE6</a> '
    "2020-11-10 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0x5BA5CFA272310BE6">[selfsig]</a>\r'
    " \r"
    " <strong>sub</strong>  4096R/018EA9E1 2020-11-10            \r"
    " sig sbind  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0x5BA5CFA272310BE6">72310BE6</a> '
    "2020-11-10 __________ 2024-11-10 <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0x5BA5CFA272310BE6">[]</a>\r'
    " </pre>"
)

key_signed = (
    ">pub</strong>  4096R/<a "
    'href="/pks/lookup?op=get&amp;search=0xE75A30416DE01D17">6DE01D17</a> '
    "2020-02-29            \r"
    " \t Fingerprint=9E10 FE43 6DA1 4366 B625  4861 E75A 3041 6DE0 1D17 \r"
    " \r"
    ' <strong>uid</strong> <span class="uid">François Martin '
    "&lt;francois@example.org&gt;</span>\r"
    " sig  sig3  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0xE75A30416DE01D17">6DE01D17</a> '
    "2020-02-29 __________ 2024-02-29 <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0xE75A30416DE01D17">[selfsig]</a>\r'
    " sig  sig3  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0x881685B5EE0FCBD3">EE0FCBD3</a> '
    "2020-07-08 __________ __________ <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0x881685B5EE0FCBD3">SPHN '
    "Data Coordination Centre &lt;data@coordination.centre&gt;</a>\r"
    " \r"
    " <strong>sub</strong>  4096R/2E2680F5 2020-02-29            \r"
    " sig sbind  <a "
    'href="/pks/lookup?op=get&amp;fingerprint=on&amp;search=0xE75A30416DE01D17">6DE01D17</a> '
    "2020-02-29 __________ 2024-02-29 <a "
    'href="/pks/lookup?op=vindex&amp;fingerprint=on&amp;search=0xE75A30416DE01D17">[]</a>\r'
    " </pre>"
)


def test_parse_key_owner_revoked():
    assert_that(parse_key(key_owner_revoked, signing_pgpkey_id)).is_type_of(
        PgpKey
    ).has_pgpkey_id("C65B47B9F8B02FD8").has_fingerprint(
        "6AD9F4E3C532C94797B7EB34C65B47B9F8B02FD8"
    ).has_key_name(
        "Jarek Testkey <test@example.org>"
    ).has_sign_status(
        PgpKeySignStatus.REVOKED
    ).has_key_length(
        4096
    ).has_email(
        "test@example.org"
    )


def test_parse_key_signing_authority_revoked():
    assert_that(parse_key(key_signing_authority_revoked, signing_pgpkey_id)).is_type_of(
        PgpKey
    ).has_pgpkey_id("6AF460F4990D4453").has_fingerprint(
        "B913358E114E6BE8C3D766A96AF460F4990D4453"
    ).has_key_name(
        "Chuck Norris (PSSS Data Manager) <chucknorris@roundhouse.gov>"
    ).has_sign_status(
        PgpKeySignStatus.REVOKED
    ).has_key_length(
        2048
    ).has_email(
        "chucknorris@roundhouse.gov"
    )


# Security: only revocations by the key owner or the signing authority are trusted!
def test_parse_key_someone_revoked():
    assert_that(parse_key(key_someone_revoked, signing_pgpkey_id)).is_type_of(
        PgpKey
    ).has_pgpkey_id("5BA5CFA272310BE6").has_fingerprint(
        "BF93D14FC220D6680E38E19E5BA5CFA272310BE6"
    ).has_key_name(
        "François 2 Testkey <francois@example.org>"
    ).has_sign_status(
        PgpKeySignStatus.UNSIGNED
    ).has_key_length(
        4096
    ).has_email(
        "francois@example.org"
    )


def test_parse_key_signed():
    assert_that(parse_key(key_signed, signing_pgpkey_id)).is_type_of(
        PgpKey
    ).has_pgpkey_id("E75A30416DE01D17").has_fingerprint(
        "9E10FE436DA14366B6254861E75A30416DE01D17"
    ).has_key_name(
        "François Martin <francois@example.org>"
    ).has_sign_status(
        PgpKeySignStatus.SIGNED
    ).has_key_length(
        4096
    )
