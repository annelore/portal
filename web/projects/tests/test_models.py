# pylint: disable=no-self-use

import pytest
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django_drf_utils.tests.models.utils import validate_field

from projects.models import (
    Node,
    Project,
    ProjectIPAddresses,
    Profile,
    Resource,
    UserNamespace,
    ProjectRole,
    DataTransfer,
    Flag,
)
from projects.tests.factories import PeriodicTaskRunFactory

User = get_user_model()


def test_node_str():
    node = Node(code="node_1", name="HPC center 1", node_status=Node.STATUS_ONLINE)
    assert str(node) == f"{node.name} ({node.code})"


def test_project_role_choices():
    choices = ProjectRole.choices()
    assert len(choices) == 4
    assert choices[0] == (1, "User")


def test_user_namespace_str():
    assert str(UserNamespace(name="ch")) == "Namespace (ch)"


class TestPeriodicTaskRun:
    @staticmethod
    @pytest.fixture
    def periodic_task_run():
        return PeriodicTaskRunFactory.build()

    def test_str(self, periodic_task_run):
        assert (
            str(periodic_task_run)
            == f"{periodic_task_run.task} ({periodic_task_run.created_at})"
        )


class TestProject:
    @pytest.fixture
    def project(self):
        return Project(gid=1500001, name="Project 1", code="project_1")

    def test_gid_label(self, project):
        assert project._meta.get_field("gid").verbose_name == "gid"

    def test_str(self, project):
        assert str(project) == f"{project.name} ({project.code})"

    longest_valid_name = (
        "ËÛÄâU(PëÏtjzÜwPÂ93ÛfJÀ2ÄÖKàWdçEoBnÖôwGmg3yÇï5WMw3qkioÀQÈIvRÔVBOÔêëüàtËF"
        "7ÈÇËDkXßOÛÀÔQÈ4cöSa8ÏCAhkßh5m(qBÖcYhàzbÈ QQÇGFs8IËNWil07ÏbKÀR6säq1OÄÈâI"
        "eâûGvEPç0YÎR0päï6HnZ4SWg9éëaOSPÂXF5îY7qM1ÇçQLMêIDïèèxzLôMzërWöïÇo6SÇhGë"
        "7wé7Cöc5ßÊWèäFètpP3Âuî5ÜBLT0P5yjWÎgrAzntDy()îé6g5AÀ96gEËLüjxïxsfRtêa3Â3"
        "tTüAtêb8ÎZ(0cNÜÀJÜhvKge17ÖwqÄÇ9xZ RûsIèAyGeàmhÊ7M99êûliYyixfÖ8êlÄméoz1J"
        "mB9cÊî28ÄqïOXieyiCöSNê9çfNeIdÏPÊ)EGÄZÜlfbaüLyÛDTü9ûj6K6xEbVGkAEwîmndëL "
        "âÔH6qJAßhtarèyGvvÏkVÄ4Q4hdËVxîqj)zßkMÎQÊï0üÂï9ÛÈÜéSÉÜbHsÊ-i5M9öÊöJnIôDk"
        "iwà2ÇöÈVïasO ôa"
    )

    test_name_valid = (
        # (name, is_valid)
        ("ABCabc - (012)ßàÀéÉèÈäÄëËïÏöÖüÜçÇâÂêÊîÎôÔûÛ", True),
        ("L'Ospitale", True),
        (longest_valid_name, True),  # upper length limit
        (longest_valid_name + " ", False),  # one character over length limit
        ("జ్ఞ‌ా", False),  # unicode characters
        ("<script>alert('Hello')</script>", False),  # XSS prevention
        (".", False),  # special characters
        ("_", False),  # special characters
        ("{", False),  # special characters
        ("[", False),  # special characters
        ("<", False),  # special characters
        ("/", False),  # special characters
        ("&", False),  # special characters
        ("!", False),  # special characters
        ("$", False),  # special characters
    )

    @pytest.mark.django_db
    @pytest.mark.parametrize("test_input, expected", test_name_valid)
    def test_validation_project_name(self, test_input, expected):
        validate_field(Project, "name", test_input, expected)

    longest_valid_id = "hi8ksu8ey1cd_r82xj6d7ry33gvm5kx7"

    test_id_valid = (
        # (name, is_valid)
        ("abc_012", True),
        (longest_valid_id, True),  # upper length limit
        (longest_valid_id + "a", False),  # one character over length limit
        ("జ్ఞ‌ా", False),  # unicode characters
        ("<script>alert('Hello')</script>", False),  # XSS prevention
        ("ABC", False),  # uppercase characters
        ("abc def", False),  # spaces
        ("abc-def", False),  # dashes
        (".", False),  # special characters
        ("{", False),  # special characters
        ("[", False),  # special characters
        ("<", False),  # special characters
        ("/", False),  # special characters
        ("&", False),  # special characters
        ("!", False),  # special characters
        ("$", False),  # special characters
    )

    @pytest.mark.django_db
    @pytest.mark.parametrize("test_input, expected", test_id_valid)
    def test_validation_code(self, test_input, expected):
        validate_field(Project, "code", test_input, expected)


class TestIPAddresses:
    def test_ip_addresses_validation(self):
        with pytest.raises(ValidationError):
            project_ip_addr = ProjectIPAddresses(
                project=None, ip_address="127.0.0.1", mask=33
            )
            project_ip_addr.clean_fields(exclude=["project", "ip_address"])

    def test_ip_addresses_str(self):
        project_ip = ProjectIPAddresses(project=None, ip_address="127.0.0.1", mask=24)
        assert str(project_ip) == "127.0.0.1/24"


class TestResource:
    def test_location_scheme(self):
        for scheme in ("http", "https", "ftp", "ftps", "ssh"):
            Resource(
                project=None, name="Test resource", location=f"{scheme}://resource.org"
            ).clean_fields(exclude=["project"])
        with pytest.raises(ValidationError):
            Resource(
                project=None, name="Test resource", location="foo://resource.org"
            ).clean_fields(exclude=["project"])

    def test_str(self):
        resource = Resource(
            project=None, name="Test resource", location="https://resource.org"
        )
        assert str(resource) == "https://resource.org"


class TestProfile:
    test_input_expected = (
        (
            ("cnorris", "Chuck", "Norris", "cnorris@roundhouse.gov", None, None),
            ("Chuck Norris (cnorris@roundhouse.gov)", ""),
        ),
        (
            (
                "CNORRIS@roundhouse.gov",
                None,
                None,
                "cnorris@roundhouse.gov",
                "chuck",
                "ch",
            ),
            ("chuck (ID: CNORRIS) (cnorris@roundhouse.gov)", "ch_chuck"),
        ),
        (
            ("CNORRIS@roundhouse.gov", None, None, None, "chuck", None),
            ("chuck (ID: CNORRIS)", "chuck"),
        ),
        (
            ("CNORRIS@roundhouse.gov", None, None, None, "chuck", "ch"),
            ("chuck (ID: CNORRIS)", "ch_chuck"),
        ),
    )

    @pytest.mark.parametrize("test_input, expected", test_input_expected)
    def test_display(self, test_input, expected):
        namespace = None if test_input[5] is None else UserNamespace(name=test_input[5])
        profile = Profile(
            user=User(
                username=test_input[0],
                first_name=test_input[1],
                last_name=test_input[2],
                email=test_input[3],
            ),
            local_username=test_input[4],
            namespace=namespace,
        )
        assert profile.display_name == expected[0]
        assert profile.display_local_username == expected[1]

    def test_str(self):
        assert str(Profile(user=User(username="bob"))) == "Profile (bob)"


class TestDataTransfer:
    test_input_valid = (
        # (requestor_pgp_key_fp, is_valid)
        (
            "A0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9",
            True,
        ),  # uppercase, upper length limit
        ("A0B1C2D3E4F5A6B7C8D9", True),  # uppercase, shorter length
        ("a0b1c2d3e4f5a6b7c8d9e0f1a2b3c4d5e6f7a8b9", False),  # lowercase
        ("A0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9C", False),  # too long
        ("A0B1 C2D3 E4F5 A6B7 C8D9 E0F1 A2B3 C4D5", False),  # spaces
        ("G0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9", False),  # not allowed characters
        ("$0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9", False),  # not allowed characters
    )

    @pytest.mark.parametrize("test_input, expected", test_input_valid)
    def test_validation_requestor_pgp_key_fp(self, test_input, expected):
        validate_field(DataTransfer, "requestor_pgp_key_fp", test_input, expected)


class TestFlag:
    test_input_valid = (
        ("code", "fhnw", True),
        ("code", "FHNW", False),
        ("code", "", False),
        ("description", "", True),
        ("description", None, True),
        ("description", "Lorem ipsum ...", True),
    )

    @pytest.mark.parametrize("field_name, test_input, expected", test_input_valid)
    @pytest.mark.django_db
    def test_validation_user_flag(self, field_name, test_input, expected):
        validate_field(Flag, field_name, test_input, expected)
