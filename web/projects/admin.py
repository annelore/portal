from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from guardian.admin import GuardedModelAdmin
from simple_history.admin import SimpleHistoryAdmin

from projects.models import (
    Message,
    Node,
    DataProvider,
    DataPackage,
    DataTransfer,
    Profile,
    Project,
    ProjectIPAddresses,
    Resource,
    UserNamespace,
    Feed,
    PgpKeySignRequest,
    DataPackageTrace,
    Flag,
    QuickAccessTile,
)

User = get_user_model()


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = "Profile"


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    inlines = (ProfileInline,)


@admin.register(Project)
class ProjectAdmin(GuardedModelAdmin, SimpleHistoryAdmin):
    list_display = ("name", "gid")


@admin.register(ProjectIPAddresses)
class ProjectIPAddressesAdmin(SimpleHistoryAdmin):
    list_display = ("ip_and_mask", "project")
    list_filter = ("project",)

    def ip_and_mask(self, obj):  # pylint: disable=no-self-use
        return str(obj)


@admin.register(Resource)
class ResourceAdmin(SimpleHistoryAdmin):
    list_display = ("name", "location", "project", "contact")
    list_filter = ("project",)


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ("tool", "data", "project", "created")
    list_filter = ("tool", "project", "created")


@admin.register(UserNamespace)
class UserNamespaceAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Node)
class NodeAdmin(GuardedModelAdmin):
    list_display = ("name", "code", "node_status")


@admin.register(DataProvider)
class DataProviderAdmin(GuardedModelAdmin):
    list_display = ("name", "code", "node")


@admin.register(DataPackage)
class DataPackageAdmin(GuardedModelAdmin, SimpleHistoryAdmin):
    list_display = (
        "metadata_hash",
        "data_transfer",
        "file_name",
        "metadata",
        "purpose",
    )


@admin.register(DataPackageTrace)
class DataPackageTraceAdmin(GuardedModelAdmin):
    list_display = ("data_package", "node", "timestamp", "status")


@admin.register(DataTransfer)
class DataTransferAdmin(GuardedModelAdmin, SimpleHistoryAdmin):
    list_display = (
        "id",
        "project",
        "max_packages",
        "status",
        "data_provider",
        "requestor",
        "purpose",
    )


@admin.register(Flag)
class FlagAdmin(GuardedModelAdmin):
    list_display = ("code", "description")


@admin.register(Feed)
class FeedAdmin(GuardedModelAdmin):
    list_display = ("label", "title", "message")


@admin.register(QuickAccessTile)
class QuickAccessTileAdmin(admin.ModelAdmin):
    list_display = ("title", "description", "url", "image", "flag", "order")


@admin.register(PgpKeySignRequest)
class PgpKeySignRequestAdmin(admin.ModelAdmin):
    list_display = ("created", "pgpkey_id", "signing_pgpkey_id")
