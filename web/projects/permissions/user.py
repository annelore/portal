import logging

from rest_framework.permissions import (
    BasePermission,
    DjangoModelPermissions,
    IsAuthenticated,
    SAFE_METHODS,
)

from identities.permissions import has_change_user_permission
from . import is_staff

logger = logging.getLogger(__name__)


class UserUniquePermission(BasePermission):
    methods = ("POST",)

    def has_permission(self, request, view):
        return (
            request.user
            and request.user.is_authenticated
            and request.method in self.methods
            and is_staff(request.user)
        )


class UserModelPermission(DjangoModelPermissions):
    """Applies same permissions to user object than to user model (`has_object_permission`
    returning `True` by default in `super()`).

    And ensures that caller is applying PATCH updates ONLY.
    """

    def has_permission(self, request, view):
        return (
            super().has_permission(request, view)
            and request.method == UserDataPermission.update_user_method
        )

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)


class UserDataPermission(IsAuthenticated):
    update_user_method = "PATCH"
    create_local_user_method = "POST"

    def has_permission(self, request, view):
        if super().has_permission(request, view):
            if request.method == self.create_local_user_method:
                return is_staff(request.user)
            return True
        return False

    # pylint: disable=too-many-return-statements
    def has_object_permission(self, request, view, obj):  # NOT called on POST!
        if request.method in SAFE_METHODS:
            # non-modifying operation
            return True

        # modifying operation
        requestor = request.user
        request_profile = request.data.get("profile", {})

        if request.method == self.update_user_method:
            if "local_username" in request_profile:
                # attempt to set username
                new_local_username = request_profile["local_username"]

                # Only requestor can change their own local username
                if requestor.id != obj.id:
                    logger.error(
                        "User (ID: %s) attempted to set local_username (%s) for user (ID: %s)",
                        requestor.id,
                        new_local_username,
                        obj.id,
                    )
                    return False

                # 'local_username' cannot be changed after it's set the first time
                if requestor.profile.local_username:
                    logger.error(
                        "User (ID: %s) attempted to change their local_username to %s",
                        requestor.id,
                        new_local_username,
                    )
                    return False

                return True

            # attempt to modify user
            if not is_staff(requestor):
                if not has_change_user_permission(requestor):
                    logger.error(
                        "Non-authorized user (ID: %s) attempted to modify user (ID: %s) "
                        "with data: %s",
                        requestor.id,
                        obj.id,
                        request.data,
                    )
                return False

            return True

        # not allowed modifying method
        logger.error(
            "User (ID: %s) attempted to modify user (ID: %s) "
            "using not allowed method %s with data: %s",
            requestor.id,
            obj.id,
            request.method,
            request.data,
        )
        return False
