from typing import Optional

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.db.models import QuerySet
from rest_framework import permissions
from guardian.shortcuts import get_objects_for_user, assign_perm

from ..models import Node, PERMISSION_NODE_ADMIN
from . import UPDATE_METHODS
from ..apps import APP_NAME as PROJECTS_APP_NAME

User = get_user_model()

PERM_NODE_ADMIN = f"{PROJECTS_APP_NAME}.{PERMISSION_NODE_ADMIN}"
PERM_NODE_VIEWER = f"{PROJECTS_APP_NAME}.view_node"
PERM_NODE_MANAGER_CHANGE = f"{PROJECTS_APP_NAME}.change_group"
PERM_NODE_MANAGER_VIEW = f"{PROJECTS_APP_NAME}.view_group"


def get_node_viewer_nodes(user: User) -> QuerySet[Node]:
    return get_objects_for_user(user, PERM_NODE_VIEWER)


def has_node_viewer_nodes(user: User) -> bool:
    return get_node_viewer_nodes(user).exists()


def has_node_viewer_permission(user: User, node: Optional[Node] = None) -> bool:
    return user.has_perm(PERM_NODE_VIEWER, node)


def get_node_admin_nodes(user: User) -> QuerySet[Node]:
    return get_objects_for_user(user, PERM_NODE_ADMIN)


def has_node_admin_nodes(user: User) -> bool:
    return get_node_admin_nodes(user).exists()


def has_node_admin_permission(user: User, node: Optional[Node] = None) -> bool:
    return user.has_perm(PERM_NODE_ADMIN, node)


def has_any_node_permissions(user: User) -> bool:
    return get_node_permission_nodes(user).exists()


def get_node_permission_nodes(user: User) -> QuerySet[Node]:
    return get_node_admin_nodes(user) | get_node_viewer_nodes(user)


def assign_node_viewer(node_viewer: Group, node: Node) -> Permission:
    return assign_perm(PERM_NODE_VIEWER, node_viewer, node)


def assign_node_admin(node_admin: Group, node: Node) -> Permission:
    return assign_perm(PERM_NODE_ADMIN, node_admin, node)


def assign_node_manager(node_manager: Group, node_viewer: Group, node_admin: Group):
    for group in (node_viewer, node_admin):
        assign_perm(PERM_NODE_MANAGER_CHANGE, node_manager, group)
        assign_perm(PERM_NODE_MANAGER_VIEW, node_manager, group)


class IsNodeAdmin(permissions.BasePermission):
    """Node Admin can modify a node except for Node ID."""

    def has_permission(self, request, view):
        user = request.user
        return (
            user
            and user.is_authenticated
            and has_node_admin_nodes(user)
            and request.method in UPDATE_METHODS
        )

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view) and has_node_admin_permission(
            request.user, obj
        )
