from typing import Optional

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.db.models import QuerySet
from guardian.shortcuts import get_objects_for_user, assign_perm
from rest_framework.permissions import BasePermission

from ..models import DataProvider, PERMISSION_DATA_PROVIDER_ADMIN
from . import UPDATE_METHODS
from ..apps import APP_NAME as PROJECTS_APP_NAME

User = get_user_model()

PERM_DATA_PROVIDER_ADMIN = f"{PROJECTS_APP_NAME}.{PERMISSION_DATA_PROVIDER_ADMIN}"
PERM_DATA_PROVIDER_VIEWER = f"{PROJECTS_APP_NAME}.view_dataprovider"
PERM_DATA_PROVIDER_MANAGER_VIEW = f"{PROJECTS_APP_NAME}.view_group"
PERM_DATA_PROVIDER_MANAGER_CHANGE = f"{PROJECTS_APP_NAME}.change_group"
PERM_DATA_PROVIDER_COORDINATOR = PERM_DATA_PROVIDER_MANAGER_VIEW


def managed_data_providers(user: User):
    """All Data Providers the given user is manager of"""
    return DataProvider.objects.filter(users=user)


def is_data_provider(user: User) -> bool:
    """Whether given user is data provider"""
    return managed_data_providers(user).exists()


def get_data_provider_viewer_data_providers(
    user: User,
) -> QuerySet[DataProvider]:
    return get_objects_for_user(user, PERM_DATA_PROVIDER_VIEWER)


def has_data_provider_viewer_data_providers(user: User) -> bool:
    return get_data_provider_viewer_data_providers(user).exists()


def has_data_provider_viewer_permission(
    user: User, data_provider: Optional[DataProvider] = None
) -> bool:
    return user.has_perm(PERM_DATA_PROVIDER_VIEWER, data_provider)


def get_data_provider_admin_data_providers(
    user: User,
) -> QuerySet[DataProvider]:
    return get_objects_for_user(user, PERM_DATA_PROVIDER_ADMIN)


def has_data_provider_admin_data_providers(user: User) -> bool:
    return get_data_provider_admin_data_providers(user).exists()


def has_data_provider_admin_permission(
    user: User, data_provider: Optional[DataProvider] = None
) -> bool:
    return user.has_perm(PERM_DATA_PROVIDER_ADMIN, data_provider)


def has_any_data_provider_permissions(user: User) -> bool:
    return get_data_provider_permission_data_providers(user).exists()


def get_data_provider_permission_data_providers(
    user: User,
) -> QuerySet[DataProvider]:
    return get_data_provider_admin_data_providers(
        user
    ) | get_data_provider_viewer_data_providers(user)


def assign_dp_technical_admin(
    dp_technical_admin: Group, dp: DataProvider
) -> Permission:
    return assign_perm(PERM_DATA_PROVIDER_ADMIN, dp_technical_admin, dp)


def assign_dp_viewer(dp_viewer: Group, dp: DataProvider) -> Permission:
    return assign_perm(PERM_DATA_PROVIDER_VIEWER, dp_viewer, dp)


def assign_dp_manager(
    dp_manager: Group,
    dp_technical_admin: Group,
    dp_viewer: Group,
    dp_coordinator: Group,
):
    for group in (dp_technical_admin, dp_viewer, dp_coordinator):
        assign_perm(PERM_DATA_PROVIDER_MANAGER_VIEW, dp_manager, group)
        assign_perm(PERM_DATA_PROVIDER_MANAGER_CHANGE, dp_manager, group)


def assign_dp_coordinator(
    dp_coordinator: Group,
    dp_technical_admin: Group,
    dp_viewer: Group,
    dp_manager: Group,
):
    for group in (dp_technical_admin, dp_viewer, dp_manager):
        assign_perm(PERM_DATA_PROVIDER_COORDINATOR, dp_coordinator, group)


class IsDataProviderAdmin(BasePermission):
    """Data Provider Admin can modify a data provider.

    Some fields are not-editable with this permission:
    - code
    - node
    """

    def has_permission(self, request, view):
        user = request.user
        return (
            user
            and user.is_authenticated
            and has_data_provider_admin_data_providers(user)
            and request.method in UPDATE_METHODS
        )

    def has_object_permission(self, request, view, obj):
        return self.has_permission(
            request, view
        ) and has_data_provider_admin_permission(request.user, obj)
