from rest_framework.permissions import BasePermission, SAFE_METHODS

from .node import has_any_node_permissions


class AuditPermission(BasePermission):
    """Staff and node accounts can read"""

    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS
            and request.user
            and request.user.is_authenticated
            and (request.user.is_staff or has_any_node_permissions(request.user))
        )
