from dataclasses import dataclass, field
from typing import Optional, Union

from django_drf_utils.config import BaseConfig, Email, Logging, Oidc, Session


@dataclass
class TicketingApi(BaseConfig):
    host: str
    username: str
    password: str
    queue: str


@dataclass
class Notification(BaseConfig):
    contact_form_recipient: str
    ticket_mail: str
    user_last_logged_in_days: int = 60


@dataclass
class Uid(BaseConfig):
    min: int
    max: int


@dataclass
class User(BaseConfig):
    default_namespace: str = "ch"
    uid: Optional[Uid] = Uid(min=1000000, max=1500000 - 1)
    gid: Optional[Uid] = Uid(min=1000000, max=1500000 - 1)


@dataclass
class Project(BaseConfig):
    gid: Optional[Uid] = Uid(min=1500000, max=2000000 - 1)


@dataclass
class Pgp(BaseConfig):
    keyserver: str
    signing_pgpkey_id: str
    minimal_key_length: int = 4096


@dataclass
class Config(BaseConfig):
    notification: Notification
    oidc: Oidc
    pgp: Pgp
    logging: Logging = field(default_factory=Logging)
    session: Session = field(default_factory=Session)
    email: Optional[Email] = None
    user: Optional[User] = field(default_factory=User)
    project: Optional[Project] = field(default_factory=Project)


def unwrap_optional_type(T: type) -> type:
    opt_types = getattr(T, "__args__", ())
    if not isinstance(None, opt_types) or getattr(T, "__origin__", None) is not Union:
        return T
    return opt_types[0]
