import os
import celery

from celery import Celery
from dotenv import load_dotenv

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "portal.settings")
load_dotenv(
    dotenv_path=os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "../.env.local"
    )
)

app = Celery("portal")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@celery.signals.setup_logging.connect
def on_setup_logging(**kwargs):
    # By default, celery will setup it's own logger, even if you have setuped in django.
    # This prevents regular (not json formatted) logger from being added
    pass
