from unittest.mock import patch

import pytest
from django.conf import settings

from portal import user_utils
from portal.config import Config
from portal.user_utils import User, create_user, update_user
from projects.mail_templates import affiliation_change_body

CREATION_CLAIMS = {
    "preferred_username": "906726174130@eduid.ch",
    "given_name": "Bob",
    "family_name": "Smith",
    "email": "bob@example.org",
    "linkedAffiliation": ["member@fhnw.ch", "staff@fhnw.ch"],
    "linkedAffiliationUniqueID": ["987321@fhnw.ch", "24680@unibas.ch"],
}


def create_bob(
    affiliation="member@chuv.ch,staff@chuv.ch",
    affiliation_id="1234567@ethz.ch,7654321@unibas.ch",
) -> User:
    user = User.objects.create_user(username="bob", email="bob@example.org")
    user.profile.affiliation = affiliation
    user.profile.affiliation_id = affiliation_id
    return user


@pytest.mark.django_db
def test_create_user():
    user_created = create_user(CREATION_CLAIMS)
    user_db = User.objects.get(username="906726174130@eduid.ch")
    assert user_created == user_db
    assert user_created.username == "906726174130@eduid.ch"
    assert user_created.first_name == "Bob"
    assert user_created.last_name == "Smith"
    assert user_created.email == "bob@example.org"
    assert user_created.profile.affiliation == "member@fhnw.ch,staff@fhnw.ch"
    assert user_created.profile.affiliation_id == "987321@fhnw.ch,24680@unibas.ch"
    assert create_user({}) is None


@pytest.mark.django_db
def test_from_local_to_federated():
    user_db = create_bob()
    assert user_db.username == "bob"
    assert user_db.first_name == ""
    assert user_db.last_name == ""
    assert user_db.profile.affiliation == "member@chuv.ch,staff@chuv.ch"
    assert user_db.profile.affiliation_id == "1234567@ethz.ch,7654321@unibas.ch"
    user_updated = create_user(CREATION_CLAIMS)
    assert user_updated.username == "906726174130@eduid.ch"
    assert user_updated.first_name == "Bob"
    assert user_updated.last_name == "Smith"
    assert user_updated.profile.affiliation == "member@fhnw.ch,staff@fhnw.ch"
    assert user_updated.profile.affiliation_id == "987321@fhnw.ch,24680@unibas.ch"


@pytest.mark.django_db
def test_federation_overwrites_local():
    user_db = create_bob()
    user_db.first_name = "Robert"
    user_db.last_name = "Martin"
    user_db.save()
    user_updated = create_user(CREATION_CLAIMS)
    assert user_updated.username == "906726174130@eduid.ch"
    assert user_updated.first_name == "Bob"
    assert user_updated.last_name == "Smith"


@pytest.mark.django_db
def test_update_user_notification():
    settings.CONFIG = Config.empty()
    # pylint: disable=no-member
    settings.CONFIG.notification.ticket_mail = "ticket@system.swiss"
    user_db = create_bob()
    old_affiliation = user_db.profile.affiliation
    old_affiliation_id = user_db.profile.affiliation_id
    assert old_affiliation == "member@chuv.ch,staff@chuv.ch"
    assert old_affiliation_id == "1234567@ethz.ch,7654321@unibas.ch"
    with patch.object(user_utils, "sendmail") as mock_method:
        user_updated = update_user(
            user_db,
            {k: v for k, v in CREATION_CLAIMS.items() if k != "preferred_username"},
        )
        assert User.objects.get(username="bob") == user_updated
        assert user_updated.username == "bob"
        assert user_updated.first_name == "Bob"
        assert user_updated.last_name == "Smith"
        assert user_updated.email == "bob@example.org"
        # Affiliation has changed
        new_affiliation = "member@fhnw.ch,staff@fhnw.ch"
        new_affiliation_id = "987321@fhnw.ch,24680@unibas.ch"
        assert user_updated.profile.affiliation == new_affiliation
        assert user_db.profile.affiliation == new_affiliation
        assert user_updated.profile.affiliation_id == new_affiliation_id
        assert user_db.profile.affiliation_id == new_affiliation_id
        mock_method.assert_called_once_with(
            subject="Affiliation change of user 'bob'",
            body=affiliation_change_body(
                user_updated,
                old_affiliation,
                new_affiliation,
            ),
            recipients=[settings.CONFIG.notification.ticket_mail],
            email_cfg=settings.CONFIG.email,
        )


@pytest.mark.parametrize(
    "old_affiliation,new_affiliation,called",
    (
        ("", "", False),
        (None, "", False),
        ("", None, False),
        ("", "staff@fhnw.ch", True),
        ("staff@fhnw.ch", None, True),
        ("member@fhnw.ch", "member@fhnw.ch", False),
        ("staff@fhnw.ch,member@fhnw.ch", "staff@fhnw.ch,member@fhnw.ch", False),
        ("member@fhnw.ch,staff@fhnw.ch", "staff@fhnw.ch,member@fhnw.ch", False),
        ("student@fhnw.ch,staff@fhnw.ch", "staff@fhnw.ch", False),
        ("student@fhnw.ch,staff@fhnw.ch", "staff@fhnw.ch,member@fhnw.ch", False),
        ("student@ethz.ch,staff@fhnw.ch", "student@fhnw.ch,member@ethz.ch", False),
        ("student@fhnw.ch,staff@fhnw.ch", "student@ethz.ch", True),
        ("student@fhnw.ch,student@ethz.ch", "student@ethz.ch", True),
    ),
)
@pytest.mark.django_db
def test_update_user(old_affiliation, new_affiliation, called):
    settings.CONFIG = Config.empty()
    # pylint: disable=no-member
    settings.CONFIG.notification.ticket_mail = "ticketing@system.swiss"
    user_db = create_bob(old_affiliation)
    assert user_db.profile.affiliation == old_affiliation

    def empty_if_none(value):
        return value if value is not None else ""

    with patch.object(user_utils, "sendmail") as mock_method:
        user_updated = update_user(
            user_db,
            {
                "linkedAffiliation": new_affiliation.split(",")
                if new_affiliation
                else [],
            },
        )
        # Affiliation has changed
        assert user_updated.profile.affiliation == empty_if_none(new_affiliation)
        assert user_db.profile.affiliation == empty_if_none(new_affiliation)
        if called:
            mock_method.assert_called_once()
        else:
            mock_method.assert_not_called()
