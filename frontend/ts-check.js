const {
  verifyTypeScriptSetup,
} = require('next/dist/lib/verifyTypeScriptSetup');
(async () => {
  console.log('Starting TypeScript checks...');
  const result = await verifyTypeScriptSetup('.', 'pages', true);
  console.log('Result:', result);
  if (!!result.warnings?.length) {
    console.error('Found warnings:', result.warnings);
    process.exit(1);
  }
})();
