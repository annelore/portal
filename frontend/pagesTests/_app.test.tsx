import {
  expectToBeInTheDocument,
  isError,
  mockConsoleError,
} from '@biomedit/next-widgets';
import { render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { makeStore } from '../src/store';
import App, { unsupportedBrowserLogMessage } from '../pages/_app';
import { I18nNamespace } from '../src/i18n';
import { useRouter } from 'next/router';
import { generatedBackendApi } from '../src/api/api';
jest.mock('next/router');

describe('App', () => {
  describe('Component', () => {
    let consoleMock;
    let navigatorMock;

    beforeAll(() => {
      navigatorMock = { userAgent: undefined };
      Object.defineProperty(window, 'navigator', {
        value: navigatorMock,
      });
    });

    beforeEach(() => {
      const useMockedRouter = useRouter as jest.MockedFunction<
        typeof useRouter
      >;
      // @ts-expect-error other props ignored as they are unused within the component
      useMockedRouter.mockReturnValue({ pathname: '/' });
      consoleMock = mockConsoleError();
    });

    afterEach(() => {
      consoleMock.mockRestore();
      navigatorMock.userAgent = undefined;
    });

    it.each`
      userAgent                                                                                                                                                                                              | isSupported
      ${'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.2924.77 Safari/537.36'}                                                                                             | ${false}
      ${'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.3809.100 Safari/537.36'}                                                                               | ${false}
      ${'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.3865.90 Safari/537.36'}                                                                                | ${true}
      ${'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:24.0) Gecko/20100101 Firefox/24.0'}                                                                                                                        | ${false}
      ${'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/77.0'}                                                                                                                    | ${false}
      ${'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/78.0'}                                                                                                                    | ${true}
      ${'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}                                                                                                                          | ${true}
      ${'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Chrome/W.X.Y.Z Safari/537.36'}                                                       | ${true}
      ${'Googlebot/2.1 (+http://www.google.com/bot.html)'}                                                                                                                                                   | ${true}
      ${'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/W.X.Y.Z Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'} | ${true}
      ${'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon'}                                                                           | ${true}
      ${'Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko; googleweblight) Chrome/38.0.1025.166 Mobile Safari/535.19'}                                  | ${true}
    `(
      'should show a browser as supported ($isSupported) if the user agent is $userAgent',
      async ({ userAgent, isSupported }) => {
        let attemptedLogin = false;
        jest
          .spyOn(generatedBackendApi, 'retrieveUserinfo')
          .mockImplementation(() => {
            attemptedLogin = true;
            return new Promise((resolve) => {
              // @ts-expect-error not relevant for logic to be tested
              resolve({});
            });
          });
        navigatorMock.userAgent = userAgent;
        // verify mock is working properly
        expect(window.navigator.userAgent).toEqual(userAgent);

        try {
          render(
            <Provider store={makeStore()}>
              <App
                initialProps={{
                  // avoid errors introduced with next-i18next v8.8.0 due to the App component being instantiated in isolation
                  pageProps: { _nextI18Next: null },
                  router: { locale: 'foo' },
                }}
                initialState={{}}
              />
            </Provider>
          );
        } catch (e) {
          if (isError(e)) {
            expect(e.message).toContain(
              'Element type is invalid: expected a string'
            );
          } else {
            throw e;
          }
        }

        await waitFor(() => {
          expectToBeInTheDocument(
            screen.queryByText(`${I18nNamespace.COMMON}:unsupportedBrowser`),
            !isSupported
          );
        });

        if (isSupported) {
          const calls = consoleMock.mock.calls;
          calls.forEach((call) => {
            expect(call).not.toContain(unsupportedBrowserLogMessage);
          });
        } else {
          expect(attemptedLogin).toBe(false);
          expect(consoleMock.mock.calls.length).toBeGreaterThanOrEqual(1);
          expect(consoleMock.mock.calls[0][0]).toContain(
            unsupportedBrowserLogMessage
          );
        }
      }
    );
  });
});
