if [ $(git status --porcelain=v1 src/api/generated 2>/dev/null | wc -l) -eq "0" ]; then
  echo "  🟢 Generated API is up to date!"
else
  echo "  🔴 Generated API is out of date! Please run 'npm run generate-api' in the frontend to re-generate the backend API."
  exit 1
fi
