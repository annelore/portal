module.exports = {
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
    keySeparator: '.',
  },
  interpolation: {
    format: (value, format) => {
      if (format === 'lowercase') {
        return value.toLowerCase();
      }
      return value;
    },
  },
  serializeConfig: false,
};
