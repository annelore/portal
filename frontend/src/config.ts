import packageJson from '../package.json';

export const version = packageJson.version;

export const contactShortName = process.env.NEXT_PUBLIC_CONTACT_SHORT_NAME;
export const contactName = process.env.NEXT_PUBLIC_CONTACT_NAME;
export const contactEmail = process.env.NEXT_PUBLIC_CONTACT_EMAIL;
// Debounce time between unique validation calls to the backend in milliseconds
export const uniqueValidationDebounce =
  process.env.NEXT_PUBLIC_UNIQUE_VALIDATION_DEBOUNCE;
