/*
 Copyright 2020 Nurjin Jafar
 Copyright 2020 Nordeck IT + Consulting GmbH.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
/**
 * Source: https://github.com/matrix-org/matrix-react-sdk/blob/develop/src/components/views/elements/EffectsOverlay.tsx
 * Significant changes were made.
 */
import React, {
  ReactElement,
  Ref,
  useEffect,
  useImperativeHandle,
  useRef,
} from 'react';
import { ICanvasEffect } from './ICanvasEffect';
import { EffectCommand, effects } from 'widgets/effects/index';

export type EffectRef = {
  start: (effect: EffectCommand) => void;
};

export const EffectsOverlay = React.forwardRef<EffectRef>(
  (_, ref: Ref<EffectRef>): ReactElement => {
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const effectsRef = useRef<Map<string, ICanvasEffect>>(
      new Map<string, ICanvasEffect>()
    );

    const lazyLoadEffectModule = async (
      name: string
    ): Promise<ICanvasEffect | null> => {
      if (!name) return null;
      let effect: ICanvasEffect | null = effectsRef.current[name] || null;
      if (effect === null) {
        const options = effects[name]?.options;
        try {
          const { default: Effect } = await import(`./${name}`);
          effect = new Effect(options);
          effectsRef.current[name] = effect;
        } catch (err) {
          console.warn("Unable to load effect module at './${name}'.", err);
        }
      }
      return effect;
    };

    useImperativeHandle(ref, () => ({
      start: (effect: EffectCommand) => {
        lazyLoadEffectModule(effect).then((module) => {
          const canvas = canvasRef.current;
          if (canvas) {
            module?.start(canvas);
          }
        });
      },
    }));

    useEffect(() => {
      const resize = () => {
        if (canvasRef.current) {
          canvasRef.current.height = window.innerHeight;
          canvasRef.current.width = window.innerWidth;
        }
      };
      const canvas = canvasRef.current;
      if (canvas) {
        canvas.height = window.innerHeight;
        canvas.width = window.innerWidth;
      }
      window.addEventListener('resize', resize, true);

      return () => {
        window.removeEventListener('resize', resize);
        // eslint-disable-next-line react-hooks/exhaustive-deps
        const currentEffects = effectsRef.current; // this is not a react node ref, warning can be safely ignored
        for (const effect in currentEffects) {
          const effectModule: ICanvasEffect = currentEffects[effect];
          if (effectModule && effectModule.isRunning) {
            effectModule.stop();
          }
        }
      };
    }, []);

    return (
      <canvas
        ref={canvasRef}
        style={{
          display: 'block',
          zIndex: 999999,
          pointerEvents: 'none',
          position: 'fixed',
          top: 0,
          right: 0,
        }}
      />
    );
  }
);

EffectsOverlay.displayName = 'EffectsOverlay';
