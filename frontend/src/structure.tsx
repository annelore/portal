import { any, PagePermissions } from './permissions';
import React from 'react';
import { $Keys } from 'utility-types';
import {
  FeedbackIcon,
  HomeIcon,
  KeyIcon,
  Menu,
  MessageIcon,
  PeopleIcon,
  ProjectIcon,
  UserMenuItemModel,
  WorkIcon,
} from '@biomedit/next-widgets';

type StructureItem = {
  title: string;
  menu?: Menu;
  permission?: $Keys<PagePermissions>;
};

type MenuItem = Partial<Menu> & Pick<StructureItem, 'title' | 'permission'>;

export const structure: StructureItem[] = [
  {
    title: 'Home',
    menu: { link: '/', icon: <HomeIcon /> },
  },
  {
    title: 'Administration',
    menu: { link: '/admin', icon: <PeopleIcon /> },
    permission: 'userManage',
  },
  {
    title: 'Projects',
    menu: { link: '/projects', icon: <ProjectIcon /> },
  },
  {
    title: 'Data Transfers',
    menu: { link: '/data-transfers', icon: <WorkIcon /> },
    permission: 'dataTransfer',
  },
  {
    title: 'Messages',
    menu: { link: '/messages', icon: <MessageIcon /> },
  },
  {
    title: 'My Keys',
    menu: { link: '/keys', icon: <KeyIcon /> },
  },
  {
    title: 'Contact Us',
    menu: { link: '/contact', icon: <FeedbackIcon /> },
  },
];

export function createMenuItems(
  permissionsByPage: PagePermissions
): MenuItem[] {
  return structure
    .map(({ title, menu, permission }) => ({ title, permission, ...menu }))
    .filter((entry) => (entry as Menu).link)
    .filter(
      (entry) =>
        entry.permission === undefined ||
        (permissionsByPage && any(permissionsByPage[entry.permission]))
    );
}

export const userMenu: Array<UserMenuItemModel> = [];
