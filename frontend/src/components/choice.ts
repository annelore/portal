import {
  DataProvider,
  Flag,
  Group,
  Node,
  Permission,
  User,
} from '../api/generated';
import { DeepRequired } from 'utility-types';
import { useCallback } from 'react';
import { Choice, useChoices } from '@biomedit/next-widgets';

export const useUserChoices = (users: DeepRequired<User>[]): Choice[] =>
  useChoices(users, 'id', 'profile.displayName');

export const useNodeChoices = (nodes: Node[]): Choice[] =>
  useChoices<Node>(nodes, 'code', 'name');

export const useFlagChoices = (flags: Flag[]): Choice[] =>
  useChoices<Flag>(flags, 'code', 'code');

export const useGroupChoices = (groups: Group[]): Choice[] =>
  useChoices<Group>(groups, 'name', 'name');

export const useDataProviderChoices = (
  dataProviders: DataProvider[]
): Choice[] =>
  useChoices<DataProvider>(
    dataProviders,
    'code',
    useCallback((dataProvider: DataProvider): string => {
      return `${dataProvider.code} (${dataProvider.name})`;
    }, [])
  );

export const usePermissionChoices = (permissions: Permission[]): Choice[] =>
  useChoices<Permission>(permissions, 'id', 'name');
