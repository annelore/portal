import React, { ReactElement } from 'react';
import { version } from '../config';
import { Typography } from '@biomedit/next-widgets';

export const Version = (): ReactElement => (
  <Typography
    title="Portal Version"
    sx={{
      textAlign: 'center',
      marginTop: 'auto!important',
      paddingBottom: 2,
    }}
  >
    {version}
  </Typography>
);
