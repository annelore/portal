import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import staffState from '../../../test-data/staffState.json';
import { UserForm } from './UserManageForm';
import { User } from '../../api/generated';
import {
  RequestVerifier,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { rest } from 'msw';
import { backend } from '../../api/api';
import listFlagsResponse from '../../../test-data/listFlagsResponse.json';
import listProjectsResponse from '../../../test-data/listProjectsResponse.json';
import listGroupsResponse from '../../../test-data/listGroupsResponse.json';

describe('UserManageForm', () => {
  let listProjects;
  const listFlags = `${backend}flags/`;
  const listGroups = `${backend}identity/group/`;

  const differentUser: User = {
    ...staffState.auth.user,
    username: 'differentUser',
  };

  let server;

  const createServer = (editedUser: string): void => {
    listProjects = `${backend}projects/`;

    server = setupMockApi(
      rest.get(listProjects, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listProjectsResponse))
      ),
      rest.get(listFlags, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listFlagsResponse))
      ),
      rest.get(listGroups, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listGroupsResponse))
      )
    );

    // msw only needs the path
    listProjects += `?ordering=name&username=${encodeURIComponent(editedUser)}`;
  };

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it.each`
    editedUser              | canDisable | description
    ${staffState.auth.user} | ${false}   | ${'prevent the currently logged in user from inactivating themself'}
    ${differentUser}        | ${true}    | ${'allow the currently logged in user to inactivate a different user'}
  `('should $description', async ({ editedUser, canDisable }) => {
    createServer(editedUser.username);
    const verifier = new RequestVerifier(server);

    render(
      <Provider
        store={makeStore(undefined, { ...getInitialState(), ...staffState })}
      >
        <UserForm user={editedUser} onClose={jest.fn()} />
      </Provider>
    );

    const isActiveCheckbox = await screen.findByRole('checkbox');
    expect(isActiveCheckbox).toBeInTheDocument();
    if (canDisable) {
      expect(isActiveCheckbox).toBeEnabled();
    } else {
      expect(isActiveCheckbox).toBeDisabled();
    }

    verifier.assertCount(3);
    verifier.assertCalled(listProjects, 1);
    verifier.assertCalled(listFlags, 1);
    verifier.assertCalled(listGroups, 1);
  });
});
