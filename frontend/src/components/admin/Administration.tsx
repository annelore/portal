import React, { ReactElement, useMemo } from 'react';
import { UserList } from './UserManage';
import { GroupList } from './GroupManage';
import { DataProviderList } from './DataProviderManage';
import { FlagManageList } from './FlagManage';
import { NodeList } from './NodeManage';
import { useSelector } from 'react-redux';
import {
  canSeeDataProviderTab,
  canSeeGroupTab,
  canSeeNodeTab,
  isStaff,
} from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { TabItem, TabPane } from '@biomedit/next-widgets';

export const Administration = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.ADMINISTRATION);
  const staff = useSelector(isStaff);
  const showNodeTab = useSelector(canSeeNodeTab);
  const showDataProviderTab = useSelector(canSeeDataProviderTab);
  const showGroupTab = useSelector(canSeeGroupTab);

  const tabItems = useMemo(() => {
    const items: TabItem[] = [];
    if (staff) {
      items.push({
        label: t('tabs.users'),
        content: <UserList />,
      });
    }
    if (showGroupTab) {
      items.push({
        label: t('tabs.groups'),
        content: <GroupList />,
      });
    }
    if (showNodeTab) {
      items.push({
        label: t('tabs.nodes'),
        content: <NodeList />,
      });
    }
    if (showDataProviderTab) {
      items.push({
        label: t('tabs.dataProviders'),
        content: <DataProviderList />,
      });
    }
    if (staff) {
      items.push({
        label: t('tabs.flags'),
        content: <FlagManageList />,
      });
    }
    return items;
  }, [showGroupTab, t, staff, showNodeTab, showDataProviderTab]);

  return <TabPane id="Administration" label={t('label')} model={tabItems} />;
};
