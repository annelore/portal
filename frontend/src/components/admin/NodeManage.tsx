import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_NODES } from '../../actions/actionTypes';
import { Node } from '../../api/generated';
import { EnhancedTable, requestAction } from '@biomedit/next-widgets';
import { IdRequired } from '../../global';
import { useNodeColumns, useNodeForm } from './AdministrationHooks';
import { isNodeAdmin, isStaff } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';

export const NodeList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.NODE_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const isFetching = useSelector((state) => state.nodes.isFetching);
  const isSubmitting = useSelector((state) => state.nodes.isSubmitting);
  const staff = useSelector(isStaff);
  const nodeAdmin = useSelector(isNodeAdmin);
  const username = useSelector((state) => state.auth.user?.username);
  const nodes = useSelector(
    (state) => state.nodes.itemList as IdRequired<Node>
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (staff) {
      dispatch(requestAction(LOAD_NODES));
    } else {
      dispatch(requestAction(LOAD_NODES, { username }));
    }
  }, [dispatch, username, staff]);

  const { openFormDialog, nodeForm } = useNodeForm();

  const columns = useNodeColumns();
  return (
    <EnhancedTable<Required<Node>>
      itemList={nodes}
      columns={columns}
      canAdd={staff}
      canEdit={staff || nodeAdmin}
      canDelete={false} // it's forbidden to delete nodes
      onEdit={openFormDialog}
      onAdd={openFormDialog}
      form={nodeForm}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
      addButtonLabel={t(`${I18nNamespace.NODE_LIST}:addButton`)}
      emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.node_plural`),
      })}
      inline
    />
  );
};
