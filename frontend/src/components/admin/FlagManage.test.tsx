import { FlagManageList } from './FlagManage';
import { backend } from '../../api/api';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { makeStore } from '../../store';
import React from 'react';
import listFlagsResponse from '../../../test-data/listFlagsResponse.json';
import { rest } from 'msw';
import '@testing-library/jest-dom/extend-expect';
import { resBody, resJson, setupMockApi } from '@biomedit/next-widgets';

describe('FlagManageList.fetchFlags', () => {
  let server;
  const createServer = (body) => {
    return setupMockApi(
      rest.get(`${backend}flags/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, body))
      )
    );
  };

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it('should display empty list', async () => {
    server = createServer([]);
    render(
      <Provider store={makeStore()}>
        <FlagManageList />
      </Provider>
    );
    // Empty message (NOT i18n'ed)
    expect(await screen.findByText('list:emptyMessage')).toBeInTheDocument();
  });

  it('should list the flags', async () => {
    server = createServer(listFlagsResponse);
    render(
      <Provider store={makeStore()}>
        <FlagManageList />
      </Provider>
    );
    // Table header (1x), plus flag rows (2x), plus table footer (1x)
    expect(await screen.findAllByRole('row')).toHaveLength(4);
    expect(await screen.findByText('sis')).toBeInTheDocument();
    expect(await screen.findByText('sphn')).toBeInTheDocument();
  });
});
