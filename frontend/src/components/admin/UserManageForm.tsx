import React, { ReactElement, useEffect } from 'react';
import { Required } from 'utility-types';
import {
  CheckboxField,
  ConfirmLabel,
  Description,
  formatDate,
  FormDialog,
  HiddenField,
  requestAction,
  SelectField,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { User } from '../../api/generated';
import { FormProvider } from 'react-hook-form';
import { useUserInfoFields } from '../nav/UserInfoBox';
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  CALL,
  CLEAR_PROJECTS,
  LOAD_FLAGS,
  LOAD_GROUPS,
  LOAD_PROJECTS,
  UPDATE_USER,
} from '../../actions/actionTypes';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { useFlagChoices, useGroupChoices } from '../choice';
import { FormFieldsContainer } from '../FormFieldsContainer';

export type FormUser = Required<Partial<User>, 'id'>;

type UserFormProps = {
  user: FormUser;
  onClose: () => void;
};

export const UserForm = ({ user, onClose }: UserFormProps): ReactElement => {
  const dispatch = useDispatch();
  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.USER_MANAGE_FORM,
  ]);

  const projects = useSelector((state) => state.project.itemList);
  const isFetchingProjects = useSelector((state) => state.project.isFetching);
  const isSubmitting = useSelector((state) => state.users.isSubmitting);
  const flags = useSelector((state) => state.flags.itemList);
  const isFetchingFlags = useSelector((state) => state.flags.isFetching);
  const currentUsername = useSelector((state) => state.auth.user?.username);
  const groups = useSelector((state) => state.groups.itemList);
  const isFetchingGroups = useSelector((state) => state.groups.isFetching);
  const username = user.username;

  const form = useEnhancedForm<User>();

  function submit(formUser: User) {
    dispatch(
      requestAction(
        UPDATE_USER,
        { user: formUser, id: String(formUser.id) },
        { type: CALL, callback: close }
      )
    );
  }

  function close() {
    dispatch({ type: CLEAR_PROJECTS });
    onClose();
  }

  const userInfoEntries = useUserInfoFields(user as User);
  userInfoEntries.push({
    caption: t(`${I18nNamespace.USER_LIST}:columns.lastLogin`),
    component: formatDate(user.lastLogin, true),
  });
  if (isFetchingProjects || !projects || !projects.length) {
    userInfoEntries.push({
      caption: t(`${I18nNamespace.USER_MANAGE_FORM}:captions.projects`),
      component: t(`${I18nNamespace.USER_MANAGE_FORM}:noProjectsPlaceholder`),
    });
  } else if (projects.length) {
    userInfoEntries.push({
      caption: t(`${I18nNamespace.USER_MANAGE_FORM}:captions.projects`),
      component: projects.map((p) => p.code).join(', '),
    });
  }

  useEffect(() => {
    batch(() => {
      dispatch(requestAction(LOAD_PROJECTS, { username, ordering: 'name' }));
      dispatch(requestAction(LOAD_FLAGS));
      dispatch(requestAction(LOAD_GROUPS));
    });
  }, [dispatch, username]);

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'md'}
        title={t(`${I18nNamespace.USER_MANAGE_FORM}:title`)}
        open={!!user}
        confirmLabel={ConfirmLabel.SAVE}
        onSubmit={submit}
        onClose={close}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: user.id }} />
        <Description entries={userInfoEntries} labelWidth="25%" />
        <FormFieldsContainer>
          <SelectField
            name="flags"
            label={t(`${I18nNamespace.USER_MANAGE_FORM}:captions.flags`)}
            initialValues={user}
            multiple
            choices={useFlagChoices(flags)}
            isLoading={isFetchingFlags}
          />
          <SelectField
            name="groups"
            label={t(`${I18nNamespace.USER_MANAGE_FORM}:captions.groups`)}
            initialValues={user}
            multiple
            choices={useGroupChoices(groups)}
            isLoading={isFetchingGroups}
          />
          <CheckboxField
            name="isActive"
            label={t(`${I18nNamespace.USER_LIST}:columns.isActive`)}
            initialValues={user}
            disabled={currentUsername === username}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
