import React, { ReactElement } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormProvider } from 'react-hook-form';
import {
  ConfirmLabel,
  email,
  FormDialog,
  HiddenField,
  idValidations,
  LabelledField,
  nameValidations,
  requestAction,
  required,
  SelectField,
  useEnhancedForm,
  useEnumChoices,
} from '@biomedit/next-widgets';
import { Node, NodeNodeStatusEnum } from '../../api/generated';
import { ADD_NODE, CALL, UPDATE_NODE } from '../../actions/actionTypes';
import { generatedBackendApi } from '../../api/api';
import { isStaff } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { FormFieldsContainer } from '../FormFieldsContainer';

type NodeFormProps = {
  node: Partial<Node>;
  onClose: () => void;
};

export const NodeForm = ({ node, onClose }: NodeFormProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.NODE_LIST]);
  const staff = useSelector(isStaff);
  const isEdit = !!node.id;

  const dispatch = useDispatch();

  const isSubmitting = useSelector((state) => state.nodes.isSubmitting);

  const form = useEnhancedForm<Node>();

  function submit(node: Node) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_NODE,
          {
            id: String(node.id),
            node,
          },
          onSuccessAction
        )
      );
    } else {
      dispatch(
        requestAction(
          ADD_NODE,
          {
            node,
          },
          onSuccessAction
        )
      );
    }
  }

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueNode({ uniqueNode: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={isEdit ? `Node: ${node?.code}` : 'New Node'}
        open={!!node}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: node.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="code"
            type="text"
            label={t(`${I18nNamespace.NODE_LIST}:columns.code`)}
            validations={idValidations()}
            unique={uniqueCheck}
            initialValues={node}
            fullWidth
            disabled={!staff}
          />
          <LabelledField
            name="name"
            type="text"
            label={t(`${I18nNamespace.NODE_LIST}:columns.name`)}
            validations={nameValidations()}
            unique={uniqueCheck}
            initialValues={node}
            fullWidth
          />
          <SelectField
            name="nodeStatus"
            label={t(`${I18nNamespace.NODE_LIST}:columns.nodeStatus`)}
            initialValues={node}
            required
            choices={useEnumChoices(NodeNodeStatusEnum)}
          />
          <LabelledField
            name="ticketingSystemEmail"
            type="email"
            label={t(`${I18nNamespace.NODE_LIST}:columns.ticketingSystemEmail`)}
            validations={[required, email]}
            initialValues={node}
            fullWidth
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
