import React, { ReactElement, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_USERS } from '../../actions/actionTypes';
import { User } from '../../api/generated';
import {
  EnhancedTable,
  requestAction,
  Tooltip,
  Typography,
  useFormDialog,
} from '@biomedit/next-widgets';
import { IdRequired } from '../../global';
import { useUserColumns } from './AdministrationHooks';
import { FormUser, UserForm } from './UserManageForm';
import { FormAddLocalUser, UserAddLocalForm } from './UserAddLocalForm';
import { isStaff } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { affiliationFieldCaption, flagFieldCaption } from '../nav/UserInfoBox';

export function UsernameField(
  username: string,
  affiliation: string | undefined,
  flags: Array<string> | undefined
): ReactElement {
  const { t } = useTranslation(I18nNamespace.USER_INFO);
  const name = useMemo(() => username, [username]);
  const userAffiliation = useMemo(
    () => (affiliation || '-').replace(',', ', '),
    [affiliation]
  );
  const userFlags = useMemo(
    () => (flags && flags.length ? flags.join(', ') : '-'),
    [flags]
  );
  return (
    <Tooltip
      title={
        <>
          <Typography key="affiliations" variant="caption" display="block">
            {t(affiliationFieldCaption)}: {userAffiliation}
          </Typography>
          <Typography key="flags" variant="caption" display="block">
            {t(flagFieldCaption)}: {userFlags}
          </Typography>
        </>
      }
    >
      <span>{name}</span>
    </Tooltip>
  );
}

export const UserList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const isFetching = useSelector((state) => state.users.isFetching);
  const isSubmitting = useSelector((state) => state.users.isSubmitting);
  const users = useSelector(
    (state) => state.users.itemList as IdRequired<User>
  );
  const staff = useSelector(isStaff);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_USERS, { includeInactive: true }));
  }, [dispatch]);

  const { closeFormDialog, data, openFormDialog } =
    useFormDialog<FormUser>(undefined);
  const {
    closeFormDialog: closeAddLocalFormDialog,
    data: addLocalData,
    openFormDialog: openAddLocalFormDialog,
  } = useFormDialog<FormAddLocalUser>({});
  const userForm = useMemo(() => {
    if (data) {
      return <UserForm user={data} onClose={closeFormDialog} />;
    }
    if (addLocalData) {
      return (
        <UserAddLocalForm
          user={addLocalData}
          onClose={closeAddLocalFormDialog}
        />
      );
    }
  }, [data, closeFormDialog, addLocalData, closeAddLocalFormDialog]);

  const columns = useUserColumns();
  return (
    <EnhancedTable<Required<User>>
      itemList={users}
      columns={columns}
      canAdd={staff}
      canEdit={staff}
      canDelete={false} // it's forbidden to delete users
      onAdd={openAddLocalFormDialog}
      onEdit={openFormDialog}
      form={userForm}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
      emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.user_plural`),
      })}
      addButtonLabel={t(`${I18nNamespace.USER_LIST}:addButton`)}
      inline
    />
  );
};
