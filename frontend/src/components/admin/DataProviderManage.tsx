import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_DATA_PROVIDERS } from '../../actions/actionTypes';
import { DataProvider } from '../../api/generated';
import { IdRequired } from '../../global';
import {
  useDataProviderColumns,
  useDataProviderForm,
} from './AdministrationHooks';
import { isDataProviderAdmin, isStaff } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { EnhancedTable, requestAction } from '@biomedit/next-widgets';

export const DataProviderList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.DATA_PROVIDER_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const isFetching = useSelector((state) => state.dataProvider.isFetching);
  const isSubmitting = useSelector((state) => state.dataProvider.isSubmitting);
  const dataProviders = useSelector(
    (state) => state.dataProvider.itemList as IdRequired<DataProvider>
  );
  const staff = useSelector(isStaff);
  const dataProviderAdmin = useSelector(isDataProviderAdmin);
  const username = useSelector((state) => state.auth.user?.username);

  const dispatch = useDispatch();

  useEffect(() => {
    if (staff) {
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
    } else {
      dispatch(requestAction(LOAD_DATA_PROVIDERS, { username }));
    }
  }, [dispatch, username, staff]);

  const { openFormDialog, dataProviderForm } = useDataProviderForm();

  const columns = useDataProviderColumns();
  return (
    <EnhancedTable<Required<DataProvider>>
      itemList={dataProviders}
      columns={columns}
      canAdd={staff}
      canEdit={staff || dataProviderAdmin}
      canDelete={false} // it's forbidden to delete data providers
      onEdit={openFormDialog}
      onAdd={openFormDialog}
      addButtonLabel={t(`${I18nNamespace.DATA_PROVIDER_LIST}:addButton`)}
      form={dataProviderForm}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
      emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.dataProvider_plural`),
      })}
      inline
    />
  );
};
