import React, { ReactElement } from 'react';
import {
  ConfirmLabel,
  email,
  FormDialog,
  LabelledField,
  requestAction,
  required,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { User } from '../../api/generated';
import { FormProvider } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import {
  ADD_LOCAL_USER,
  CALL,
  CLEAR_PROJECTS,
} from '../../actions/actionTypes';
import { generatedBackendApi } from '../../api/api';
import { FormFieldsContainer } from '../FormFieldsContainer';

export type FormAddLocalUser = Pick<
  Partial<User>,
  'firstName' | 'lastName' | 'email'
>;

type UserFormProps = {
  user: FormAddLocalUser;
  onClose: () => void;
};

export const UserAddLocalForm = ({
  user,
  onClose,
}: UserFormProps): ReactElement => {
  const dispatch = useDispatch();

  const isSubmitting = useSelector((state) => state.users.isSubmitting);

  const form = useEnhancedForm<User>();

  function submit(user: User) {
    dispatch(
      requestAction(ADD_LOCAL_USER, { user }, { type: CALL, callback: close })
    );
  }

  function close() {
    dispatch({ type: CLEAR_PROJECTS });
    onClose();
  }

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueUser({ uniqueUser: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title="Add local user"
        open={!!user}
        confirmLabel={ConfirmLabel.SAVE}
        onSubmit={submit}
        onClose={close}
        isSubmitting={isSubmitting}
      >
        <FormFieldsContainer>
          <LabelledField
            name="firstName"
            type="text"
            label="First name"
            fullWidth
            required
          />
          <LabelledField
            name="lastName"
            type="text"
            label="Last name"
            fullWidth
            required
          />
          <LabelledField
            name="email"
            type="email"
            label="Email address"
            validations={[required, email]}
            unique={uniqueCheck}
            required
            fullWidth
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
