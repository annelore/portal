import { useFieldArray, useFormContext } from 'react-hook-form';
import { styled } from '@mui/material/styles';
import {
  AutocompleteArrayField,
  Box,
  Choice,
  FabButton,
  isNotEmpty,
} from '@biomedit/next-widgets';
import React, {
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { ListItem } from '../projectForm/IpRangeList';
import { useSelector } from 'react-redux';
import { usePermissionChoices } from '../choice';
import { generatedBackendApi } from '../../api/api';
import produce from 'immer';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { isStaff } from '../selectors';
import { GroupPermissionsObject } from '../../api/generated';

type AvailableObjects = Record<string, Choice[]>;
type AvailableObjectsIsFetching = Record<string, boolean>;

const StyledAutocompleteArrayField = styled(AutocompleteArrayField)(
  ({ theme }) => ({
    width: '100%',
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  })
);

export const PermissionsObjectList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.GROUP_MANAGE_FORM,
  ]);
  const arrayName = 'permissionsObject';

  const { control, getValues, watch, reset } = useFormContext();

  const { fields, append, remove } = useFieldArray({
    control,
    name: arrayName,
  });

  const permissions = useSelector((state) => state.permissions.itemList);
  const permissionChoices = usePermissionChoices(permissions);
  const isFetchingPermissions = useSelector(
    (state) => state.permissions.isFetching
  );
  const staff = useSelector(isStaff);

  const watchPermission = watch('permissionsObject');

  const [availableObjects, setAvailableObjects] = useState<AvailableObjects>(
    {}
  );
  const [availableObjectsIsFetching, setAvailableObjectsIsFetching] =
    useState<AvailableObjectsIsFetching>({});

  useEffect(() => {
    const permissionsToFetch = watchPermission
      .filter(
        (row: GroupPermissionsObject) =>
          // row.permission is undefined when a new row is added
          row.permission !== undefined &&
          // row.permission becomes null when permission (first column in the form) is cleared
          row.permission !== null &&
          !availableObjectsIsFetching[row.permission] &&
          !(row.permission in availableObjects)
      )
      .map((row) => row.permission);

    if (permissionsToFetch.length > 0) {
      const newAvailableObjectsIsFetching = produce(
        availableObjectsIsFetching,
        (draft: typeof availableObjectsIsFetching) => {
          permissionsToFetch.forEach((p) => {
            draft[p] = true;
          });
        }
      );
      setAvailableObjectsIsFetching(newAvailableObjectsIsFetching);

      generatedBackendApi
        .listObjectByPermissionViewSets({ permId: permissionsToFetch })
        .then((result) => {
          setAvailableObjects(
            produce(availableObjects, (draft: AvailableObjects) => {
              for (const perm in result) {
                draft[perm] = result[perm].map((obj) => ({
                  key: obj.id,
                  value: obj.id,
                  label: obj.name,
                }));
              }
            })
          );
        })
        .finally(() => {
          setAvailableObjectsIsFetching(
            produce(
              newAvailableObjectsIsFetching,
              (draft: AvailableObjectsIsFetching) => {
                permissionsToFetch.forEach((p) => {
                  draft[p] = false;
                });
              }
            )
          );
        });
    }
  }, [availableObjects, availableObjectsIsFetching, watchPermission]);

  useEffect(() => {
    if (!isFetchingPermissions) {
      // new data is available, which could change `defaultValue` of `objects` fields
      // see https://github.com/react-hook-form/react-hook-form/issues/1558#issuecomment-623196472
      reset();
    }
  }, [isFetchingPermissions, reset]);

  const onClick = useCallback(() => {
    const values = getValues()[arrayName];
    const len = values.length;
    // Append only if empty or if last entry is not empty
    if (len === 0 || isNotEmpty(values[len - 1])) {
      append({});
    }
  }, [append, getValues]);

  const fieldNodes = useMemo(
    () =>
      fields.map((field, index) => {
        const permission =
          getValues()['permissionsObject']?.[index]?.['permission'];
        return (
          <ListItem key={field.id}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'nowrap',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
              }}
            >
              <FabButton
                size="small"
                icon="del"
                onClick={() => remove(index)}
                sx={{
                  marginRight: 1,
                  // 'del' small sized button is 40x40. We specify a 'minWidth' so that it does
                  // not get resized too small. Otherwise it looks ugly.
                  minWidth: 40,
                }}
                disabled={!staff}
              />
              <StyledAutocompleteArrayField
                field={field}
                index={index}
                label={t(`${I18nNamespace.COMMON}:models.permission`)}
                arrayName={arrayName}
                fieldName="permission"
                required
                choices={permissionChoices}
                isLoading={isFetchingPermissions}
                width="50%"
                disabled={!staff}
              />
              <StyledAutocompleteArrayField
                field={field}
                index={index}
                label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.object`)}
                arrayName={arrayName}
                fieldName="objects"
                required
                multiple
                choices={availableObjects[permission] ?? []}
                isLoading={availableObjectsIsFetching[permission]}
                width="50%"
                disabled={!staff}
              />
            </Box>
          </ListItem>
        );
      }),
    [
      getValues,
      fields,
      availableObjects,
      availableObjectsIsFetching,
      isFetchingPermissions,
      permissionChoices,
      remove,
      staff,
      t,
    ]
  );

  return (
    <div>
      <FabButton size="small" icon="add" onClick={onClick} disabled={!staff} />
      {fieldNodes}
    </div>
  );
};
