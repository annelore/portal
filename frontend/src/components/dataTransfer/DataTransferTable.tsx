import React, { ReactElement, useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  DataTransfer,
  Project,
  ProjectUsersRolesEnum,
} from '../../api/generated';
import {
  DELETE_DATA_TRANSFER,
  LOAD_DATA_PROVIDERS,
} from '../../actions/actionTypes';
import {
  MaxPackagesToggleState,
  useDataTransferForm,
} from './DataTransferForm';
import {
  EnhancedTable,
  requestAction,
  Tooltip,
  TooltipText,
  Typography,
  useDialogState,
} from '@biomedit/next-widgets';
import { IdRequired } from '../../global';
import { Required } from 'utility-types';
import {
  useDataTransferColumns,
  useGlobalDataTransferFilter,
} from './DataTransferHooks';
import { DataTransferDetail } from './DataTransferDetail';
import { isStaff } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';

type DataProviderFieldProps = { code?: string };

export function DataProviderField({
  code,
}: DataProviderFieldProps): ReactElement {
  const dataProviders = useSelector((state) => state.dataProvider.itemList);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!dataProviders.length) {
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
    }
  }, [dataProviders, dispatch]);

  const dataProvider = dataProviders.find(
    (dataProvider) => dataProvider.code === code
  );
  if (dataProvider) {
    return <TooltipText title={dataProvider.name} text={dataProvider.code} />;
  } else {
    return <>{code}</>;
  }
}

export function MaxPackagesField(maxPackages: number): ReactElement {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);
  return (
    <>
      {maxPackages === MaxPackagesToggleState.UNLIMITED
        ? t<string>('maxPackages', { context: String(maxPackages) })
        : maxPackages}
    </>
  );
}

export function RequestorField({
  requestorFirstName,
  requestorLastName,
  requestorEmail,
  requestorDisplayId,
  requestorPgpKeyFp,
}: Pick<
  DataTransfer,
  | 'requestorFirstName'
  | 'requestorLastName'
  | 'requestorEmail'
  | 'requestorDisplayId'
  | 'requestorPgpKeyFp'
>): ReactElement {
  const name = useMemo(
    () => `${requestorFirstName} ${requestorLastName}`,
    [requestorFirstName, requestorLastName]
  );
  const fingerprint = requestorPgpKeyFp || 'No PGP fingerprint';

  const requestorRest: Array<string | undefined> = useMemo(
    () => [requestorDisplayId, requestorEmail],
    [requestorDisplayId, requestorEmail]
  );

  return (
    <Tooltip
      title={
        <>
          {requestorRest.map((part) => (
            <Typography
              key={'requestor-rest-' + part}
              variant="caption"
              display="block"
            >
              {part}
            </Typography>
          ))}
          <Typography key="fingerprint" variant="caption" display="block">
            {fingerprint}
          </Typography>
        </>
      }
    >
      <span>{name}</span>
    </Tooltip>
  );
}

export const dtrTableTitle = 'Data Transfers';
export const dtrTableAddButtonLabel = 'Data Transfer';

export const DataTransferTable = (
  project: IdRequired<Project>
): ReactElement => {
  const isFetching = useSelector((state) => state.project.isFetching);
  const isSubmitting = useSelector((state) => state.dataTransfers.isSubmitting);
  const staff = useSelector(isStaff);
  const canAdd = !project.archived && staff;

  const dispatch = useDispatch();
  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_DATA_TRANSFER, { id }));
    },
    [dispatch]
  );

  const { openFormDialog, dtrForm } = useDataTransferForm(project.id);

  const columns = useDataTransferColumns(false);

  const { item, setItem, onClose, open } = useDialogState<DataTransfer>();

  const globalFilter = useGlobalDataTransferFilter();

  const getItemName = useCallback(
    (item: Required<DataTransfer>) => String(item.id),
    []
  );

  return (
    <>
      <DataTransferDetail dtr={item} onClose={onClose} open={open} />
      <EnhancedTable<Required<DataTransfer>>
        itemList={project?.dataTransfers}
        columns={columns}
        title={dtrTableTitle}
        canAdd={canAdd}
        canEdit={canAdd}
        canDelete={staff}
        onAdd={openFormDialog}
        onEdit={openFormDialog}
        onRowClick={setItem}
        form={dtrForm}
        onDelete={deleteItem}
        addButtonLabel={dtrTableAddButtonLabel}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        globalFilter={globalFilter}
        getItemName={getItemName}
        inline
      />
    </>
  );
};

export function isDataManager(userId: number, project: Project): boolean {
  return !!project.users?.some(
    (u) => u.id === userId && u.roles.includes(ProjectUsersRolesEnum.DM)
  );
}
