import React, { ReactElement, useMemo } from 'react';
import {
  CancelLabel,
  ColoredStatus,
  Description,
  Dialog,
  Field,
  formatItemFields,
  FormattedItemField,
  ListModel,
  ListPage,
  TabItem,
  TabPane,
} from '@biomedit/next-widgets';
import { DataTransfer, ProjectPackages } from '../../api/generated';
import { MaxPackagesField, RequestorField } from './DataTransferTable';
import { IdRequired } from '../../global';
import { DataPackageTrace } from './DataPackageTrace';
import { statuses } from './DataTransferHooks';

export type DataTransferDetailProps = {
  dtr: DataTransfer | null;
  open: boolean;
  onClose: () => void;
};

export const DataTransferDetail = ({
  dtr,
  open,
  onClose,
}: DataTransferDetailProps): ReactElement => {
  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    if (!dtr) {
      return null;
    }

    const model: ListModel<DataTransfer> = {
      fields: [
        Field({
          caption: 'Project',
          getProperty: (dtr: DataTransfer) => dtr.projectName,
          key: 'projectName',
        }),
        Field({
          caption: 'Transfer ID',
          getProperty: (dtr: DataTransfer) => dtr.id,
          key: 'transferId',
        }),
        Field({
          caption: 'Max Number of Packages',
          getProperty: (dtr: DataTransfer) => dtr.maxPackages,
          render: MaxPackagesField,
          key: 'maxPackages',
        }),
        Field({
          caption: 'Transferred Packages',
          getProperty: (dtr: DataTransfer) => dtr.packages?.length,
          key: 'packageCount',
        }),
        Field({
          caption: 'Requestor',
          getProperty: (dtr: DataTransfer) => dtr,
          render: RequestorField,
          key: 'requestor',
        }),
        Field({
          caption: 'Purpose',
          getProperty: (dtr: DataTransfer) => dtr.purpose,
          key: 'purpose',
        }),
      ],
    };
    return formatItemFields<DataTransfer>(model, dtr);
  }, [dtr]);

  const logModel: ListModel<ProjectPackages> = useMemo(() => {
    return {
      getCaption: (pkg) => pkg.fileName,
      fields: [
        Field({
          getProperty: (pkg: ProjectPackages) => pkg,
          key: 'packages',
          render: DataPackageTrace,
        }),
      ],
    };
  }, []);

  const tabModel: TabItem[] = useMemo(
    () => [
      {
        label: 'Details',
        content: <Description entries={detailEntries} labelWidth={300} />,
      },
      {
        label: 'Log',
        content: (
          <ListPage<IdRequired<ProjectPackages>>
            model={logModel}
            emptyMessage={
              "This data transfer doesn't have any data packages yet."
            }
            itemList={dtr?.packages as ProjectPackages[]}
            canAdd={false}
            canDelete={false}
            canEdit={false}
            isFetching={false}
            isSubmitting={false}
          />
        ),
      },
    ],
    [detailEntries, dtr, logModel]
  );

  return (
    <Dialog
      open={open}
      isSubmitting={false} // read-only dialog
      fullWidth={true}
      maxWidth={'lg'}
      title={
        <span>
          <span>{`${dtr?.id} - ${dtr?.projectName}`}</span>
          <ColoredStatus
            value={dtr?.status}
            statuses={statuses}
            sx={{
              paddingLeft: 5,
              display: 'inline',
            }}
          />
        </span>
      }
      cancelLabel={CancelLabel.CLOSE}
      onClose={onClose}
    >
      <TabPane
        id="DataTransferDetail"
        label="Switch between details of the data transfer and the log of data packages."
        model={tabModel}
      />
    </Dialog>
  );
};
