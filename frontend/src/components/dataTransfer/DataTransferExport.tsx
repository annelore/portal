import { Json2CsvTransform, parse, transforms } from 'json2csv';
import { DataTransfer } from '../../api/generated';
import { useSelector } from 'react-redux';
import React, { ReactElement } from 'react';
import {
  FabButton,
  getFilenameWithTimestamp,
  Writeable,
} from '@biomedit/next-widgets';
import { saveAs } from 'file-saver';

type DataTransferCsv = Omit<Partial<Writeable<DataTransfer>>, 'maxPackages'> & {
  maxPackages: number | string;
};

type DataTransferTransform = Json2CsvTransform<
  DataTransferCsv,
  DataTransferCsv
>;

export function dataTransfersToCsv(
  dtrs: Array<Writeable<DataTransfer>>
): string {
  const removeRedundantFields: DataTransferTransform = (item) => {
    delete item['requestor'];
    delete item['requestorFirstName'];
    delete item['requestorLastName'];
    delete item['requestorEmail'];
    delete item['requestorDisplayId'];
    delete item['packages.dataTransfer'];
    delete item['packages.purpose'];
    delete item['packages.projectId'];
    return item;
  };

  return parse(dtrs, {
    transforms: [
      transforms.unwind({ paths: ['packages', 'packages.trace'] }),
      transforms.flatten(),
      removeRedundantFields,
    ],
  });
}

/**
 * Forces Excel to use "," as separator when splitting columns in a csv file.
 *
 * In European versions of Excel, it uses ";" as the delimiter by default (to avoid ambiguity with "," used as decimal
 * separator), while in American versions of Excel, "," is used as the delimiter by default (since "." is used as
 * decimal separator there).
 * By explicitly adding {@code sep=,} to the top of a csv file, Excel will be forced to using "," to split the columns,
 * while ignoring the first line in the opened spreadsheet.
 *
 * @see https://web.archive.org/web/20201116112112/https://kb.paessler.com/en/topic/2293-i-have-trouble-opening-csv-files-with-microsoft-excel-is-there-a-quick-way-to-fix-this#reply-30893
 * @param csv string with the contents of the csv file, as to be interpreted by Excel, delimited by ","
 */
export function addExcelSeparatorHeader(csv: string): string {
  return 'sep=,\n' + csv;
}

export const DataTransferExport = (): ReactElement | null => {
  const dtrs = useSelector((state) => state.dataTransfers.itemList);

  if (!dtrs || !dtrs.length) {
    return null;
  }

  const dtrsCsv = addExcelSeparatorHeader(dataTransfersToCsv(dtrs));
  const csvBlob = new Blob([dtrsCsv], { type: 'text/plain;charset=utf-8' });

  return (
    <FabButton
      icon="download"
      size="small"
      tooltip="Download as .csv"
      onClick={(): void =>
        saveAs(csvBlob, getFilenameWithTimestamp('data-transfers', '.csv'), {
          autoBom: true,
        })
      }
    />
  );
};
