import { Column } from 'react-table';
import { Required } from 'utility-types';
import { DataTransfer, DataTransferStatusEnum } from '../../api/generated';
import { useCallback, useMemo } from 'react';
import { MaxPackagesToggleState, UiDataTransfer } from './DataTransferForm';
import { DataProviderField, RequestorField } from './DataTransferTable';
import {
  ColoredStatus,
  GlobalFilterFunction,
  isInteger,
  Status,
  text,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';

/**
 * '-1' (standing for 'unlimited'/'infinity') gets converted into 'Number.MAX_SAFE_INTEGER'
 * for sorting purposes.
 * We can not use 'infinity'. Otherwise addition (in sorting) will not work.
 */
const maxPackagesValueOrInfinity = (value: number): number =>
  value === MaxPackagesToggleState.UNLIMITED ? Number.MAX_SAFE_INTEGER : value;

export const statuses: Status<DataTransferStatusEnum>[] = [
  {
    color: 'amber',
    value: DataTransferStatusEnum.INITIAL,
  },
  {
    color: 'green',
    value: DataTransferStatusEnum.AUTHORIZED,
  },
  {
    color: 'grey',
    value: DataTransferStatusEnum.EXPIRED,
  },
  {
    color: 'red',
    value: DataTransferStatusEnum.UNAUTHORIZED,
  },
];

export const useDataTransferColumns = (
  withProjectName: boolean
): Array<Column<Required<DataTransfer>>> => {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);
  return useMemo(() => {
    const columns: Array<Column<Required<DataTransfer>>> = [
      {
        id: 'id',
        Header: 'ID',
        accessor: 'id',
      },
      {
        id: 'dataProvider',
        Header: 'Data Provider',
        accessor: 'dataProvider',
        Cell: (row) => DataProviderField({ code: row.value }),
        sortType: 'caseInsensitive',
      },
      {
        id: 'maxPackages',
        Header: 'Maximum number of packages',
        accessor: (value: UiDataTransfer) => {
          const maxPackages = value.maxPackages;
          return maxPackages === MaxPackagesToggleState.UNLIMITED
            ? t<string>('maxPackages', { context: String(maxPackages) })
            : maxPackages;
        },
        sortType: (rowA, rowB) => {
          const a = maxPackagesValueOrInfinity(rowA.original.maxPackages);
          const b = maxPackagesValueOrInfinity(rowB.original.maxPackages);
          return a - b;
        },
      },
      {
        id: 'requestorName',
        Header: 'Requestor',
        // value returned by accessor is used for sorting and filtering and represents the array `row.value` in `Cell`
        accessor: (data: UiDataTransfer) => [
          data.requestorFirstName,
          data.requestorLastName,
          data.requestorEmail,
          data.requestorDisplayId,
          data.requestorPgpKeyFp,
        ],
        Cell: (row) =>
          RequestorField({
            requestorFirstName: row.value[0],
            requestorLastName: row.value[1],
            requestorEmail: row.value[2],
            requestorDisplayId: row.value[3],
            requestorPgpKeyFp: row.value[4],
          }),
        sortType: (rowA, rowB) => {
          const fullName = (row) =>
            (
              row.original.requestorFirstName + row.original.requestorLastName
            ).toLowerCase();
          return fullName(rowA).localeCompare(fullName(rowB));
        },
      },
      {
        id: 'purpose',
        Header: 'Purpose',
        accessor: 'purpose',
      },
      {
        id: 'status',
        Header: 'Status',
        accessor: 'status',
        Cell: (row) => ColoredStatus({ value: row.value, statuses }),
      },
    ];

    if (withProjectName) {
      columns.unshift({
        id: 'projectName',
        Header: 'Project',
        accessor: 'projectName',
        sortType: 'caseInsensitive',
      });
    }

    return columns;
  }, [t, withProjectName]);
};
export const useGlobalDataTransferFilter = (): GlobalFilterFunction<
  Required<DataTransfer>
> =>
  useCallback<GlobalFilterFunction<Required<DataTransfer>>>(
    (rows, columnIds, filterValue) => {
      if (isInteger(filterValue)) {
        return rows.filter(
          (row) =>
            row.values['id'] == filterValue ||
            row.values['maxPackages'] == filterValue
        );
      } else {
        return text(rows, columnIds, filterValue);
      }
    },
    []
  );
