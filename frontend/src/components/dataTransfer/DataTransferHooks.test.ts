import '@testing-library/jest-dom/extend-expect';
import {
  DataTransfer,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
} from '../../api/generated';
import { renderHook } from '@testing-library/react-hooks';
import { useGlobalDataTransferFilter } from './DataTransferHooks';
import { Row } from 'react-table';
import { GlobalFilterFunction } from '@biomedit/next-widgets';
import { Required } from 'utility-types';

let index = 0;

const lastName = 'Requestor Last Name';

const email = 'Requestor Email';

const getRow = ({
  id = 1,
  maxPackages = -1,
  firstName = '1',
  displayId = '1',
  pgpKeyFp = '1A1B1C1D1E1F',
  projectName = '1',
}): Row<Required<DataTransfer>> => {
  firstName = `Requestor First Name ${firstName}`;
  displayId = `Requestor DisplayId ${displayId}`;
  projectName = `Test Project ${projectName}`;
  return {
    cells: [],
    allCells: [],
    values: {
      id: id,
      maxPackages: maxPackages,
      requestorName: [firstName, lastName, email, displayId, pgpKeyFp],
      projectName: projectName,
    },
    getRowProps: jest.fn(),
    index: index++,
    original: {
      project: 7,
      maxPackages: maxPackages,
      dataProvider: 'Test Data Provider 1',
      purpose: DataTransferPurposeEnum.PRODUCTION,
      requestorPgpKeyFp: pgpKeyFp,
      id: id,
      packages: [],
      projectName: projectName,
      projectArchived: false,
      requestor: 7,
      requestorDisplayId: displayId,
      requestorEmail: email,
      requestorFirstName: firstName,
      requestorLastName: lastName,
      requestorName: `${firstName} ${lastName} (${displayId}) (${email})`,
      status: DataTransferStatusEnum.AUTHORIZED,
    },
    id: `${index++}`,
    subRows: [],
  };
};

const oneRow: Array<Row<Required<DataTransfer>>> = [getRow({ maxPackages: 1 })];

const oneRowMixed: Array<Row<Required<DataTransfer>>> = [
  getRow({
    id: 0,
    maxPackages: 1,
    firstName: '2',
    displayId: '3',
    pgpKeyFp: '4',
    projectName: '5',
  }),
];

const twoRows: Array<Row<Required<DataTransfer>>> = [
  getRow({
    firstName: '2',
    displayId: '2',
    pgpKeyFp: '2A2B',
    projectName: '2',
  }),
  getRow({ id: 2, maxPackages: 1 }),
];

const twoRowsPartial: Array<Row<Required<DataTransfer>>> = [
  getRow({
    id: 10,
  }),
  getRow({ pgpKeyFp: '10AF1' }),
];

const columnIds: string[] = [
  'id',
  'maxPackages',
  'requestorName',
  'projectName',
];

describe('DataTransferHooks', () => {
  describe('useGlobalDataTransferFilter', () => {
    let callback: GlobalFilterFunction<Required<DataTransfer>>;

    beforeEach(() => {
      const { result } = renderHook(() => useGlobalDataTransferFilter());
      callback = result.current;
    });

    it('should ignore values present in "original"', () => {
      const filteredRows = callback(oneRow, columnIds, 'Test Data Provider 1');
      expect(filteredRows).toHaveLength(0);
    });

    it.each`
      rows              | query                       | expectedIds | description
      ${oneRow}         | ${'Requestor'}              | ${[1]}      | ${'with one row containing the query in multiple fields'}
      ${oneRow}         | ${'1'}                      | ${[1]}      | ${'with one row containing the query in mutliple fields'}
      ${oneRow}         | ${'Requestor First Name 1'} | ${[1]}      | ${"where one row has the query as the requestor's first name"}
      ${oneRow}         | ${'Chuck Norris'}           | ${[]}       | ${'with one row not containing the query at all'}
      ${oneRowMixed}    | ${'0'}                      | ${[0]}      | ${'with one row, matching on id'}
      ${oneRowMixed}    | ${'1'}                      | ${[0]}      | ${'with one row, matching on maxPackages'}
      ${oneRowMixed}    | ${'2'}                      | ${[]}       | ${'with one row, NOT matching on firstName'}
      ${oneRowMixed}    | ${'3'}                      | ${[]}       | ${'with one row, NOT matching on displayId'}
      ${oneRowMixed}    | ${'4'}                      | ${[]}       | ${'with one row, NOT matching on pgpKeyFp'}
      ${oneRowMixed}    | ${'5'}                      | ${[]}       | ${'with one row, NOT matching on projectName'}
      ${twoRows}        | ${'Requestor'}              | ${[1, 2]}   | ${'with two rows having multiple fields matching the query'}
      ${twoRows}        | ${'1'}                      | ${[1, 2]}   | ${'with two rows, first one matching in id and second one matching in maxPackages'}
      ${twoRows}        | ${'2'}                      | ${[2]}      | ${'with two rows, only second one matching in id'}
      ${twoRows}        | ${'1b'}                     | ${[2]}      | ${'with two rows, only second one (partially) matching in pgpKeyFp'}
      ${twoRows}        | ${'2a'}                     | ${[1]}      | ${'with two rows, only first one (partially) matching in pgpKeyFp'}
      ${twoRowsPartial} | ${'1'}                      | ${[1]}      | ${'with two rows, only matching row with id 1 and NOT row with id 10'}
      ${twoRowsPartial} | ${'0'}                      | ${[]}       | ${'with two rows, NO matches on rows with id 1 and 10'}
    `(
      'should contain the ids $expectedIds when searching for $query $description',
      ({ rows, query, expectedIds }) => {
        const filteredRows = callback(rows, columnIds, query);
        expect(filteredRows.map((row) => row.values.id)).toStrictEqual(
          expectedIds
        );
      }
    );
  });
});
