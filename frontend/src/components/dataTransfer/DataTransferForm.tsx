import React, { ReactElement, useCallback, useEffect, useState } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { DeepRequired, Required } from 'utility-types';
import {
  AutocompleteField,
  Button,
  Choice,
  CircularProgress,
  ConfirmLabel,
  FixedChildrenHeight,
  FormDialog,
  HiddenField,
  logger,
  requestAction,
  SelectField,
  StyleMap,
  ToggleButton,
  ToggleButtonGroup,
  useEnhancedForm,
  useEnumChoices,
  useFormDialog,
} from '@biomedit/next-widgets';
import {
  ADD_DATA_TRANSFER,
  CALL,
  CLEAR_USER_KEYS,
  LOAD_DATA_PROVIDERS,
  LOAD_KEYS,
  LOAD_USER_KEYS,
  LOAD_USERS,
  UPDATE_DATA_TRANSFER,
} from '../../actions/actionTypes';
import {
  DataTransfer,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  ListPgpKeysSignStatusEnum,
  ListUsersRoleEnum,
  User,
} from '../../api/generated';
import { FormProvider } from 'react-hook-form';
import { TestId } from '../../testId';
import { isStaff } from '../selectors';
import { useDataProviderChoices, useUserChoices } from '../choice';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';

export enum MaxPackagesToggleState {
  UNLIMITED = -1,
  ONE_PACKAGE = 1,
}

export type UiDataTransfer = Required<
  Partial<DataTransfer>,
  'maxPackages' | 'purpose' | 'project'
>;

export type DataTransferFormProps = {
  transfer: UiDataTransfer;
  close: () => void;
};

const log = logger('DataTransferForm');

export function useDataTransferForm(projectId: number | undefined = undefined) {
  const newDtr =
    projectId !== undefined
      ? {
          maxPackages: MaxPackagesToggleState.ONE_PACKAGE,
          project: +projectId,
          purpose: DataTransferPurposeEnum.PRODUCTION,
        }
      : undefined;
  const { closeFormDialog, data, ...rest } =
    useFormDialog<UiDataTransfer>(newDtr);
  const dtrForm = data && (
    <DataTransferForm transfer={data} close={closeFormDialog} />
  );
  return {
    dtrForm,
    ...rest,
  };
}

export const DataTransferForm = ({
  transfer,
  close,
}: DataTransferFormProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);

  const isEdit = !!transfer.id;

  const dispatch = useDispatch();

  const isFetchingDataProvider = useSelector(
    (state) => state.dataProvider.isFetching
  );
  const isFetchingKeys = useSelector((state) => state.keys.isFetching);
  const isFetchingUserKeys = useSelector((state) => state.userKeys.isFetching);
  const isFetchingUsers = useSelector((state) => state.users.isFetching);

  const isSubmitting = useSelector((state) => state.project.isSubmitting);
  const users = useSelector(
    (state) => state.users.itemList
  ) as DeepRequired<User>[];
  const dataProviders = useSelector((state) => state.dataProvider.itemList);
  const userKeys = useSelector((state) => state.userKeys.itemList);
  const keys = useSelector((state) => state.keys.itemList);
  const requestorFieldVisible = useSelector(isStaff);
  const userChoices = useUserChoices(users);
  const dataProviderChoices = useDataProviderChoices(dataProviders);
  const dataTransferPurposeChoices = useEnumChoices(DataTransferPurposeEnum);
  const dataTransferStatusChoices = useEnumChoices(DataTransferStatusEnum);

  const form = useEnhancedForm<DataTransfer>({ defaultValues: transfer });

  const { reset, getValues, watch } = form;

  function submit(dataTransfer: DataTransfer) {
    const onSuccessAction = { type: CALL, callback: close };
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_DATA_TRANSFER,
          {
            id: String(dataTransfer.id),
            dataTransfer,
          },
          onSuccessAction
        )
      );
    } else {
      dispatch(
        requestAction(
          ADD_DATA_TRANSFER,
          {
            dataTransfer,
          },
          onSuccessAction
        )
      );
    }
  }

  const watchRequestor = watch('requestor');

  useEffect(() => {
    batch(() => {
      if (requestorFieldVisible) {
        dispatch(
          requestAction(LOAD_USERS, {
            projectId: transfer.project,
            role: ListUsersRoleEnum.DM,
          })
        );
      } else {
        dispatch(
          requestAction(LOAD_KEYS, {
            signStatus: ListPgpKeysSignStatusEnum.SIGNED,
          })
        );
      }
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
    });
  }, [transfer.project, dispatch, requestorFieldVisible]);

  useEffect(() => {
    if (requestorFieldVisible) {
      if (watchRequestor) {
        const requestorUser = users.find((user) => user.id === watchRequestor);
        if (requestorUser) {
          const requestorEmail = requestorUser.email;
          if (requestorEmail) {
            dispatch(
              requestAction(LOAD_USER_KEYS, {
                email: requestorEmail,
                signStatus: ListPgpKeysSignStatusEnum.SIGNED,
              })
            );
          } else {
            // should only be the case for local users without email address
            log.warn({
              message: 'User has no email address!',
              json: requestorUser,
            });
            dispatch({ type: CLEAR_USER_KEYS });
          }
        } else {
          // no requestor selected
          dispatch({ type: CLEAR_USER_KEYS });
        }
      } else {
        dispatch({ type: CLEAR_USER_KEYS });
      }
    }
  }, [dispatch, watchRequestor, users, requestorFieldVisible]);

  const [maxPackagesToggleState, setMaxPackagesToggleState] =
    useState<MaxPackagesToggleState>(transfer.maxPackages);

  const handleMaxPackageStateToggled = useCallback(
    (_event, newToggleState) => {
      if (newToggleState !== null) {
        // update max packages on switching between "unlimited" and "one package"
        const formValues = getValues();
        formValues.maxPackages = newToggleState;
        reset(formValues);

        // update toggle
        setMaxPackagesToggleState(newToggleState);
      }
    },
    [setMaxPackagesToggleState, getValues, reset]
  );

  const checkForKeys = useCallback(() => {
    dispatch(requestAction(LOAD_KEYS));
  }, [dispatch]);

  if (!users) {
    return <CircularProgress />;
  }

  const keysChoices: Choice[] = (requestorFieldVisible ? userKeys : keys).map(
    (key) => ({
      key: key.fingerprint as string,
      value: key.fingerprint as string,
      label: `${key.keyName} (${key.fingerprint})`,
    })
  );
  const styles: StyleMap = {
    buttons: {
      marginTop: 30,
      float: 'right',
    },
    saveButton: {
      marginLeft: 5,
    },
    firstRow: {
      marginTop: '1.5rem',
      marginBottom: '1.5rem',
    },
    missingKey: {
      marginBottom: '1rem',
    },
  };

  // in edit mode, requestor's keys are shown instead
  if (!isFetchingKeys && !keys.length && !requestorFieldVisible) {
    return (
      <>
        <p style={styles.missingKey}>
          Before you can create a data transfer, you must upload your public key
          to the keyserver.
        </p>
        <div style={styles.missingKey}>
          <span>If you already have a PGP key pair, you can </span>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://sett.readthedocs.io/en/stable/sett_key_management.html#uploading-public-keys-to-the-default-sett-keyserver"
          >
            upload it to the keyserver using the following instructions.
          </a>
        </div>
        <div style={styles.missingKey}>
          <span>Otherwise, you will need to </span>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://sett.readthedocs.io/en/stable/sett_key_management.html#generating-a-new-public-private-pgp-key-pair"
          >
            create a PGP key pair first using the following instructions.
          </a>
        </div>
        <Button color="primary" variant="outlined" onClick={checkForKeys}>
          Check for keys
        </Button>
      </>
    );
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        title={isEdit ? `Data Transfer #${transfer?.id}` : 'New Data Transfer'}
        open={!!transfer}
        isSubmitting={isSubmitting}
        onSubmit={submit}
        onClose={close}
        confirmLabel={ConfirmLabel.SAVE}
      >
        <HiddenField
          name="project"
          initialValues={{ project: transfer.project }}
        />
        <HiddenField name="id" initialValues={{ id: transfer.id }} />
        <div style={styles.firstRow}>
          <ToggleButtonGroup
            value={maxPackagesToggleState}
            exclusive
            onChange={handleMaxPackageStateToggled}
          >
            {[
              MaxPackagesToggleState.UNLIMITED,
              MaxPackagesToggleState.ONE_PACKAGE,
            ].map((enumItem) => {
              return (
                <ToggleButton key={enumItem} value={enumItem}>
                  {t<string>('maxPackages', { context: String(enumItem) })}
                </ToggleButton>
              );
            })}
          </ToggleButtonGroup>
          <HiddenField
            name="maxPackages"
            initialValues={{
              maxPackages: maxPackagesToggleState,
            }}
          />
        </div>
        <FixedChildrenHeight>
          <SelectField
            name="dataProvider"
            label="Data Provider"
            initialValues={transfer}
            required
            isLoading={isFetchingDataProvider}
            choices={dataProviderChoices}
          />
          <SelectField
            name="purpose"
            label="Purpose"
            initialValues={transfer}
            required
            choices={dataTransferPurposeChoices}
            disabled={!!transfer?.packages?.length}
          />
          {isEdit && (
            <SelectField
              name="status"
              label="Status"
              initialValues={transfer}
              required
              choices={dataTransferStatusChoices}
            />
          )}
          {requestorFieldVisible && (
            <AutocompleteField
              name="requestor"
              label="Requestor"
              initialValues={transfer}
              required
              testId={TestId.REQUESTOR}
              choices={userChoices}
              isLoading={isFetchingUsers}
              width={500}
            />
          )}
          <AutocompleteField
            name="requestorPgpKeyFp"
            label="Key"
            testId={TestId.FINGERPRINT}
            initialValues={transfer}
            required
            choices={keysChoices}
            isLoading={isFetchingUserKeys}
            width={500}
          />
        </FixedChildrenHeight>
      </FormDialog>
    </FormProvider>
  );
};
