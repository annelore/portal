import React from 'react';
import {
  configure,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { DataTransferForm } from './DataTransferForm';
import '@testing-library/jest-dom/extend-expect';
import {
  expectQueryParameter,
  mockConsoleWarn,
  openAutocompleteDropdown,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { Provider } from 'react-redux';
import { backend } from '../../api/api';
import {
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  ListUsersRoleEnum,
} from '../../api/generated';
import { getInitialState, makeStore, RootState } from '../../store';
import { TestId } from '../../testId';
import listDataProviderResponse from '../../../test-data/listDataProviderResponse.json';
import staffState from '../../../test-data/staffState.json';
import { rest } from 'msw';

const requestorLabel = 'Requestor';
const keyLabel = 'Key';

const projectId = 1;
const transferId = 2;

const user1Email = 'one.pgp.key@sib.swiss';
const user2Email = 'two.pgp.keys@sib.swiss';

const user1Key1 = '1100000000000000000000000000000000000000';
const user2Key1 = '2100000000000000000000000000000000000000';
const user2Key2 = '2200000000000000000000000000000000000000';

const user1Key1Name = `One PGP Key 1 <${user1Email}>`;
const user2Key1Name = `Two PGP Keys 1 <${user2Email}>`;
const user2Key2Name = `Two PGP Keys 2 <${user2Email}>`;

const user1Key1Label = `${user1Key1Name} (${user1Key1})`;
const user2Key1Label = `${user2Key1Name} (${user2Key1})`;

const requestor1Name = `One PGP Key (ID: 1) (${user1Email})`;
const requestor2Name = `Two PGP Keys (ID: 2) (${user2Email})`;

const dataTransfer = {
  project: projectId,
  maxPackages: -1,
  status: DataTransferStatusEnum.INITIAL,
  dataProvider: 'kispi',
  requestor: 1,
  packages: [],
  requestorName: requestor1Name,
  purpose: DataTransferPurposeEnum.PRODUCTION,
  projectName: 'Test',
  requestorPgpKeyFp: user1Key1,
  id: transferId,
};

const usersResponse = [
  {
    url: 'http://localhost:8000/backend/users/1/',
    id: 1,
    username: '1@eduid.ch',
    email: user1Email,
    first_name: 'One',
    last_name: 'PGP Key',
    profile: {
      affiliation: 'affiliate@sib.swiss',
      local_username: 'onekey',
      display_name: requestor1Name,
      uid: 1000001,
      gid: 1000001,
      namespace: 'ch',
      display_local_username: 'ch_onekey',
    },
  },
  {
    url: 'http://localhost:8000/backend/users/2/',
    id: 2,
    username: '2@eduid.ch',
    email: user2Email,
    first_name: 'Two',
    last_name: 'PGP Keys',
    profile: {
      affiliation: 'affiliate@sib.swiss',
      local_username: 'twokeys',
      display_name: requestor2Name,
      uid: 1000002,
      gid: 1000002,
      namespace: 'ch',
      display_local_username: 'ch_twokeys',
    },
  },
];

const pgpKeyResponse1 = [
  {
    pgpkey_id: 'E75A30416DE01D17',
    fingerprint: user1Key1,
    key_name: user1Key1Name,
    sign_status: true,
  },
];

const pgpKeyResponse2 = [
  {
    pgpkey_id: 'A24D8AB929A6AF5A',
    fingerprint: user2Key1,
    key_name: user2Key1Name,
    sign_status: true,
  },
  {
    pgpkey_id: '582ADB3900D9E893',
    fingerprint: user2Key2,
    key_name: user2Key2Name,
    sign_status: true,
  },
];

describe('DataTransferForm', () => {
  let store;
  let server;

  beforeAll(() => {
    server = setupMockApi(
      rest.get(`${backend}users/`, (req, res, ctx) => {
        const expectedRole = ListUsersRoleEnum.DM;
        const actualRole = req.url.searchParams.get('role');
        const expectedProjectId = String(projectId);
        const actualProjectId = req.url.searchParams.get('project_id');
        if (
          actualRole === expectedRole &&
          actualProjectId === expectedProjectId
        ) {
          return res(resJson(ctx), resBody(ctx, usersResponse));
        } else {
          console.error(
            `MockAPI: Expected project_id to be '${expectedProjectId}' and role to be '${expectedRole}', but instead got project_id '${actualProjectId}' and role ${actualRole}`
          );
          throw req;
        }
      }),
      rest.get(`${backend}data-provider/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listDataProviderResponse))
      ),
      rest.get(`${backend}pgpkey/`, (req, res, ctx) => {
        expectQueryParameter(req, 'sign_status', 'SIGNED');

        const email = req.url.searchParams.get('email');
        if (email === user1Email) {
          return res(resJson(ctx), resBody(ctx, pgpKeyResponse1));
        } else if (email === user2Email) {
          return res(resJson(ctx), resBody(ctx, pgpKeyResponse2));
        } else {
          console.error('MockAPI: Unexpected email:', email);
          throw req;
        }
      })
    );

    configure({
      asyncUtilTimeout: 2000,
    });
  });

  afterAll(() => {
    server.close();
  });

  const initialState: RootState = {
    ...getInitialState(),
    ...staffState,
  };

  beforeEach(async () => {
    store = makeStore(undefined, initialState);
  });

  describe('Edit', () => {
    it('should clear the key field when requestor is changed', async () => {
      const spy = mockConsoleWarn();

      render(
        <Provider store={store}>
          <DataTransferForm
            transfer={dataTransfer}
            close={() => {
              return;
            }}
          />
        </Provider>
      );

      // default (from API call): Requestor 1 and Key 1
      await screen.findByDisplayValue(user1Key1Label);
      await screen.findByDisplayValue(requestor1Name);

      // change requestor to 2
      openAutocompleteDropdown(requestorLabel);
      fireEvent.click(await screen.findByText(requestor2Name));
      await screen.findByDisplayValue(requestor2Name);

      // expect requestor to be 2, and key to be cleared
      await waitFor(() => {
        expect(screen.getByTestId(TestId.REQUESTOR)).toHaveValue(
          requestor2Name
        );
        expect(screen.getByTestId(TestId.FINGERPRINT)).toHaveValue('');
      });

      // set key to 1 (of requestor 2)
      openAutocompleteDropdown(keyLabel);
      fireEvent.click(await screen.findByText(user2Key1Label));

      // expect key to be 1 (of requestor 2)
      await screen.findByDisplayValue(user2Key1Label);

      // change requestor to 1
      openAutocompleteDropdown(requestorLabel);
      fireEvent.click(await screen.findByText(requestor1Name));

      // expect requestor to be 1, and key to be cleared
      await waitFor(() => {
        expect(screen.getByTestId(TestId.REQUESTOR)).toHaveValue(
          requestor1Name
        );
        expect(screen.getByTestId(TestId.FINGERPRINT)).toHaveValue('');
      });

      expect(spy).toHaveBeenCalledTimes(2);
      const calls = spy.mock.calls;
      const warn0 = calls[0][0];
      const warn1 = calls[1][0];
      expect(warn0).toContain(
        `{"key":"${user1Key1}","value":"${user1Key1}","label":"${user1Key1Label}"}`
      );
      expect(warn1).toContain(
        `{"key":"${user2Key1}","value":"${user2Key1}","label":"${user2Key1Label}"}`
      );

      spy.mockRestore();
    }, 10000);
  });
});
