import {
  DataProviderField,
  DataTransferTable,
  isDataManager,
} from './DataTransferTable';
import {
  Project,
  ProjectDataTransfers,
  ProjectDataTransfersPurposeEnum,
  ProjectDataTransfersStatusEnum,
  ProjectUsersRolesEnum,
} from '../../api/generated';
import { backend } from '../../api/api';
import {
  RequestVerifier,
  resBody,
  resJson,
  setInputValue,
  setupMockApi,
} from '@biomedit/next-widgets';
import { Provider } from 'react-redux';
import { makeStore } from '../../store';
import React from 'react';
import listDataProviderResponse from '../../../test-data/listDataProviderResponse.json';
import { rest } from 'msw';
import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';

describe('DataTransferTable', () => {
  let server;
  const listDataProviders = `${backend}data-provider/`;

  beforeAll(() => {
    server = setupMockApi(
      rest.get(listDataProviders, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listDataProviderResponse))
      )
    );
  });

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  describe('isDataManager', () => {
    const project: Project = {
      name: 'The Blair Witch Project',
      code: 'blair_witch',
      users: [
        {
          id: 1,
          roles: [ProjectUsersRolesEnum.DM],
        },
        { id: 2, roles: [] },
        {
          id: 3,
          roles: [ProjectUsersRolesEnum.PL, ProjectUsersRolesEnum.PM],
        },
      ],
    };

    it.each`
      userId | isValid
      ${1}   | ${true}
      ${2}   | ${false}
      ${3}   | ${false}
      ${4}   | ${false}
    `(
      'user $userId should be DM for project: $isValid',
      ({ userId, isValid }) => {
        expect(isDataManager(userId, project)).toEqual(isValid);
      }
    );
  });

  describe('fetchDataProviders', () => {
    it('should only perform one API call even if DataProviderField is rendered multiple times', async () => {
      const verifier = new RequestVerifier(server);
      verifier.assertCount(0);
      // The additional '<DataProviderField />' is intentional!
      render(
        <Provider store={makeStore()}>
          <DataProviderField />
          <DataProviderField />
        </Provider>
      );
      verifier.assertCount(1);
      verifier.assertCalled(listDataProviders, 1);
    });
  });

  describe('Component', () => {
    describe('Global Filter', () => {
      const getDtr = (
        id: number,
        maxPackages: number
      ): ProjectDataTransfers => ({
        project: 1,
        maxPackages: maxPackages,
        status: ProjectDataTransfersStatusEnum.INITIAL,
        dataProvider: 'uzh',
        requestor: 2,
        purpose: ProjectDataTransfersPurposeEnum.TEST,
        requestorPgpKeyFp: '9E10FE436DA14366B6254861E75A30416DE01D17',
        id: id,
        packages: [],
        requestorName:
          'Chuck Norris (ID: 63457666153) (chuck.norris@roundhouse.kick)',
        requestorDisplayId: 'ID: 63457666153',
        requestorFirstName: 'Chuck',
        requestorLastName: 'Norris',
        requestorEmail: 'chuck.norris@roundhouse.kick',
        projectName: 'Test Project 1',
      });

      const project: Project = {
        code: 'test_project_1',
        name: 'Test Project 1',
        id: 1,
        dataTransfers: [getDtr(1, -1), getDtr(5, 1), getDtr(15, -1)],
        users: [],
      };

      const assertDtrVisible = async (
        accessibleName: string,
        isVisible: boolean
      ) => {
        const row = await screen.queryByRole('row', {
          name: accessibleName,
        });
        if (isVisible) {
          expect(row).toBeInTheDocument();
        } else {
          expect(row).not.toBeInTheDocument();
        }
      };

      beforeEach(() => {
        render(
          <Provider store={makeStore()}>
            <DataTransferTable {...project} />
          </Provider>
        );
      });

      const displayId = project?.dataTransfers?.[0]?.requestorDisplayId;
      const pgpKeyFp = project?.dataTransfers?.[0]?.requestorPgpKeyFp;

      it.each`
        searchQuery  | dtr1Visible | dtr5Visible | dtr15Visible | description
        ${undefined} | ${true}     | ${true}     | ${true}      | ${'show all DTRs by default'}
        ${'1'}       | ${true}     | ${true}     | ${false}     | ${'match DTR 1 (by id) and DTR 5 (maxPackages)'}
        ${'5'}       | ${false}    | ${true}     | ${false}     | ${'only match DTR with id 5'}
        ${'15'}      | ${false}    | ${false}    | ${true}      | ${'only match DTR with id 15'}
        ${displayId} | ${true}     | ${true}     | ${true}      | ${'show all DTRs (as all are by the same requestor)'}
        ${pgpKeyFp}  | ${true}     | ${true}     | ${true}      | ${'show all DTRs (as all are by the same requestor)'}
      `(
        'should $description when searching for $searchQuery',
        async ({ searchQuery, dtr1Visible, dtr5Visible, dtr15Visible }) => {
          const search = screen.getByRole('textbox', { name: 'search' });

          if (searchQuery) {
            setInputValue(search, searchQuery);
          }

          await assertDtrVisible(
            '1 uzh maxPackages Chuck Norris TEST INITIAL',
            dtr1Visible
          );
          await assertDtrVisible(
            '5 uzh 1 Chuck Norris TEST INITIAL',
            dtr5Visible
          );
          await assertDtrVisible(
            '15 uzh maxPackages Chuck Norris TEST INITIAL',
            dtr15Visible
          );
        }
      );
    });
  });
});
