import React, { ReactElement } from 'react';
import { styled } from '@mui/material/styles';
import { ProjectPackages, ProjectTraceStatusEnum } from '../../api/generated';
import {
  formatDate,
  logger,
  red,
  Step,
  Stepper,
  Tooltip,
  Typography,
} from '@biomedit/next-widgets';

const ErrorContainer = styled('div')(() => ({
  color: red[500],
}));

const log = logger('DataPackageTrace');

export type DataPackageTraceSteps = {
  activeStepIndex: number;
  steps: DataPackageTraceStep[];
};

export type DataPackageTraceStep = {
  location: string; // location of the data - can be name of data provider, name of node or project workspace
  text?: string; // extra text displayed in the step's tooltip
  enter?: Date | true; // date if available, or `true` if date of event unknown
  error?: Array<Date | true>; // if NOT undefined, enter will always be defined as a Date and processed will NOT be defined!
  processed?: Date | true;
};

export const makeTraceSteps = (
  pkg: ProjectPackages | undefined
): DataPackageTraceSteps => {
  const emptySteps: DataPackageTraceSteps = { steps: [], activeStepIndex: -1 };

  if (!pkg) {
    return emptySteps;
  }

  const {
    id,
    dataTransfer,
    trace,
    destinationNode,
    dataProvider,
    senderPgpKeyInfo,
  } = pkg;

  if (!trace || !trace.length || !destinationNode || !dataProvider) {
    return emptySteps;
  }

  const logPrefix = `DataPackage with id [${id}] from DataTransfer [${dataTransfer}]`;

  const steps: DataPackageTraceStep[] = [
    {
      location: dataProvider,
      text: senderPgpKeyInfo,
      processed: true,
    },
  ];
  const output = { steps, activeStepIndex: -1 };

  let currentStep: DataPackageTraceStep | undefined;

  for (let i = 0; i < trace.length; i++) {
    const currentTrace = trace[i];

    if (!currentTrace.node) {
      log.error({
        message: `${logPrefix} doesn't contain a node!`,
      });
      continue;
    }

    if (!currentStep) {
      currentStep = { location: currentTrace.node };
    }

    const timestamp = currentTrace.timestamp ?? true;

    switch (currentTrace.status) {
      case ProjectTraceStatusEnum.ENTER:
        currentStep.enter = timestamp;
        break;
      case ProjectTraceStatusEnum.ERROR:
        if (!currentStep.error) {
          currentStep.error = [];
        }
        currentStep.error.push(timestamp);
        break;
      case ProjectTraceStatusEnum.PROCESSED:
        currentStep.processed = timestamp;
        steps.push(currentStep);
        currentStep = undefined;
        break;
      default:
        log.error({
          message: `${logPrefix} has unexpected status [${currentTrace.status}]`,
        });
    }
  }

  if (currentStep) {
    // in case PROCESSED wasn't the last trace, `currentStep` still needs to be added
    steps.push(currentStep);
  }

  const lastTrace = trace[trace.length - 1];

  const isTransferred = lastTrace.node === destinationNode;

  output.activeStepIndex = steps.length - 1;

  if (!isTransferred && lastTrace.node) {
    // package has not reached destinationNode yet, add empty step with destinationNode
    steps.push({ location: destinationNode });
  }

  const projectSpaceStep: DataPackageTraceStep = {
    location: 'Project Space',
  };

  if (isTransferred && lastTrace.status === ProjectTraceStatusEnum.PROCESSED) {
    // package was successfully transferred to project space
    projectSpaceStep.enter = true;
  }

  steps.push(projectSpaceStep);

  if (projectSpaceStep.enter) {
    output.activeStepIndex = steps.length - 1;
  }

  return output;
};

export type DataPackageTraceStepLabelProps = {
  step: DataPackageTraceStep;
};

export const DataPackageTraceStepLabel = ({
  step,
}: DataPackageTraceStepLabelProps): ReactElement => {
  function getDate(dateOrTrue: Date | true, key?: string) {
    return (
      dateOrTrue !== true && (
        <Typography display="inline" key={key}>
          {' '}
          ({formatDate(dateOrTrue, true)})
        </Typography>
      )
    );
  }

  const enter = step.enter && (
    <div>
      <Typography display="inline">Arrived</Typography>
      {getDate(step.enter)}
    </div>
  );

  const errors = step.error?.map((date) => {
    const keySuffix = `-${step.location}-error-${date.valueOf()}`;
    return (
      <ErrorContainer key={`error-container-${keySuffix}`}>
        <Typography key={`title${keySuffix}`} display="inline">
          Error
        </Typography>
        {getDate(date, `date${keySuffix}`)}
      </ErrorContainer>
    );
  });

  const processed = step.processed && (
    <div>
      <Typography display="inline">Sent</Typography>
      {getDate(step.processed)}
    </div>
  );

  const content = (
    <div>
      <Typography variant={'subtitle1'}>{step.location}</Typography>
      {enter}
      {errors}
      {processed}
    </div>
  );

  if (step.text && step.text.length) {
    return (
      <Tooltip
        title={
          <Typography variant="caption" display="block">
            {step.text}
          </Typography>
        }
      >
        {content}
      </Tooltip>
    );
  } else {
    return content;
  }
};

export const DataPackageTrace = (
  pkg: ProjectPackages | undefined
): ReactElement => {
  const { steps, activeStepIndex } = makeTraceSteps(pkg);

  return (
    <Stepper activeStep={activeStepIndex}>
      {steps.map((step) => (
        <Step key={'step-' + step.location}>
          <DataPackageTraceStepLabel
            key={'label-' + step.location}
            step={step}
          />
        </Step>
      ))}
    </Stepper>
  );
};
