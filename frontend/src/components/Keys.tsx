import React, { ReactElement, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_KEYS, SEND_KEY_SIGN_REQUEST } from '../actions/actionTypes';
import {
  LoadingButton,
  requestAction,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@biomedit/next-widgets';
import { PgpKey, PgpKeySignStatusEnum } from '../api/generated';

export const Keys = (): ReactElement => {
  const dispatch = useDispatch();

  const keys: PgpKey[] = useSelector((state) => state.keys.itemList);
  const isSubmitting = useSelector((state) => state.keys.isSubmitting);

  useEffect(() => {
    dispatch(requestAction(LOAD_KEYS));
  }, [dispatch]);

  const sendKeySignRequest = useCallback(
    (id) => {
      dispatch(
        requestAction(SEND_KEY_SIGN_REQUEST, {
          pgpKeySignRequest: { pgpkeyId: id },
        })
      );
    },
    [dispatch]
  );

  if (!keys || keys.length === 0) {
    return <div>You dont have any keys on the keyserver</div>;
  }

  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Key</TableCell>
          <TableCell>Fingerprint</TableCell>
          <TableCell>Sign Status</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {keys.map(({ keyName, fingerprint, signStatus, pgpkeyId, link }) => (
          <TableRow key={fingerprint}>
            <TableCell>{keyName}</TableCell>
            <TableCell>
              <a href={link} target="_blank" rel="noopener noreferrer">
                {fingerprint}
              </a>
            </TableCell>
            <TableCell>
              {signStatus === PgpKeySignStatusEnum.UNSIGNED ? (
                <LoadingButton
                  onClick={() => sendKeySignRequest(pgpkeyId)}
                  loading={isSubmitting}
                >
                  Request Signature
                </LoadingButton>
              ) : (
                signStatus
              )}
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};
