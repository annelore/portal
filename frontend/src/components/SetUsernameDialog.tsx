import React, { ReactElement, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  FormDialog,
  LabelledField,
  requestAction,
  required,
  useEnhancedForm,
  username,
} from '@biomedit/next-widgets';
import { CHANGE_USERNAME } from '../actions/actionTypes';
import { FormProvider } from 'react-hook-form';
import { Alert } from '@mui/material';

type UsernameFormData = {
  username: string;
};

export const SetUsernameDialog = (): ReactElement => {
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.auth.user);
  const form = useEnhancedForm<UsernameFormData>();
  const isSubmitting = useSelector((state) => state.auth.isSubmitting);
  const isExistingUsername = useSelector(
    (state) => state.auth.isExistingUsername
  );
  const [existingUsername, setExistingUsername] = useState<
    string | undefined
  >();

  const name = 'username';

  const watchUsername = form.watch(name);

  const submit = ({ username }: UsernameFormData) => {
    if (username) {
      dispatch(
        requestAction(CHANGE_USERNAME, {
          user: {
            profile: {
              localUsername: username,
            },
          },
          id: String(userInfo?.id),
        })
      );
    }
  };

  const fieldError = useMemo(() => {
    const isStillExistingUsername =
      existingUsername && watchUsername !== existingUsername;
    if (isExistingUsername && !isStillExistingUsername) {
      setExistingUsername(watchUsername);
      return { type: 'validate', message: 'This username already exists' };
    }
  }, [isExistingUsername, watchUsername, existingUsername]);

  return (
    <FormProvider {...form}>
      <FormDialog<UsernameFormData>
        title="Set username"
        text={
          <Alert severity="warning">Username can NOT be changed later!</Alert>
        }
        open={!userInfo?.profile?.localUsername}
        onSubmit={submit}
        isSubmitting={isSubmitting}
      >
        <LabelledField
          type="text"
          name={name}
          label="Username"
          fieldError={fieldError}
          validations={[required, username]}
          autofocus
          fullWidth
        />
      </FormDialog>
    </FormProvider>
  );
};
