import React, { ReactElement, useEffect, useMemo } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { Overwrite, Required } from 'utility-types';
import {
  CircularProgress,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  idValidations,
  LabelledField,
  nameValidations,
  requestAction,
  SelectField,
  useEnhancedForm,
  useFormDialog,
} from '@biomedit/next-widgets';
import { userRoles } from '../../permissions';
import {
  ADD_PROJECT,
  CALL,
  LOAD_NODES,
  LOAD_USERS,
  updateProjectAction,
} from '../../actions/actionTypes';
import { FormProvider } from 'react-hook-form';
import {
  Project,
  ProjectUsers,
  ProjectUsersRolesEnum,
} from '../../api/generated';
import { arrayName, IpRangeList } from './IpRangeList';
import { UsersList } from './UsersList';
import { generatedBackendApi } from '../../api/api';
import { useNodeChoices } from '../choice';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { styled } from '@mui/material/styles';
import { FormFieldsContainer } from '../FormFieldsContainer';

export const InvisibleUl = styled('ul')(() => ({
  listStyleType: 'none',
}));

// TODO: refactor in #101
type ConvertedProjectUserFields = {
  roles: Partial<Record<ProjectUsersRolesEnum, boolean>>;
};
export type ConvertedProjectUser = Overwrite<
  ProjectUsers,
  ConvertedProjectUserFields
>;

type ConvertedProject = Overwrite<
  UiProject,
  {
    users: ConvertedProjectUser[];
  }
>;

export function preProcessProject(project: ConvertedProject): UiProject {
  // This is to convert `{role1: true, role2: false}` into `[role1]`
  const { users = [], ...rest } = project;
  return {
    ...rest,
    users: users.map(({ roles, ...userContent }) => ({
      roles: Object.entries(roles)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .filter(([_r, isSet]) => isSet)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .map(([r, _isSet]) => ProjectUsersRolesEnum[r]),
      ...userContent,
    })),
  };
}

// TODO: refactor in #101
export function convertPermissions(project: UiProject): ConvertedProject {
  // This is to convert `[perm1, perm2]` into `{perm1: true, perm2: true}`
  const { users, ...rest } = project;
  return {
    ...rest,
    users: users?.map(({ roles, ...userContent }) => ({
      roles: Object.fromEntries(roles.map((r) => [r, true])),
      ...userContent,
    })),
  };
}

type ProjectFormProps = {
  project: UiProject;
  close: () => void;
};

type UiProject = Required<Partial<Project>, 'users'>;

export function useProjectForm() {
  const newProject = {
    permissions: { edit: { users: Object.keys(userRoles) } },
    users: [],
  };
  const { closeFormDialog, data, ...rest } = useFormDialog<UiProject>(
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore FIXME in #170
    preProcessProject(newProject)
  );
  const projectForm = data && (
    <ProjectForm project={data} close={closeFormDialog} />
  );
  return {
    projectForm,
    ...rest,
  };
}

export const nameLabel = 'captions.name';
export const projectCodeLabel = 'captions.code';
export const destinationLabel = 'captions.destination';

export function ProjectForm({
  project,
  close,
}: ProjectFormProps): ReactElement {
  const { t } = useTranslation(I18nNamespace.PROJECT_FORM);

  const dispatch = useDispatch();

  const initialValues = convertPermissions(project);
  const isSubmitting = useSelector((state) => state.project.isSubmitting);
  const isFetchingProject = useSelector((state) => state.project.isFetching);
  const isFetchingNodes = useSelector((state) => state.nodes.isFetching);
  const users = useSelector((state) => state.users.itemList);
  const nodes = useSelector((state) => state.nodes.itemList);
  const nodeAdminNodes = useSelector(
    (state) => state.auth.user?.manages?.nodeAdmin
  );

  const isEdit = !!project.id;
  const isCreate = !isEdit;

  const isDisabled = (fieldName: string): boolean =>
    !(isCreate || (isEdit && !!project.permissions?.edit?.[fieldName]));
  const isCodeDisabled = useMemo(() => isDisabled('code'), [isDisabled]);
  const isNameDisabled = useMemo(() => isDisabled('name'), [isDisabled]);
  const isDestDisabled = useMemo(() => isDisabled('destination'), [isDisabled]);
  const isIpEnabled = useMemo(() => !isDisabled(arrayName), [isDisabled]);

  const nodeChoices = useNodeChoices(
    nodeAdminNodes?.length && !isDestDisabled ? nodeAdminNodes : nodes
  );

  const form = useEnhancedForm<Project | ConvertedProject>({
    defaultValues: initialValues,
  });

  const { setError } = form;

  const handleSubmit = (project) => {
    const hasProjectLeader = project.users?.some(
      (u) => u.roles[ProjectUsersRolesEnum.PL]
    );
    // Dispatch if we have at least one PL
    if (hasProjectLeader) {
      const processedProject = preProcessProject(project);
      const onSuccessAction = { type: CALL, callback: close };
      if (project.id) {
        dispatch(
          updateProjectAction(processedProject as Project, onSuccessAction)
        );
      } else {
        dispatch(
          requestAction(
            ADD_PROJECT,
            { project: processedProject as Project },
            onSuccessAction
          )
        );
      }
    } else {
      setError('users', {
        message: t('noProjectLeaderError'),
      });
    }
  };

  useEffect(() => {
    batch(() => {
      dispatch(requestAction(LOAD_NODES));
      dispatch(requestAction(LOAD_USERS));
    });
  }, [dispatch]);

  if (isFetchingProject || !initialValues || !users) {
    return <CircularProgress />;
  }

  const nameValue = form.getValues().name;
  const projectCodeValue = form.getValues().code;

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueProject({ uniqueProject: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={project?.name ?? t('newProject')}
        open={!!project}
        onSubmit={handleSubmit}
        onClose={close}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={initialValues} />
        <FormFieldsContainer>
          <LabelledField
            name="code"
            type="text"
            unique={uniqueCheck}
            label={t(projectCodeLabel)}
            initialValues={initialValues}
            validations={idValidations(t(nameLabel), nameValue)}
            disabled={isCodeDisabled}
            fullWidth
          />
          <LabelledField
            name="name"
            type="text"
            unique={uniqueCheck}
            label={t(nameLabel)}
            validations={nameValidations(t(projectCodeLabel), projectCodeValue)}
            initialValues={initialValues}
            disabled={isNameDisabled}
            fullWidth
          />
          <SelectField
            name="destination"
            label={t(destinationLabel)}
            initialValues={initialValues}
            isLoading={isFetchingNodes}
            required
            choices={nodeChoices}
            disabled={isDestDisabled}
          />
        </FormFieldsContainer>
        <h4>{t('subtitles.users')}</h4>
        <UsersList permissions={project?.permissions?.edit?.users} />
        {isIpEnabled && (
          <>
            <h4>{t('subtitles.ipRanges')}</h4>
            <IpRangeList />
          </>
        )}
      </FormDialog>
    </FormProvider>
  );
}
