import { useFieldArray, useFormContext } from 'react-hook-form';
import { styled } from '@mui/material/styles';
import {
  Box,
  FabButton,
  ipAddress,
  isNotEmpty,
  LabelledArrayField,
  maxValue,
  minValue,
  required,
} from '@biomedit/next-widgets';
import React, { ReactElement, useCallback } from 'react';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';

export const ListItem = styled('div')(({ theme }) => ({
  marginLeft: theme.spacing(5),
  paddingBottom: theme.spacing(1),
  '&:last-child': {
    paddingBottom: 0,
  },
}));

export const arrayName = 'ipAddressRanges';

export const IpRangeList = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.IP_RANGE_LIST);

  const { control, getValues } = useFormContext();

  const { fields, append, remove } = useFieldArray({
    control,
    name: arrayName,
  });

  const onClick = useCallback(() => {
    // 'getValues' could return 'undefined'
    const values = getValues()[arrayName] || [];
    const len = values.length;
    // Append only if empty or if last entry is not empty
    if (len === 0 || isNotEmpty(values[len - 1])) {
      append({});
    }
  }, [append, getValues]);

  return (
    <div>
      <FabButton size="small" icon="add" onClick={onClick} />
      {fields.map((field, index) => (
        <ListItem key={field.id}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              flexWrap: 'nowrap',
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}
          >
            <FabButton
              size="small"
              icon="del"
              onClick={() => remove(index)}
              sx={{
                marginRight: 1,
                // 'del' small sized button is 40x40. We specify a 'minWidth' so that it does
                // not get resized too small. Otherwise it looks ugly.
                minWidth: 40,
              }}
            />
            <LabelledArrayField
              field={field}
              index={index}
              type="text"
              label={t('captions.ipAddress')}
              validations={[required, ipAddress]}
              arrayName={arrayName}
              fieldName="ipAddress"
              sx={{
                width: '30%',
                marginRight: 2,
              }}
            />
            <LabelledArrayField
              field={field}
              index={index}
              type="number"
              label={t('captions.mask')}
              validations={[required, minValue(0), maxValue(32)]}
              arrayName={arrayName}
              fieldName="mask"
              sx={{
                width: '30%',
                marginRight: 2,
              }}
            />
          </Box>
        </ListItem>
      ))}
    </div>
  );
};
