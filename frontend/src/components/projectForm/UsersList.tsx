import { Controller, useFieldArray, useFormContext } from 'react-hook-form';
import { useSelector } from 'react-redux';
import React, { ReactElement, useState } from 'react';
import { userRoles } from '../../permissions';
import {
  AutoSelectBase,
  CheckboxArrayField,
  HiddenArrayField,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TooltipText,
  UserChip,
} from '@biomedit/next-widgets';
import {
  ProjectPermissionsEditUsersEnum,
  ProjectUsers,
  ProjectUsersRolesEnum,
} from '../../api/generated';
import { I18nNamespace } from '../../i18n';
import { useTranslation } from 'next-i18next';
import { ConvertedProjectUser } from './ProjectForm';

type UserListProps = {
  permissions?: Array<ProjectPermissionsEditUsersEnum>;
};

export const UsersList = ({ permissions }: UserListProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.PROJECT_USER_LIST]);
  const arrayName = 'users';

  const {
    control,
    clearErrors,
    getValues,
    formState: { errors },
  } = useFormContext();

  const { fields, append, remove } = useFieldArray({
    control,
    name: arrayName,
  });

  const users = useSelector((state) => state.users.itemList);
  const [inputValue, setInputValue] = useState<string | undefined>();

  const choices = users.map((user) => ({
    label: user?.profile?.displayName,
    displayName: user?.profile?.displayName,
    affiliation: user?.profile?.affiliation,
    id: user.id,
    ...user,
  }));

  const choicesFields = ['firstName', 'lastName', 'username', 'affiliation'];

  const addUser = (projectUser: ConvertedProjectUser) => {
    if (!fields.some((field) => String(field.id) === String(projectUser.id))) {
      // initialize checkboxes to be empty
      projectUser.roles = {
        DM: false,
        PL: false,
        PM: false,
        USER: false,
      };
      append(projectUser);
    }
    setInputValue('');
  };

  function hasRole(
    role: ProjectUsersRolesEnum | ProjectPermissionsEditUsersEnum
  ): boolean {
    return !!permissions?.includes(role as ProjectPermissionsEditUsersEnum);
  }

  const handleChange = () => {
    const projectLeader = getValues()[arrayName].find(
      (user) => user.roles?.[ProjectUsersRolesEnum.PL]
    );
    if (projectLeader) {
      clearErrors(arrayName);
    }
  };

  function isRoleSelected(
    field: ProjectUsers,
    role: ProjectUsersRolesEnum
  ): boolean {
    return field?.roles?.[role];
  }

  const tooltips: Record<userRoles, string> = {
    [userRoles.PL]:
      'Responsible and accountable for the project. Can add/remove the Permissions Managers in this project.',
    [userRoles.PM]: 'Can add/remove Data Managers and Users in this project.',
    [userRoles.DM]:
      'Can request Data transfers for this project. The DM needs to generate the cryptographic key pair used for encryption of the data package at the data provider side and is responsible for the decryption of data packages within the BioMedIT Project Space.',
    [userRoles.USER]: 'Can access the project space on the Node.',
  };

  return (
    <div>
      {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore // TODO
        <AutoSelectBase
          onSelect={addUser}
          choices={choices}
          fields={choicesFields}
          placeholder="Add users here"
          inputValue={inputValue || ''}
          error={errors?.[arrayName]}
          onChange={(event) => {
            setInputValue(event.target.value);
          }}
        />
      }
      {fields.length > 0 && (
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>User</TableCell>
              {Object.values<string>(userRoles).map((permission) => (
                <TableCell key={permission}>
                  <TooltipText text={permission} title={tooltips[permission]} />
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {fields.map((field, index) => {
              const displayName = field?.['displayName'];
              const nameUser = `${arrayName}[${index}]`;
              const roles = Object.keys(
                userRoles
              ) as Array<ProjectUsersRolesEnum>;

              /*
               * Make sure only users can be deleted which only have selected roles the current user is allowed to change.
               *
               * For example: If the current user is permissions manager, they cannot change whether or not users are project leaders.
               *              This means they should also not be able to delete any users from the list which are project leaders,
               *              else they would also remove the project leader's roles by doing so.
               */
              const canDelete = roles.every((r) => {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore TODO
                if (isRoleSelected(field as ProjectUsers, r)) {
                  return hasRole(r);
                }
                return true;
              });

              const onDelete = canDelete
                ? (): void => remove(index)
                : undefined;

              return (
                <TableRow key={field.id}>
                  <TableCell>
                    <Controller
                      control={control}
                      name={`${nameUser}.displayName`}
                      defaultValue={displayName}
                      render={() => (
                        <UserChip user={displayName} onDelete={onDelete} />
                      )}
                    />
                  </TableCell>
                  {roles.map((r) => {
                    const readOnlyPermission = !hasRole(r);
                    return (
                      <TableCell key={r}>
                        <HiddenArrayField
                          arrayName={arrayName}
                          fieldName="id"
                          index={index}
                          field={field}
                        />
                        <CheckboxArrayField
                          arrayName={arrayName}
                          fieldName={`roles.${r}`}
                          index={index}
                          field={field}
                          disabled={readOnlyPermission}
                          disabledTitle={t('disabledTitle')}
                          handleChange={handleChange}
                        />
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      )}
    </div>
  );
};
