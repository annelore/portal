import submitProjectForm from '../../../test-data/submitProjectForm.json';
import submitProjectFormProcessed from '../../../test-data/submitProjectFormProcessed.json';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import {
  destinationLabel,
  nameLabel,
  preProcessProject,
  ProjectForm,
  projectCodeLabel,
} from './ProjectForm';
import { Provider } from 'react-redux';
import React from 'react';
import { getInitialState, makeStore } from '../../store';
import {
  ConfirmLabel,
  mockConsoleWarn,
  resBody,
  resJson,
  setInputValue,
  setupMockApi,
} from '@biomedit/next-widgets';
import { rest } from 'msw';
import { backend } from '../../api/api';
import listUsersResponse from '../../../test-data/listUsersResponse.json';
import '@testing-library/jest-dom/extend-expect';
import {
  Project,
  ProjectUsers,
  ProjectUsersRolesEnum,
} from '../../api/generated';
import listNodesResponse from '../../../test-data/listNodesResponse.json';
import staffState from '../../../test-data/staffState.json';
import authStateNodeAdmin from '../../../test-data/authStateNodeAdmin.json';
import { AuthState } from '../../reducers/auth';

describe('ProjectForm', () => {
  describe('preProcessProject', () => {
    it('should transform object with permissions into array of permission keys', () => {
      expect(preProcessProject(submitProjectForm)).toEqual(
        submitProjectFormProcessed
      );
    });
  });

  describe('Component', () => {
    const enterText = (
      inputs: HTMLInputElement[],
      name: string,
      value: string
    ) => {
      const nameField = inputs.find((el) => el.name === name);
      if (!nameField) {
        throw new Error(`No input field could be found for '${name}'`);
      }
      setInputValue(nameField, value);
    };

    let server;

    beforeAll(() => {
      server = setupMockApi(
        rest.get(`${backend}nodes/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, listNodesResponse))
        ),
        rest.get(`${backend}users/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, listUsersResponse))
        )
      );
    });

    afterEach(() => {
      // Reset any runtime handlers tests may use.
      server.resetHandlers();
    });

    afterAll(() => {
      server.close();
    });

    it('should display an error when ID and name are identical (case-sensitive)', async () => {
      const project: Omit<Project, 'code' | 'name'> = {
        destination: 'sis',
        users: [
          {
            id: 1,
            roles: [ProjectUsersRolesEnum.PL],
          },
        ],
      };
      render(
        <Provider store={makeStore()}>
          <ProjectForm
            project={project}
            close={() => {
              return;
            }}
          />
        </Provider>
      );
      const inputs = screen.getAllByRole('textbox') as HTMLInputElement[];
      const sameValue = 'du';
      enterText(inputs, 'name', sameValue);
      enterText(inputs, 'code', sameValue);
      // Ensure that the node is already selected
      await screen.findByText(node1Name);
      fireEvent.click(screen.getByText(ConfirmLabel.SAVE));
      await screen.findByText(`Must not be the same as ${nameLabel}`);
      await screen.findByText(`Must not be the same as ${projectCodeLabel}`);
    });

    const node1Name = 'SIS';
    const node2Name = 'sciCORE';

    const renderForm = (
      authState: Record<'auth', AuthState>,
      nodeName: string,
      destination = true,
      users: Array<ProjectUsers> = [],
      isEdit = false
    ) =>
      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            ...authState,
          })}
        >
          <ProjectForm
            project={{
              id: isEdit ? 1 : undefined,
              users: users,
              destination: listNodesResponse.find(
                (node) => node.name === nodeName
              )?.code,
              permissions: {
                edit: {
                  destination: destination,
                },
              },
            }}
            close={() => {
              return;
            }}
          />
        </Provider>
      );

    it.each`
      authState             | selectableNodes           | nonSelectableNodes | description
      ${staffState}         | ${[node1Name, node2Name]} | ${[]}              | ${'allow destination to be any of the nodes for staff'}
      ${authStateNodeAdmin} | ${[node1Name]}            | ${[node2Name]}     | ${'only allow destination to be one of the nodes the user is node admin of'}
    `(
      'should $description',
      async ({ authState, selectableNodes, nonSelectableNodes }) => {
        const consoleSpy = mockConsoleWarn();

        for (const selectableNode of selectableNodes) {
          const { findByText } = renderForm(authState, selectableNode);
          await findByText(selectableNode);
          expect(consoleSpy).not.toHaveBeenCalled();
        }

        for (const nonSelectableNode of nonSelectableNodes) {
          const { queryByText, findAllByText } = renderForm(
            authState,
            nonSelectableNode
          );
          await findAllByText(destinationLabel);
          expect(queryByText(nonSelectableNode)).not.toBeInTheDocument();
        }

        if (nonSelectableNodes.length) {
          await waitFor(() => {
            const warnings = consoleSpy.mock.calls;
            expect(warnings).toHaveLength(nonSelectableNodes.length * 3);
            const expectedWarning =
              'MUI: You have provided an out-of-range value';
            for (const warning of warnings) {
              expect(warning[0]).toContain(expectedWarning);
            }
          });
        } else {
          expect(consoleSpy).not.toHaveBeenCalled();
        }
      }
    );

    it('should show destination node when editing a project the node admin is NOT associated with but is project leader of', async () => {
      const { findByText } = renderForm(
        // @ts-expect-error json does not support enums
        authStateNodeAdmin,
        node2Name,
        false,
        [
          {
            username: '63457666153@eduid.ch',
            email: 'chuck.norris@roundhouse.kick',
            firstName: 'Chuck',
            lastName: 'Norris',
            displayName:
              'Chuck Norris (ID: 63457666153) (chuck.norris@roundhouse.kick)',
            affiliation: '',
            affiliationId: '',
            id: 2,
            roles: [ProjectUsersRolesEnum.PL],
          },
        ],
        true
      );
      await findByText(node2Name);
    });
  });
});
