import { DefaultRootState } from 'react-redux';
import { createSelector } from 'reselect';

export const isStaff = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.staff;

export const isNodeAdmin = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.nodeAdmin;

export const isNodeViewer = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.nodeViewer;

export const isDataProviderAdmin = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.dataProviderAdmin;

export const isDataProviderViewer = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.dataProviderViewer;

export const isGroupManager = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.groupManager;

export const canSeeNodeTab = createSelector(
  isStaff,
  isNodeAdmin,
  isNodeViewer,
  (staff, nodeAdmin, nodeViewer) => staff || nodeAdmin || nodeViewer
);

export const canSeeDataProviderTab = createSelector(
  isStaff,
  isDataProviderAdmin,
  isDataProviderViewer,
  (staff, dataProviderAdmin, dataProviderViewer) =>
    staff || dataProviderAdmin || dataProviderViewer
);

export const canSeeGroupTab = createSelector(
  isStaff,
  isGroupManager,
  (staff, groupManager) => staff || groupManager
);

export const canAddProjects = (state: DefaultRootState): boolean =>
  state.auth.pagePermissions.project.add;
