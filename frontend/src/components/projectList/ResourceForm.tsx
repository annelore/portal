import React, { ReactElement } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormProvider } from 'react-hook-form';
import {
  ConfirmLabel,
  email,
  FormDialog,
  HiddenField,
  LabelledField,
  required,
  url,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { Project, ProjectResources } from '../../api/generated';
import { CALL, updateProjectAction } from '../../actions/actionTypes';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import produce from 'immer';
import { FormFieldsContainer } from '../FormFieldsContainer';

type ResourceFormProps = {
  project: Project;
  resource: Partial<ProjectResources>;
  onClose: () => void;
};

export const ResourceForm = ({
  project,
  resource,
  onClose,
}: Readonly<ResourceFormProps>): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.PROJECT_RESOURCE_LIST,
    I18nNamespace.COMMON,
  ]);
  const dispatch = useDispatch();

  const isSubmitting = useSelector((state) => state.project.isSubmitting);
  const isEdit = !!resource.id;

  const form = useEnhancedForm<ProjectResources>();

  function submit(resourceForm: ProjectResources) {
    const newProject = produce(project, (draft) => {
      if (isEdit) {
        const index = project.resources!.findIndex(
          (resource) => resource.id == resourceForm.id
        );
        draft.resources![index] = resourceForm;
      } else {
        draft.resources!.push(resourceForm);
      }
    });

    dispatch(
      updateProjectAction(newProject, {
        type: CALL,
        callback: onClose,
      })
    );
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={isEdit ? `${t('columns.name')}: ${resource?.name}` : 'New '}
        open={!!resource}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: resource.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="name"
            type="text"
            validations={required}
            label={t(`${I18nNamespace.PROJECT_RESOURCE_LIST}:columns.name`)}
            initialValues={resource}
            fullWidth
          />
          <LabelledField
            name="location"
            type="text"
            validations={[required, url]}
            label={t(`${I18nNamespace.PROJECT_RESOURCE_LIST}:columns.location`)}
            initialValues={resource}
            fullWidth
          />
          <LabelledField
            name="description"
            type="text"
            label={t(
              `${I18nNamespace.PROJECT_RESOURCE_LIST}:columns.description`
            )}
            initialValues={resource}
            fullWidth
          />
          <LabelledField
            name="contact"
            type="text"
            validations={email}
            label={t(`${I18nNamespace.PROJECT_RESOURCE_LIST}:columns.contact`)}
            initialValues={resource}
            fullWidth
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
