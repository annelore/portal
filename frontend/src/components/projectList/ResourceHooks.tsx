import { Column } from 'react-table';
import { Required } from 'utility-types';
import { Project, ProjectResources } from '../../api/generated';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import React, { useMemo } from 'react';
import { useFormDialog, EmailLink } from '@biomedit/next-widgets';
import { ResourceForm } from './ResourceForm';
import { ResourceField } from './ResourceField';

export const useResourceColumns = (): Array<
  Column<Required<ProjectResources>>
> => {
  const { t } = useTranslation(I18nNamespace.PROJECT_RESOURCE_LIST);
  return useMemo(() => {
    return [
      {
        id: 'name',
        Header: t<string>('columns.name'),
        accessor: (row) => row,
        Cell: (row) => ResourceField(row.value),
      },
      {
        id: 'description',
        Header: t<string>('columns.description'),
        accessor: 'description',
      },
      {
        id: 'contact',
        Header: t<string>('columns.contact'),
        accessor: 'contact',
        Cell: (row) => (row.value ? EmailLink({ email: row.value }) : ''),
      },
    ];
  }, [t]);
};

export const useResourceForm = (project: Project) => {
  const newResource = {
    name: undefined,
    location: undefined,
    description: undefined,
    contact: undefined,
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<ProjectResources>>(newResource);
  const resourceForm = data && (
    <ResourceForm project={project} resource={data} onClose={closeFormDialog} />
  );
  return {
    resourceForm,
    ...rest,
  };
};
