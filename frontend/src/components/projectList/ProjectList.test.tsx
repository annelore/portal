import { configure, fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore, RootState } from '../../store';
import React from 'react';
import authStateNodeAdmin from '../../../test-data/authStateNodeAdmin.json';
import listDataProviderResponse from '../../../test-data/listDataProviderResponse.json';
import listUsersResponse from '../../../test-data/listUsersResponse.json';
import staffState from '../../../test-data/staffState.json';
import '@testing-library/jest-dom/extend-expect';
import {
  allProjectsKey,
  archivedProjectsKey,
  captionProjectCode,
  ProjectList,
  projectListItemName,
} from './ProjectList';
import {
  Project,
  ProjectDataTransfers,
  ProjectDataTransfersPurposeEnum,
  ProjectDataTransfersStatusEnum,
  ProjectPermissionsEdit,
  ProjectPermissionsEditUsersEnum,
  ProjectResources,
  ProjectUsersRolesEnum,
} from '../../api/generated';
import {
  addIconButtonLabelPrefix,
  DeepWriteable,
  deleteIconButtonLabelPrefix,
  editIconButtonLabelPrefix,
  expectToBeInTheDocument,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import {
  resourceTableAddButtonLabel,
  resourceTableTitle,
} from './ResourceTable';
import {
  dtrTableAddButtonLabel,
  dtrTableTitle,
} from '../dataTransfer/DataTransferTable';
import { rest } from 'msw';
import { backend, generatedBackendApi } from '../../api/api';

let server;

beforeAll(() => {
  configure({
    asyncUtilTimeout: 10000,
  });
});

afterAll(() => {
  server.close();
});

function createProject(
  withDtr: boolean,
  withResource: boolean,
  roles: ProjectUsersRolesEnum[],
  edit: ProjectPermissionsEdit,
  archived: boolean
): Project {
  const dtr: Array<ProjectDataTransfers> = [
    {
      id: 1,
      packages: [],
      requestorName:
        'Chuck Norris (ID: 63457666153) (chuck.norris@roundhouse.kick)',
      requestorDisplayId: 'ID: 63457666153',
      requestorFirstName: 'Chuck',
      requestorLastName: 'Norris',
      requestorEmail: 'chuck.norris@roundhouse.kick',
      projectName: 'Test Project 1',
      project: 1,
      maxPackages: 1,
      status: ProjectDataTransfersStatusEnum.INITIAL,
      dataProvider: 'area51',
      requestor: 2,
      purpose: ProjectDataTransfersPurposeEnum.TEST,
      requestorPgpKeyFp: '1100000000000000000000000000000000000000',
    },
  ];

  const resource: Array<ProjectResources> = [
    {
      id: 1,
      name: 'Croissants',
      location: 'http://croissant.ch',
      description: 'Fresh Croissants!',
      contact: '',
    },
  ];

  return {
    id: 1,
    gid: 1500000,
    permissions: {
      edit,
    },
    dataTransfers: withDtr ? dtr : [],
    name: 'Test Project 1',
    code: 'test_project_1',
    destination: 'sis',
    ipAddressRanges: [],
    resources: withResource ? resource : [],
    users: roles.length
      ? [
          {
            username: '63457666153@eduid.ch',
            email: 'chuck.norris@roundhouse.kick',
            firstName: 'Chuck',
            lastName: 'Norris',
            displayName:
              'Chuck Norris (ID: 63457666153) (chuck.norris@roundhouse.kick)',
            affiliation: '',
            affiliationId: '',
            id: 2,
            roles,
          },
        ]
      : [],
    archived,
  };
}

const editUser = {
  name: false,
  code: false,
  destination: false,
  users: [],
  ipAddressRanges: false,
  resources: false,
};

const editNodeAdmin = {
  name: true,
  code: true,
  destination: false,
  users: [
    ProjectPermissionsEditUsersEnum.PM,
    ProjectPermissionsEditUsersEnum.DM,
    ProjectPermissionsEditUsersEnum.PL,
    ProjectPermissionsEditUsersEnum.USER,
  ],
  ipAddressRanges: true,
  resources: true,
};

const editPl = {
  name: false,
  code: false,
  destination: false,
  users: [
    ProjectPermissionsEditUsersEnum.PM,
    ProjectPermissionsEditUsersEnum.DM,
    ProjectPermissionsEditUsersEnum.USER,
  ],
  ipAddressRanges: false,
  resources: false,
};

const editStaff = {
  name: true,
  code: true,
  destination: true,
  users: [
    ProjectPermissionsEditUsersEnum.PL,
    ProjectPermissionsEditUsersEnum.PM,
    ProjectPermissionsEditUsersEnum.DM,
    ProjectPermissionsEditUsersEnum.USER,
  ],
  ipAddressRanges: true,
  resources: true,
};

type WriteableProject = DeepWriteable<Project>;

const itemList: Array<WriteableProject> = [
  createProject(false, true, [ProjectUsersRolesEnum.USER], editUser, false),
  createProject(true, false, [ProjectUsersRolesEnum.USER], editUser, false),
  createProject(false, false, [], editNodeAdmin, false),
  createProject(false, false, [ProjectUsersRolesEnum.PL], editPl, false),
  createProject(false, true, [ProjectUsersRolesEnum.PL], editPl, false),
  createProject(false, true, [ProjectUsersRolesEnum.USER], editUser, true),
  createProject(true, false, [ProjectUsersRolesEnum.USER], editUser, true),
  createProject(false, false, [], editUser, true),
  createProject(false, false, [ProjectUsersRolesEnum.PL], editUser, true),
  createProject(false, true, [ProjectUsersRolesEnum.PL], editUser, true),
  createProject(true, false, [ProjectUsersRolesEnum.USER], editStaff, false),
  createProject(false, true, [ProjectUsersRolesEnum.PL], editStaff, false),
];

describe('ProjectList', () => {
  describe('Component', () => {
    const renderProjectList = (project?: WriteableProject, staff = false) => {
      // mocked since project is parametrized as an object, for `msw` the backend representation would be necessary
      jest
        .spyOn(generatedBackendApi, 'listProjectsRaw')
        .mockImplementation(() => {
          return new Promise((resolve) => {
            // @ts-expect-error not relevant for logic to be tested
            resolve({
              value: () =>
                new Promise((resolve2) => {
                  // @ts-expect-error not relevant for logic to be tested
                  resolve2([project]);
                }),
            });
          });
        });

      server = setupMockApi(
        rest.get(`${backend}users/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, listUsersResponse))
        ),
        rest.get(`${backend}data-provider/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, listDataProviderResponse))
        )
      );

      // @ts-expect-error JSON doesn't support enums
      const state: DeepWriteable<RootState> = {
        ...getInitialState(),
        ...{
          project: {
            isFetching: false,
            isSubmitting: false,
            itemList: project ? [project] : [itemList[0]],
            ipAddress: '127.0.0.1',
          },
          ...{
            dataProvider: {
              isFetching: false,
              isSubmitting: false,
              itemList: listDataProviderResponse,
            },
          },
          ...{
            auth: staff ? staffState.auth : authStateNodeAdmin.auth,
          },
        },
      };
      render(
        <Provider
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore project role is expected to be an enum, which it can't be in a json file
          store={makeStore(undefined, state)}
        >
          <ProjectList />
        </Provider>
      );
    };

    it('should allow adding a new project as node admin', async () => {
      renderProjectList();
      await screen.findByRole('button', {
        name: addIconButtonLabelPrefix + projectListItemName,
      });
    });

    it.each`
      projectIndex | staff    | showResources | canAddResource | showDtrs | canAddDtr | canEditProject | canDeleteProject
      ${0}         | ${false} | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${1}         | ${false} | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${false}
      ${2}         | ${false} | ${false}      | ${true}        | ${false} | ${false}  | ${true}        | ${false}
      ${3}         | ${false} | ${false}      | ${false}       | ${false} | ${false}  | ${true}        | ${false}
      ${4}         | ${false} | ${true}       | ${false}       | ${false} | ${false}  | ${true}        | ${false}
      ${5}         | ${false} | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${6}         | ${false} | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${false}
      ${6}         | ${true}  | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${true}
      ${7}         | ${false} | ${false}      | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${8}         | ${false} | ${false}      | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${9}         | ${false} | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${9}         | ${true}  | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${true}
      ${10}        | ${true}  | ${false}      | ${true}        | ${true}  | ${true}   | ${true}        | ${true}
      ${11}        | ${true}  | ${true}       | ${true}        | ${true}  | ${true}   | ${true}        | ${true}
    `(
      'should show resources ($showResources), ' +
        'can add resource ($canAddResource), ' +
        'show DTRs ($showDtrs), ' +
        'can add DTR ($canAddDtr), ' +
        'can edit the project ($canEditProject), ' +
        'can delete the project ($canDeleteProject), ' +
        'on project with index $projectIndex, ' +
        'if user is staff ($staff)',
      async ({
        projectIndex,
        staff,
        showResources,
        canAddResource,
        showDtrs,
        canAddDtr,
        canEditProject,
        canDeleteProject,
      }) => {
        const project = itemList[projectIndex];
        renderProjectList(project, staff);

        if (project.archived) {
          // archived projects are only visible under "Archived Projects"
          fireEvent.mouseDown(await screen.findByText(allProjectsKey));
          fireEvent.click(await screen.findByText(archivedProjectsKey));
        }

        const accordion = await screen.findByText(project.name);
        accordion.click();
        await screen.findByText(captionProjectCode); // wait for project to be expanded

        const resources = await screen.queryByText(resourceTableTitle);
        expectToBeInTheDocument(resources, showResources || canAddResource);

        if (showResources) {
          const resource = project?.resources?.[0].name;
          expect(resource).toBeDefined();
          await screen.getByText(resource as string); // safe cast as we verified it to be defined
        } else {
          expect(project.resources).toHaveLength(0);
        }

        const addResourceBtn = screen.queryByRole('button', {
          name: addIconButtonLabelPrefix + resourceTableAddButtonLabel,
        });
        expectToBeInTheDocument(addResourceBtn, canAddResource);

        const dtrs = screen.queryByText(dtrTableTitle);
        expectToBeInTheDocument(dtrs, showDtrs);

        const addDtrBtn = screen.queryByRole('button', {
          name: addIconButtonLabelPrefix + dtrTableAddButtonLabel,
        });
        expectToBeInTheDocument(addDtrBtn, canAddDtr);

        const editProjectBtn = screen.queryByRole('button', {
          name: editIconButtonLabelPrefix + projectListItemName,
        });
        expectToBeInTheDocument(editProjectBtn, canEditProject);

        const deleteProjectBtn = screen.queryByRole('button', {
          name: deleteIconButtonLabelPrefix + projectListItemName,
        });
        expectToBeInTheDocument(deleteProjectBtn, canDeleteProject);
      }
    );
  });
});
