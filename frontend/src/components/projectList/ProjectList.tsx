import React, { ReactElement, useCallback, useMemo, useState } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  FabButton,
  Field,
  ListModel,
  ListPage,
  Select,
  Toolbar,
  Warning,
  DeleteDialog,
  requestAction,
} from '@biomedit/next-widgets';
import {
  ARCHIVE_PROJECT,
  CALL,
  DELETE_PROJECT,
  LOAD_DATA_PROVIDERS,
  LOAD_PROJECTS,
  LOAD_USERS,
  UNARCHIVE_PROJECT,
} from '../../actions/actionTypes';
import { DataTransferTable } from '../dataTransfer/DataTransferTable';
import { useProjectForm } from '../projectForm/ProjectForm';
import { IdRequired } from '../../global';
import { Project } from '../../api/generated';
import { canAddProjects, canSeeNodeTab, isStaff } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { ResourceTable } from './ResourceTable';
import { AdditionalActionButtonFactory } from '@biomedit/next-widgets/esm/lib/components/list/ListHooks';

function renderWarning(data: string | null) {
  return <Warning tooltip={data} />;
}

function formatWarning(prj: Project): string | null {
  if (prj['ipAddressInRange']) {
    // TODO: move postProcessProjects (ipAddressInRange) to here
    return null;
  }
  return 'Your IP address is not within a range of the project';
}

export const captionProjectCode = 'Project Code';

const useProjectModel = (): ListModel<IdRequired<Project>> => {
  const staff = useSelector(isStaff);

  return useMemo(
    () => ({
      getCaption: (prj: Project) => prj.name ?? 'Project',
      fields: [
        Field({
          caption: captionProjectCode,
          getProperty: (prj: Project) => prj.code,
          key: 'code',
        }),
        Field({
          getProperty: formatWarning,
          key: 'ipAddressInRange',
          render: renderWarning,
        }),
        Field({
          getProperty: (prj: Project) => prj,
          key: 'resources',
          render: ResourceTable,
          hideIf: (item) =>
            !(item?.resources?.length || item?.permissions?.edit?.resources),
        }),
        Field({
          getProperty: (prj: Project) => prj,
          key: 'dataTransfers',
          render: DataTransferTable,
          hideIf: (item) =>
            !(item?.dataTransfers?.length || staff) ||
            !!(staff && item?.archived && !item?.dataTransfers?.length),
        }),
      ],
    }),
    [staff]
  );
};

type ProjectFilterChoice = {
  label: string;
  filter: (p: Project, userId: number) => boolean;
};

export const projectListItemName = 'project';
export const allProjectsKey = 'filter.allProjects';
export const archivedProjectsKey = 'filter.archivedProjects';

export const ProjectList = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.PROJECT_LIST);

  const dispatch = useDispatch();

  const itemList = useSelector((state) => state.project.itemList);
  const isFetching = useSelector((state) => state.project.isFetching);
  const isSubmitting = useSelector((state) => state.project.isSubmitting);
  const canAdd = useSelector(canAddProjects);
  const canDelete = useSelector(
    (state) => state.auth.pagePermissions.project.del
  );
  const user = useSelector((state) => state.auth.user);
  const canSeeNodeProjects = useSelector(canSeeNodeTab);
  const hasProjects = useSelector(
    (state) => !!state.auth.user?.permissions?.hasProjects
  );
  const loadItems = useCallback(() => {
    batch(() => {
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
      dispatch(requestAction(LOAD_USERS));
      dispatch(requestAction(LOAD_PROJECTS, { ordering: 'name' }));
    });
  }, [dispatch]);

  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_PROJECT, { id }));
    },
    [dispatch]
  );

  const projectModel = useProjectModel();
  const { openFormDialog, projectForm } = useProjectForm();

  const [filterIndex, setFilterIndex] = useState<number>(0);
  const { filteredProjects, selectProps } = useMemo(() => {
    const showFilter = hasProjects && canSeeNodeProjects;
    if (!showFilter)
      return { filteredProjects: itemList, selectProps: undefined };
    const userId = user?.id;
    const projectFilterChoices: ProjectFilterChoice[] = [
      { label: t(allProjectsKey), filter: (p) => !p.archived },
      {
        label: t('filter.myProjects'),
        filter: (p, userId) =>
          !p.archived && p.users.some((user) => user.id === userId),
      },
      {
        label: t(archivedProjectsKey),
        filter: (p) => !!p.archived,
      },
    ];
    const { filter, label: filterLabel } = projectFilterChoices[filterIndex];
    const dropDownEntries = projectFilterChoices.map((choice, index) => ({
      label: choice.label,
      value: index,
    }));

    let filteredProjects: Project[] = [];
    if (userId !== undefined)
      filteredProjects = itemList.filter((p) => filter(p, userId));
    return {
      filteredProjects,
      selectProps: { choices: dropDownEntries, value: filterLabel },
    };
  }, [canSeeNodeProjects, filterIndex, itemList, user, hasProjects, t]);

  const canEdit = useCallback((item: IdRequired<Project>) => {
    if (item.archived) {
      return false;
    }
    const edit = item?.permissions?.edit;
    for (const key in edit) {
      if (Object.prototype.hasOwnProperty.call(edit, key)) {
        const value = edit[key];
        if (Array.isArray(value)) {
          if (value.length) {
            return true;
          }
        } else {
          if (value) {
            return true;
          }
        }
      }
    }
    return false;
  }, []);

  const getItemName = useCallback((item: IdRequired<Project>) => item.name, []);

  const [archiveDialogItem, setArchiveDialogItem] = useState<
    string | undefined
  >();

  const onArchiveDialogClose = useCallback(
    () => setArchiveDialogItem(undefined),
    []
  );

  const additionalActionButtons = useMemo<
    Array<AdditionalActionButtonFactory<Project>>
  >(
    () => [
      (item) => {
        if (item?.permissions?.archive && !item?.archived) {
          const archiveLabel = t('actionButtons.archive');
          return (
            <>
              <DeleteDialog
                title={t('archiveDialog.title')}
                text={t('archiveDialog.text')}
                open={archiveDialogItem == item?.name}
                itemName={t('project')}
                deleteItemName={archiveDialogItem}
                isSubmitting={isSubmitting}
                onDelete={(isConfirmed) => {
                  if (isConfirmed) {
                    dispatch(
                      requestAction(
                        ARCHIVE_PROJECT,
                        {
                          id: String(item?.id),
                        },
                        {
                          type: CALL,
                          callback: onArchiveDialogClose,
                        }
                      )
                    );
                  } else {
                    onArchiveDialogClose();
                  }
                }}
              />
              <FabButton
                key="archive"
                aria-label={archiveLabel}
                title={archiveLabel}
                size="small"
                icon="archive"
                onClick={() => setArchiveDialogItem(item?.name)}
              />
            </>
          );
        } else {
          return null;
        }
      },
      (item) => {
        if (item?.permissions?.archive && item?.archived) {
          const unarchiveLabel = t('actionButtons.unarchive');
          return (
            <FabButton
              key="unarchive"
              aria-label={unarchiveLabel}
              title={unarchiveLabel}
              size="small"
              icon="unarchive"
              onClick={() =>
                dispatch(
                  requestAction(UNARCHIVE_PROJECT, { id: String(item?.id) })
                )
              }
            />
          );
        } else {
          return null;
        }
      },
    ],
    [dispatch, t, archiveDialogItem, isSubmitting, onArchiveDialogClose]
  );

  return (
    <>
      {selectProps && (
        <Toolbar sx={{ marginBottom: 2.5 }}>
          <Select setValue={setFilterIndex} {...selectProps} />
        </Toolbar>
      )}
      <ListPage<IdRequired<Project>>
        model={projectModel}
        emptyMessage={t('emptyMessage')}
        itemList={filteredProjects}
        itemName={t(projectListItemName)}
        loadItems={loadItems}
        isFetching={isFetching}
        openForm={openFormDialog}
        form={projectForm}
        canAdd={canAdd}
        canDelete={canDelete}
        canEdit={canEdit}
        deleteItem={deleteItem}
        getItemName={getItemName}
        isSubmitting={isSubmitting}
        additionalActionButtons={additionalActionButtons}
      />
    </>
  );
};
