import React, { ReactElement, useState } from 'react';
import { SSHDialog } from '../sshDialog';
import { ProjectResources } from '../../api/generated';
import { isClient, Tooltip } from '@biomedit/next-widgets';
import { Link } from '@mui/material';
import { LinkProps } from '@mui/material/Link/Link';

const preventDefault = (event) => event.preventDefault();

type ResourceLinkProps = LinkProps & {
  resource: ProjectResources;
};

const ResourceLink = ({
  resource,
  href = undefined,
  onClick = undefined,
  ...other
}: Readonly<ResourceLinkProps>): ReactElement => {
  href = href || resource.location;
  return (
    <Tooltip title={resource.location}>
      <Link href={href} onClick={onClick} {...other}>
        {resource.name}
      </Link>
    </Tooltip>
  );
};

const DefaultComponent = (resource): ReactElement => {
  const other = isClient()
    ? { href: resource.location, rel: 'noopener noreferrer', target: '_blank' }
    : { href: '#', onClick: preventDefault };
  return <ResourceLink resource={resource} {...other} />;
};

const SSHField = (resource): ReactElement => {
  const protocol = 'ssh';
  const path = protocol && resource.location.substr(protocol.length + 3);
  const [open, setOpen] = useState<boolean>(false);

  return (
    <>
      <ResourceLink
        resource={resource}
        onClick={() => setOpen(true)}
        href="#"
      />
      <SSHDialog
        open={open}
        onClose={() => setOpen(false)}
        command={'ssh ' + path}
      />
    </>
  );
};

const componentByProtocol = new Map([['ssh', SSHField]]);

export const ResourceField = (resource: ProjectResources): ReactElement => {
  const matches = resource.location?.match(/[a-z]*(?=:\/\/)/);
  const protocol = matches && matches[0];
  const Component =
    (protocol && componentByProtocol.get(protocol)) || DefaultComponent;
  return <Component {...resource} />;
};
