import React, {
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useRef,
} from 'react';
import { styled } from '@mui/material/styles';
import { FeedList } from './FeedList';
import {
  Box,
  Grid,
  isChristmas,
  isEaster,
  requestAction,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@biomedit/next-widgets';
import Image, { ImageProps } from 'next/image';
import { Paper } from '@mui/material';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../i18n';
import { EffectRef, EffectsOverlay } from 'widgets/effects/EffectsOverlay';
import { EffectCommand } from 'widgets/effects';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_QUICK_ACCESSES } from '../actions/actionTypes';
import { IdRequired } from '../global';
import { QuickAccessTile } from '../api/generated';
import orderBy from 'lodash/orderBy';

const EasterEgg = styled('span')(() => ({
  cursor: 'pointer',
}));

const ImageContainer = styled('div')(({ theme }) => ({
  marginTop: theme.spacing(1.5),
}));

const GridItemContent = styled(Paper)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'space-between',
  padding: theme.spacing(2),
  width: theme.spacing(21),
  height: theme.spacing(21),
}));

declare type GridItemProps = {
  href: string;
  description: string;
  image: Pick<ImageProps, 'src' | 'title'>;
};

const GridItem = ({
  href,
  image,
  description,
}: GridItemProps): ReactElement => {
  const imageSize = 75;
  return (
    <Grid item>
      <a href={href} target="_blank" rel="noopener noreferrer">
        <GridItemContent variant="outlined">
          <ImageContainer>
            <Image
              alt={description + ' Logo/Image'}
              height={imageSize}
              width={imageSize}
              src={image.src as string}
              title={image.title}
            />
          </ImageContainer>
          <Box display="flex" height={'30%'}>
            <Typography
              sx={{
                margin: 'auto',
                color: 'text.secondary',
              }}
              variant="caption"
            >
              {description}
            </Typography>
          </Box>
        </GridItemContent>
      </a>
    </Grid>
  );
};

export function Home(): ReactElement {
  const { t } = useTranslation(I18nNamespace.HOME);

  const christmas = useMemo(() => {
    return isChristmas();
  }, []);

  const easter = useMemo(() => {
    return isEaster();
  }, []);

  const effectRef = useRef<EffectRef>(null);

  const onEasterEggClick = useCallback(() => {
    effectRef.current?.start(EffectCommand.CONFETTI);
  }, []);

  const quickAccesses = orderBy(
    useSelector(
      (state) => state.quickAccesses.itemList as IdRequired<QuickAccessTile>
    ),
    ['order']
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_QUICK_ACCESSES));
  }, [dispatch]);

  return (
    <>
      {christmas && <h3>{t('seasonal.christmas')}</h3>}
      {easter && (
        <>
          <EffectsOverlay ref={effectRef} />
          <h3>
            {t('seasonal.easter')}{' '}
            <EasterEgg onClick={onEasterEggClick}>
              {t('seasonal.easteregg')}
            </EasterEgg>
          </h3>
        </>
      )}
      <Table aria-label="simple table">
        <TableHead>
          <TableRow key={'table-row-header'}>
            <TableCell
              align="center"
              sx={{
                border: 'none',
              }}
              width="60%"
            >
              <h3>{t('header.quickAccess')}</h3>
            </TableCell>
            <TableCell
              align="center"
              sx={{
                border: 'none',
              }}
              width="40%"
            >
              <h3>{t('header.feed')}</h3>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow key={'table-row-content'}>
            <TableCell
              align="center"
              sx={{
                border: 'none',
                verticalAlign: 'top',
              }}
            >
              <Grid
                container
                spacing={3}
                alignItems="stretch"
                justifyContent="center"
                wrap="wrap"
              >
                {quickAccesses.map(({ id, title, description, url, image }) => (
                  <GridItem
                    key={id}
                    href={url}
                    description={title}
                    image={{ title: description, src: image }}
                  />
                ))}
              </Grid>
            </TableCell>
            <TableCell
              sx={{
                border: 'none',
                verticalAlign: 'top',
              }}
            >
              <FeedList />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </>
  );
}
