import React, { ReactElement, useEffect, useMemo, useState } from 'react';
import { styled } from '@mui/material/styles';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_MESSAGES } from '../../actions/actionTypes';
import { Message } from '../../api/generated';
import {
  Divider,
  isChristmas,
  isEaster,
  PaddingBox,
  Typography,
  List,
  requestAction,
} from '@biomedit/next-widgets';
import { IconButton, Menu, Popover } from '@mui/material';
import { AccountCircle, Announcement, ChatBubble } from '@mui/icons-material';

const UserMenuContainer = styled('div')(({ theme }) => ({
  ...{
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingLeft: theme.spacing(0.5),
    backgroundColor: 'rgba(227, 231, 232, 0.83)',
  },
  ...(isChristmas() && {
    backgroundImage: `url(/bg_snow.png);`,
  }),
  ...(isEaster() && {
    background:
      'linear-gradient(#e3e7e8b8, #e3e7e8b8), url(/bg_easter.jpg) no-repeat top right;',
  }),
}));

const ChristmasHat = styled('img')(() => ({
  position: 'absolute',
  top: -13,
  bottom: 0,
  left: 11,
  right: 0,
  zIndex: 1,
  pointerEvents: 'none',
}));

const UserIconContainer = styled('span')(() => ({
  position: 'relative',
}));

type UserMenuProps = {
  children: React.ReactNode;
};

export const UserMenu = React.forwardRef<HTMLDivElement, UserMenuProps>(
  ({ children, ...other }, ref): ReactElement => {
    const [anchorEl, setAnchorEl] = useState(null);
    const handleClick = (event) => setAnchorEl(event.currentTarget);
    const handleClose = () => setAnchorEl(null);
    const [anchorElMsg, setAnchorElMsg] = useState(null);
    const handleClickMsg = (event) => setAnchorElMsg(event.currentTarget);
    const handleCloseMsg = () => setAnchorElMsg(null);
    const dispatch = useDispatch();
    const messages: Message[] = useSelector((state) =>
      state.messages.itemList.slice(-5)
    );

    useEffect(() => {
      dispatch(requestAction(LOAD_MESSAGES));
    }, [dispatch]);
    const christmas = useMemo(() => {
      return isChristmas();
    }, []);

    return (
      <UserMenuContainer>
        <UserIconContainer>
          <IconButton
            sx={{
              zIndex: 0,
            }}
            aria-owns={anchorEl ? 'user-menu' : undefined}
            aria-haspopup="true"
            onClick={handleClick}
            size="large"
          >
            <AccountCircle />
          </IconButton>
          {christmas && (
            <ChristmasHat
              src="/christmas_hat.png"
              width="30"
              height="22"
              alt="Christmas hat on top of user icon"
            />
          )}
        </UserIconContainer>
        {messages && (
          <IconButton
            aria-owns={anchorElMsg ? 'msg-menu' : undefined}
            aria-haspopup="true"
            onClick={handleClickMsg}
            size="large"
            sx={{ marginLeft: 1 }}
          >
            <Announcement />
          </IconButton>
        )}
        <Menu
          id="user-menu"
          anchorEl={anchorEl}
          //Aligns the user menu to appear on the top left of the user icon.
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
          open={Boolean(anchorEl)}
          onClose={handleClose}
          ref={ref}
          {...other}
        >
          {children}
        </Menu>
        {messages && messages.length > 0 && (
          <Popover
            id="msg-menu"
            anchorEl={anchorElMsg}
            open={Boolean(anchorElMsg)}
            onClose={handleCloseMsg}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
          >
            <PaddingBox>
              <Typography variant="h5">Messages</Typography>
              <List
                icon={<ChatBubble />}
                items={messages.map(
                  (msg) => msg?.project?.name + ' - ' + msg.tool
                )}
              />
            </PaddingBox>
            <Divider />
            <PaddingBox>
              <Typography>
                <Link href="/messages">See all messages</Link>
              </Typography>
            </PaddingBox>
          </Popover>
        )}
      </UserMenuContainer>
    );
  }
);
UserMenu.displayName = 'UserMenu';
