import React, { ForwardedRef, ReactElement } from 'react';
import { useSelector } from 'react-redux';
import {
  Description,
  FormattedItemField,
  PaddingBox,
  Typography,
} from '@biomedit/next-widgets';
import { User, Userinfo } from '../../api/generated';
import {
  getAffiliation,
  getEduidUsername,
  getLocalUsername,
} from '../../utils';
import { I18nNamespace } from '../../i18n';
import { useTranslation } from 'next-i18next';

function isUserInfo(userOrInfo: User | Userinfo): userOrInfo is Userinfo {
  return (
    !!userOrInfo &&
    ('manages' in userOrInfo ||
      'permissions' in userOrInfo ||
      'ipAddress' in userOrInfo)
  );
}

export const emailFieldCaption = `${I18nNamespace.COMMON}:email`;
export const flagFieldCaption = `${I18nNamespace.COMMON}:models.flag_plural`;
export const groupFieldCaption = `${I18nNamespace.COMMON}:models.group_plural`;
export const affiliationFieldCaption = `${I18nNamespace.USER_INFO}:affiliation_plural`;
export const dataProviderForFieldCaption = `${I18nNamespace.USER_INFO}:dataProviderFor`;
export const displayNameFieldCaption = `${I18nNamespace.USER_INFO}:displayName`;
export const eduidUsernameFieldCaption = `${I18nNamespace.USER_INFO}:eduidUsername`;
export const ipAddressFieldCaption = `${I18nNamespace.USER_INFO}:ipAddress`;
export const localUsernameFieldCaption = `${I18nNamespace.USER_INFO}:localUsername`;

function addMultiValueField(fieldName: string, fieldValues, infoBox) {
  if (!!fieldValues && fieldValues.length) {
    infoBox.push({
      caption: fieldName,
      component: (
        <div>
          {fieldValues.map((field) => (
            <Typography key={field} variant={'body1'}>
              {field}
            </Typography>
          ))}
        </div>
      ),
    });
  }
}

export function useUserInfoFields(
  userInfo: User | Userinfo | null
): FormattedItemField[] {
  const { t } = useTranslation([I18nNamespace.COMMON, I18nNamespace.USER_LIST]);
  if (!userInfo) {
    return [];
  }
  const { firstName, lastName, email } = userInfo;
  const localUsername = getLocalUsername(userInfo);
  const eduidUsername = getEduidUsername(userInfo);
  const affiliation = getAffiliation(userInfo);
  const displayName =
    (firstName || lastName) &&
    (firstName ? firstName + ' ' : '') + (lastName ? lastName : '');

  const fields: FormattedItemField[] = [
    { caption: t(displayNameFieldCaption), component: displayName },
    { caption: t(eduidUsernameFieldCaption), component: eduidUsername },
    { caption: t(localUsernameFieldCaption), component: localUsername ?? '' },
    { caption: t(emailFieldCaption), component: email },
    { caption: t(affiliationFieldCaption), component: affiliation },
  ];

  if (isUserInfo(userInfo)) {
    fields.push({
      caption: t(ipAddressFieldCaption),
      component: userInfo.ipAddress,
    });

    const managedDataProviders = userInfo.manages?.dataProviders;
    if (!!managedDataProviders && managedDataProviders.length) {
      fields.push({
        caption: t(dataProviderForFieldCaption),
        component: (
          <div>
            {managedDataProviders.map((dp) => (
              <Typography key={dp.name} variant={'body1'}>
                {dp.name}
              </Typography>
            ))}
          </div>
        ),
      });
    }

    // Add "Groups" and "Flags" fields to the Userinfo box that lists all the
    // groups a user belongs to, and all flags that are active for the user.
    addMultiValueField(t(groupFieldCaption), userInfo.groups, fields);
    addMultiValueField(t(flagFieldCaption), userInfo.flags, fields);
  }

  return fields.filter(({ component }) => component !== undefined);
}

export const UserInfoBox = React.forwardRef(
  (props, ref: ForwardedRef<HTMLDivElement>): ReactElement => {
    const userInfo = useSelector((state) => state.auth.user);
    const userInfoEntries = useUserInfoFields(userInfo);

    return (
      <PaddingBox ref={ref}>
        <Description
          title="Userinfo"
          entries={userInfoEntries}
          labelWidth="45%"
        />
      </PaddingBox>
    );
  }
);
UserInfoBox.displayName = 'UserInfoBox';
