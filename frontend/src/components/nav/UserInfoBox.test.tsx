import { DataProvider, Userinfo } from '../../api/generated';
import {
  useUserInfoFields,
  UserInfoBox,
  emailFieldCaption,
  dataProviderForFieldCaption,
  affiliationFieldCaption,
  displayNameFieldCaption,
  eduidUsernameFieldCaption,
  localUsernameFieldCaption,
  ipAddressFieldCaption,
} from './UserInfoBox';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { getInitialState, makeStore, RootState } from '../../store';
import React from 'react';
import { initialState } from '../../reducers/auth';
import { DeepWriteable } from '@biomedit/next-widgets';

describe('UserInfoBox', () => {
  const ip = '127.0.0.1';

  const getUser = (): Userinfo => {
    return {
      username: '63457666153@eduid.ch',
      email: 'chuck.norris@roundhouse.kick',
      firstName: 'Chuck',
      lastName: 'Norris',
      profile: {
        affiliation: '',
        localUsername: 'cnorris',
        displayName:
          'Chuck Norris (ID: 63457666153) (chuck.norris@roundhouse.kick)',
        displayId: 'ID: 63457666153',
        uid: 1000000,
        gid: 1000000,
        namespace: 'ch',
        displayLocalUsername: 'ch_cnorris',
      },
      flags: [],
      id: 2,
      ipAddress: ip,
      permissions: {
        manager: true,
        staff: true,
        dataManager: true,
        dataProvider: true,
        groupManager: true,
        nodeAdmin: false,
        nodeViewer: false,
        dataProviderAdmin: false,
        dataProviderViewer: false,
        hasProjects: true,
      },
    };
  };

  const getDp = (i: number): DataProvider => ({
    code: `dp${i}`,
    name: `Data Provider ${i}`,
    node: `Node ${i}`,
  });

  let user: Userinfo;

  beforeEach(() => {
    user = getUser();
  });

  describe('useUserInfoFields', () => {
    const basicUserInfoFieldAmount = 6;

    it('should contain field captions in the right order', () => {
      user.manages = {
        dataProviders: [getDp(0)],
      };
      const fields = useUserInfoFields(user);
      expect(fields.map((field) => field.caption)).toStrictEqual([
        displayNameFieldCaption,
        eduidUsernameFieldCaption,
        localUsernameFieldCaption,
        emailFieldCaption,
        affiliationFieldCaption,
        ipAddressFieldCaption,
        dataProviderForFieldCaption,
      ]);
    });

    it('should contain the correct field values', () => {
      const fields = useUserInfoFields(user);
      expect(fields.map((field) => field.component)).toStrictEqual([
        'Chuck Norris',
        '63457666153@eduid.ch',
        'cnorris',
        'chuck.norris@roundhouse.kick',
        '-',
        '127.0.0.1',
      ]);
    });

    it('should return only basic user info fields if "manages" is "undefined"', () => {
      const fields = useUserInfoFields(user);
      expect(fields).toHaveLength(basicUserInfoFieldAmount);
    });

    it('should return only basic user info fields if "manages" is an empty object', () => {
      user.manages = {};
      const fields = useUserInfoFields(user);
      expect(fields).toHaveLength(basicUserInfoFieldAmount);
    });

    it('should return only basic user info fields if "manages" is an object with only empty arrays', () => {
      user.manages = {
        dataProviders: [],
      };
      const fields = useUserInfoFields(user);
      expect(fields).toHaveLength(basicUserInfoFieldAmount);
    });

    it('should also return one data provider field if user manages exactly one data provider', () => {
      user.manages = { dataProviders: [getDp(0)] };
      const fields = useUserInfoFields(user);
      expect(fields).toHaveLength(basicUserInfoFieldAmount + 1);
    });

    it('should also return one data provider field if user manages more than one data provider', () => {
      user.manages = { dataProviders: [getDp(0), getDp(1)] };
      const fields = useUserInfoFields(user);
      expect(fields).toHaveLength(basicUserInfoFieldAmount + 1);
    });

    it('should also return one flag field if user has flags', () => {
      const fields = useUserInfoFields({
        ...user,
        flags: ['funwithflags', 'anotheflag'],
      });
      expect(fields).toHaveLength(basicUserInfoFieldAmount + 1);
    });
  });

  describe('Component', () => {
    let state: DeepWriteable<RootState>;

    beforeEach(() => {
      state = {
        ...getInitialState(),
        auth: {
          ...initialState,
          user: getUser(),
        },
      };
    });

    it('should display caption and component of fields', async () => {
      render(
        <Provider store={makeStore(undefined, state as RootState)}>
          <UserInfoBox />
        </Provider>
      );

      await screen.findByText(ipAddressFieldCaption);
      await screen.findByText(ip);
    });

    it('should not display fields that are undefined', async () => {
      delete state.auth.user?.ipAddress;

      render(
        <Provider store={makeStore(undefined, state as RootState)}>
          <UserInfoBox />
        </Provider>
      );

      expect(screen.queryByText(ipAddressFieldCaption)).not.toBeInTheDocument();
      expect(screen.queryByText(ip)).not.toBeInTheDocument();

      expect(
        screen.queryByText(dataProviderForFieldCaption)
      ).not.toBeInTheDocument();
    });

    it('should show all data providers the user manages', async () => {
      const dp0 = getDp(0);
      const dp1 = getDp(1);

      // defined in beforeEach
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      state.auth.user!.manages = {
        dataProviders: [dp0, dp1],
      };

      render(
        <Provider store={makeStore(undefined, state as RootState)}>
          <UserInfoBox />
        </Provider>
      );

      await screen.findByText(dataProviderForFieldCaption);
      await screen.findByText(dp0.name);
      await screen.findByText(dp1.name);
    });
  });
});
