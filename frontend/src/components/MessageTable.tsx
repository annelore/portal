import React, { ReactElement, useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_MESSAGES } from '../actions/actionTypes';
import { Message } from '../api/generated';
import { IdRequired } from '../global';
import { Column } from 'react-table';
import {
  EnhancedTable,
  formatDate,
  requestAction,
} from '@biomedit/next-widgets';

const emptyMessage = 'You have no messages.';

export const MessageTable = (): ReactElement => {
  const dispatch = useDispatch();

  const itemList = useSelector((state) => state.messages.itemList);
  const isFetching = useSelector((state) => state.messages.isFetching);

  const loadItems = useCallback(() => {
    dispatch(requestAction(LOAD_MESSAGES));
  }, [dispatch]);

  const columns: Array<Column<IdRequired<Message>>> = useMemo(
    () => [
      {
        id: 'tool',
        Header: 'Tool',
        accessor: 'tool',
      },
      {
        id: 'data',
        Header: 'Data',
        accessor: 'data',
      },
      {
        id: 'projectName',
        Header: 'Project',
        accessor: (data) => data.project?.name,
      },
      {
        id: 'created',
        Header: 'Created',
        accessor: (data) => formatDate(data.created),
      },
    ],
    []
  );

  return (
    <EnhancedTable<IdRequired<Message>>
      emptyMessage={emptyMessage}
      columns={columns}
      itemList={itemList}
      loadItems={loadItems}
      isFetching={isFetching}
      isSubmitting={false} // read-only
    />
  );
};
