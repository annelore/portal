import { CHANGE_USERNAME, CHECK_AUTH, LOGIN } from '../actions/actionTypes';
import { AnyAction } from 'redux';
import produce from 'immer';
import { Userinfo, UserinfoPermissions } from '../api/generated';
import { noPermissions, PagePermissions } from '../permissions';
import { StatusCodes } from 'http-status-codes';

export type AuthState = {
  isAuthenticated: boolean;
  isFetching: boolean;
  isSubmitting: boolean;
  isExistingUsername: boolean;
  user: Userinfo | null;
  pagePermissions: PagePermissions;
};

export const initialState: AuthState = {
  isAuthenticated: false,
  isFetching: false,
  isSubmitting: false,
  isExistingUsername: false,
  user: null,
  pagePermissions: noPermissions,
};

export function auth(
  state: AuthState = initialState,
  action: AnyAction
): AuthState {
  return produce(state, (draft: AuthState) => {
    switch (action.type) {
      case CHECK_AUTH.request:
        draft.isFetching = true;
        break;
      case LOGIN:
        draft.isFetching = false;
        draft.user = action.response;
        draft.pagePermissions = pagePermissions(action.response.permissions);
        draft.isAuthenticated = true;
        break;
      case CHECK_AUTH.failure:
        draft.isFetching = false;
        draft.isAuthenticated = false;
        break;
      case CHANGE_USERNAME.request:
        draft.isSubmitting = true;
        break;
      case CHANGE_USERNAME.success:
        draft.isSubmitting = false;
        draft.user = action.response;
        draft.isExistingUsername = false;
        break;
      case CHANGE_USERNAME.failure:
        draft.isSubmitting = false;
        if (action.error.status === StatusCodes.CONFLICT) {
          draft.isExistingUsername = true;
        }
        break;
    }
  });
}

function pagePermissions(roles: UserinfoPermissions): PagePermissions {
  return {
    project: {
      del: !!roles.staff,
      add: !!roles.staff || !!roles.nodeAdmin,
      edit: !!(roles.manager || roles.staff || roles.nodeAdmin),
    },
    dataTransfer: {
      dataManager: !!roles.dataManager,
      projectLeader: !!roles.projectLeader,
      staff: !!roles.staff,
      dataProvider: !!roles.dataProvider,
      nodeViewer: !!roles.nodeViewer,
      nodeAdmin: !!roles.nodeAdmin,
    },
    userManage: {
      staff: !!roles.staff,
      nodeAdmin: !!roles.nodeAdmin,
      nodeViewer: !!roles.nodeViewer,
      dataProviderAdmin: !!roles.dataProviderAdmin,
      dataProviderViewer: !!roles.dataProviderViewer,
      groupManager: !!roles.groupManager,
    },
  };
}
