import { LOAD_KEYS, SEND_KEY_SIGN_REQUEST } from '../actions/actionTypes';
import produce from 'immer';
import { AnyAction } from 'redux';
import { Mutable } from 'utility-types';
import {
  BaseReducerState,
  initialBaseReducerState,
} from '@biomedit/next-widgets';
import {
  PgpKey,
  PgpKeySignRequest,
  PgpKeySignStatusEnum,
} from '../api/generated';
import { IdRequired } from '../global';

export interface KeyAction extends AnyAction {
  response?: PgpKey[] | PgpKeySignRequest;
}

export type KeysState = BaseReducerState<IdRequired<PgpKey>>;

export function keys(
  state: KeysState = initialBaseReducerState<IdRequired<PgpKey>>(),
  action: KeyAction
): KeysState {
  return produce(state, (draft: KeysState) => {
    switch (action.type) {
      case LOAD_KEYS.request:
        draft.isFetching = true;
        break;
      case LOAD_KEYS.success:
        draft.isFetching = false;
        if (action.response) {
          draft.itemList = action.response as PgpKey[];
        } else {
          draft.itemList =
            initialBaseReducerState<IdRequired<PgpKey>>().itemList;
        }
        break;
      case LOAD_KEYS.failure:
        draft.isFetching = false;
        break;
      case SEND_KEY_SIGN_REQUEST.request:
        draft.isSubmitting = true;
        break;
      case SEND_KEY_SIGN_REQUEST.success:
        draft.isSubmitting = false;
        updateSignStatus(
          draft.itemList,
          (action.response as PgpKeySignRequest).pgpkeyId
        );
        break;
      case SEND_KEY_SIGN_REQUEST.failure:
        draft.isSubmitting = false;
        break;
    }
  });
}

function updateSignStatus(keys: PgpKey[], keyId: string) {
  const key: Mutable<PgpKey> | undefined = keys.find(
    (key) => key.pgpkeyId === keyId
  );
  if (key !== undefined) key.signStatus = PgpKeySignStatusEnum.PENDING;
}
