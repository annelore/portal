import { combineReducers } from 'redux';
import { auth } from './auth';
import { projectReducer } from './project';
import { keys } from './keys';
import {
  Contact,
  DataProvider,
  DataTransfer,
  Feed,
  Flag,
  Group,
  Message,
  Node,
  Permission,
  PgpKey,
  QuickAccessTile,
  User,
} from '../api/generated';
import {
  ADD_CONTACT,
  ADD_DATA_PROVIDER,
  ADD_FLAG,
  ADD_GROUP,
  ADD_LOCAL_USER,
  ADD_NODE,
  CLEAR_USER_KEYS,
  DELETE_DATA_TRANSFER,
  DELETE_GROUP,
  LOAD_DATA_PROVIDERS,
  LOAD_DATA_TRANSFERS,
  LOAD_FEED,
  LOAD_FLAGS,
  LOAD_GROUPS,
  LOAD_MESSAGES,
  LOAD_NODES,
  LOAD_PERMISSIONS,
  LOAD_QUICK_ACCESSES,
  LOAD_USER_KEYS,
  LOAD_USERS,
  UPDATE_DATA_PROVIDER,
  UPDATE_DATA_TRANSFER,
  UPDATE_FLAG,
  UPDATE_GROUP,
  UPDATE_NODE,
  UPDATE_USER,
} from '../actions/actionTypes';
import { reducer, toast } from '@biomedit/next-widgets';
import { IdRequired } from '../global';

export const reducers = combineReducers({
  auth,
  toast,
  project: projectReducer,
  keys,
  contact: reducer<IdRequired<Contact>>({ add: ADD_CONTACT }),
  users: reducer<IdRequired<User>>({
    load: LOAD_USERS,
    update: UPDATE_USER,
    add: ADD_LOCAL_USER,
  }),
  nodes: reducer<IdRequired<Node>>({
    load: LOAD_NODES,
    update: UPDATE_NODE,
    add: ADD_NODE,
  }),
  messages: reducer<IdRequired<Message>>({ load: LOAD_MESSAGES }),
  dataProvider: reducer<IdRequired<DataProvider>>({
    load: LOAD_DATA_PROVIDERS,
    update: UPDATE_DATA_PROVIDER,
    add: ADD_DATA_PROVIDER,
  }),
  dataTransfers: reducer<IdRequired<DataTransfer>>({
    load: LOAD_DATA_TRANSFERS,
    del: DELETE_DATA_TRANSFER,
    update: UPDATE_DATA_TRANSFER,
  }),
  feed: reducer<IdRequired<Feed>>({ load: LOAD_FEED }),
  flags: reducer<IdRequired<Flag>>({
    load: LOAD_FLAGS,
    update: UPDATE_FLAG,
    add: ADD_FLAG,
  }),
  groups: reducer<IdRequired<Group>>({
    load: LOAD_GROUPS,
    update: UPDATE_GROUP,
    add: ADD_GROUP,
    del: DELETE_GROUP,
  }),
  permissions: reducer<IdRequired<Permission>>({
    load: LOAD_PERMISSIONS,
  }),
  quickAccesses: reducer<IdRequired<QuickAccessTile>>({
    load: LOAD_QUICK_ACCESSES,
  }),
  userKeys: reducer<IdRequired<PgpKey>>({
    load: LOAD_USER_KEYS,
    clear: CLEAR_USER_KEYS,
  }),
});
