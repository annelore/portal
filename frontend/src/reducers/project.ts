import {
  ADD_DATA_TRANSFER,
  ADD_PROJECT,
  ARCHIVE_PROJECT,
  CLEAR_PROJECTS,
  DELETE_DATA_TRANSFER,
  DELETE_PROJECT,
  LOAD_PROJECTS,
  LOGIN,
  UNARCHIVE_PROJECT,
  UPDATE_DATA_TRANSFER,
  UPDATE_PROJECT,
} from '../actions/actionTypes';
import { AnyFunction, isIpInRange } from '@biomedit/next-widgets';
import { AnyAction } from 'redux';
import produce from 'immer';
import { Project } from '../api/generated';
import { Mutable } from 'utility-types';

export type ProjectState = {
  isFetching: boolean;
  isSubmitting: boolean;
  itemList: Project[];
  ipAddress?: string;
};

export const initialProjectState: ProjectState = {
  isFetching: false,
  isSubmitting: false,
  itemList: [],
  ipAddress: undefined,
};

export function projectReducer(
  state: ProjectState = initialProjectState,
  action: AnyAction
): ProjectState {
  return produce(state, (draft: ProjectState) => {
    switch (action.type) {
      case LOGIN:
        draft.ipAddress = action.response.ipAddress;
        break;
      case LOAD_PROJECTS.request:
        draft.isFetching = true;
        break;
      case LOAD_PROJECTS.success:
        draft.isFetching = false;
        draft.itemList = postProcessProjects(action.response, draft.ipAddress);
        break;
      case LOAD_PROJECTS.failure:
        draft.isFetching = false;
        break;
      case UPDATE_PROJECT.request:
        draft.isSubmitting = true;
        break;
      case UPDATE_PROJECT.success:
        draft.isSubmitting = false;
        draft.itemList = updateItems(
          draft.itemList,
          postProcessProject(draft.ipAddress)(action.response)
        );
        break;
      case UPDATE_PROJECT.failure:
        draft.isSubmitting = false;
        break;
      case ADD_PROJECT.request:
        draft.isSubmitting = true;
        break;
      case ADD_PROJECT.success:
        draft.isSubmitting = false;
        draft.itemList.push(
          postProcessProject(draft.ipAddress)(action.response)
        );
        break;
      case ADD_PROJECT.failure:
        draft.isSubmitting = false;
        break;
      case DELETE_PROJECT.request:
        draft.isSubmitting = true;
        break;
      case DELETE_PROJECT.success:
        draft.isSubmitting = false;
        draft.itemList = deleteProject(draft.itemList, action);
        break;
      case DELETE_PROJECT.failure:
        draft.isSubmitting = false;
        break;
      case ARCHIVE_PROJECT.request:
        draft.isSubmitting = true;
        break;
      case ARCHIVE_PROJECT.success:
        draft.isSubmitting = false;
        draft.itemList = updateItems(
          draft.itemList,
          postProcessProject(draft.ipAddress)(action.response)
        );
        break;
      case ARCHIVE_PROJECT.failure:
        draft.isSubmitting = false;
        break;
      case UNARCHIVE_PROJECT.request:
        draft.isSubmitting = true;
        break;
      case UNARCHIVE_PROJECT.success:
        draft.isSubmitting = false;
        draft.itemList = updateItems(
          draft.itemList,
          postProcessProject(draft.ipAddress)(action.response)
        );
        break;
      case UNARCHIVE_PROJECT.failure:
        draft.isSubmitting = false;
        break;
      case ADD_DATA_TRANSFER.request:
        draft.isSubmitting = true;
        break;
      case ADD_DATA_TRANSFER.success:
        draft.isSubmitting = false;
        addDataTransfer(draft.itemList, action);
        break;
      case ADD_DATA_TRANSFER.failure:
        draft.isSubmitting = false;
        break;
      case DELETE_DATA_TRANSFER.request:
        draft.isSubmitting = true;
        break;
      case DELETE_DATA_TRANSFER.success:
        draft.isSubmitting = false;
        deleteDataTransfer(draft.itemList, action);
        break;
      case DELETE_DATA_TRANSFER.failure:
        draft.isSubmitting = false;
        break;
      case UPDATE_DATA_TRANSFER.request:
        draft.isSubmitting = true;
        break;
      case UPDATE_DATA_TRANSFER.success: {
        draft.isSubmitting = false;
        updateDataTransfer(draft.itemList, action);
        break;
      }
      case UPDATE_DATA_TRANSFER.failure:
        draft.isSubmitting = false;
        break;
      case CLEAR_PROJECTS:
        draft.itemList = [];
        break;
    }
  });
}

interface WithId {
  id: number;
}

function updateItems<T extends WithId>(items: T[], changed: T): T[] {
  const changedIndex = items.findIndex((item) => item.id === changed.id);
  items[changedIndex] = changed;
  return items;
}

function postProcessProjects(projects, ip: string | undefined) {
  return projects.map(postProcessProject(ip));
}
function postProcessProject(ip: string | undefined): AnyFunction {
  return (project) => ({
    ipAddressInRange: isIpInProjectRanges(ip, project.ipAddressRanges),
    ...project,
  });
}

function isIpInProjectRanges(ip: string | undefined, ipAddressRanges): boolean {
  return (
    !ip ||
    !ipAddressRanges ||
    ipAddressRanges.length === 0 ||
    ipAddressRanges.some(({ ipAddress, mask }) =>
      isIpInRange(ip, ipAddress, mask)
    )
  );
}

function deleteDataTransfer(itemList: Mutable<Project>[], action: AnyAction) {
  const affectedProject = itemList.find((project) =>
    project.dataTransfers?.some((dtr) => dtr.id === action.requestArgs.id)
  );
  if (affectedProject) {
    affectedProject.dataTransfers =
      affectedProject.dataTransfers?.filter(
        (transfer) => transfer.id !== action.requestArgs.id
      ) ?? [];
  }
}

function updateDataTransfer(itemList: Mutable<Project>[], action: AnyAction) {
  const affectedProject = itemList.find(
    (project) => project.id === action.response.project
  );

  if (affectedProject) {
    affectedProject.dataTransfers = updateItems(
      affectedProject.dataTransfers ?? [],
      action.response
    );
  }
}

function addDataTransfer(itemList: Project[], action: AnyAction) {
  itemList
    .find((project) => project.id === action.response.project)
    ?.dataTransfers?.push(action.response);
}

function deleteProject(itemList: Project[], action: AnyAction) {
  return itemList.filter((proj) => proj.id !== action.requestArgs.id);
}
