import { User, Userinfo } from './api/generated';

export function getPageTitle(name: string) {
  return `Portal / ${name}`;
}

export function getEduidUsername(
  user: User | Userinfo | null
): string | undefined {
  return user?.username;
}

export function getLocalUsername(
  user: User | Userinfo | null
): string | undefined {
  return user?.profile?.localUsername ?? undefined;
}

export function getDisplayName(
  user: User | Userinfo | null
): string | undefined {
  return user?.profile?.displayName;
}

export function getAffiliation(
  user: User | Userinfo | null
): string | undefined {
  return (user?.profile?.affiliation || '-').replace(',', ', ');
}
