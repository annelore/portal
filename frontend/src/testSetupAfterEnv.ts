import { LogLevel, mockI18n } from '@biomedit/next-widgets';
import * as nextWidgets from '@biomedit/next-widgets/lib/lib/logger';
import failOnConsole from 'jest-fail-on-console';

/**
 * Importing next during test applies the built-in fetch polyfill by Next.js
 *
 * @see https://github.com/vercel/next.js/discussions/13678#discussioncomment-22383 How to use built-in fetch in tests?
 * @see https://nextjs.org/blog/next-9-4#improved-built-in-fetch-support Next.js Blog - Improved Built-in Fetch Support
 * @see https://jestjs.io/docs/en/configuration#setupfilesafterenv-array About setupFilesAfterEnv usage
 */
require('next');

mockI18n();

failOnConsole();

// Avoid error "crypto.getRandomValues() not supported."
jest.spyOn(nextWidgets, 'logger').mockImplementation((namespace: string) => {
  const log = (data) =>
    console.error(
      'Log was made for namespace: ' +
        namespace +
        ' with data: ' +
        JSON.stringify(data)
    );
  return {
    [LogLevel.debug]: log,
    [LogLevel.error]: log,
    [LogLevel.http]: log,
    [LogLevel.info]: log,
    [LogLevel.silly]: log,
    [LogLevel.verbose]: log,
    [LogLevel.warn]: log,
  };
});
