import { declareAction, requestAction } from '@biomedit/next-widgets';
import { Project, UpdateProjectRequest } from '../api/generated';
import { generatedBackendApi } from '../api/api';
import { Action } from 'redux';

export const CHECK_AUTH = declareAction(
  'CHECK_AUTH',
  generatedBackendApi.retrieveUserinfo
);
export const LOGIN = 'LOGIN';

//---------------------------------------------------------------------

export const CHANGE_USERNAME = declareAction(
  'CHANGE_USERNAME',
  generatedBackendApi.partialUpdateUser
);

//---------------------------------------------------------------------

export const ADD_CONTACT = declareAction(
  'ADD_CONTACT',
  generatedBackendApi.createContact
);

//---------------------------------------------------------------------

export const ADD_LOCAL_USER = declareAction(
  'ADD_LOCAL_USER',
  generatedBackendApi.createUser
);
export const LOAD_USERS = declareAction(
  'LOAD_USERS',
  generatedBackendApi.listUsers
);
export const UPDATE_USER = declareAction(
  'UPDATE_USER',
  generatedBackendApi.partialUpdateUser
);

//---------------------------------------------------------------------

export const LOAD_PROJECTS = declareAction(
  'LOAD_PROJECTS',
  generatedBackendApi.listProjects
);
export const CLEAR_PROJECTS = 'CLEAR_PROJECTS';
export const UPDATE_PROJECT = declareAction(
  'UPDATE_PROJECT',
  generatedBackendApi.updateProject
);
export const updateProjectAction = <OnSuccessAction extends Action>(
  project: Project,
  onSuccess?: OnSuccessAction
): Action<typeof UPDATE_PROJECT.request> & UpdateProjectRequest =>
  requestAction(UPDATE_PROJECT, { id: String(project.id), project }, onSuccess);

export const ADD_PROJECT = declareAction(
  'ADD_PROJECT',
  generatedBackendApi.createProject
);
export const DELETE_PROJECT = declareAction(
  'DELETE_PROJECT',
  generatedBackendApi.destroyProject
);
export const ARCHIVE_PROJECT = declareAction(
  'ARCHIVE_PROJECT',
  generatedBackendApi.archiveProject
);
export const UNARCHIVE_PROJECT = declareAction(
  'UNARCHIVE_PROJECT',
  generatedBackendApi.unarchiveProject
);

//---------------------------------------------------------------------

export const LOAD_MESSAGES = declareAction(
  'LOAD_MESSAGES',
  generatedBackendApi.listMessages
);
export const LOAD_FEED = declareAction(
  'LOAD_FEED',
  generatedBackendApi.listFeeds
);
export const LOAD_KEYS = declareAction(
  'LOAD_KEYS',
  generatedBackendApi.listPgpKeys
);
export const LOAD_USER_KEYS = declareAction(
  'LOAD_USER_KEYS',
  generatedBackendApi.listPgpKeys
);
export const CLEAR_USER_KEYS = 'CLEAR_USER_KEYS';
export const SEND_KEY_SIGN_REQUEST = declareAction(
  'SEND_KEY_SIGN_REQUEST',
  generatedBackendApi.createPgpKeySignRequest
);

//---------------------------------------------------------------------
export const LOAD_NODES = declareAction(
  'LOAD_NODES',
  generatedBackendApi.listNodes
);
export const ADD_NODE = declareAction(
  'ADD_NODE',
  generatedBackendApi.createNode
);
export const UPDATE_NODE = declareAction(
  'UPDATE_NODE',
  generatedBackendApi.updateNode
);

//---------------------------------------------------------------------
export const LOAD_FLAGS = declareAction(
  'LOAD_FLAGS',
  generatedBackendApi.listFlags
);
export const ADD_FLAG = declareAction(
  'ADD_FLAG',
  generatedBackendApi.createFlag
);
export const UPDATE_FLAG = declareAction(
  'UPDATE_FLAG',
  generatedBackendApi.updateFlag
);

//---------------------------------------------------------------------
export const LOAD_GROUPS = declareAction(
  'LOAD_GROUPS',
  generatedBackendApi.listGroups
);
export const ADD_GROUP = declareAction(
  'ADD_GROUP',
  generatedBackendApi.createGroup
);
export const UPDATE_GROUP = declareAction(
  'UPDATE_GROUP',
  generatedBackendApi.updateGroup
);
export const DELETE_GROUP = declareAction(
  'DELETE_GROUP',
  generatedBackendApi.destroyGroup
);

//---------------------------------------------------------------------
export const LOAD_PERMISSIONS = declareAction(
  'LOAD_PERMISSIONS',
  generatedBackendApi.listPermissions
);

//---------------------------------------------------------------------
export const LOAD_DATA_PROVIDERS = declareAction(
  'LOAD_DATA_PROVIDERS',
  generatedBackendApi.listDataProviders
);
export const ADD_DATA_PROVIDER = declareAction(
  'ADD_DATA_PROVIDER',
  generatedBackendApi.createDataProvider
);
export const UPDATE_DATA_PROVIDER = declareAction(
  'UPDATE_DATA_PROVIDER',
  generatedBackendApi.updateDataProvider
);

//---------------------------------------------------------------------
export const LOAD_DATA_TRANSFERS = declareAction(
  'LOAD_DATA_TRANSFERS',
  generatedBackendApi.listDataTransfers
);
export const ADD_DATA_TRANSFER = declareAction(
  'ADD_DATA_TRANSFER',
  generatedBackendApi.createDataTransfer
);
export const DELETE_DATA_TRANSFER = declareAction(
  'DELETE_DATA_TRANSFER',
  generatedBackendApi.destroyDataTransfer
);
export const UPDATE_DATA_TRANSFER = declareAction(
  'UPDATE_DATA_TRANSFER',
  generatedBackendApi.updateDataTransfer
);

//---------------------------------------------------------------------
export const LOAD_QUICK_ACCESSES = declareAction(
  'LOAD_QUICK_ACCESSES',
  generatedBackendApi.listQuickAccessTiles
);

//---------------------------------------------------------------------
export const REDIRECT = 'REDIRECT';
export const CALL = 'CALL';
