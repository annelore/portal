/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface UniqueNode
 */
export interface UniqueNode {
    /**
     * 
     * @type {string}
     * @memberof UniqueNode
     */
    id?: string;
    /**
     * 
     * @type {string}
     * @memberof UniqueNode
     */
    code?: string;
    /**
     * 
     * @type {string}
     * @memberof UniqueNode
     */
    name?: string;
}

export function UniqueNodeFromJSON(json: any): UniqueNode {
    return UniqueNodeFromJSONTyped(json, false);
}

export function UniqueNodeFromJSONTyped(json: any, ignoreDiscriminator: boolean): UniqueNode {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': !exists(json, 'id') ? undefined : json['id'],
        'code': !exists(json, 'code') ? undefined : json['code'],
        'name': !exists(json, 'name') ? undefined : json['name'],
    };
}

export function UniqueNodeToJSON(value?: UniqueNode | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'code': value.code,
        'name': value.name,
    };
}


