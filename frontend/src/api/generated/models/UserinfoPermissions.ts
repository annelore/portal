/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface UserinfoPermissions
 */
export interface UserinfoPermissions {
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly manager?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly staff?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly dataManager?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly projectLeader?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly dataProvider?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly dataProviderAdmin?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly dataProviderViewer?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly nodeAdmin?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly nodeViewer?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly groupManager?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof UserinfoPermissions
     */
    readonly hasProjects?: boolean;
}

export function UserinfoPermissionsFromJSON(json: any): UserinfoPermissions {
    return UserinfoPermissionsFromJSONTyped(json, false);
}

export function UserinfoPermissionsFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserinfoPermissions {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'manager': !exists(json, 'manager') ? undefined : json['manager'],
        'staff': !exists(json, 'staff') ? undefined : json['staff'],
        'dataManager': !exists(json, 'data_manager') ? undefined : json['data_manager'],
        'projectLeader': !exists(json, 'project_leader') ? undefined : json['project_leader'],
        'dataProvider': !exists(json, 'data_provider') ? undefined : json['data_provider'],
        'dataProviderAdmin': !exists(json, 'data_provider_admin') ? undefined : json['data_provider_admin'],
        'dataProviderViewer': !exists(json, 'data_provider_viewer') ? undefined : json['data_provider_viewer'],
        'nodeAdmin': !exists(json, 'node_admin') ? undefined : json['node_admin'],
        'nodeViewer': !exists(json, 'node_viewer') ? undefined : json['node_viewer'],
        'groupManager': !exists(json, 'group_manager') ? undefined : json['group_manager'],
        'hasProjects': !exists(json, 'has_projects') ? undefined : json['has_projects'],
    };
}

export function UserinfoPermissionsToJSON(value?: UserinfoPermissions | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
    };
}


