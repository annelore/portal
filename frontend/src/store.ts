import { applyMiddleware, createStore, Store, StoreEnhancer } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import createSagaMiddleware from 'redux-saga';
import { reducers } from './reducers';
import { rootSaga } from './actions/sagas';
import { Context, createWrapper } from 'next-redux-wrapper';

export type RootState = ReturnType<typeof reducers>;

export function makeStore(
  context?: Context,
  preloadedState?: RootState
): Store {
  const sagaMiddleware = createSagaMiddleware();

  const middlewares = [sagaMiddleware];

  const enhancers: StoreEnhancer = composeWithDevTools(
    applyMiddleware(...middlewares)
  );

  const store = preloadedState
    ? createStore(reducers, preloadedState, enhancers)
    : createStore(reducers, enhancers);

  sagaMiddleware.run(rootSaga);

  return store;
}

export function getInitialState(): RootState {
  return reducers(undefined, { type: 'NOOP' });
}

export const wrapper = createWrapper<Store<RootState>>(makeStore);
