import {
  // UseColumnOrderInstanceProps,
  // UseColumnOrderState,
  // UseExpandedHooks,
  // UseExpandedInstanceProps,
  // UseExpandedOptions,
  // UseExpandedRowProps,
  // UseExpandedState,
  // UseFiltersColumnOptions,
  // UseFiltersColumnProps,
  // UseFiltersInstanceProps,
  // UseFiltersOptions,
  // UseFiltersState,
  UseGlobalFiltersColumnOptions,
  UseGlobalFiltersInstanceProps,
  UseGlobalFiltersOptions,
  UseGlobalFiltersState,
  // UseGroupByCellProps,
  // UseGroupByColumnOptions,
  // UseGroupByColumnProps,
  // UseGroupByHooks,
  // UseGroupByInstanceProps,
  // UseGroupByOptions,
  // UseGroupByRowProps,
  // UseGroupByState,
  UsePaginationInstanceProps,
  UsePaginationOptions,
  UsePaginationState,
  // UseResizeColumnsColumnOptions,
  // UseResizeColumnsColumnProps,
  // UseResizeColumnsOptions,
  // UseResizeColumnsState,
  // UseRowSelectHooks,
  // UseRowSelectInstanceProps,
  // UseRowSelectOptions,
  // UseRowSelectRowProps,
  // UseRowSelectState,
  // UseRowStateCellProps,
  // UseRowStateInstanceProps,
  // UseRowStateOptions,
  // UseRowStateRowProps,
  // UseRowStateState,
  UseSortByColumnOptions,
  UseSortByColumnProps,
  UseSortByHooks,
  UseSortByInstanceProps,
  UseSortByOptions,
  UseSortByState,
} from 'react-table';
import { AnyObject } from './global';

// from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-table

/**
 * NOTE: Every time you use a new plugin with react-table, remove the commented out line in this file of the plugin!
 *
 * Example: {@code useTable({...}, useGlobalFilter)}
 * => here you would need to uncomment `useGlobalFilter`
 */
declare module 'react-table' {
  // take this file as-is, or comment out the sections that don't apply to your plugin configuration

  export interface TableOptions<
    D extends AnyObject
  > extends UseGlobalFiltersOptions<D>,
      // UseExpandedOptions<D>,
      // UseFiltersOptions<D>,
      // UseGroupByOptions<D>,
      UsePaginationOptions<D>,
      // UseResizeColumnsOptions<D>,
      // UseRowSelectOptions<D>,
      // UseRowStateOptions<D>,
      UseSortByOptions<D>,
      // note that having Record here allows you to add anything to the options, this matches the spirit of the
      // underlying js library, but might be cleaner if it's replaced by a more specific type that matches your
      // feature set, this is a safe default.
      // eslint-disable-next-line @typescript-eslint/no-empty-interface
      Record<string, any> {}

  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface Hooks<D extends AnyObject = AnyObject>
    extends UseSortByHooks<D> {}
  // UseExpandedHooks<D>,
  // UseGroupByHooks<D>,
  // UseRowSelectHooks<D>,

  export interface TableInstance<
    D extends AnyObject = AnyObject
  > extends UseGlobalFiltersInstanceProps<D>,
      // UseColumnOrderInstanceProps<D>,
      // UseExpandedInstanceProps<D>,
      // UseFiltersInstanceProps<D>,
      // UseGroupByInstanceProps<D>,
      UsePaginationInstanceProps<D>,
      // UseRowSelectInstanceProps<D>,
      // UseRowStateInstanceProps<D>,
      UseSortByInstanceProps<D> {}

  export interface TableState<
    D extends AnyObject = AnyObject
  > extends UseGlobalFiltersState<D>,
      // UseColumnOrderState<D>,
      // UseExpandedState<D>,
      // UseFiltersState<D>,
      // UseGroupByState<D>,
      UsePaginationState<D>,
      // UseResizeColumnsState<D>,
      // UseRowSelectState<D>,
      // UseRowStateState<D>,
      UseSortByState<D> {}

  export interface ColumnInterface<
    D extends AnyObject = AnyObject
  > extends UseGlobalFiltersColumnOptions<D>,
      // UseFiltersColumnOptions<D>,
      // UseGroupByColumnOptions<D>,
      // UseResizeColumnsColumnOptions<D>,
      UseSortByColumnOptions<D> {}

  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface ColumnInstance<D extends AnyObject = AnyObject>
    extends UseSortByColumnProps<D> {}
  // UseFiltersColumnProps<D>,
  // UseGroupByColumnProps<D>,
  // UseResizeColumnsColumnProps<D>,

  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface Cell<
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    D extends AnyObject = AnyObject,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars, @typescript-eslint/no-empty-interface
    V = any
  > {}
  //extends UseGroupByCellProps<D>,
  // UseRowStateCellProps<D> {}

  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface Row<
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    D extends AnyObject = AnyObject
  > {}
  // extends UseExpandedRowProps<D>,
  // UseGroupByRowProps<D>,
  // UseRowSelectRowProps<D>,
  // UseRowStateRowProps<D> {}
}
