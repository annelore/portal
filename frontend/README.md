# Frontend

**run backend server**

```sh
./web/manage.py runserver localhost:8000
```

This will run the app on port 8000 of your machine. If you make any
changes to the backend, it will automatically restart the server for
you.

**run frontend**

First, **copy** `.env.sample` to `.env` and edit the values to match your application.

Then, make sure you have all the necessary node modules installed:

```sh
npm install
```

This will read all the packages listed in the `package.json` file,
downloads them from the npm repository (npmjs.com) and installs them
in the `node_modules/` folder.

Next step is to start the project. For this, you need to issue this
command:

```sh
npm start
```

(this has the same effect as `npm run start`)

This will read your local `package.json` file and look in the `scripts` section for a script called `start`.
In our case, the `scripts` entry will look like this:

```json
{
  "scripts": {
    "start": "next dev"
  }
}
```

In other words, it just runs the app using [Next.js](https://nextjs.org/docs/getting-started).

By default, the server will start at http://localhost:3000/
