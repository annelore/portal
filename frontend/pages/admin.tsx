import React, { ReactElement } from 'react';
import { Administration } from '../src/components/admin/Administration';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../src/i18n';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { getPageTitle } from '../src/utils';

export const AdministrationPage = (): ReactElement => {
  return (
    <PageBase
      title={getPageTitle('Administration')}
      content={Administration}
      props={{}}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.ADMINISTRATION,
      I18nNamespace.BOOLEAN,
      I18nNamespace.COMMON,
      I18nNamespace.DATA_PROVIDER_LIST,
      I18nNamespace.FLAG_LIST,
      I18nNamespace.GROUP_LIST,
      I18nNamespace.GROUP_MANAGE_FORM,
      I18nNamespace.LIST,
      I18nNamespace.NODE_LIST,
      I18nNamespace.USER_INFO,
      I18nNamespace.USER_LIST,
      I18nNamespace.USER_MANAGE_FORM,
    ])),
  },
});

export default AdministrationPage;
