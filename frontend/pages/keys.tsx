import React, { ReactElement } from 'react';
import { Keys } from '../src/components/Keys';
import { PageBase } from '@biomedit/next-widgets';
import { getPageTitle } from '../src/utils';
import { FrontendGetStaticProps } from '@biomedit/next-widgets';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../src/i18n';

export const KeysPage = (): ReactElement => {
  return <PageBase title={getPageTitle('My Keys')} content={Keys} props={{}} />;
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default KeysPage;
