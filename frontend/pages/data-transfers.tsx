import React, { ReactElement } from 'react';
import { DataTransferOverviewTable } from '../src/components/dataTransfer/DataTransferOverviewTable';
import { DataTransferExport } from '../src/components/dataTransfer/DataTransferExport';
import { I18nNamespace } from '../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { getPageTitle } from '../src/utils';

export const DataTransferPage = (): ReactElement => {
  return (
    <PageBase
      title={getPageTitle('Data Transfers')}
      content={DataTransferOverviewTable}
      toolbar={DataTransferExport}
      props={{}}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.DATA_TRANSFER,
      I18nNamespace.LIST,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default DataTransferPage;
