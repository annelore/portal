import React, { ReactElement } from 'react';
import { Home } from '../src/components/Home';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../src/i18n';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { getPageTitle } from '../src/utils';

export const IndexPage = (): ReactElement => {
  return (
    <PageBase
      title={getPageTitle('BioMedIT User Portal')}
      content={Home}
      props={{}}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.FEED_LIST,
      I18nNamespace.HOME,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default IndexPage;
