import React, { ReactElement } from 'react';
import { MessageTable } from '../src/components/MessageTable';
import { PageBase } from '@biomedit/next-widgets';
import { getPageTitle } from '../src/utils';
import { FrontendGetStaticProps } from '@biomedit/next-widgets';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../src/i18n';

export const MessagePage = (): ReactElement => {
  return (
    <PageBase
      title={getPageTitle('Messages')}
      content={MessageTable}
      props={{}}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default MessagePage;
