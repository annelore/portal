import React, { ReactElement } from 'react';
import { ProjectList } from '../src/components/projectList/ProjectList';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../src/i18n';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { getPageTitle } from '../src/utils';

export const ProjectPage = (): ReactElement => {
  return (
    <PageBase
      title={getPageTitle('Projects')}
      content={ProjectList}
      props={{}}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.DATA_TRANSFER,
      I18nNamespace.IP_RANGE_LIST,
      I18nNamespace.PROJECT_FORM,
      I18nNamespace.PROJECT_LIST,
      I18nNamespace.PROJECT_USER_LIST,
      I18nNamespace.PROJECT_RESOURCE_LIST,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default ProjectPage;
