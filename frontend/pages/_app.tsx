import type { AppProps } from 'next/app';
import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CHECK_AUTH } from '../src/actions/actionTypes';
import { UserInfoBox } from '../src/components/nav/UserInfoBox';
import { SetUsernameDialog } from '../src/components/SetUsernameDialog';
import { contactEmail } from '../src/config';
import { wrapper } from '../src/store';
import { appWithTranslation, Trans } from 'next-i18next';
import { useRouter } from 'next/router';
import nextI18NextConfig from '../next-i18next.config.js';
import {
  Box,
  Container,
  Divider,
  EmailLink,
  ErrorBoundary,
  Header,
  isClient,
  logger,
  LogListener,
  LogoutMenuItem,
  Nav,
  NavItem,
  offLog,
  onLog,
  requestAction,
  ThemeProvider,
  ToastBar,
  ListItemAction,
  UserMenuItemModel,
} from '@biomedit/next-widgets';
import { winstonLogger } from '../src/logger';
import { createMenuItems, userMenu } from '../src/structure';
import { UserMenu } from '../src/components/nav/UserMenu';
import { logoutUrl } from '../src/api/api';
import { supportedBrowsers } from '../src/supportedBrowsers';
import isbot from 'isbot';
import { I18nNamespace } from '../src/i18n';
import { Version } from '../src/components/Version';

export const unsupportedBrowserLogMessage = 'Browser is not supported!';

function App({ Component, pageProps }: AppProps) {
  const isSupportedBrowser: boolean | undefined = useMemo(() => {
    if (isClient()) {
      const userAgent = (window.navigator || navigator).userAgent;
      if (isbot(userAgent)) {
        // prevent bots from crawling contact email address
        return true;
      }
      return supportedBrowsers.test(userAgent);
    } else {
      return undefined;
    }
  }, []);

  const dispatch = useDispatch();

  const isFetching = useSelector((state) => state.auth.isFetching);
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const permissionGroups = useSelector((state) => state.auth.pagePermissions);
  const { pathname } = useRouter();
  useEffect(() => {
    if (isSupportedBrowser !== false && !isAuthenticated) {
      dispatch(requestAction(CHECK_AUTH));
    }
  }, [dispatch, isAuthenticated, isSupportedBrowser]);

  useEffect(() => {
    const log = winstonLogger();
    const listener: LogListener = (data) => {
      log.log(data);
    };
    onLog(listener);
    return () => {
      offLog(listener);
    };
  }, []);

  const log = logger('App');

  if (isSupportedBrowser === false) {
    log.warn({ message: unsupportedBrowserLogMessage });
    return (
      <Trans
        i18nKey={`${I18nNamespace.COMMON}:unsupportedBrowser`}
        values={{ contactEmail }}
        components={{
          email: <EmailLink email={contactEmail ?? ''} />,
          ulist: <ul />,
          litem: <li />,
          break: <br />,
        }}
      />
    );
  }

  const subjectPrefix = 'Portal: ';

  return (
    <ErrorBoundary
      subjectPrefix={subjectPrefix}
      contactEmail={contactEmail ?? ''}
    >
      <ThemeProvider>
        <div>
          <ToastBar
            subjectPrefix={subjectPrefix}
            contactEmail={contactEmail ?? ''}
          />
          {isAuthenticated && !isFetching && (
            <Box display="flex">
              <SetUsernameDialog />
              <Nav>
                <UserMenu>
                  <UserInfoBox />
                  <Divider />
                  <LogoutMenuItem logoutUrl={logoutUrl} />
                  {userMenu.map((menuItem: UserMenuItemModel) => (
                    <ListItemAction
                      action={menuItem.action}
                      key={menuItem.title}
                      primary={menuItem.title}
                      icon={menuItem.icon}
                    />
                  ))}
                </UserMenu>
                {createMenuItems(permissionGroups).map((menuItem) => (
                  <NavItem
                    selected={menuItem.link == pathname}
                    href={menuItem.link ?? '#'}
                    key={'MenuItem-' + menuItem.title}
                    primary={menuItem.title}
                    icon={menuItem.icon}
                  />
                ))}
                <Version />
              </Nav>
              <Container maxWidth={false}>
                <>
                  <Header
                    logoSrc="/co_logo.png"
                    logoWidth="250"
                    logoHeight="80"
                    logoText="SPHN/SIB Logo"
                  />
                  <Component {...pageProps} />
                </>
              </Container>
            </Box>
          )}
        </div>
      </ThemeProvider>
    </ErrorBoundary>
  );
}

export default wrapper.withRedux(appWithTranslation(App, nextI18NextConfig));
