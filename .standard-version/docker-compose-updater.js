const images = ["web", "frontend"];

function parseImage(contents) {
  return contents.match(`image: (?<registry>.*)/${images[0]}:(?<version>.*)`)
    .groups;
}

module.exports.readVersion = function (contents) {
  return parseImage(contents).version;
};

module.exports.writeVersion = function (contents, version) {
  const image = parseImage(contents);
  function changeVersion(contents, imageName) {
    return contents.replace(
      new RegExp(`image: (.*)/${imageName}:${image.version}`, "g"),
      `image: $1/${imageName}:${version}`
    );
  }
  return images.reduce(changeVersion, contents);
};
